<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoResguardosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('no_resguardos', function (Blueprint $table) {
            $table->id();
            $table->string('NoResguardo',4);
            $table->unsignedBigInteger('persona_id');

            $table->foreign('persona_id')->unique()->references('id')->on('personas');
            $table->timestamps();
        });
    }

    /***/
    public function down()
    {
        Schema::dropIfExists('no_resguardos');
    }
}

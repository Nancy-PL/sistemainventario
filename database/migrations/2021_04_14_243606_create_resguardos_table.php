<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResguardosTable extends Migration
{
    /***/
    public function up()
    {
        //Crear la tabla para guardar los resguardos
        Schema::create('resguardos', function (Blueprint $table) {
            $table->id();
            $table->string('tipo',15);
            $table->unsignedBigInteger('persona_id');
            $table->boolean('activo'); 
            $table->boolean('UA')->nullable();
            $table->boolean('SI')->nullable();
            $table->dateTime('delete_at')->nullable();
            $table->timestamps();
            $table->foreign('persona_id')->unique()->references('id')->on('personas')
            ->onUpdate('cascade')->onDelete('cascade');
        });
       
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resguardos');
    }
}

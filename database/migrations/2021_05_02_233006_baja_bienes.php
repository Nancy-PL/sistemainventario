<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BajaBienes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           //Crear la tabla para guardar los resguardos Temporales
           Schema::create('BajaBienes', function (Blueprint $table) {
            $table->id();
            $table->string('Motivo_baja',300)->nullable();
            $table->string('Observaciones',800);
            $table->date('FechaBaja');
            $table->unsignedBigInteger('bienes_id');
            $table->foreign('bienes_id')->unique()->references('id')->on('bienes')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('BajaBienes');

    }
}

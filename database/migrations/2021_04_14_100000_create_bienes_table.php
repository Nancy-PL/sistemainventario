<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBienesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Crear la tabla para almacenar bienes.
        Schema::create('bienes', function (Blueprint $table) {
            $table->id();
            $table->string('Modelo',100);
            $table->string('Estado',10);
            $table->string('Caracteristicas',800)->nullable();
            $table->string('Observaciones',800)->nullable();
            $table->integer('Status');
            $table->string('ClaveBien',25)->nullable();
            $table->string('Departamento',25);
            $table->boolean('Activo');
            $table->unsignedBigInteger('Clasificacion_id')->unsigned()->nullable();
            $table->unsignedBigInteger('areas_id')->unsigned()->nullable();
            $table->unsignedBigInteger('marca_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('Clasificacion_id')->references('id')->on('clasificacions')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('areas_id')->references('id')->on('areas')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('marca_id')->references('id')->on('marcas')->onUpdate('cascade')->onDelete('set null');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bienes');
    }
}
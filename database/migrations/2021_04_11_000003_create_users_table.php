<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   //  Crear la tabla para almacenar usuarios.
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name',30)->unique();
            $table->string('password',255);
            $table->boolean('Activo');
            $table->unsignedBigInteger('persona_id');
            $table->unsignedBigInteger('roles_id');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('persona_id')->unique()->references('id')->on('personas');
            $table->foreign('roles_id')->references('id')->on('roles');

        });
        //  Crear la tabla para almacenar los movimientos de cada usuario
       /* Schema::create('users_detail', function (Blueprint $table) {
            $table->id();
            $table->string('movimiento');
            $table->timestamps();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        }); */
        //  Crear la tabla para guardar el registro de los logueos.
        Schema::create('logueo', function (Blueprint $table) {
            $table->id();
            $table->dateTime('inicio_login')->nullable();
            $table->dateTime('fin_login')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        }); 
                   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('users_detail');
        Schema::dropIfExists('logueo');
    }
}
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorialBiensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Crear la tabla para el historial de bienes.
        Schema::create('historial_biens', function (Blueprint $table) {
            $table->id();
            $table->dateTime('fecha_alta');
            $table->dateTime('fecha_baja')->nullable();
            $table->string('usuario_alta');
            $table->string('usuario_baja')->nullable();
            $table->unsignedBigInteger('bien_id');
            $table->foreign('bien_id')->references('id')->on('bienes')
            ->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_biens');
    }
}

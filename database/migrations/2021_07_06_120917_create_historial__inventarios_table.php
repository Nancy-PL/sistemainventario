<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorialInventariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial__inventarios', function (Blueprint $table) {
            $table->id();
            $table->string('NoInventarioAnterior',150)->nullable();
            $table->timestamps();
            
            $table->unsignedBigInteger('bienes_id');
            $table->foreign('bienes_id')->unique()->references('id')->on('bienes')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial__inventarios');
    }
}

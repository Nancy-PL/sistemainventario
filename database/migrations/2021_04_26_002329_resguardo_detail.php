<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ResguardoDetail extends Migration
{
    
    public function up()
    {
        //Crear la tabla para guardar bienes de los resguardos
        Schema::create('resguardo_detail', function (Blueprint $table) {
            $table->id();
            $table->date('fechaAsignacion');
            $table->date('fechaCancelacion')->nullable();
            $table->boolean('Activo');
            $table->string('User')->nullable();
            $table->unsignedBigInteger('ResguardoAnterior_id')->nullable();
            $table->unsignedBigInteger('bien_id');
            $table->unsignedBigInteger('resguardo_id');
            $table->foreign('resguardo_id')->references('id')->on('resguardos')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('ResguardoAnterior_id')->references('id')->on('resguardos')
            ->onUpdate('cascade')->onDelete('set null');
            $table->foreign('bien_id')->references('id')->on('bienes')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down()  
    {
        Schema::dropIfExists('resguardo_detail');

    }
    
}

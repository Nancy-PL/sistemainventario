<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetallesalidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Crear la tabla para guardar los bienes salientes
        Schema::create('detallesalidas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('salida_equipos_id');
            $table->unsignedBigInteger('bien_id');
            $table->boolean('Activo');
            $table->timestamps();
            $table->foreign('salida_equipos_id')->references('id')->on('salida_equipos')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('bien_id')->references('id')->on('bienes')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detallesalidas');
    }
}

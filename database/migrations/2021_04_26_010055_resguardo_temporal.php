<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Validation\Rules\Unique;

class ResguardoTemporal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          //Crear la tabla para guardar los resguardos Temporales
          Schema::create('resguardos_temporal', function (Blueprint $table) {
            $table->id();
            $table->string('folio',20);
            $table->date('fecha_orden');
            $table->string('No_orden',50);
            $table->string('Observaciones',800)->nullable();
            $table->date('fecha_inicio');
            $table->date('fecha_vencimiento');
            $table->date('fecha_devolucion');
            $table->boolean('Activo');
            $table->unsignedBigInteger('resguardo_id');
            $table->foreign('resguardo_id')->unique()->references('id')->on('resguardos')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resguardos_temporal');

    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleBiensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_biens', function (Blueprint $table) {
            $table->id();
            $table->string('NoSerie',100)->nullable();
            $table->string('factura',25)->nullable();
            $table->string('ubicacion',800)->nullable();
            $table->timestamps();
            
            $table->unsignedBigInteger('bienes_id');
            $table->foreign('bienes_id')->unique()->references('id')->on('bienes')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_biens');
    }
}

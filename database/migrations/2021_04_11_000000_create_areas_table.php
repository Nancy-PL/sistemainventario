<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;


class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   //  Crear la tabla para almacenar las areas.
        Schema::create('areas', function (Blueprint $table) {
            $table->id();
            $table->string('Nombre',150);
            $table->string('Responsable',150)->nullable();
            $table->string('Cargo',150)->nullable();
            $table->string('letra',5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas');
    }
}

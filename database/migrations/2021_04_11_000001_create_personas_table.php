<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Crear la tabla para guardar datos del personal
        Schema::create('personas', function (Blueprint $table) {
            $table->id();
            $table->string('Nombre',30);
            $table->string('ApellidoP',30);
            $table->string('ApellidoM',30);
            $table->string('Profesion',10);
            $table->boolean('Activo');
            $table->unsignedBigInteger('area_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('area_id')->references('id')->on('areas')
            ->onUpdate('cascade')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}

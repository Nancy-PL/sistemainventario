<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalidaEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Crear la tabla para guardar las salidas de equipos
        Schema::create('salida_equipos', function (Blueprint $table) {
            $table->id();
            $table->string('referencia',800);
            $table->date('fecha_notificacion');
            $table->date('fecha_vencimiento');
            $table->date('fecha_Entrega')->nullable();
            $table->boolean('Activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salida_equipos');
    }
}

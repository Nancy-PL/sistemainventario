<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClasificacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Crear la tabla para guardar clasificación

        Schema::create('clasificacions', function (Blueprint $table) {
            $table->id();
            $table->string('clave',100);
            $table->string('concepto',100);    
            $table->boolean('NoInventario')->nullable();     
            $table->boolean('activo'); 
            $table->string('Departamento',5);         
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clasificacions');
    }
}

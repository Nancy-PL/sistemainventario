<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BajaPersonal extends Migration
{
    public function up()
    {
         //Crear la tabla para guardar datos del personal
         Schema::create('BajaPersona', function (Blueprint $table) {
            $table->id();
            $table->string('Motivo',300);
            $table->string('UsuarioBaja',30);
            $table->date('Fecha');
            $table->unsignedBigInteger('persona_id');
            $table->foreign('persona_id')->references('id')->on('personas')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('BajaPersona');

    }
}

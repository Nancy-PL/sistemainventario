<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMejorasEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mejoras_equipos', function (Blueprint $table) {
            $table->id();
            $table->string('MejorasEquipo',800)->nullable();

            $table->timestamps();
            $table->unsignedBigInteger('bienes_id');
            $table->foreign('bienes_id')->unique()->references('id')->on('bienes')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mejoras_equipos');
    }
}

<?php

namespace Database\Seeders;

//use App\Models\Permission;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{

    public function run()
    {
        //CREACION DE ROLES DE USUARIO
        $role1 = Role::create(['id' => '1','name' => 'Administrador']);
        $role2 = Role::create(['id' => '2','name' => 'Capturista Administrativo']);
        $role3 = Role::create(['id' => '3','name' => 'Capturista UTIC']);

        //////////////////////////////////// HOME /////////////////////////////////////////////////////////
        Permission::create(['name' => 'dashboard.index', 'guard_name' => 'web'])->syncRoles([$role1]);
        ///////////////////////////////////USUARIOS //////////////////////////////////////////////////////////////
        Permission::create(['name' => 'ListaUsuario.index', 'guard_name' => 'web'])->syncRoles([$role1]);
        Permission::create(['name' => 'ListaUsuario.edit', 'guard_name' => 'web'])->syncRoles([$role1]);
        Permission::create(['name' => 'Users.index', 'guard_name' => 'web'])->syncRoles([$role1]);
        Permission::create(['name' => 'ActivarUsuario.index', 'guard_name' => 'web'])->syncRoles([$role1]);

        Permission::create(['name' => 'register.index', 'guard_name' => 'web'])->syncRoles([$role1]);
        Permission::create(['name' => 'register.create', 'guard_name' => 'web'])->syncRoles([$role1]);
        Permission::create(['name' => 'register.edit', 'guard_name' => 'web'])->syncRoles([$role1]);
        /////////////////////////////////// AREAS /////////////////////////////////////////////////////////
        Permission::create(['name' => 'Areas.index', 'guard_name' => 'web'])->syncRoles([$role1, $role2, $role3]);
        Permission::create(['name' => 'Areas.create', 'guard_name' => 'web'])->syncRoles([$role1]);
        Permission::create(['name' => 'Areas.edit', 'guard_name' => 'web'])->syncRoles([$role1]);
        /////////////////////////////// PERSONAS ///////////////////////////////////////////////////////////
        Permission::create(['name' => 'personas.index', 'guard_name' => 'web'])->syncRoles([$role1, $role2, $role3]);
        Permission::create(['name' => 'personas.create', 'guard_name' => 'web'])->syncRoles([$role1, $role2, $role3]);
        Permission::create(['name' => 'personas.edit', 'guard_name' => 'web'])->syncRoles([$role1, $role2, $role3]);
        Permission::create(['name' => 'BajaPersonas.index', 'guard_name' => 'web'])->syncRoles([$role1]);
        Permission::create(['name' => 'ActivarPersonas.index', 'guard_name' => 'web'])->syncRoles([$role1]);
        /////////////////////////////// CLASIFICACIÓN ////////////////////////////////////////////////////////
        Permission::create(['name' => 'Clasificacion.index', 'guard_name' => 'web'])->syncRoles([$role1, $role2, $role3]);
        Permission::create(['name' => 'Clasificacion.create', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'Clasificacion.edit', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        /////////////////////////////// MARCAS ///////////////////////////////////////////////////////////////
        Permission::create(['name' => 'Marcas.index', 'guard_name' => 'web'])->syncRoles([$role1, $role2, $role3]);
        Permission::create(['name' => 'Marcas.edit', 'guard_name' => 'web'])->syncRoles([$role1, $role2, $role3]);
        /////////////////////////////// BIENES UTIC /////////////////////////////////////////////////////
        Permission::create(['name' => 'BienesUtic.index', 'guard_name' => 'web'])->syncRoles([$role1, $role2, $role3]);
        Permission::create(['name' => 'BienesUtic.create', 'guard_name' => 'web'])->syncRoles([$role1, $role2, $role3]);
        Permission::create(['name' => 'BienesUtic.edit', 'guard_name' => 'web'])->syncRoles([$role1, $role2, $role3]);
        Permission::create(['name' => 'BienesUtic.destroy', 'guard_name' => 'web'])->syncRoles([$role1, $role2, $role3]);
        Permission::create(['name' => 'BajaBienUtic.index', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        Permission::create(['name' => 'ListabajaUtic.listaUtic', 'guard_name' => 'web'])->syncRoles([$role1]);
        Permission::create(['name' => 'HistorialBienesUTIC.indexSI', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        /////////////////////// ASIGNACION RESGUARDOS UTIC////////////////////////////////////////////
        Permission::create(['name' => 'ResguardosUtic.index', 'guard_name' => 'web'])->syncRoles([$role1, $role2, $role3]);
        Permission::create(['name' => 'ResguardosUtic.create', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        Permission::create(['name' => 'ResguardosUtic.edit', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        //////////////////////////// REASIGNACION RESGUARDO UTIC //////////////////////////////////////
        Permission::create(['name' => 'ReasignacionUtic.index', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        /////////////////////////// ACTIVACION RESGUARDO UTIC    //////////////////////////////////////
        Permission::create(['name' => 'ActivacionUtic.index', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        ////////////////////////// ENTREGA DE EQUIPOS UTIC /////////////////////////////////////////
        Permission::create(['name' => 'EntregaEquipo.index', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        ////////////////////////// / RESGUARDO TEMPORAL UTIC //////////////////////////////////////
        Permission::create(['name' => 'ResguardoTemporal.index', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        Permission::create(['name' => 'ResguardoTemporal.create', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        Permission::create(['name' => 'CancelarResguardoTem.index', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        Permission::create(['name' => 'ResguardosTemporales.index', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        //////////////////////////  IMPRESION DE RESGUARDOS UTIC //////////////////////////////////
        Permission::create(['name' => 'ResguardoPDFUtic.resguardo', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        /////////////////////////// REPORTES UTIC /////////////////////////////////////////////
        Permission::create(['name' => 'ReportesUTIC.reportesUTIC', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        //////////////////////////////////// SALIDAS DE EQUIPO UTIC/////////////////////////////////
        Permission::create(['name' => 'SalidaEquipo.index', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        Permission::create(['name' => 'listadoSalida.listadoSalidas', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        Permission::create(['name' => 'SalidasEquipo.index', 'guard_name' => 'web'])->syncRoles([$role1, $role3]);
        //////////////////////////Bienes Administrativos ////////////////////////////////////////////////////////
        Permission::create(['name' => 'BienesUA.index', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'BienesUA.create', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'BienesUA.edit', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'BienesUA.destroy', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'BajaBien.index', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'listaBajaBien.lista', 'guard_name' => 'web'])->syncRoles([$role1]);
        Permission::create(['name' => 'DetalleBien.index', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'HistorialBien.index', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        ////////////////////////////// RESGUARDOS ADMON /////////////////////////////////////////////////////////
        Permission::create(['name' => 'AsignacionBien.index', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'AsignacionBien.create', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        //////////////////////////////REASIGNACIONES ADMIN///////////////////////////////////////////////////////
        Permission::create(['name' => 'reasignacion.index', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'Activacion.index', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        //////////////////////////  IMPRESION DE RESGUARDOS ADMINISTRATIVOS //////////////////////////////
        Permission::create(['name' => 'ReporteGral.ReporteGeneral', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'ResguardoAdmiPDF.resguardo', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        /////////////////////////// REPORTES ADMINISTRATIVOS /////////////////////////////////////////////
        Permission::create(['name' => 'ReporteAmon.reportes', 'guard_name' => 'web'])->syncRoles([$role1, $role2]);
        /////////////////////////// HISTORIAL DE USUARIOS ///////////////////////////////////////////////
        Permission::create(['name' => 'LogsUser.index', 'guard_name' => 'web'])->syncRoles([$role1]);
        Permission::create(['name' => 'getLogs.getLogsUser', 'guard_name' => 'web'])->syncRoles([$role1]);
        Permission::create(['name' => 'get-getsession.getsession', 'guard_name' => 'web'])->syncRoles([$role1]);


    }
}

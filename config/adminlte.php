<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | Here you can change the default title of your admin panel.
    |
    | For detailed instructions you can look the title section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'title' => 'OSFE',
    'title_prefix' => '',
    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Favicon
    |--------------------------------------------------------------------------
    |
    | Here you can activate the favicon.
    |
    | For detailed instructions you can look the favicon section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'use_ico_only' => false,
    'use_full_favicon' => false,

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | Here you can change the logo of your admin panel.
    |
    | For detailed instructions you can look the logo section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'logo' => '<b>Sistema Inventario</b>',
    'logo_img' => false,
    'logo_img_class' => false,
    'logo_img_xl' => null,
    'logo_img_xl_class' => false,
    'logo_img_alt' => false,

    /*
    |--------------------------------------------------------------------------
    | User Menu
    |--------------------------------------------------------------------------
    |
    | Here you can activate and change the user menu.
    |
    | For detailed instructions you can look the user menu section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'usermenu_enabled' => true,
    'usermenu_header' => false,
    'usermenu_header_class' => 'bg-success',
    'usermenu_image' => false,
    'usermenu_desc' => false,
    'usermenu_profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Here we change the layout of your admin panel.
    |
    | For detailed instructions you can look the layout section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'layout_topnav' => null,
    'layout_boxed' => false,
    'layout_fixed_sidebar' => true,
    'layout_fixed_navbar' => true,
    'layout_fixed_footer' => null,
    'layout_dark_mode' => null,

    /*
    |--------------------------------------------------------------------------
    | Authentication Views Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the authentication views.
    |
    | For detailed instructions you can look the auth classes section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'classes_auth_card' => 'card-outline card-primary',
    'classes_auth_header' => '',
    'classes_auth_body' => '',
    'oncontextmenu="return false" onkeydown="return false"',
    'classes_auth_footer' => '',
    'classes_auth_icon' => '',
    'classes_auth_btn' => 'btn-flat btn-primary',

    /*
    |--------------------------------------------------------------------------
    | Admin Panel Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the admin panel.
    |
    | For detailed instructions you can look the admin panel classes here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'classes_body' =>  'oncontextmenu="return false" onkeydown="return false"',
    'classes_brand' => 'bg-with',
    'classes_brand_text' => '',
    'classes_content_wrapper' => '',
    'classes_content_header' => '',
    'classes_content' => '',
    'classes_sidebar' => 'sidebar-dark-success elevation-4',
    'classes_sidebar_nav' => '',
    'classes_topnav' => 'navbar-white navbar-light',
    'classes_topnav_nav' => 'navbar-expand',
    'classes_topnav_container' => 'container',

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar of the admin panel.
    |
    | For detailed instructions you can look the sidebar section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'sidebar_mini' => false,
    'sidebar_collapse' => false,
    'sidebar_collapse_auto_size' => false,
    'sidebar_collapse_remember' => false,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,

    /*
    |--------------------------------------------------------------------------
    | Control Sidebar (Right Sidebar)
    |--------------------------------------------------------------------------
    |
    | Here we can modify the right sidebar aka control sidebar of the admin panel.
    |
    | For detailed instructions you can look the right sidebar section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => true,
    'right_sidebar_push' => true,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Here we can modify the url settings of the admin panel.
    |
    | For detailed instructions you can look the urls section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'use_route_url' => false,
    'dashboard_url' => 'Home',
    'logout_url' => 'logout',
    'login_url' => 'login',
    'register_url' => 'register',
    'password_reset_url' => 'password/reset',
    'password_email_url' => 'password/email',
    'profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Laravel Mix
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Laravel Mix option for the admin panel.
    |
    | For detailed instructions you can look the laravel mix section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Other-Configuration
    |
    */

    'enabled_laravel_mix' => false,
    'laravel_mix_css_path' => 'css/app.css',
    'laravel_mix_js_path' => 'js/app.js',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar/top navigation of the admin panel.
    |
    | For detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Menu-Configuration
    |
    */

    'menu' => [
        [
            'text'    => 'Usuarios',
            'icon'    => 'fas fa-fw fa-user',
            'can' => 'ListaUsuario.index',
            'submenu' => [
                    [
                        'text'    => 'Alta Usuarios',
                        'icon_color' => 'success',
                        'url'     => 'register',
                        'can' => 'register.create',
                    ],
                    [
                        'text'    => 'Lista Usuarios',
                        'icon_color' => 'success',
                        'url'     => 'ListaUsuario',
                        'can' => 'ListaUsuario.index',
                    ],
                    [
                        'text'    => 'Activar Usuarios',
                        'icon_color' => 'success',
                        'url'     => 'ActivarUsuario',
                        'can' => 'ActivarUsuario.index',
                    ],
            ],
        ],
        [
            'text'    => 'Personal',
            'icon'    => 'fas fa-fw fa-user',
            'submenu' => [
                    [
                        'text'    => 'Alta Personal',
                        'icon_color' => 'success',
                        'url'     => 'personas/create',
                        'can' => 'personas.create',
                    ],
                    [
                        'text'    => 'Lista Personal',
                        'icon_color' => 'success',
                        'url'     => 'personas',
                        'can' => 'personas.index',

                    ],
                    [
                        'text'    => 'Activar Personal',
                        'icon_color' => 'success',
                        'url'     => 'ActivarPersonas',
                        'can' => 'ActivarPersonas.index',

                    ],
            ],
        ],
        [
            'text'    => 'Catálogos',
            'icon'    => 'fas fa-fw  fa-book',
            'submenu' => [
                [
                    'text'    => 'Clasificación ',
                    'icon_color' => 'success',
                    'submenu' => [
                        [
                            'text'    => 'Agregar ',
                            'icon_color' => 'success',
                            'url'     => 'Clasificacion/create',
                            'can' => 'Clasificacion.create'
                        ],
                        [
                            'text'    => 'Lista',
                            'icon_color' => 'success',
                            'url'     => 'Clasificacion',
                            'can' => 'Clasificacion.index'
                        ],

                    ],
                ],
                [
                    'text'    => 'Área',
                    'icon_color' => 'success',
                    'submenu' => [
                        [
                            'text'    => 'Agregar',
                            'icon_color' => 'success',
                            'url'     => 'Areas/create',
                            'can' => 'Areas.create'
                        ],
                        [
                            'text'    => 'Listado',
                            'icon_color' => 'success',
                            'url'     => 'Areas',
                            'can' => 'Areas.index'
                        ],
                    ],
                ],
                
                [
                    'text'    => 'jefatura',
                    'icon_color' => 'success',
                    'submenu' => [
                        [
                            'text'    => 'Agregar',
                            'icon_color' => 'success',
                            'url'     => 'jefatura/create',
                            'can' => 'SalidaEquipo.index'
                        ],
                        [
                            'text'    => 'Listado',
                            'icon_color' => 'success',
                            'url'     => 'jefatura',
                            'can' => 'SalidaEquipo.index'
                        ],
                    ],
                ],
                [
                    'text'    => 'Marcas',
                    'icon_color' => 'success',
                    'url'     => 'Marcas',
                    'can' => 'Marcas.index'
                ],
            ],
        ],

        [
            'text'        => 'Bienes',
            'icon'    => 'fas fa-fw fa-bars',
            'label_color' => 'success',
            'submenu' => [
                [
                    'text'    => 'Muebles',
                    'icon'    => 'fas fa-fw  fa-folder-open',
                    'can' => 'BienesUA.create',
                    'submenu' => [
                       
                        [
                            'text'    => 'Alta del bien',
                            'icon_color' => 'success',
                            'url'     => 'Bienes/create',
                            'can' => 'BienesUA.create'
                        ], 
                        [
                            'text' => 'Lista de bienes',
                            'icon_color' => 'success',
                            'url'  => 'Bienes',
                            'can' => 'BienesUA.index'

                        ],
                        [
                            'text'    => 'Baja del bien',
                            'icon_color' => 'red',
                            'url'     => 'BajaBien',
                            'can' => 'BajaBien.index'
                        ],
                        [
                            'text'    => 'Lista baja bien',
                            'icon_color' => 'red',
                            'url'     => '/bienbaja',
                            'can' => 'listaBajaBien.lista'
                        ],
                    ],

                ],
                [
                    'text'    => 'Cómputo y Accesorios ',
                    'icon'    => 'fas fa-fw  fa-desktop',
                    'can' => 'BienesUtic.index',
                    'submenu' => [
                        [
                            'text'    => 'Alta del bien',
                            'icon_color' => 'success',
                            'url'     => 'BienesUtic/create',
                            'can' => 'BienesUtic.create'

                        ],
                        [
                            'text' => 'Lista de bienes',
                            'icon_color' => 'success',
                            'url'  => 'BienesUtic',
                            'can' => 'BienesUtic.index'
                        ],
                        [
                            'text'    => 'Baja del bien',
                            'icon_color' => 'red',
                            'url'     => 'BajaBienUTIC',
                            'can' => 'BajaBienUtic.index'
                        ],
                        [
                            'text'    => 'Lista Baja bien',
                            'icon_color' => 'red',
                            'url'     => '/bienbajaUtic',
                            'can' => 'ListabajaUtic.listaUtic'
                        ],
                    ],
                ],
            ],
        ],

        [
            'text'        => 'Resguardo',
            'icon'    => 'fas fa-fw  fa-folder-open',
            'label_color' => 'success',
            'submenu' => [
                [
                    'text'    => 'Muebles',
                    'icon'    => 'fas fa-fw  fa-folder-open',
                    'can' => 'AsignacionBien.index',
                    'submenu' => [
                        [
                            'text' => 'Búsqueda',
                            'icon_color' => 'success',
                            'url'  => 'AsignacionBien',
                            'can' => 'AsignacionBien.index'

                        ],
                        [
                            'text'    => 'Asignación Bien',
                            'icon_color' => 'success',
                            'url'     => 'AsignacionBien/create',
                            'can' => 'AsignacionBien.create'
                        ],
                        [
                            'text'    => 'Reasignar',
                            'icon_color' => 'success',
                            'url'     => 'reasignacion',
                            'can' => 'reasignacion.index'
                        ],
                        [
                            'text'    => 'Activación de resguardos',
                            'icon_color' => 'success',
                            'url'     => 'Activacion',
                            'can' => 'Activacion.index'
                        ],


                    ],

                ],


                [
                    'text'    => 'Cómputo y accesorios',
                    'icon'    => 'fas fa-fw  fa-desktop',
                    'submenu' => [
                        [
                            'text' => 'Búsqueda',
                            'icon_color' => 'success',
                            'url'  => 'AsignacionBienUTIC',
                            'can' => 'ResguardosUtic.index'
                        ],
                        [
                            'text'    => 'Asignación Bien',
                            'icon_color' => 'success',
                            'url'     => 'AsignacionBienUTIC/create',
                            'can' => 'ResguardosUtic.create'
                        ],
                        [
                            'text'    => 'Reasignar ',
                            'icon_color' => 'success',
                            'url'     => 'reasignacionUtic',
                            'can' => 'ReasignacionUtic.index'
                        ],
                        [
                            'text'    => 'Activación ',
                            'icon_color' => 'success',
                            'url'     => 'ActivacionUTIC',
                            'can' => 'ActivacionUtic.index'
                        ],
                      
                    ],
                ]
            ],


        ],
        
        [ 
            'text'    => 'Impresión Resguardos',
            'icon'    => 'fas fa-fw  fa-download',

            'submenu' =>
        
                [
                   
                        [
                            'text'    => 'General',
                            'icon_color' => 'success',
                            'url'     => 'ReporteGeneral',
                            'can' => 'ReporteGral.ReporteGeneral'
                        ],
                        [
                            'text'    => 'Muebles',
                            'icon_color' => 'success',
                            'url'     => 'Resguardo',
                            'can' => 'ResguardoAdmiPDF.resguardo'
                        ],
                        [
                            'text'    => 'Cómputo y accesorios',
                            'icon_color' => 'success',
                            'url'     => 'ResguardoUtic',
                            'can' => 'ResguardoPDFUtic.resguardo'
                        ],

                
            ],


        ],
        [
           
                    'text'    => 'Salida equipos',
                    'icon'    => 'fas fa-fw  fa-folder-open',
                    'can' => 'listadoSalida.listadoSalidas',
                    'submenu' => [
                        [
                            'text'    => 'Salidas',
                            'icon_color' => 'success',
                            'url'     => 'SalidaEquipo',
                            'can' => 'SalidaEquipo.index'
                        ],
                      
                        [
                            'text'    => 'Listado Salida',
                            'icon_color' => 'success',
                            'url'     => 'ListadoSalidas',
                            'can' => 'listadoSalida.listadoSalidas'
                        ],
                        
                        [
                            'text'    => 'Resguardo temporal',
                            'icon_color' => 'success',
                            'url'     => 'ResguardoTemporal/create',
                            'can' => 'ResguardoTemporal.create'
                        ],
                        [
                            'text' => 'Lista de Temporales',
                            'icon_color' => 'success',
                            'url'  => 'ListaResguardoTemporal',
                            'can' => 'ResguardoTemporal.index'
                        ],
                        [
                            'text'    => 'Cancelación R. Temporal',
                            'icon_color' => 'success',
                            'url'     => 'CancelarResguardoTemp',
                            'can' => 'CancelarResguardoTem.index'
                        ],
                    ],

        ],
        [ 
            'text'    => 'Impresión de salidas',
            'icon'    => 'fas fa-fw  fa-print',
            'can' => 'ResguardoTemporal.index',
            'submenu' => [
                [
                    'text'    => 'Salidas Equipo',
                    'icon_color' => 'success',
                    'url'     => 'SalidaEquipos',
                    'can' => 'SalidaEquipo.index'
                ],
                [
                    'text' => 'Resguardo Temporal',
                    'icon_color' => 'success',
                    'url'  => 'ResguardanteTemporal',
                    'can' => 'ResguardoTemporal.index'
                ]
            ],

                
            


        ],
        [
            'text'    => 'Control IP',
            'icon'    => 'fa fa-rss',
            
            'submenu'    => [
                [
                    'text'    => 'Registro',
                    'icon_color' => 'success',
                    'url'     => 'Control_Ip/create',
                    'can' => 'SalidaEquipo.index'
                ],
                [
                    'text'    => 'Listado',
                    'icon_color' => 'success',
                    'url'     => 'Control_Ip',
                    'can' => 'SalidaEquipo.index'
                ],
                [
                    'text'    => 'listado de Bajas',
                    'icon_color' => 'success',
                    'url'     => 'BajaIP',
                    'can' => 'SalidaEquipo.index'
                ],
                [
                    'text'    => 'Disponibilidad de Ip',
                    'icon_color' => 'success',
                    'url'     => 'BajaIP/create',
                    'can' => 'SalidaEquipo.index'
                ],
                [
                    'text'    => 'Historial de personas',
                    'icon_color' => 'success',
                    'url'     => 'historialControlIp',
                    'can' => 'SalidaEquipo.index'
                ],
                [
                    'text'    => 'Reportes',
                    'icon'    => 'fas fa-fw fa-book',
                    'url'     => 'ReporteControl',
                    'can' => 'SalidaEquipo.index'
                ],
            ],

        ],
        [
            'text'    => 'Reportes',
            'icon'    => 'fas fa-fw fa-book',
            
            'submenu'    => [
                [
                    'text'    => 'Administración',
                    'icon_color' => 'success',
                    'url'     => 'Reportes',
                    'can' => 'ReporteAmon.reportes'
                ],
                [
                    'text'    => 'Utic',
                    'icon_color' => 'success',
                    'url'     => 'ReportesUTIC',
                    'can' => 'ReportesUTIC.reportesUTIC'
                ],
            ],

        ],
        [
            'text'    => 'Historial bienes Muebles',
            'icon'    => 'fa fa-fw fa-hourglass-half',
            'url'     => 'HistorialBien',
            'can' => 'HistorialBien.index'
        ],
        [
            'text'    => 'Historial Equipo de Computo',
            'icon'    => 'fa fa-fw fa-hourglass-half',
            'url'     => 'HistorialBienesUTIC',
            'can' => 'HistorialBienesUTIC.indexSI'
        ],
        [
            'text'    => 'Historial de Usuarios',

            'icon'    => 'fas fa-fw fa-users',
            // <ion-icon name="person-circle-outline"></ion-icon>,
            'url'     => 'LogsUser',
            'can' => 'LogsUser.index'
        ],

    ],


    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Here we can modify the menu filters of the admin panel.
    |
    | For detailed instructions you can look the menu filters section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Menu-Configuration
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\DataFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Here we can modify the plugins used inside the admin panel.
    |
    | For detailed instructions you can look the plugins section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Plugins-Configuration
    |
    */

    'plugins' => [
        'Datatables' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css',
                ],
            ],
        ],
        'Select2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        'Chartjs' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        'Sweetalert2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/sweetalert2@8',
                ],
            ],
        ],
        'Pace' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Livewire
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Livewire support.
    |
    | For detailed instructions you can look the livewire here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Other-Configuration
    */

    'livewire' => false,
];



    
@extends('adminlte::page')

@section('title', 'Home')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
@stop

@section('content')
<section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes -->
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h4 align="center">USUARIOS</h4>
                            <p>Nuevo usuario</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="register" class="small-box-footer">Registrar<i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-4">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h4 align="center">BIENES</h4>

                            <p>Nuevo bien</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="Bienes/create" class="small-box-footer">Alta bien mueble <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-4">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h4 align="center">BIENES</h4>

                            <p>Nuevo bien</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="BienesUtic/create" class="small-box-footer">Alta Equipo de cómputo <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-4">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h4 align="center">HISTORIAL BIENES</h4>
                            <p>ver historial</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="HistorialBien" class="small-box-footer">Bienes muebles <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
    </section>
    <section>
        <div class="row ml-2">

            <div class="card card-success card-outline  col-lg-6">
                <div class="card-header">
                    <h5 class="card-title">Resguardos Temporales</h5>
                    <a href="CancelarResguardoTemp" class="btn btn-outline-success float-right">Entregar equipo</a>

                </div>
                <div class="card-body">
                    <p class="card-text">Vencimiento próximo de resguardos.</p>
                    <!-- Listado de resguardo -->
                    <table id="resguardo" class="table table-bordered yajra-datatable">
                        <thead class="table-success text-black">
                            <tr>
                                <th scope="col">Prof.</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">ApellidoP</th>
                                <th scope="col">ApellidoM</th>
                                <th scope="col">Fecha De Vencimiento</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div><!-- /.card -->
            </div>
            <div class="card card-success card-outline ml-3 col-lg-5">
                <div class="card-header">
                    <h5 class="card-title">Control de direcciónes IP</h5>
                    <a href="Control_Ip" class="btn btn-outline-success float-right">Listado de direcciones IP</a>

                </div>
                <div class="card-body">
                    <!-- Listado de dirección ip -->
                    <table id="Ip" class="table table-striped ip">
                        <thead class="table-outline-success text-black table-sm">
                            <tr>
                                <th scope="col">IP</th>
                                <th scope="col">Nombre del personal</th>
                                <th scope="col">Inicia</th>
                                <th scope="col">Vence</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div><!-- /.card -->
            </div>
        </div>
    </section>

    <section>
        <div class="row ml-2">
            <div class="card card-success card-outline mr-2 col-lg-6">
                <div class="card-header">
                    <h5 class="card-title">Salida de Equipos</h5>
                    <a href="ListadoSalidas" class="btn btn-outline-success float-right">Entregar equipo</a>
                </div>
                <div class="card-body">
                    <p class="card-text">Vencimiento próximo de salidas</p>
                    <!-- Listado de resguardo -->
                    <table id="resguardo" class="table table-striped Salida">
                        <thead class="table-outline-success text-black table-sm">
                            <tr>
                                <th scope="col">Referencia</th>
                                <th scope="col">Fecha De Notificacion</th>
                                <th scope="col">Fecha De Vencimiento</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div><!-- /.card -->
            </div>
            <div class="card card-success card-outline col-lg-5">
                <div class="card-header">
                    <h5 class="card-title">Logueos de usuario</h5>
                </div>
                <div class="card-body">
                    <!-- Listado de logueos -->
                    <table id="logueos" class="table table-striped logueos">
                        <thead class="table-outline-success text-black table-sm">
                            <tr>
                                <th scope="col">Usuario</th>
                                <th scope="col">inicio de sesión</th>
                                <th scope="col">fin de sesión</th>
                                <th scope="col">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div><!-- /.card -->
            </div>

        </div>

    </section>
    @stop

    @section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
    @stop

    @section('js')
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
    // Paginación y estilo de la tabla área mediante ajax
    $(function() {
        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            "order": [
                [4, "desc"]
            ],
            "lengthMenu": [
                [4, 10, 50, -1],
                [4, 10, 50, "All"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            ajax: "{{ route('get-getResguardoVence') }}",
            columns: [{
                    data: 'Profesion',
                    name: 'Profesion'
                },
                {
                    data: 'Nombre',
                    name: 'Nombre'
                },
                {
                    data: 'ApellidoP',
                    name: 'ApellodoP'
                },
                {
                    data: 'ApellidoM',
                    name: 'ApellidoM'
                },
                {
                    data: "fecha_vencimiento",
                    name: "fecha_vencimiento",
                    "render": function(data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) +
                            "/" + date.getFullYear();
                    }
                }
            ]
        });

    });

    // Paginación y estilo de la tabla área mediante ajax
    $(function() {
        var table = $('.Salida').DataTable({
            processing: true,
            serverSide: true,
            bInfo: false,
            "order": [
                [2, "desc"]
            ],
            "lengthMenu": [
                [4, 10, 50, -1],
                [4, 10, 50, "All"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            ajax: "{{ route('get-getSalidaEquipoVence') }}",
            columns: [{
                    data: 'referencia',
                    name: 'referencia'
                },
                {
                    data: "fecha_notificacion",
                    name: "fecha_notificacion",
                    "render": function(data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) +
                            "/" + date.getFullYear();
                    }
                },
                {
                    data: "fecha_vencimiento",
                    name: "fecha_vencimiento",
                    "render": function(data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) +
                            "/" + date.getFullYear();
                    }
                },
            ]
        });

    });
    // Paginación y estilo de la tabla logueos inicio y fin de sesion
    $(function() {
        var table = $('.logueos').DataTable({
            processing: true,
            serverSide: true,
            bInfo: false,
            "order": [
                [2, "asc"]
            ],
            "lengthMenu": [
                [4, 10, 50, -1],
                [4, 10, 50, "All"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            ajax: "{{ route('get-getsession') }}",
            columns: [{
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'inicio_login',
                    name: 'inicio_login'
                },
                {
                    data: 'fin_login',
                    name: 'fin_login'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },

            ]
        });

    });
    // Control de direcciones ip
    $(function() {
        var table = $('.ip').DataTable({
            processing: true,
            serverSide: true,
            bInfo: false,
            "order": [
                [2, "asc"]
            ],
            "lengthMenu": [
                [4, 10, 50, -1],
                [4, 10, 50, "All"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            ajax: "{{ route('get-getProximosIp') }}",
            columns: [{
                    data: 'ip',
                    name: 'ip'
                },
                {
                    data: 'NombreP',
                    name: 'NombreP'
                },
                {
                    data: 'inicia',
                    name: 'inicia',
                    orderable: true,
                    searchable: true
                },
                {
                    data: 'vence',
                    name: 'vence',
                    orderable: true,
                    searchable: true
                },

            ]
        });

    });
    </script>
    @stop
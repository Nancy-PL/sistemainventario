@extends('adminlte::page')

@section('title', 'Home')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
@stop

@section('content')

<section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes -->
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h4 align="center">PERSONAL</h4>
                            <p>Alta personal</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="personas/create" class="small-box-footer">Registrar<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-4">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h4 align="center">BIENES</h4>
                            <p>Nuevo bien</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="Bienes/create" class="small-box-footer">Alta bien mueble <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-4">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h4 align="center">BIENES</h4>
                            <p>Nuevo bien</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="BienesUtic/create" class="small-box-footer">Alta Equipo de cómputo <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-4">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h4 align="center">RESGUARDOS</h4>
                            <p>Reasignación de resguardo</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="reasignacion" class="small-box-footer">Reasignación<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
    </section>
    <section>
        <div class="card card-success card-outline col-lg-10">
            <div class="card-header">
                <h5 class="card-title">HISTORIAL DE MUEBLES</h5>
            </div>
            <div class="card-body">
                <p class="card-text">MUEBLES</p>
                <!-- Listado de resguardo -->
                <table id="bienes" class="table table-striped Salida">
                    <thead class="table-outline-success text-black table-sm">
                        <tr>
                            <th scope="col">Clasificación</th>
                            <th scope="col">No. de serie </th>
                            <th scope="col">No. Inventario Actual</th>
                            <th scope="col">Usuario alta</th>
                            <th scope="col">Fecha de alta</th>
                            <th scope="col">Usuario Baja</th>
                            <th scope="col">Fecha de baja</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div><!-- /.card -->
        </div>
    </section>
    <section>
        <div class="card card-success card-outline col-lg-10">
            <div class="card-header">
                <h5 class="card-title">HISTORIAL DE CÓMPUTO Y ACCESORIOS</h5>

            </div>
            <div class="card-body">
                <p class="card-text">CÓMPUTO Y ACCESORIOS</p>
                <!-- Listado de resguardo -->
                <table id="bienes" class="table table-bordered yajra-datatable">
                    <thead class="table-success text-black">
                        <tr>
                            <th scope="col">Clasificación</th>
                            <th scope="col">No. de serie </th>
                            <th scope="col">No. Inventario Actual</th>
                            <th scope="col">Usuario alta</th>
                            <th scope="col">Fecha de alta</th>
                            <th scope="col">Usuario Baja</th>
                            <th scope="col">Fecha de baja</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div><!-- /.card -->
        </div>
    </section>
    @stop


    @section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
    @stop

    @section('js')
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // Paginación y estilo de la tabla área mediante ajax
        $(function() {
            var table = $('.Salida').DataTable({
                processing: true,
                serverSide: true,
                "order": [
                    [4, "desc"]
                ],
                "lengthMenu": [
                    [4, 10, 50, -1],
                    [4, 10, 50, "All"]
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                },
                ajax: "{{ route('get-HistorialBienesUA') }}",
                columns: [{
                        data: 'concepto',
                        name: 'concepto'
                    },
                    {
                        data: 'NoSerie',
                        name: 'NoSerie'
                    },
                    {
                        data: 'ClaveBien',
                        name: 'ClaveBien'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: "fecha_alta",
                        name: "fecha_alta",
                        "render": function(data) {
                            var date = new Date(data);
                            var month = date.getMonth() + 1;
                            return (month.length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
                        }
                    },
                    {
                        data: 'userBaja',
                        name: 'userBaja'
                    },
                    {
                        data: "fecha_baja",
                        name: "fecha_baja",
                        "render": function(data) {
                            if (data) {
                                var date = new Date(data);
                                var month = date.getMonth() + 1;
                                return (month.length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
                            } else {
                                return null;
                            }

                        }
                    },


                ]
            });

        });

        // Paginación y estilo de la tabla área mediante ajax
        $(function() {
            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                "order": [
                    [2, "desc"]
                ],

                "lengthMenu": [
                    [4, 10, 50, -1],
                    [4, 10, 50, "All"]
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                },
                ajax: "{{ route('get-HistorialBienesSI-UA') }}",
                columns: [{
                        data: 'concepto',
                        name: 'concepto'
                    },
                    {
                        data: 'NoSerie',
                        name: 'NoSerie'
                    },
                    {
                        data: 'ClaveBien',
                        name: 'ClaveBien'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: "fecha_alta",
                        name: "fecha_alta",
                        "render": function(data) {
                            var date = new Date(data);
                            var month = date.getMonth() + 1;
                            return (month.length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
                        }
                    },
                    {
                        data: 'userBaja',
                        name: 'userBaja'
                    },
                    {
                        data: "fecha_baja",
                        name: "fecha_baja",
                        "render": function(data) {
                            if (data) {
                                var date = new Date(data);
                                var month = date.getMonth() + 1;
                                return (month.length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
                            } else {
                                return null;
                            }

                        }
                    },


                ]
            });

        });
    </script>
    @stop
<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100 rounded">
    <div>
        {{ $logo }}
    </div>

    <div class="w-full sm:max-w-md   shadow-md overflow-hidden sm:rounded-lg rounded">
        {{ $slot }}
    </div>
</div>

@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">LISTA DE PERSONAL</h1>
@stop


@section('content')
<!-- Mensaje del No. de inventario o No. de control -->
<div class="card-header">
    @if($message=Session::get('UPS'))
    <div class="col-12 alert-warning alert-dismissable fade show" role="alert">
        <h5>Error al dar de baja</h5>
        <span>{{$message}}</span>

    </div>
    @endif
    @if($message=Session::get('Listo'))
    <div class="col-12 alert-success alert-dismissable fade show" role="alert">
        <h5>Operación exitosa</h5>
        <span>{{$message}}</span>

    </div>
    @endif
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $messages)
            <li>{{ $messages }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
<form action="BajaPersonas" method="POST">
    <!--token para verificar que el usuario autenticado es quien en realidad está haciendo la petición-->
    @csrf
    <!-- Listado de personas -->

    <!-- Listado de Usuario -->
    <table id="personas" class="table table-bordered yajra-datatable" style="width:100%">
        <thead class="table-success text-black">

            <tr>
                @can('BajaPersonas.index')
                <th>Baja</th>
                @endcan
                @cannot('BajaPersonas.index')
                <th width="0px"></th>
                @endcan
                <th scope="col">Profesión</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido Paterno</th>
                <th scope="col">Apellido Materno</th>
                <th scope="col">Área</th>
                <th scope="col">Jefatura</th>
                <th scope="col">Fecha de Ingreso</th>
                <th scope="col">Editar</th>

            </tr>
        </thead>
        <tbody  style="text-transform:uppercase;">

        </tbody>
    </table>
    <!-- motivo de la baja -->
    <div id="motivo" style="display:none;" class=" row justify-content-md-center mt-3 mr-3">
        <x-label type="hidden" for="motivo" class="mt-3" :value="__('Motivo de la baja')" />
    </div>
    <div id="Motivos" style="display:none;" class=" row justify-content-md-center mt-3 mr-3">
        <x-input id="Motivos" placeholder="Motivo de la baja" class="form-control col-sm-5" type="text" name="Motivos"
            style=" text-transform:uppercase;" value="" onkeyup="javascript:this.value=this.value.toUpperCase();"
            tabindex="5" required />
    </div>
    <div class="modal fade" id="create">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Dar de baja</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>×</span>
                    </button>

                </div>
                <div class="modal-body">
                    ¿Esta seguro que quiere dar de baja a esta persona?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-primary" value="Dar de baja">
                </div>
            </div>
        </div>
    </div>
    @can('BajaPersonas.index')

    <a href="submit" class="btn btn-outline-danger " data-toggle="modal" data-target="#create">
        Dar de baja
    </a>
    @endcan

</form>
@stop

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">
@stop

@section('js')

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script>
// Paginación y estilo de la tabla usuario
$(function() {
    var table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        orderCellsTop: true,
        fixedHeader: true,
        "scrollX": true,
        "order": [
            [6, "desc"]
        ],

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        ajax: "{{ route('get-getPersonass') }}",
        columns: [{
                data: 'actions',
                name: 'actions',
                orderable: false,
                searchable: false,
                "render": function(data) {
                    "@can('BajaPersonas.index')"
                    return data;
                    "@endcan"
                    "@cannot('BajaPersonas.index')"
                    return "";
                    "@endcan"

                }
            },
            {
                data: 'Profesion',
                name: 'Profesion'
            },
            {
                data: 'Nombre',
                name: 'Nombre'
            },
            {
                data: 'ApellidoP',
                name: 'ApellidoP'
            },
            {
                data: 'ApellidoM',
                name: 'ApellidoM'
            },
            {
                data: 'NombreA',
                name: 'NombreA'
            },
            {
                data: 'NombreJ',
                name: 'NombreJ'
            },
            {
                data: 'created_at',
                name: 'created_at',
                "render": function(data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) +
                        "/" + date.getFullYear();
                }
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            },


        ]
    });

    //buscar por encabezado
    $(document).ready(function() {
        $('#personas thead tr').clone(true).appendTo('#personas thead');
        $('#personas thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            if ( title != 'Baja') {
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');

                $('input', this).on('keyup change', function() {
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            }else{
                $(this).html('<input type="text />');

            }
        });
    });
});

$('#Persona').on('change', function() {


}).change();

function persona() {
    divC = document.getElementById("motivo");
    divC.style.display = "";

    divT = document.getElementById("Motivos");
    divT.style.display = "";
    $('#Motivo').prop("required", true);

}
</script>
@stop
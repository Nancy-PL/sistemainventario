@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<h1 class="d-flex justify-content-center" style="color:#62A31B">EDICIÓN DE PERSONAL </h1>
@stop

@section('content')
<!-- Post editar personas -->
<form action="../../personas/{{$persona->id}}" method="POST" class="was-validated">
    <!--token para verificar que el usuario autenticado es quien en realidad está haciendo la petición-->
    @csrf
    <!--Establece la clave y el valor dados en la colección-->
    @method('PUT')
    <div class="card-header">
            <!--Imprimiendo los valores de las validaciones-->
            @if (count($errors) > 0)
        <div class="alert alert-danger">
            <p>Corrige los siguientes errores:</p>
            <ul>
                @foreach ($errors->all() as $messages)
                <li>{{ $messages }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
    <div class="form-row">
        <!-- Nombre -->
        <div class="col-md-4 mb-3">
            <x-label for="Nombre" class="mt-3" :value="__('Nombre')" />
            <x-input id="Nombre" class="form-control" type="text" name="Nombre" :value="old('Nombre')" style="text-transform:uppercase;" value="{{$persona->Nombre}}"   tabindex="1" required autofocus />
        </div>
        <!-- Apellido Paterno -->
        <div class="col-md-4 mb-3">
            <x-label for="ApellidoP" class="mt-3" :value="__('Apellido Paterno ')" />
            <x-input id="ApellidoP" placeholder=" Ingrese  Apellido Paterno" class="form-control" type="text" name="ApellidoP" :value="old('ApellidoP')" style="text-transform:uppercase;" value="{{$persona->ApellidoP}}"   tabindex="2" required autofocus />
        </div>
        <!-- Apellido Materno -->
        <div class="col-md-4 mb-3">
            <x-label for="ApellidoM" class="mt-3" :value="__('Apellido Materno ')" />
            <x-input id="ApellidoM" placeholder=" Ingrese Apellido Materno" class="form-control" type="text" name="ApellidoM" :value="old('ApellidoM')" style="text-transform:uppercase;" value="{{$persona->ApellidoM}}"   tabindex="2" required autofocus />
        </div>
        <!-- Abreviatura de la Profesion  -->
        <div class="col-md-4 mb-3">
            <x-label for="Profesion" class="mt-3" :value="__('Abrevia tu profesión')" />
            <x-input id="Profesion" placeholder="Ejemplo ing." class="form-control" type="text" name="Profesion" :value="old('Profesion')" style="text-transform:uppercase;" value="{{$persona->Profesion}}"   tabindex="3" required autofocus />
        </div>
        <!--Seleccione area -->
        <div class="col-md-4 mb-3"  >
            <x-label for="area" class="mt-3" value="{{__('Ingresar Área:')}}" />
            <div class="form-group">
                <select name="area"  class="custom-select js-example-basic-single" style="text-transform: uppercase;" required>
                    <option value="">SELECCIONA ÁREA</option>
                    @foreach ($areas as $areas)
                    <option  value="{{$areas->id}}" @if($persona->area_id == $areas->id ) selected @endif>{{$areas->Nombre}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <!--Jefatura-->
        <div class="col-md-4 mb-3">
            <x-label class="mt-3" for="areas" :value="__('Jefatura')" />
            <div class="form-group" style="text-transform: uppercase;">
                <select name="Jefatura" id="Jefatura" class="custom-select js-example-basic-single"  tabindex="5"  >
                    <option  value="">SELECCIONA JEFATURA</option>
                    @foreach ($Jefatura as $Jefatura)
                    <option  style="text-transform: uppercase;" value="{{$Jefatura->id}}">{{$Jefatura->Nombre}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <!--Submit Guardar -->
    <div class="row justify-content-md-center">
        <button type="submit" class="btn btn-success mt-3 mr-3 col-sm-3" tabindex="4">Guardar</button>
    </div>
</form>
@stop

@section('css')
<link rel="stylesheet" href="../../css/bootstrap.min.css">
@stop

<!--Llamas a los estilos js -->
@section('js')

<!--Estilos del select2-->
<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script>
@stop
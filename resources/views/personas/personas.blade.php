@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right">
    <img src="../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mt-20" style="color:#008000" class="was-validated">ALTA PERSONAL</h1>
@stop

@section('content')
<!-- Alta de personas -->
<form action="../personas" method="POST" class=" was-validated">
    @csrf
    <div class="card-header">
        <!--Imprimiendo los valores de las validaciones-->
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <p>Corrige los siguientes errores:</p>
            <ul>
                @foreach ($errors->all() as $messages)
                <li>{{ $messages }}</li>
                @endforeach
            </ul>
        </div>
        @endif
         <!--Imprimiendo los valores de las validaciones-->
    @if($message=Session::get('UPS'))
            <div class="col-12 alert-success alert-dismissable fade show" role="alert">
                <span>{{$message}}</span>
            </div>
    @endif
    </div>
    <!-- Nombre -->
    <div class="form-row">
        <div class="col-md-4 mb-3">
            <x-label class="mt-3" :value="__('Nombre')" />
            <x-input id="Nombre" placeholder=" Ingrese Nombre de la persona" class="form-control" type="text" name="Nombre" :value="old('Nombre')" style="text-transform:uppercase;" value=""  onchange="ValidandoExistencia({{$personas}})"   tabindex="1" required autofocus />
        </div>
        <!-- Apellido Paterno -->
        <div class="col-md-4 mb-3">
            <x-label for="ApellidoP" class="mt-3" :value="__('Apellido Paterno ')" />
            <x-input id="ApellidoP" placeholder=" Ingrese  Apellido Paterno" class="form-control" type="text" name="ApellidoP" :value="old('ApellidoP')" style="text-transform:uppercase;" value="" onchange="ValidandoExistencia({{$personas}})"   tabindex="2" required autofocus />
        </div>
        <!-- Apellido Materno -->
        <div class="col-md-4 mb-3">
            <x-label for="ApellidoM" class="mt-3" :value="__('Apellido Materno ')" />
            <x-input id="ApellidoM" placeholder=" Ingrese Apellido Materno" class="form-control" type="text" name="ApellidoM" :value="old('ApellidoM')" style="text-transform:uppercase;" value=""   onchange="ValidandoExistencia({{$personas}})" tabindex="3" required autofocus />
        </div>
        <!-- Profesion  -->
        <div class="col-md-4 mb-3">
            <x-label for="Profesion" class="mt-3" :value="__('Abrevia tu profesión')" />
            <x-input id="Profesion" placeholder="Ejemplo ing." class="form-control" type="text" name="Profesion" :value="old('Profesion')" style="text-transform:uppercase;" value=""   tabindex="4" required autofocus />
        </div>
     


        <!--Area-->
        <div class="col-md-4 mb-3">
            <x-label class="mt-3" for="areas" :value="__('Área')" />
            <div class="form-group" style="text-transform: uppercase;">
                <select name="area" id="area" class="custom-select js-example-basic-single"  tabindex="5"  required>
                    <option  value="">SELECCIONA ÁREA</option>
                    @foreach ($areas as $areas)
                    <option  style="text-transform: uppercase;" value="{{$areas->id}}">{{$areas->Nombre}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <!--Jefatura-->
        <div class="col-md-4 mb-3">
            <x-label class="mt-3" for="areas" :value="__('Jefatura')" />
            <div class="form-group" style="text-transform: uppercase;">
                <select name="Jefatura" id="Jefatura" class="custom-select js-example-basic-single"  tabindex="5"  >
                    <option  value="">SELECCIONA JEFATURA</option>
                    @foreach ($Jefatura as $Jefatura)
                    <option  style="text-transform: uppercase;" value="{{$Jefatura->id}}">{{$Jefatura->Nombre}}</option>
                    @endforeach
                </select>
            </div>
        </div>

    </div>
    <!--Guardar-->
    <div class="row justify-content-md-center">
        <button type="submit" class="btn btn-outline-success mt-3 mb-3 col-sm-3" tabindex="6">Guardar</button>
    </div>
    <div class="col-10 ">
        <x-label class="mt-3" :value="__('Resultado de la busqueda:')" />
        <x-input id="NombrePersona" class="form-control" type="text" name="NombrePersona" />
    </div>
</form>
@stop

@section('css')
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/select2.min.css">


@stop

@section('js')
<script src="{{ asset('/js/select2.min.js') }}"></script>

<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.js-example-basic-single').select2({
            theme: "classic"
        });

    });
    sorter: data => data.sort((a, b) => a.text.localeCompare(b.text));

    function eliminarDiacriticos(texto) {
        return texto.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    }

    function ValidandoExistencia(personas) {

        var Nombre = eliminarDiacriticos(document.getElementById("Nombre").value);
        var ApellidoP = eliminarDiacriticos(document.getElementById("ApellidoP").value);
        var ApellidoM = eliminarDiacriticos(document.getElementById("ApellidoM").value);
        
        Nombre=Nombre.split(" ").join("").toUpperCase();
        ApellidoP=ApellidoP.split(" ").join("").toUpperCase();
        ApellidoM=ApellidoM.split(" ").join("").toUpperCase();

        var NombreCompleto=Nombre+ApellidoP+ApellidoM;

        if(document.getElementById("area").value!=""){
                var area = document.getElementById("area").value;
                var NombreCompleto=Nombre+ApellidoP+ApellidoM+area;
        }
        var bandera=0;
        personas.forEach(function(persona) {
            var NombreBD=eliminarDiacriticos(persona.Nombre).split(" ").join("")+eliminarDiacriticos(persona.ApellidoP).split(" ").join("")+eliminarDiacriticos(persona.ApellidoM).split(" ").join("");
            var NombreBDAP=eliminarDiacriticos(persona.Nombre).split(" ").join("")+eliminarDiacriticos(persona.ApellidoP).split(" ").join("");
            var NombreSolo=eliminarDiacriticos(persona.Nombre).split(" ").join("");

            if ( NombreBDAP== NombreCompleto) {
                document.getElementById("NombrePersona").value=persona.Profesion+" "+persona.Nombre+" "+persona.ApellidoP+" "+persona.ApellidoM;
            }
            if ( NombreSolo== NombreCompleto) {
                document.getElementById("NombrePersona").value=persona.Profesion+" "+persona.Nombre+" "+persona.ApellidoP+" "+persona.ApellidoM;
            }
            if ( NombreBD== NombreCompleto &&  bandera==0) {
                document.getElementById("NombrePersona").value=persona.Profesion+" "+persona.Nombre+" "+persona.ApellidoP+" "+persona.ApellidoM;
                alert("Nombre de la persona existente");
                bandera=1;
            }else  if(document.getElementById("area").value!="" ){            
                var NombreBDA=NombreBD+persona.area_id;
                            
                if(NombreBDA== NombreCompleto &&  bandera==0){
                    alert("Persona existente");
                    bandera=1;
                }

            }
           
           
        })    
    }
    </script>
@stop
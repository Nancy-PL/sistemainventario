
<link rel="stylesheet" href="public/css/bootstrap.min.css">


    
<x-guest-layout>
    <div class="row justify-content-md-center mt-5">
        <div class="card w-50">
            <div class="card-header" align="center">
                <x-auth-card>
                    <x-slot name="logo">
                        <img src="{{asset('public/images/logotipoOSFE.png')}}" width="170">
                    </x-slot>
                </x-auth-card>
            </div>
            <x-auth-card align="center">
                <x-slot name="logo">
                </x-slot>

                <!-- Estado de sesión-->
                <x-auth-session-status class="mb-4" :status="session('status')" />
                <!-- Validación de errores -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />
                @if($message=Session::get('UPS'))
                <div class="col-12 alert-warning alert-dismissable fade show" role="alert">
                    <span>{{$message}}</span>

                </div>
                @endif
                <form method="POST" action="public/login">
                    <!--El csrf es un token se utiliza para verificar que el usuario autenticado es la persona que realmente realiza las solicitudes a la aplicación. -->
                    @csrf

                    <!-- Nombre de usuario -->
                    <div class="row justify-content-md-center">
                        <x-label for="name" class="mt-3" :value="__('NOMBRE DE USUARIO:')" />
                    </div>

                    <div class="row justify-content-md-center">
                        <x-input id="name" placeholder="Nombre del Usuario" class="form-control col-sm-5 " type="text" name="name" :value="old('nameU')" required autofocus />
                    </div>

                    <!-- Contraseña -->
                    <div class="row justify-content-md-center" class="mt-4">
                        <x-label for="password" class="mt-3" :value="__('CONTRASEÑA:')" />
                    </div>

                    <div class="row justify-content-md-center" class="mt-4">
                        <x-input id="password" class="form-control col-sm-5 " type="password" name="password" required autocomplete="current-password" />
                    </div>

                    <div class="row justify-content-md-center" class="mt-4">

                        <x-button class="btn btn-dark ml-3 mt-3">
                            {{ __('Iniciar Sesión') }}
                        </x-button>

                    </div>

                </form>
                <div class="card-footer">
                </div>
            </x-auth-card>

</x-guest-layout>
</div>
</div>
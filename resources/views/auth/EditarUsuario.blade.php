@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<h1 class="d-flex justify-content-center" style="color:#008000">Editar Usuario</h1>
@stop

@section('content')
<!--Creas  un recurso con el metodo POST  -->
<form method="POST" action="../{{$users->id}}" class="was-validated">
    <!--El csrf es un token se utiliza para verificar que el usuario autenticado es la persona que realmente realiza las solicitudes a la aplicación. -->
    @csrf
    @method('PUT')
    <div class="row justify-content-md-center">
        <div class="card w-50">
            <div class="card-header">
                <div class="alert alert-success">
                    <small>La contraseña debe contener: </small>
                    <small>mínimo 8 caracteres, </small>
                    <small>mayúsculas, </small>
                    <small>minúsculas, </small>
                    <small>dígitos de base 10 (del 0 al 9), </small>
                    <small>caracteres no alfanuméricos.</small>
                </div>
                <!--Enviando mensajes de error resultado de las validaciones-->
                @error('name')
                <div class="alert alert-danger">
                    <small>El nombre de usuario ya esta en uso</small>
                </div>
                @enderror
   
                @error('password')
                <div class="alert alert-danger">
                <small>*La contraseña no cumple con lo establecido</small>
                </div>
                @enderror
            </div>
            <!-- Nombre -->
            <div class="row justify-content-md-center ">
                <x-label for="NombreA" class="mt-3" :value="__('Nombre')" />
            </div>
            <div class="row justify-content-md-center ">
                <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->

                <div class=" form-group">
                    <select class="custom-select " name="persona_id" disabled>
                        <option value="" selected></option>
                        @foreach ($persona as $persona)     
                        <option value="{{$persona->Persona_id}}" @if($persona->Persona_id == $users->persona_id)selected @endif>{{$persona->NombreCompleto}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!-- Nombre del Usuario -->
            <div class=" row justify-content-md-center">
                <x-label for="name" class="mt-3" :value="__('Nombre del Usuario')" />
            </div>
            <div class="row justify-content-md-center">
                <input id="name" placeholder="Nombre del Usuario" class="form-control col-sm-5" type="text" name="name" value="{{$users->name}}" tabindex="3" required autofocus />
            </div>
            <!-- Contraseña -->
            <div class=" row justify-content-md-center">
                <x-label for="password" class="mt-3"/>
            </div>
           <!-- <div class=" row justify-content-md-center mt-3" style="color:#117A65">
                <x-label>La contraseña debe de tener minimo 8 caracteres incluyendo mayúscula (A-Z),</x-label><br>
                <x-label>Caracteres en minúscula(a-z), </x-label><br>
                <x-label>Base 10 dígitos(0-9),</x-label><br>
                <x-label>No alfanumérico(por ejemplo:@$-_.*!%*#?&)</x-label><br>
            </div>-->
            <div class="row justify-content-md-center">
                <input id="password" placeholder="Contraseña" class="form-control col-sm-5 mb-3" type="password" name="password"  tabindex="4" required autofocus />
            </div>
            <!--Seleccione Rol -->
            <div class="row justify-content-md-center">
                <x-label for="role_id" class="mt-3" value="{{__('Ingresar el rol:')}}" />
            </div>
            <div class="row justify-content-md-center">
                <div class="form-group">
                    <!--Seleccionas un rol haciendo consulta a la bd de la tabla rol del campo nombre del rol  -->
                    <select name="role_id" class="custom-select" required>
                        <option value="">SELECCIONA ROL</option>
                        @foreach ($roles as $rol)
                        <option value="{{$rol->Rol_id}}" @if($rol->Rol_id == $users->roles_id)selected @endif>{{$rol->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <button type="submit" class="btn btn-success mt-3 mb-3 col-sm-3" tabindex="4">Guardar</button>
            </div>
        </div>
    </div>
</form>
@stop

<!--Llamas a los estilos css -->
@section('css')
<link rel="stylesheet" href="css/select2.min.css">
<link rel="stylesheet" href="../../css/bootstrap.min.css">
@stop
<!--Llamas a los estilos js -->
@section('js')
<script src="js/select2.min.js"></script>

<!--Estilos del select2-->
<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script>
@stop
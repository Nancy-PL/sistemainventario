@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">ACTIVAR USUARIO</h1>
@stop
@section('content')
<div class="card-header">
    <!-- Mensaje del No. de inventario o No. de control -->
    <div class="row">
        @if($message=Session::get('Listo'))
        <div class="col-12 alert-success alert-dismissable fade show" role="alert">
            <h5>Operacion existosa</h5>
            <span>{{$message}}</span>
        </div>
        @endif
        @if($message=Session::get('UPS'))
        <div class="col-12 alert-danger alert-dismissable fade show" role="alert">
            <h5>Error</h5>
            <span>{{$message}}</span>
        </div>
        @endif
    </div>
</div>
<form action="ActivarUsuario" method="POST">
    <!--token para verificar que el usuario autenticado es quien en realidad está haciendo la petición-->
    @csrf
    <div class="float-left mb-50 mt-2">
    <a href="UsuarioImprimir" class="button btn btn-success">
    <i class="fas fa-file-excel  " aria-hidden="true" ></i>
    </a> 
    </div>
    <!-- Listado de Usuario -->
    <table id="Usuario" class="table table-bordered yajra-datatable">
        <thead class="table-success text-black">

            <tr>
                <th>Activar</th>
                <th scope="col">Profesión</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido Paterno</th>
                <th scope="col">Apellido Materno</th>
                <th scope="col">Nombre de Usuario</th>
                <th scope="col">Área</th>
                <th scope="col">Rol de usuario</th>
                <th scope="col">Fecha de Creación</th>

            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <div class="modal fade" id="create">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Activar este usuario</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>×</span>
                    </button>
                </div>
                <div class="modal-body">
                    ¿Esta seguro que quiere activar este usuario?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-primary" value="Activar">
                </div>
            </div>
        </div>
    </div>
    <a href="submit" class="btn btn-outline-success justify-content-center "  data-toggle="modal" data-target="#create">
        Activar
    </a>


</form>

@stop

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">
@stop

@section('js')

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>

<script>
       // Paginación y estilo de la tabla usuario
       $(function() {
        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            orderCellsTop: true,
            fixedHeader: true,
            "scrollX": true,
            "order": [
                [8, "desc"]
            ],

            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            ajax: "{{ route('get-getUsersA') }}",
            columns: [{
                    data: 'actions',
                    name: 'actions',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'Profesion',
                    name: 'Profesion'
                },
                {
                    data: 'NombreP',
                    name: 'NombreP'
                },
                {
                    data: 'ApellidoP',
                    name: 'ApellidoP'
                },
                {
                    data: 'ApellidoM',
                    name: 'ApellidoM'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'NombreA',
                    name: 'NombreA'
                },
                {
                    data: 'NombreRol',
                    name: 'NombreRol'
                },
                {
                    data: 'created_at',
                    name: 'created_at',
                    "render": function(data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return date.getDate() + "/" +(month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
                    }
                },
               
            ]
        });
        
        //buscar por encabezado
        $(document).ready(function() {
                $('#Usuario thead tr').clone(true).appendTo( '#Usuario thead' );
                $('#Usuario thead tr:eq(1) th').each( function (i) {
                    var title = $(this).text();
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            
                    $( 'input', this ).on( 'keyup change', function () {
                        if ( table.column(i).search() !== this.value ) {
                            table
                                .column(i)
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );  
            } );
    });
</script>
@stop
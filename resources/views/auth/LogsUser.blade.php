@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">Historial de usuario</h1>
@stop
@section('content')
<div class="card-header">
    <!-- Mensaje del No. de inventario o No. de control -->
    <div class="row">
        <!--Historial de movimientos-->
        <h5 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">Movimientos del usuario</h5>
    </div>
</div>
    <!--Select de la lista de personal-->
    <nav class="navbar navbar-light float-center ml-10" style="width: 900px;">
        <form class="form-inline">
            <div class="row justify-content-md-center ">
                <!-- obtener fecha   -->
                    <x-label>Intervalo de fechas: </x-label>
                    <input type="date" onchange="cambia()" class="ml-3" id="FechaInicio"  name="FechaInicio"  tabindex="1" required autofocus /> 
                    <input type="date" class="ml-3" id="FechaFin"  name="FechaFin" tabindex="2" required autofocus />
                    
            <!--buscar logs del dia indicado-->                
            <button class="btn btn-outline-success  float-right ml-3" type="submit">Buscar</button><br>
            </div>
        </form>
    </nav>
<!-- Listado de Usuario -->
<table id="Usuario" class="table table-bordered yajra-datatable">
    <thead class="table-success text-black">

        <tr>
            <th scope="col">Usuario</th>
            <th scope="col">Movimiento</th>
            <th scope="col">Cambios</th>
            <th scope="col">Fecha de Creación</th>
        </tr>
    </thead>
    @foreach($activity as $activity)
    <tr>
        <td>{{$activity->name}}</td>
        <td>{{$activity->description}}</td> 
        <?php /*$obj = json_decode($activity->changes);
              $obj=explode("old", $activity->changes);
              $objeto=end($obj);
              $objeto = preg_replace('/[0-9\@\.\;\" " "{""}:"]+/', '', $objeto);
              $objeto = preg_replace('/[,]+/', '    *    ', $objeto);

       // print $obj->{'attributes'}; // 12345
//        <td>{{$activity->changes}}</td>
        ?>

    <?php if(isset($obj[1])){ 
        $obj=explode("attributes", $obj[1]);
    ?>
        
        <td>{{@$obj[0]}}</td>
    <?php } else { ?>
        <td> </td>
        <?php }*/ ?>
        <td>{{$activity->changes}}</td>
        <td>{{$activity->created_at}}</td>
    </tr>
    @endforeach

    <tbody>
    </tbody>
</table>


@stop
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">
@stop

@section('js')

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>


<script>
    // Paginación y estilo de la tabla bienes
    $(document).ready(function() {
        $('#Usuario').DataTable({
            "lengthMenu": [
                [5, 10, 50, -1],
                [5, 10, 50, "All"]
            ],
            "order": [
                [3, "desc"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }
        });
    });

    

    function cambia() {

        $FechaInicio=document.getElementById("FechaInicio").value;
        document.getElementById("FechaFin").setAttribute("min", $FechaInicio);
    }
   
</script>
@stop
@extends('adminlte::page')

@section('title', 'OSFE')
<!--Aqui Empieza la lista de Clasificación DE BIENES ADMINISTRATIVOS -->
@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">CLASIFICACIÓN DE BIENES</h1>
@stop


@section('content')
<div class="card-header">
</div>

<div class="row justify-content-md-center mt-5 ">
    <a href="ClasificaciónImprimir" class="button btn btn-success" ALIGN="center">
        <i class="fas fa-file-excel  " aria-hidden="true"></i>
    </a>
</div>
<!--se hace la creación de la tabla con los campos correspondientes a Clasificación de bienes -->
<div class="row justify-content-md-center">
    <form action="/Clasificacion" method="POST">
        <!-- Listado de Usuario -->
        <table id="Clasificacion" class="table table-bordered yajra-datatable" style="width:100%">
            <thead class="table-success text-black">

                <tr>
                    <th scope="col">Clave</th>
                    <th scope="col">Concepto</th>
                    <th scope="col">Clase</th>
                    <th scope="col">Depto.</th>
                    <th scope="col">Fecha</th>
                    @can('Clasificacion.edit')
                    <th>Editar</th>
                    @endcan
                    @cannot('Clasificacion.edit')
                    <th></th>
                    @endcan
                </tr>
            </thead>
            <tbody  style="text-transform:uppercase;">

            </tbody>
        </table>
</div>

@stop
<!--Hace referencia a los estilos css -->
@section('css')
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/jquery.dataTables.min.css">
<link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
@stop

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    // Paginación y estilo de la tabla usuario
    $(function() {
        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            "order": [
                [4, "desc"]
            ],

            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            ajax: "{{ route('get-getClasificacionAD') }}",
            columns: [{
                    data: 'clave',
                    name: 'clave'
                },
                {
                    data: 'concepto',
                    name: 'concepto'
                },
                {
                    data: 'NoInventario',
                    name: 'NoInventario',
                    "render": function(data) {

                        if (data == 1) {
                            NoInventario = "No de inventario"
                        } else {
                            NoInventario = "No de control"

                        }

                        return NoInventario;
                    }
                },
                {
                    data: 'Departamento',
                    name: 'Departamento'
                },
                {
                    data: 'created_at',
                    name: 'created_at',
                    "render": function(data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return date.getDate() + "/" + (month.length > 1 ? month : "" + month) + "/" + date.getFullYear();
                    }
                },

                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false,
                    "render": function(data) {
                        '@can("Clasificacion.edit")'
                        return data;
                        '@endcan'
                        '@cannot("Clasificacion.edit")'
                        return "";
                        '@endcan'

                    }

                    
                },
            ]
        });

    });
</script>
@stop
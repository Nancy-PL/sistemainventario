@extends('adminlte::page')

@section('title', 'OSFE')


@section('content_header')
<div class="float-right mb-50">
    <img src="../../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">EDITAR CLASIFICACIÓN</h1>
@stop


@section('content')
<form action="../../Clasificacion/{{$clasificacions->id}}" method="POST" class="was-validated">
@csrf
    @method('PUT')
    <div class="row justify-content-md-center">
        <div class="card w-50">
            <div class="card-header">
                <h1 class="card-title" style="color:#008000">Editar</h1><br>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <p>Corrige los siguientes errores:</p>
                    <ul>
                        @foreach ($errors->all() as $messages)
                        <li>{{ $messages }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>

            <!-- Clave -->
            <div class="row justify-content-md-center">
                <x-label for="Clave" class="mt-3" :value="__('Clave')" />
            </div>
            <div class="row justify-content-md-center">
                <input id="Clave" placeholder="Clave" class="form-control col-sm-5" type="text" name="Clave" style=" text-transform:uppercase;" value="{{$clasificacions->clave}}"    tabindex="2" required autofocus />
            </div>
            <!-- Concepto -->
            <div class="row justify-content-md-center">
                <x-label for="Concepto" class="mt-3" :value="__('Concepto')" />
            </div>
            <div class="row justify-content-md-center">
                <input id="Concepto" placeholder="Concepto" class="form-control col-sm-5" type="text" name="Concepto" style=" text-transform:uppercase;" value="{{$clasificacions->concepto}}"    tabindex="3" required autofocus />
            </div>
            <!--No de Inventario o de control -->
            <div class="row justify-content-md-center">
                <x-label for="ClaveCta" class="mt-3" :value="__('Generar:')" />
            </div>
            <div class=" row justify-content-md-center">
            <select name="NoInventario" class="custom-select col-sm-5 mt-3 mb-3" onChange="ClaveCtaOnChange(this)">
                @foreach ($noInventario as $i)
                    <option value="{{ $i['value'] }}" @if($i['value']==$clasificacions->NoInventario) selected="selected" @endif>{{ $i['name'] }}</option>
                @endforeach
                </select><br>
            </div>
            <!--Departamento-->
            <div class="row justify-content-md-center">
                <x-label for="Departamento" class="mt-3" :value="__('Departamento:')" />
            </div>
            <div class=" row justify-content-md-center">
                <select name="Departamento" class="custom-select col-sm-5 mt-3 mb-3">
                  @foreach ($departamento as $item)
                           <option value="{{ $item['value'] }}" @if($item['value']===$clasificacions->Departamento) selected="selected" @endif>{{ $item['name'] }}</option>
                   @endforeach
                </select>
            </div>
            <div class="row justify-content-md-center">
                <button type="submit" class="btn btn-success mt-3 mb-3 col-sm-3" tabindex="6">Agregar</button>
            </div>

        </div>


    </div>

    </div>
    </div>
</form>
@stop


@section('css')
<link rel="stylesheet" href="../../css/dataTables.bootstrap5.min.css">
<link rel="stylesheet" href="../../css/bootstrap.min.css">

@stop

@section('js')
<script src="{{ asset('js/jquery-3.5.1.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap5.min.js') }}"></script>
@stop

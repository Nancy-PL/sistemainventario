@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">CLASIFICACIÓN DE BIENES</h1>
@stop


@section('content')
<form action="../Clasificacion" method="POST" class="was-validated">
    @csrf
    <div class="row justify-content-md-center">
        <div class="card w-50">
            <div class="card-header">
                <h1 class="card-title" style="color:#008000">Registrar</h1><br>
                <!--Imprimiendo los valores de las validaciones-->
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <p>Corrige los siguientes errores:</p>
                    <ul>
                        @foreach ($errors->all() as $messages)
                        <li>{{ $messages }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>

            <!-- Clave -->
            <div class="row justify-content-md-center">
                <x-label for="Clave" class="mt-3" :value="__('Clave')" />
            </div>
            <div class="row justify-content-md-center">
                <x-input id="Clave" placeholder="Clave" class="form-control col-sm-5" type="text" name="Clave" style=" text-transform:uppercase;" value=""   :value="old('Clave')" tabindex="2" required autofocus />
            </div>
            <!-- Concepto -->
            <div class="row justify-content-md-center">
                <x-label for="Concepto" class="mt-3" :value="__('Concepto')" />
            </div>
            <div class="row justify-content-md-center">
                <x-input id="Concepto" placeholder="Concepto" class="form-control col-sm-5" type="text" name="Concepto" style=" text-transform:uppercase;" value=""   :value="old('Concepto')" tabindex="3" required autofocus />
            </div>
            <!--No de Inventario o de control -->
            <div class="row justify-content-md-center">
                <x-label for="ClaveCta" class="mt-3" :value="__('Generar:')" />
            </div>
            <div class=" row justify-content-md-center">
                <select name="NoInventario" class="custom-select col-sm-5 mt-3 mb-3" onChange="ClaveCtaOnChange(this)">
                    <option value="0">No de Control</option>
                    <option value="1">No de inventario</option>
                </select><br>
            </div>
            <!--Departamento-->
            <div class="row justify-content-md-center">
                <x-label for="Departamento" class="mt-3" :value="__('Departamento:')" />
            </div>
            <div class=" row justify-content-md-center">
                <select name="Departamento" class="custom-select col-sm-5 mt-3 mb-3">
                    <option value="UA" selected>UA</option>
                    <option value="SI">SI</option>
                </select>
            </div>

            <div class="row justify-content-md-center">
                <button type="submit" class="btn btn-outline-success mt-3 mb-3 col-sm-3" tabindex="6">Agregar</button>
            </div>

        </div>

    </div>
    </div>

    </div>
</form>

@stop

@section('css')
<link rel="stylesheet" href="../css/dataTables.bootstrap5.min.css">
<link rel="stylesheet" href="../css/bootstrap.min.css">

@stop

@section('js')
<script src="{{ asset('js/jquery-3.5.1.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap5.min.js') }}"></script>
@stop
@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">LISTADO DE ÁREAS</h1>
@stop


@section('content')

<!-- Listado de  Areas -->
<div class="card-header">
</div>
<div class="float-left mb-50 mt-2">
    <a href="AreaImprimir" class="button btn btn-success">
        <i class="fas fa-file-excel mb-2 " aria-hidden="true"></i>
    </a>
</div>
@can('Areas.create')
<a href="../Areas/create" class="button">
    <i class="fa fa-plus-circle  fa-3x ml-2" style="color:#3A3E3C; "></i>
</a>
@endcan

<table id="area" class="table table-bordered yajra-datatable"  style="text-transform: uppercase;">
    <thead class="table-success text-black">
        <tr>
            <th>Nombre</th>
            <th>Responsable</th>
            <th>Cargo</th>
            <th>Letra</th>
            <th>Fecha</th>
            @can('Areas.edit')
            <th>Editar</th>
            @endcan
            @cannot('Areas.edit')
            <th></th>
            @endcan


        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@stop

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/jquery.dataTables.min.css">
<link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
@stop

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>


<script>
    // Paginación y estilo de la tabla área mediante ajax
    $(function() {
        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            "order": [
                [4, "desc"]
            ],

            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            ajax: "{{ route('get-Area') }}",
            columns: [{
                    data: 'Nombre',
                    name: 'Nombre'
                },
                {
                    data: 'Responsable',
                    name: 'Responsable'
                },
                {
                    data: 'Cargo',
                    name: 'Cargo'
                },
                {
                    data: 'letra',
                    name: 'letra'
                },
                {
                    data: "created_at",
                    name: "created_at",
                    "render": function(data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
                    }


                }, {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false,
                    "render": function(data) {
                        '@can("Areas.edit")'
                        return data;
                        '@endcan'
                        '@cannot("Areas.edit")'
                        return "";
                        '@endcan'

                    }

                },
            ]
        });

    });
</script>
@stop
@extends('adminlte::page')

@section('title', 'OSFE')



@section('content_header')
<div class="float-right mb-50">
    <img src="../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">CATÁLOGO  DE ÁREAS</h1>
@stop


@section('content')
<!-- Dar de Alta Area -->
<form action="../Areas" method="POST">

    <!--token para verificar que el usuario autenticado es quien en realidad está haciendo la petición-->
    @csrf
    <div class="row justify-content-md-center">
        <div class="card w-50">
            <div class="card-header" align="center">
                <h1 class="card-title " style="color:#008000">Registrar área</h1><br>
                <!--Imprimiendo los valores de las validaciones-->
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <p>Corrige los siguientes errores:</p>
                    <ul>
                        @foreach ($errors->all() as $messages)
                        <li>{{ $messages }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
            <!-- Nombre -->
            <div class="row justify-content-md-center">
                <x-label for="NombreA" class="mt-3" :value="__('Nombre')" />
            </div>
            <div class="row justify-content-md-center">
                <x-input id="NombreA" placeholder="Nombre del Área" class="form-control col-sm-5 " type="text" name="NombreA" :value="old('NombreA')" style="text-transform:uppercase;" value=""  tabindex="1" required autofocus />
            </div>
           
            <!-- Responsable -->
            <div class="row justify-content-md-center">
                <x-label for="ResponsableA" class="mt-3" :value="__('Responsable')" />
            </div>
            <div class="row justify-content-md-center">
                <x-input id="ResponsableA" placeholder="Responsable del Área" class="form-control col-sm-5" type="text" name="ResponsableA" :value="old('ResponsableA')" tabindex="2" required style="text-transform:uppercase;" value=""  autofocus />
            </div>
            <!-- Cargo -->
            <div class=" row justify-content-md-center">
                <x-label for="CargoA" class="mt-3" :value="__('Cargo')" />
            </div>
            <div class="row justify-content-md-center">
                <x-input id="CargoA" placeholder="Cargo del Área" class="form-control col-sm-5" type="text" name="CargoA" :value="old('CargoA')" style="text-transform:uppercase;" value=""  tabindex="3" required autofocus />
            </div>
            <!-- Letra -->
            <div class=" row justify-content-md-center">
                <x-label for="LetraA" class="mt-3" :value="__('Letra')" />
            </div>
            <div class="row justify-content-md-center">
                <x-input id="LetraA" placeholder="Letra del Área" class="form-control col-sm-5 mb-3" type="text" name="LetraA" :value="old('LetraA')" style="text-transform:uppercase;" value=""  tabindex="4" required autofocus />
            </div>
            <!-- Guardar -->
            <div class="row justify-content-md-center">
                <button type="submit" class="btn btn-success mt-3 mb-3 col-sm-3" tabindex="5">Guardar</button>
            </div>
        </div>
    </div>

</form>
@stop


@section('css') 
   <link rel="stylesheet" href="../css/bootstrap.min.css">
@stop

    
@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="../../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">EDITAR ÁREA</h1>
@stop

@section('content')
<!-- Editar Area -->
<form action="../{{$Area->id}}" method="POST" class=" was-validated">
    <!--token para verificar que el usuario autenticado es quien en realidad está haciendo la petición-->
    @csrf
    <!--Establece la clave y el valor dados en la colección-->
    @method('PUT')
    <div class="row justify-content-md-center">
        <div class="card w-50">
            <div class="card-header">
                <h1 class="card-title" style="color:#008000">Editar Area</h1><br>
                <!--Validación de datos-->
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <p>Corrige los siguientes errores:</p>
                    <ul>
                        @foreach ($errors->all() as $messages)
                        <li>{{ $messages }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
            <!-- Nombre -->
            <div class="row justify-content-md-center">
                <x-label for="NombreA" class="mt-3" :value="__('Nombre')" />
            </div>
            <div class="row justify-content-md-center">
                <input id="NombreA" placeholder="Nombre del Area" class="form-control col-sm-8" type="text" name="NombreA" :value="old('NombreA')" style="text-transform:uppercase;" value='{{$Area->Nombre}} '  tabindex="1" required autofocus />
            </div>
            <!-- Responsable -->
            <div class="row justify-content-md-center">
                <x-label for="ResponsableA" class="mt-3" :value="__('Responsable')" />
            </div>
            <div class="row justify-content-md-center">
                <input id="ResponsableA" placeholder="Responsable del Area" class="form-control col-sm-8" type="text" name="ResponsableA" :value="old('ResponsableA')" style="text-transform:uppercase;" value="{{$Area->Responsable}}"   tabindex="2" required autofocus />
            </div>
            <!-- Cargo -->
            <div class=" row justify-content-md-center">
                <x-label for="CargoA" class="mt-3" :value="__('Cargo')" />
            </div>
            <div class="row justify-content-md-center">
                <input id="CargoA" placeholder="Cargo del Area" class="form-control col-sm-8" type="text" name="CargoA" :value="old('CargoA')" style="text-transform:uppercase;" value="{{$Area->Cargo}}"   tabindex="3" required autofocus />
            </div>
            <!-- Letra -->
            <div class=" row justify-content-md-center">
                <x-label for="LetraA" class="mt-3" :value="__('Letra')" />
            </div>
            <div class="row justify-content-md-center">
                <input id="LetraA" placeholder="Letra del Area" class="form-control col-sm-8 mb-3" type="text" name="LetraA" :value="old('LetraA')" style="text-transform:uppercase;" value="{{$Area->letra}}"   tabindex="3" required autofocus />
            </div>
            <!-- Guardar cambios -->
            <div class="row justify-content-md-center">
                <button type="submit" class="btn btn-success mt-3 mb-3 col-sm-3" tabindex="4">Guardar</button>
            </div>
        </div>
    </div>

</form>
@stop

@section('css')
<link rel="stylesheet" href="../../css/bootstrap.min.css">


@stop

@section('js')
@stop
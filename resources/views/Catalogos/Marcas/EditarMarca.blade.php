@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="../../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center" style="color:#008000">Editar Marca</h1>
<!--Imprimiendo errores de la Validación de datos-->
@if (count($errors) > 0)
<div class="alert alert-danger">
    <p>Corrige los siguientes errores:</p>
    <ul>
        @foreach ($errors->all() as $messages)
        <li>{{ $messages }}</li>
        @endforeach
    </ul>
</div>
@endif
@stop

@section('content')
<form action="../../Marcas/{{$marcas->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="row justify-content-md-center mt-5">
        <label for="form-label" class="mr-5">Nombre</label>
        <input id="Marca" name="Marca" type="text" style="text-transform:toUpperCase" class="form-control col-sm-5" value="{{$marcas->nombre}}"   tabindex="1" required autofocus>
    </div>
    <div class="row justify-content-md-center mt-5">
        <a href="../../Marcas" class="btn btn-secondary mr-3" tabindex="2">Cancelar</a>
        <button type="submit" class="btn btn-success" tabidex="3">Guardar</button>
    </div>
</form>
@stop

@section('css')
<link rel="stylesheet" href="../../css/bootstrap.min.css">

@stop

@section('js')
@stop
@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center" style="color:#008000">CATÁLOGOS DE MARCAS</h1>
@stop

@section('content')

<!-- Alta de marcas -->
<form action="Marcas" method="POST">
  @csrf


  <div class="card-header">

    <!--Imprimiendo los valores de las validaciones-->
    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <p>Corrige los siguientes errores:</p>
      <ul>
        @foreach ($errors->all() as $messages)
        <li>{{ $messages }}</li>
        @endforeach
      </ul>
    </div>
    @endif
  </div>

  <!-- Alta de marcas -->
  <div class="form-group row justify-content-md-center">
    <button type="button" class="btn btn-success mt-3 mb-3 col-sm-3" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Agregar Nueva Marca</button>
  </div>

  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">


          <h5 class="modal-title" id="exampleModalLabel">Nueva Marca</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group row justify-content-md-center">
              <label for="Marca" class="col-form-label" :value="__('Nombre')">Marca</label>
            </div>
            <div class="form-group row justify-content-md-center">
              <x-input id="Marca" placeholder="Nombre de la marca" class="form-control col-sm-5" type="text" name="Marca" style=" text-transform:toUpperCase" value=""   :value="old('NombreM')" tabindex="1" required autofocus />
            </div>

          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-success mt-3 mb-3 col-sm-3" tabindex="4">Agregar</button>

        </div>
      </div>
    </div>
  </div>
  <div class="row justify-content-md-center mt-5 ">
    <h5 style="color:#008000">LISTADO DE MARCAS</h5>
  </div>
  <div class="row justify-content-md-center">
    <table id="marcas" class="table table-striped table-bordered shadow-lg display nowrap mt-4" style="width:50%" style="color:#008000">
      <thead class="table-success text-black">
        <tr>
          <th scope="col">Nombre de la Marca</th>
          <th scope="col">Editar</th>
        </tr>
      </thead>
      <tbody>
        <!-- Imprimir mediante la consulta en la tabla marca desde la bd-->
        @foreach ($marcas as $Marca)
        <tr>
          <td>{{$Marca->nombre}}</td>
          <td>


            <a href="Marcas/{{$Marca->id}}/edit" class="button row justify-content-md-center">
              <i class="fa  fa-edit  fa-2x" aria-hidden="true" style="color:#238752; "></i>

            </a>
            @csrf
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>

    @stop

    @section('css')
    <link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/dataTables.bootstrap5.min.css">
    @stop

    @section('js')
    <script src="{{ asset('js/jquery-3.5.1.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
 
    <script>
      //Estilo de la tabla área
      $(document).ready(function() {
        $('#marcas').DataTable({
          "scrollX": true,
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
          }


        });

      });


      $('#exampleModal').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body input').val(recipient)
      })
    </script>
    @stop
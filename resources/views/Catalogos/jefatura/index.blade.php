@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">LISTADO DE JEFATURAS
</h1>
@stop


@section('content')

<!-- Listado de  Areas -->
<div class="card-header">
</div>
<div class="float-left mb-50 mt-2">

</div>
@can('Areas.create')
<a href="jefatura/create" class="button">
    <i class="fa fa-plus-circle  fa-3x ml-2" style="color:#3A3E3C; "></i>
</a>
@endcan
<div style="overflow-x:scroll;600px;">
    <table class="table table-bordered yajra-datatable" style="text-transform: uppercase;">
        <thead class="table-success text-black">
            <tr>
                <th>Nombre</th>
                <th>Nombre del personal</th>
                <th>Área</th>
                <th>Fecha de alta</th>
                <th>Editar</th>

            </tr>
        </thead>
        <tbody>
        </tbody>
    </stable>
</div>
@stop

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/jquery.dataTables.min.css">
<link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
@stop

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>


<script>
// Paginación y estilo de la tabla área mediante ajax
$(function() {
    var table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        // scrollX: true,

        "order": [
            [3, "desc"]
        ],

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        ajax: "{{ route('get-jefatura') }}",
        columns: [{
                data: 'Nombre',
                name: 'Nombre'
            },
            {
                data: 'NombreP',
                name: 'NombreP'
            },
            {
                data: 'NombreA',
                name: 'NombreA'
            },
            {
                data: "Fecha_Captura",
                name: "Fecha_Captura",
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
                "render": function(data) {
                    return data;

                }

            },

        ]
    });

});
</script>
@stop
@extends('adminlte::page')

@section('title', 'OSFE')



@section('content_header')
<div class="float-right mb-50">
    <img src="../../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">Jefaturas</h1>
@stop


@section('content')
<div class="row justify-content-md-center">
    <div class="card w-50">
        <div class="card-header" align="center">
            <h1 class="card-title " style="color:#008000">Editar</h1><br>
            <!--Imprimiendo los valores de las validaciones-->
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <p>Corrige los siguientes errores:</p>
                <ul>
                    @foreach ($errors->all() as $messages)
                    <li>{{ $messages }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>

        <!-- Editar un Control_Ip -->
        <form action="../{{$jefatura->id}}" method="POST" class=" was-validated">
            <!--token para verificar que el usuario autenticado es quien en realidad está haciendo la petición-->
            @csrf
            <!--Establece la clave y el valor dados en la colección-->
            @method('PUT')
            <!--token para verificar que el usuario autenticado es quien en realidad está haciendo la petición-->
            @csrf
            <!-- área -->
            <div class="row justify-content-md-center">
                <x-label class="mt-3" :value="__('Área')" />
            </div>
            <div class="row justify-content-md-center ">
                <select class="js-example-basic-single" name="area" id="area" 
                    required autofocus>
                    <option value="" selected>Seleccionar Área</option>
                    @foreach ($areas as $area)
                    <option value="{{$area->id}}" @if($area->id==$jefatura->idArea) selected="selected"
                        @endif>{{$area->Nombre}}</option>
                    @endforeach
                </select>
            </div>
            <!-- Nombre -->
            <div class="row justify-content-md-center">
                <x-label class="mt-3" :value="__('Personal')" />
            </div>
            <div class="row justify-content-md-center">
                <select style="text-transform:uppercase;" class="custom-select js-example-basic-single col-md-5"
                    id="personas_id" :value="old('personas_id')" name="personas_id" tabindex="2" required autofocus>
                    <option style="text-transform:uppercase;" value="" data-codigo="" selected>Seleccionar nombre del
                        personal</option>
                    @foreach ($personas as $personas)
                    <option style="text-transform:uppercase;" value="{{$personas->id}}"
                        data-codigo="{{$personas->area_id}}" @if($personas->id==$jefatura->idPersona)
                        selected="selected" @endif>{{$personas->Profesion}} {{$personas->Nombre}}
                        {{$personas->ApellidoP}}
                        {{$personas->ApellidoM}}</option>
                    @endforeach
                </select>
            </div>
               <!-- Nombre -->
               <div class="row justify-content-md-center">
                <x-label  class="mt-3" :value="__('Nombre')" />
            </div>
            <div class="row justify-content-md-center">
                <input id="ip" placeholder="Nombre de la jefatura " class="form-control col-sm-5 " type="text" name="Nombre"
                    :value="old('Nombre')" style="text-transform:uppercase;" value="{{$jefatura->Nombre}}" tabindex="3" maxlength="140" required
                    autofocus />
            </div>
            <!-- Guardar -->
            <div class="row justify-content-md-center">
                <button type="submit" class="btn btn-success mt-3 mb-3 col-sm-3" tabindex="7">Guardar</button>
            </div>
        </form>
    </div>
</div>
@stop


@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="../../css/bootstrap.min.css">
<link href="../../css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="../../css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="../../css/sweetalert2.min.css">
@stop

@section('js')
<script src="../../js/sweetalert2.min.js"></script>
<script src="../../js/jquery.dataTables.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/dataTables.bootstrap4.min.js"></script>
<script src="../../js/jquery.validate.js"></script>
<script src="../../js/select2.min.js"></script>
<script>
// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {    
    $('.js-example-basic-single').select2({
        theme: "classic"
    });
});

</script>

@stop
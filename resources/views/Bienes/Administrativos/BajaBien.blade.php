@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">BAJA DEL BIEN MUEBLE</h1>
@stop

@section('content')

<form action="BajaBien" method="POST" id="bien" class=" was-validated">
    @csrf
    <div class="card-header">
        <!--Imprimiendo errores de la Validaciones de los campos--->
        <!--Imprimiendo los valores de las validaciones-->
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $messages)
                <li>{{ $messages }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <!-- Mensaje de la validacion -->
        <div class="row">
        @if($message=Session::get('UPS'))
    <div class="col-12 alert-success alert-dismissable fade show" role="alert">
        <h5>Operación correcta pero algunos bienes no se dieron de baja</h5>
        <span>{{$message}}</span>

    </div>
    @endif
    @if($message=Session::get('listo'))
    <div class="col-12 alert-success alert-dismissable fade show" role="alert">
        <h5>Operación realizada con éxito</h5>
        <span>{{$message}}</span>

    </div>
    @endif
        </div>
    </div>
   <!--Bienes seleccionados-->
   <input class="bienes" type="hidden" id="bienes" name="bienes" value="">
    <table  id="bienesT" class="table table-bordered yajra-datatable" style="width:100%">
        <thead class="table-success text-black">

            <tr>
                <th>Baja</th>
                <th scope="col">Profesión</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido Paterno</th>
                <th scope="col">Apellido Materno</th>
                <th scope="col">Clasificación</th>
                <th scope="col">No.de Inventario Actual</th>
                <th scope="col">No. de Inventario Anterior</th>
                <th scope="col">Marca</th>
                <th scope="col">Modelo</th>
                <th scope="col">No. de serie </th>
                <th scope="col">No. de Folio Fiscal</th>
                <th scope="col">Estado</th>
                <th scope="col">Ubicación</th>
                <th scope="col">Características</th>
                <th scope="col">Observaciones</th>
                <th scope="col">Fecha de Asignación</th>

            </tr>
        </thead>
        <tbody style="text-transform:uppercase;">

        </tbody>
    </table>
    <!-- Motivo de la baja  -->
    <div class="row justify-content-md-center">
        <div class=" w-100" type="hidden">
            <div class="row justify-content-md-center">
                <x-label for="Motivo" class="mt-3" :value="__('Motivo de la baja')" />
            </div>
            <div class="row justify-content-md-center">
                <x-input id="Motivo" placeholder="Motivo de la baja" class="form-control col-sm-5 " type="text" name="Motivo" :value="old('Motivo')" style="text-transform:uppercase;" value="" onkeyup="javascript:this.value=this.value.toUpperCase();" tabindex="1" required autofocus />
            </div>

            <!--Visualizando Observaciones-->
            <div class="row justify-content-md-center">
                <x-label for="observaciones" class="mt-3" :value="__('Observaciones')" />
            </div>
            <div class="row justify-content-md-center">
                <textarea class="form-control col-sm-5 mb-3" placeholder="Observaciones" name="Observaciones" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" tabindex="2" required></textarea>
            </div>
            <!--MODAL PARA BAJA DEL BIEN -->
            <div class="modal fade" id="create">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4>Dar de baja</h4>
                            <button type="button" class="close" data-dismiss="modal">
                                <span>×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            ¿Esta seguro que quiere dar de baja a este bien?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <input type="submit" class="btn btn-primary" value="Dar de baja">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <a href="submit" class="btn btn-outline-danger BajaBien" data-toggle="modal" data-target="#create">
                    Dar de baja
                </a>
            </div>
        </div>
    </div>
    @stop

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">

<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">
@stop

@section('js')
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="js/jquery.validate.js"></script>
    <script>
        // Paginación y estilo de la tabla área mediante ajax
        $(function() {
            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                orderCellsTop: true,
                fixedHeader: true,
                "scrollX": true,                
                "order": [
                    [16, "desc"]
                ],

                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                },
                ajax: "{{ route('get-getBajaBienes') }}",
                columns: [{
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'Profesion',
                        name: 'Profesion'
                    },
                    {
                        data: 'Nombre',
                        name: 'Nombre'
                    },
                    {
                        data: 'ApellidoP',
                        name: 'ApellidoP'
                    },

                    {
                        data: 'ApellidoM',
                        name: 'ApellidoM'
                    },
                    {
                        data: 'concepto',
                        name: 'concepto'
                    },
                    {
                        data: 'ClaveBien',
                        name: 'ClaveBien'
                    },
                    {
                        data: 'NoInventarioAnterior',
                        name: 'NoInventarioAnterior'
                    },

                    {
                        data: 'nombre',
                        name: 'nombre'
                    },
                    {
                        data: 'Modelo',
                        name: 'Modelo'
                    },
                    {
                        data: 'NoSerie',
                        name: 'NoSerie'
                    },
                    {
                        data: 'factura',
                        name: 'factura'
                    },

                    {
                        data: 'Estado',
                        name: 'Estado'
                    },
                    {
                        data: 'ubicacion',
                        name: 'ubicacion'
                    },
                    {
                        data: 'Caracteristicas',
                        name: 'Caracteristicas'
                    },
                    {
                        data: 'Observaciones',
                        name: 'Observaciones'
                    },
                    {
                        data: "fechaAsignacion",
                        name: "fechaAsignacion",
                        "render": function(data) {
                            var date = new Date(data);
                            var month = date.getMonth() + 1;
                            return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
                        }
                    },
                ]
            });

            //buscar por encabezado
            $(document).ready(function() {
                $('#bienesT thead tr').clone(true).appendTo( '#bienesT thead' );
                $('#bienesT thead tr:eq(1) th').each( function (i) {
                    var title = $(this).text();
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            
                    $( 'input', this ).on( 'keyup change', function () {
                        if ( table.column(i).search() !== this.value ) {
                            table
                                .column(i)
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );  
            } );
        });
        
    
    var bienes = [];
    $(document).on('change', '.checkboxClass', function() {
        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz
            bienes[bienes.length] = $(this).val();
        } else {
            // Si no está marcado, elimine el valor de la matriz
            var index = bienes.indexOf($(this).val());
            if (index > -1) {
                bienes.splice(index, 1);
            }
        }
    });

    $(document).on('change', '.header-checkbox', function() {

        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz           
            $('.checkboxClass').each(function() {
                bienes[bienes.length] = $(this).val();
            });
        } else {
            // Si no está marcado, elimine el valor de la matriz
            $('.checkboxClass').each(function() {
                var index = bienes.indexOf($(this).val());
                if (index > -1) {
                    bienes.splice(index, 1);
                }
            });
        }
    });

    // al dar click en submit asignar los valores del array se envian al formulario
    $('.BajaBien').click(function() {
        var bien = '';
        // leer bienes usando for loop
       // alert(bienes);
        document.getElementById("bienes").value = bienes;
    });
    </script>
    @stop
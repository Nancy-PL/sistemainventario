@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">HISTORIAL DE BIENES MUEBLES</h1>@stop

@section('content')

<div class="form-row  justify-content-md-center ml-8 ">

    <nav class="navbar navbar-light float-center" style="width: 900px;">
        <form class="form-inline">
        <div class="row justify-content-center ">   
        <input id="NoSerie" placeholder="No. Inventario o No. control" class="form-control" type="text" name="Noserie" style=" text-transform:toUpperCase"  onkeyup="javascript:this.value=this.value.toUpperCase(); " :value="old('NombreM')" tabindex="1" required autofocus />       
                    <!--buscar el resguardo de la persona-->
                    <button class="btn btn-outline-success  float-right ml-5" type="submit">Buscar</button><br>                
            </div>
        </form>
    </nav>
</div>
<table id="bienes" class="table table-bordered  yajra-datatable ">
    <thead class="table-success text-black">
        <tr>
        <th scope="col">Status</th>
        <th scope="col">Clasificación</th>
            <th scope="col">No. Serie</th>
            <th scope="col">No. Inventario anterior</th>
            <th scope="col">No. Inventario actual</th>
            <th scope="col">Usuario alta</th>
            <th scope="col">Fecha alta</th>
            <th scope="col">Usuario baja</th>
            <th scope="col">Fecha baja</th>
            <th scope="col">Resguardante</th>
            <th scope="col">Fecha asignación</th>
            <th scope="col">Fecha de entrega</th>

        </tr>
    </thead>
    <tbody style="text-transform:uppercase;">

    @foreach ($Bienes as $Resguardo)
            @if($Resguardo->concepto)
            @if($Resguardo->Departamento=="UA")

            <tr>
            @if($Resguardo->ActivoR==1 && $Resguardo->ActivoB==1)
            <td><span class="badge badge-success">Activo</span></td>
            @else
            <td><span class="badge badge-danger">Inactivo</span></td>
            @endif
        <td>{{ $Resguardo->concepto}}</td>
        <td>{{ $Resguardo->NoSerie}}</td>
        <td>{{ $Resguardo->NoInventarioAnterior}}</td>
        <td>{{ $Resguardo->ClaveBien}}</td>
        <td>{{ $Resguardo->name}}</td>
        <td>{{ $Resguardo->fecha_alta}}</td>
        <td>{{ $Resguardo->userBaja}}</td>
        <td>{{ $Resguardo->fecha_baja}}</td>
        <td>{{ $Resguardo->Nombre}} {{ $Resguardo->ApellidoP}} {{ $Resguardo->ApellidoM}}</td>
        <td>{{ $Resguardo->fechaAsignacion}}</td>
        <td>{{ $Resguardo->fechaCancelacion}}</td>

        </tr>
        @endif

        @endif
        @endforeach

    </tbody>
</table>
@stop

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">

<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">
@stop

@section('js')
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script>
    $(document).ready(function() {
        var table = $('#bienes').DataTable({
                "scrollX": true, 
            "lengthMenu": [
                [5, 10, 50, -1],
                [5, 10, 50, "All"]
            ],
            order: [
                [0, "asc"]
            ],

            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }

        });
    });

</script>
@stop
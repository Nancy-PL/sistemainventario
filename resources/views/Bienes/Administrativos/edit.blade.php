@extends('adminlte::page')

@section('title', 'Sistema Web')

@section('content_header')
<div class="float-right mb-50">
    <img src="../../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">EDICIÓN DE UN BIEN</h1>
@stop


@section('content')
<!-- Editar de bienes Administrativos -->

<form action="../../Bienes/{{$id}}" method="POST" class=" was-validated">
    <!--token para verificar que el usuario autenticado es quien en realidad está haciendo la petición-->
    @csrf
    @csrf
    <!--Establece la clave y el valor dados en la colección-->
    @method('PUT')
    <div class="card-header">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <p>Corrige los siguientes errores:</p>
            <ul>
                @foreach ($errors->all() as $messages)
                <li>{{ $messages }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
    </div>
        <!--Llamas la consulta  la bd para requerir los datos del Clasificacion del bien -->
        <div class="form-row">
        <!-- Ubicacion  -->
        <div class="col-md-3 mb-3 ml-5">
            <x-label for="Ubicacion" class="mt-3" :value="__('Ubicación')" />
            <input id="Ubicacion" placeholder="Ubicacion" class="form-control" type="text" name="Ubicacion"  style="text-transform:uppercase;"   tabindex="2" required autofocus
            
         @if($detalle_biens)
            value="{{$detalle_biens->ubicacion}}"  />
        @else
            value=""  />
        @endif
        </div>

        <!-- Modelo -->
        <div class="col-md-2 mb-3">
            <x-label for="Modelo" class="mt-3" :value="__('Modelo')" />
            <input id="Modelo" placeholder=" Ingrese Modelo de" class="form-control" type="text" name="Modelo" :value="old('Modelo')" style="text-transform:uppercase;" value="{{$bienes->Modelo}}"  tabindex="3" required autofocus />
        </div>
        <!-- No de serie -->
        <div class="col-md-3 mb-3">

            <x-label for="NoSerie" class="mt-3" :value="__('No. de Serie ')" />
            <input id="NoSerie" placeholder=" Ingrese el No. de Serie" class="form-control" type="text" name="NoSerie" :value="old('NoSerie')" style="text-transform:uppercase;"  tabindex="4"
            @if($detalle_biens)
                value="{{$detalle_biens->NoSerie}}" 
            @else
                value="" 
            @endif

            />
        </div>
        <!-- No. de folio fiscal  -->
        <div class="col-md-3 mb-3">
            <x-label for="factura" class="mt-3" :value="__('No. de Folio Fiscal')" />
            <input id="factura" placeholder="No. de folio fiscal" class="form-control" type="text" name="factura"  style="text-transform:uppercase;"  tabindex="5"  required autofocus
            @if($detalle_biens)
                value="{{$detalle_biens->factura}}"  
            @else
                value="" 
            @endif

            />
        </div>
        <!-- No. de Inventario anterior  -->
        <div class="col-md-3 mb-3 ml-5">
            <x-label for="NoInventarioAnterior"  class="mt-3" :value="__('Numero de Inventario Anterior')" />
            <input id="NoInventarioAnterior" placeholder="No. de Inventario" class="form-control" type="text" name="NoInventarioAnterior"   style="text-transform:uppercase;"  tabindex="6"
        @if($historial__inventarios)
         value=" {{$historial__inventarios->NoInventarioAnterior}}"  
        @else
       value=""  
        @endif
        />
        </div>

        <!--Marca-->
        <div class="col-md-2 mb-3">
            <x-label for="Marca" class="mt-3" :value="__('Marca')" />
            <div class="form-group">
                <select id="marca_id" name="marca_id" class="custom-select" tabindex="7" required>
                    <option value="">SELECCIONA MARCA</option>
                    @foreach ($marcas as $marca)
                    <option value="{{$marca->id}}" @if($marca->id == $bienes->marca_id  ) selected @endif>{{$marca->nombre}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <!--Estado-->
        <div class="col-md-2 mb-3">
            <x-label for="Estado" class="mt-3" :value="__('Estado')" />
            <div class="form-group">
                <select class="custom-select js-example-basic-single " name="Estado" tabindex="8"  required>
                <option value="BUENO"@if("BUENO"== $bienes->Estado ) selected @endif>BUENO</option>
                    <option value="REGULAR"@if("REGULAR" == $bienes->Estado ) selected @endif>REGULAR</option>
                    <option value="MALO"@if("MALO" == $bienes->Estado ) selected @endif>MALO </option>
  
                </select>
            </div>
        </div>
    </div>
    
    <div class="form-row">
    <!-- Mejoras al equipo  -->
    <div class="col-md-3 mb-3  ml-5">
            <x-label for="MejorasEquipo" class="mt-3" :value="__('Mejoras al equipo')" />
            <textarea class="form-control" name="MejorasEquipo"  style="text-transform:uppercase;"   tabindex="9"  >@if($mejoras_equipos){{$mejoras_equipos->MejorasEquipo}}@endif</textarea>
        </div>
        <div class="col-md-4  mb-3 ml-5">
            <x-label for="caracteristicas" class="mt-3" :value="__('Características')" />
            <textarea class="form-control" name="Caracteristicas"  style="text-transform:uppercase;"   tabindex="10"  >{{ $bienes->Caracteristicas}}</textarea>
        </div>
        <!--Visualizando Observaciones-->

        <div class="col-md-4  mb-3">
            <x-label for="observaciones" class="mt-3" :value="__('Observaciones')" />
            <textarea class="form-control" name="Observaciones"   style="text-transform:uppercase;"  tabindex="11"  >{{$bienes->Observaciones}}</textarea>
        </div>

    </div>
    <!--Visualizando No. de Inventario Nuevo-->
    <div id="NumeroInventario" class=" row justify-content-md-center  mt-3 mr-3">
        <x-label type="hidden" for="NumeroInventario" :value="__('Clave: ')" />
    </div>
    <div id="NumeroInventarioActual" class=" row justify-content-md-center">
    <input  class="form-control col-md-3 mb-3" for="NumeroInventarioActual"  value="{{$bienes->ClaveBien}}" disabled/>
    </div>



    <!--Guardar-->
    <div class="row justify-content-md-center">
    <a href="/BienesUtic/" class="btn btn-dark mt-3 mb-3 col-sm-3 mr-3">Regresar</a>
        <button type="submit" class="btn btn-success mt-3 mb-3 col-sm-3" tabindex="11">Editar</button>
    </div>
</form>
@stop

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="../../css/bootstrap.min.css">
@stop

@section('js')
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/jquery.validate.js"></script>
@stop

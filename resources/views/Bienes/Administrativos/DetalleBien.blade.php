@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">DETALLE DEL BIEN</h1>
@stop

@section('content')
<a href="../Bienes" class="button">
    <i class="fa  fa-reply  fa-2x" aria-hidden="true" style="color:#238752; "></i>
</a>

<CENTER>

        <TABLE id="bienes" class="table table-bordered"  style="width:50%">

            <thead class="thead-align-items-end"">
                <tr align=left>    
                <TH>DATOS</TH>  <TD>CONCEPTO</TD>
                </=left>

                <TR ALIGN=left> 
                <TH>Clasificación</TH>  @foreach ($bienes as $bienes)<td>{{$bienes->concepto}}</td>
                @if($bienes->Status == 1)
                    @php $valor = 'Ocupado'; @endphp
                    @else
                    @php $valor = 'Disponible'; @endphp
                    @endif

                <TR ALIGN=left>   
                <TH>No. de Serie</TH> <td>{{$bienes->NoSerie}}</td>
                </TR>

                <TR ALIGN=left>   
                <TH>No. de Inventario </TH><td>{{$bienes->NoInventarioAnterior}}</td>
                </TR>

                <TR ALIGN=left>   
                <TH>No. de Inventario Actual</TH><td>{{$bienes->ClaveBien}}</td>
                </TR>
            
                <TR ALIGN=left>    
                <TH>Modelo</TH> <td>{{$bienes->Modelo}}</td>
                </TR>

                <TR ALIGN=left>  
                <TH>Marca</TH><td>{{$bienes->nombre}}</td>
                </TR>

                <TR ALIGN=left>    
                <TH>Estado</TH> <td>{{$bienes->Estado}}</td>
                </TR>

                <TR ALIGN=left>  
                <TH>Mejoras del Equipo</TH><td>{{$bienes->MejorasEquipo}}</td>
                </TR>

                <TR ALIGN=left>  
                <TH>Características</TH><td>{{$bienes->Caracteristicas}}</td>
                </TR>

                <TR ALIGN=left>  
                <TH>Observaciones</TH> <td>{{$bienes->Observaciones}}</td>
                </TR>
                <TR ALIGN=left>   
                <TH>Disponibilidad</TH><td>{{$valor}}</td>
                </TR>

                <TR ALIGN=left>    
                <TH>Fecha de Creación</TH> <td>{{$bienes->created_at}}</td>
                </TR>

                <TR ALIGN=left>   
                <TH>Usuario Alta</TH><td>
                @if($bienes->name)
                    {{$bienes->name}}
                @else
                    {{$bienes->usuario_alta}}
                @endif
                </td>
                @break

                @endforeach
                </TR>
            </thead>
        </TABLE>
</CENTER>
                   <!--Guardar-->
    <div class=" row justify-content-md-center">
                <a href="../Bienes/" class="btn btn-dark mt-3 mb-3 col-sm-3 mr-3">Regresar</a>
                </div>


    @stop

  
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link href="../css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <link href="../css/dataTables.bootstrap4.min.css" rel="stylesheet">
@stop

    
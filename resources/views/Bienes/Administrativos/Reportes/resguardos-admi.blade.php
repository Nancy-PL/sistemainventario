<table>
    <thead>
        <tr>
            <th scope="col">Área</th>
            <th scope="col">Resguardante</th>
            <th scope="col">No. Inventario Actual</th>
            <th scope="col">No. Inventario Anterior</th>
            <th scope="col">Clasificación</th>
            <th scope="col">Marca</th>
            <th scope="col">Modelo</th>
            <th scope="col">No. de Serie </th>
            <th scope="col">No.Folio Fiscal</th>
            <th scope="col">Estado</th>
            <th scope="col">Ubicación</th>
            <th scope="col">Características</th>
            <th scope="col">Observaciones</th>
            <th scope="col">Mejoras al equipo</th>
            <th scope="col">Fecha de Asignación</th>
            

            
        </tr>
    </thead>
    <tbody>
             @foreach ($Resguardo as $resguardos)
            <tr>
            <td>{{$resguardos->NombreA}}</td>
            <td>{{$resguardos->Profesion}} {{$resguardos->Nombre}} {{$resguardos->ApellidoP}} {{$resguardos->ApellidoM}}</td>
            <td>{{$resguardos->ClaveBien}}</td>
            <td>{{$resguardos->NoInventarioAnterior}}</td>
            <td>{{$resguardos->concepto}}</td>
            <td>{{$resguardos->nombre}}</td>
            <td>{{$resguardos->Modelo}}</td>
            <td>{{$resguardos->NoSerie}}</td>
            <td>{{$resguardos->factura}}</td>
            <td>{{$resguardos->Estado}}</td>
            <td>{{$resguardos->ubicacion}}</td>
            <td>{{$resguardos->Caracteristicas}}</td>
            <td>{{$resguardos->Observaciones}}</td>
            <td>{{$resguardos->MejorasEquipo}}</td>
            <td>{{$resguardos->fechaAsignacion}}</td>
            
            </tr>
            @endforeach
    </tbody>
</table>
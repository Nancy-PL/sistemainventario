<table>
        <thead>
           <tr>
                <th scope="col">Concepto</th>
                <th scope="col">Clave</th>
                <th scope="col">Clase</th>
                <th scope="col">Depto</th>
                <th scope="col">Fecha</th>
               
            </tr>
        </thead>
        <tbody>
           <!--se crea un foreach para llamar los campos que se obtienen de la base de datos -->
           @foreach ($clasificacions as $clasificacions)
            @if($clasificacions->NoInventario == 1)
                    @php $valor = 'No. de Inventario'; @endphp
                    @else
                    @php $valor = 'No. de Control'; @endphp
                    @endif
            <tr>
                <td>{{$clasificacions->concepto}}</td>
                <td>{{$clasificacions->clave}}</td>
                <td>{{$valor}}</td>
                <td>{{$clasificacions->Departamento}}</td>
                <td>{{$clasificacions->created_at}}</td>
                
            </tr>
            @endforeach
        </tbody>
</table>
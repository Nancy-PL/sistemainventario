<table>
    <thead>
        <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Responsable</th>
            <th scope="col">Cargo</th>
            <th scope="col">Letra</th>
            <th scope="col">Fecha</th>
        </tr>
    </thead>
    <tbody>
        <!-- Imprimir mediante la consulta en la tabla área desde la bd-->
        @foreach ($areas as $areas)
        <tr>
            <td>{{$areas->Nombre}}</td>
            <td>{{$areas->Responsable}}</td>
            <td>{{$areas->Cargo}}</td>
            <td>{{$areas->letra}}</td>
            <td>{{$areas->created_at}}</td>
            
        </tr>
        @endforeach
    </tbody>
</table>
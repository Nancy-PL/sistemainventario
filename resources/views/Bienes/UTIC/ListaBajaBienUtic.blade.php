@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">LISTA DE EQUIPO DE CÓMPUTO Y ACCESORIOS</h1>
@stop

@section('content')
<form action="bienbajaUtic" method="POST" id="bien">
    <div class="card-header">
    </div>
 

   
    <table id="bienes" class="table table-bordered  yajra-datatable" style="width:100%">
        <thead class="table-success text-black">
            <tr>
                <th scope="col">Fecha Baja</th>
                <th scope="col">Usuario baja</th>
                <th scope="col">Fecha de Creación</th>
                <th scope="col">Usuario alta</th>
                <th scope="col">Clasificación</th>
                <th scope="col">No. de serie </th>
                <th scope="col">No. de Folio Fiscal</th>
                <th scope="col">No. Inventario Anterior</th>
                <th scope="col">No. Inventario Actual</th>
                <th scope="col">Modelo</th>
                <th scope="col">Marca</th>
                <th scope="col">Estado</th>
                <th scope="col">Ubicación</th>
                <th scope="col">Motivo Baja</th>
                <th scope="col">Observaciones de la Baja</th>
            </tr>
        </thead>
        <tbody  style="text-transform:uppercase;">
        </tbody>
    </table>
    @stop

    @section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">
    @stop

    @section('js')
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/dataTables.bootstrap4.min.js"></script>
    <script src="js/jquery.validate.js"></script>
    <script>
        // Paginación y estilo de la tabla área mediante ajax
        $(function() {
            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                orderCellsTop: true,
                fixedHeader: true,
                "scrollX": true, 
                "order": [
                    [0, "desc"]
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                },
                ajax: "{{ route('get-ListaBajaUtic') }}",
                columns: [
                    {
                       
                        data: "fecha_baja",
                        name: "fecha_baja",
                        "render": function(data) {
                            if(data && data!='0000-00-00 00:00:00'){
                                var date = new Date(data);
                                var month = date.getMonth() + 1;
                                if(month<10){
                                    return date.getDate() + "/" +(month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
                                }else{
                                    return date.getDate() + "/" +month + "/" + date.getFullYear();
                                }
                            }else{
                                return "S/F";
                            }
                         }
                    },{
                        data: 'Userbaja',
                        name: 'Userbaja',
                        "render": function(data) {
                            if (!data) {
                                return 'S/U';
                            } else {
                                return data;
                            }
                         }
                    },
                    {
                        data: "created_at",
                        name: "created_at",
                        "render": function(data) {
                            var date = new Date(data);
                            var month = date.getMonth() + 1;
                            if(month<10){
                                return date.getDate() + "/" +(month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
                            }else{
                                return date.getDate() + "/" +month + "/" + date.getFullYear();
                            }
                        }
                    },
                    {
                        data: 'UserAlta',
                        name: 'UserAlta'
                    },
                    {
                        data: 'concepto',
                        name: 'concepto'
                    },
                    {
                        data: 'NoSerie',
                        name: 'NoSerie'
                    },
                    {
                        data: 'factura',
                        name: 'factura',
                        "render": function(data) {
                            if (!data) {
                                return 'S/N';
                            } else {
                                return data;
                            }
                         }
                    },
                    {
                        data: 'NoInventarioAnterior',
                        name: 'NoInventarioAnterior',
                        "render": function(data) {
                            if (!data) {
                                return 'S/N';
                            } else {
                                return data;
                            }
                         }
                    },
                    {
                        data: 'ClaveBien',
                        name: 'ClaveBien',
                        "render": function(data) {
                            if (!data || data == '			') {
                                return 'S/N';
                            } else {
                                return data;
                            }
                         }
                    },
                    {
                        data: 'Modelo',
                        name: 'Modelo'
                    }, 
                    {
                        data: 'nombre',
                        name: 'nombre'
                    },
                    {
                        data: 'Estado',
                        name: 'Estado'
                    },
                    {
                        data: 'ubicacion',
                        name: 'ubicacion'
                    },
                    {
                        data: 'Motivo_baja',
                        name: 'Motivo_baja',
                        "render": function(data) {
                            if (!data) {
                                return 'S/M';
                            } else {
                                return data;
                            }
                         }
                    },
                    {
                        data: 'ObvervacionesBaja',
                        name: 'ObvervacionesBaja',
                        "render": function(data) {
                            if (!data) {
                                return 'S/O';
                            } else {
                                return data;
                            }
                         }
                    },
         
                ]
            });

            //buscar por encabezado
            $(document).ready(function() {
                $('#bienes thead tr').clone(true).appendTo( '#bienes thead' );
                $('#bienes thead tr:eq(1) th').each( function (i) {
                    var title = $(this).text();
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            
                    $( 'input', this ).on( 'keyup change', function () {
                        if ( table.column(i).search() !== this.value ) {
                            table
                                .column(i)
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );  
            } );
        });
    </script>
    @stop
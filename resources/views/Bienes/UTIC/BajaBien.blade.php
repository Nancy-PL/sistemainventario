@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">BAJA DE EQUIPO DE COMPUTO Y ACCESORIOS </h1>
@stop


@section('content')
<!-- Mensaje de la validacion -->
<div class="row">
    @if($message=Session::get('UPS'))
    <div class="col-12 alert-success alert-dismissable fade show" role="alert">
        <h5>Operación correcta pero algunos bienes no se dieron de baja</h5>
        <span>{{$message}}</span>

    </div>
    @endif
    @if($message=Session::get('listo'))
    <div class="col-12 alert-success alert-dismissable fade show" role="alert">
        <h5>Operación realizada con éxito</h5>
        <span>{{$message}}</span>

    </div>
    @endif
</div>
<form action="BajaBienUTIC" method="POST" id="bien" class=" was-validated">
    @csrf
    <div class="card-header">
        <!--Imprimiendo errores de la Validaciones de los campos--->
        <!--Imprimiendo los valores de las validaciones-->
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <p>Corrige los siguientes errores:</p>
            <ul>
                @foreach ($errors->all() as $messages)
                <li>{{ $messages }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>

    <!--Bienes seleccionados-->
    <input class="bienes" type="hidden" id="bienes" name="bienes" value="">
    <table id="bienesT" class="table table-bordered yajra-datatable">
        <thead class="table-success text-black">

            <tr>
                <th>Baja</th>
                <th scope="col">Nombre Completo</th>
                <th scope="col">Clasificación</th>
                <th scope="col">No. Inventario Actual</th>
                <th scope="col">No. Inventario Anterior</th>
                <th scope="col">No. Folio Fiscal</th>
                <th scope="col">Ubicación</th>
                <th scope="col">Marca</th>
                <th scope="col">Modelo</th>
                <th scope="col">No de Serie </th>
                <th scope="col">Estado</th>
                <th scope="col">Características</th>
                <th scope="col">Observaciones</th>
                <th scope="col">Fecha de Asignación</th>

            </tr>
        </thead>
        <tbody>
        <tr>

            @foreach ($Bienes as $Resguardo)
                <td><input class="checkboxClass" type="checkbox" name="biens[]"  value="{{$Resguardo->id}}"  ></td>
                <td>{{ $Resguardo->Profesion}} {{ $Resguardo->Nombre}} {{ $Resguardo->ApellidoP}} {{ $Resguardo->ApellidoM}}</td>
                <td>{{ $Resguardo->concepto}}</td>
                <td>{{ $Resguardo->ClaveBien}}</td>
                <td>{{ $Resguardo->NoInventarioAnterior}}</td>
                <td>{{ $Resguardo->factura}}</td>
                <td>{{ $Resguardo->ubicacion}}</td>
                <td>{{ $Resguardo->name}}</td>
                <td>{{ $Resguardo->Modelo}}</td>
                <td>{{ $Resguardo->NoSerie}}</td>
                <td>{{ $Resguardo->Estado}}</td>
                <td>{{ $Resguardo->Caracteristicas}}</td>
                <td>{{ $Resguardo->Observaciones}}</td>
                <td>{{ $Resguardo->fechaAsignacion}}</td>


            </tr>
            @endforeach


        </tbody>
    </table>
    <!-- Motivo de la baja  -->
    <div class="row justify-content-md-center">
        <div class=" w-100" type="hidden">
            <div class="row justify-content-md-center">
                <x-label for="Motivo" class="mt-3" :value="__('Motivo de la baja')" />
            </div>
            <div class="row justify-content-md-center">
                <x-input id="Motivo" placeholder="Motivo de la baja" class="form-control col-sm-5 " type="text" name="Motivo" :value="old('Motivo')" style="text-transform:uppercase;" value="" onkeyup="javascript:this.value=this.value.toUpperCase();" tabindex="1" required autofocus />
            </div>

            <!--Visualizando Observaciones-->
            <div class="row justify-content-md-center">
                <x-label for="observaciones" class="mt-3" :value="__('Observaciones')" />
            </div>
            <div class="row justify-content-md-center">
                <textarea class="form-control col-sm-5 mb-3" placeholder="Escriba las Observaciones" name="Observaciones" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" tabindex="2" required></textarea>
            </div>


            <div class="modal fade" id="create">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4>Dar de baja</h4>
                            <button type="button" class="close" data-dismiss="modal">
                                <span>×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            ¿Esta seguro que quiere dar de baja a este bien?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <input type="submit" class="btn btn-primary" value="ok">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <a href="submit"  class="btn btn-outline-danger  BajaBien" data-toggle="modal" data-target="#create">
                    Dar de baja
                </a>
            </div>

        </div>
</form>
@stop
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">
@stop

@section('js')
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script>
    $(document).ready(function() {
        var table = $('#bienesT').DataTable({
            "scrollX": true,
            "lengthMenu": [
                [5, 10, 50, -1],
                [5, 10, 50, "All"]
            ],
            order: [
                [13, "asc"]
            ],

            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }

        });
    });


    
    var bienes = [];
    $(document).on('change', '.checkboxClass', function() {
        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz
            bienes[bienes.length] = $(this).val();
        } else {
            // Si no está marcado, elimine el valor de la matriz
            var index = bienes.indexOf($(this).val());
            if (index > -1) {
                bienes.splice(index, 1);
            }
        }
    });

    $(document).on('change', '.header-checkbox', function() {

        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz           
            $('.checkboxClass').each(function() {
                bienes[bienes.length] = $(this).val();
            });
        } else {
            // Si no está marcado, elimine el valor de la matriz
            $('.checkboxClass').each(function() {
                var index = bienes.indexOf($(this).val());
                if (index > -1) {
                    bienes.splice(index, 1);
                }
            });
        }
    });

    // al dar click en submit asignar los valores del array se envian al formulario
    $('.BajaBien').click(function() {
        var bien = '';
        // leer bienes usando for loop
       // alert(bienes);
        document.getElementById("bienes").value = bienes;
    });
</script>
@stop
@extends('adminlte::page')

@section('title', 'Sistema Web')

@section('content_header')
<div class="float-right mb-50">
    <img src="../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">AGREGAR EQUIPO DE CÓMPUTO Y ACCESORIOS</h1>
@stop

@section('content')
<form action="../BienesUtic" method="POST" id="bien" class=" was-validated">
    @csrf
    @foreach ($areas as $area)
    @if($area->Nombre=="ALMACEN")
    <input type="hidden" id="almacen" value="{{$area->id}}" name="almacen" />
    <input type="hidden" id="almacenNombre" value="{{$area->Responsable}}" name="almacenNombre" />
    @break
    @endif
    @endforeach
    <input type="hidden" id="Tipo" value="{{Session::get('tipoR')}}" name="Tipo" />
    <input type="hidden" id="Resguardante" value="{{Session::get('Resguardo')}}" name="Resguardante" />
  
    <div class="card-header">
        <!-- Mensaje del No. de inventario o No. de control -->
        <div class="row">
            @if($message=Session::get('Listo'))
            <div class="col-12 alert-success alert-dismissable fade show" role="alert">
                <h5>La operación se completó correctamente</h5>
                <span>{{$message}}</span>
            </div>
            @endif
            
            @if($message=Session::get('Ups'))
            <div class="col-12 alert-warning alert-dismissable fade show" role="alert">
            <h5>No se encontró al personal</h5>
                <span>{{$message}}</span>
            </div>
            @endif
        </div>
        <!--Imprimiendo errores de la Validaciones de los campos--->
        <!--Imprimiendo los valores de las validaciones-->
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <p>Corrige los siguientes errores:</p>
            <ul>
                @foreach ($errors->all() as $messages)
                <li>{{ $messages }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <a onclick="Finalizar();" id="finalizar" name="finalizar" class="btn btn-outline-success float-right mb-50" float-right">Finalizar</a>
    </div>

    <div class="form-row  justify-content-md-center ">
        <!--Tipo de Resguardo-->
        <div class="col-md-2 mb-5">
            <x-label for="Resguardo" class="mt-3" :value="__('Tipo de  Resguardo')" />
            <div class="form-group">
                <select onchange="TipoResguardo(this)" class="custom-select js-example-basic-single " name="EstadoT" id="EstadoT" tabindex="5" required>
                    <option value="" selected>Resguardo</option>
                    <option value="PERMANENTE" @if( "PERMANENTE"==Session::get('tipoR')) selected @endif>PERMANENTE</option>
                    <option value="ALMACEN" @if( "ALMACEN"==Session::get('tipoR')) selected @endif>ALMACÉN </option>
                </select>
            </div>

        </div>

        <!-- Nombre -->
        <div class="col-md-3 mb-5">
            <x-label for="NombreA" class="mt-3" :value="__('Nombre de la persona a asignar')" />
            <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->
            <div class="form-group">
                <select class="custom-select js-example-basic-single " id="personas_id" name="personas_id" tabindex="1" required disabled>
                    <option value="" selected>Seleccionar nombre del personal</option>

                    @foreach ($personas as $personas)
                    <option value="{{$personas->id}}" data-codigo="{{$personas->area_id}}" @if( $personas->id == Session::get('Resguardo')) selected @endif>{{$personas->Profesion}} {{$personas->Nombre}} {{$personas->ApellidoP}} {{$personas->ApellidoM}}</option>
                    @endforeach
                </select>
            </div>

        </div>
    </div>

    <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del Clasificacion del bien -->
    <div class="form-row">
        <div class="col-md-3 mb-3">
            <x-label for="Clasifica" class="mt-3" :value="__('Clasificación')" />
            <div class="form-group">
                <select name="Clasificacion_id" id="Clasificacion_id" :value="old('Clasificacion_id')" class="custom-select js-example-basic-single" tabindex="2" required>
                    <option value="" data-codigo="">SELECCIONA CLASIFICACION</option>
                    @foreach ( $clasificacions as $clasificacions)
                    <option value="{{$clasificacions->id}}" data-codigo="{{$clasificacions->NoInventario}}">{{$clasificacions->concepto}}</option>
                    @endforeach
                </select>

            </div>
        </div>
        <!--Area-->
        <div class="col-md-3 mb-3">
            <x-label class="mt-3" for="areas" :value="__('Área')" />
            <div class="form-group">
                <select name="area" id="area" class="custom-select js-example-basic-single"  :value="old('area')" tabindex="3" required>
                    <option value="">SELECCIONA ÁREA</option>
                    @foreach ($areas as $areas)
                    <option value="{{$areas->id}}"@if( $areas->id == Session::get('Areaid')) selected @endif>{{$areas->Nombre}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <!--Marca-->
        <div class="col-md-2 mb-3">
            <x-label for="Marca" class="mt-3" :value="__('Marca')" />
            <div class="form-group">
                <select id="marca_id" name="marca_id" class="custom-select js-example-basic-single" :value="old('marca_id')" tabindex="4" required>
                    <option value="">SELECCIONA MARCA</option>
                    @foreach ($marcas as $marca)
                    <option value="{{$marca->id}}">{{$marca->nombre}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <!--Estado-->
        <div class="col-md-2 mb-3">
            <x-label for="Estado" class="mt-3" :value="__('Estado')" />
            <div class="form-group">
                <select  id="Estado_id" class="custom-select js-example-basic-single "  :value="old('Estado')"  name="Estado" tabindex="5" required>
                    <option value="BUENO" selected>BUENO</option>
                    <option value="REGULAR">REGULAR</option>
                    <option value="MALO">MALO </option>
                </select>
            </div>
        </div>
        <!-- Ubicacion  -->
        <div class="col-md-3 mb-3">
            <x-label for="Ubicacion" class="mt-3" :value="__('Ubicación')" />
            <x-input id="Ubicacion" placeholder="Ubicacion" class="form-control" type="text" name="Ubicacion" :value="old('Ubicacion')" style="text-transform:uppercase;"  maxlength="500" tabindex="6" required autofocus />
        </div>

        <!-- Modelo -->
        <div class="col-md-2 mb-3">
            <x-label for="Modelo" class="mt-3" :value="__('Modelo')" />
            <x-input id="Modelo" placeholder=" Ingrese Modelo de" class="form-control" type="text" name="Modelo" :value="old('Modelo')" style="text-transform:uppercase;"  tabindex="7" maxlength="90"  required autofocus />
        </div>
        <!-- No de serie -->
        <div class="col-md-3 mb-3">
            <x-label for="NoSerie" class="mt-3" :value="__('No. de Serie ')" />
            <x-input id="NoSerie" placeholder=" Ingrese el No. de Serie" class="form-control" type="text" name="NoSerie" :value="old('NoSerie')" style="text-transform:uppercase;"   tabindex="8"  maxlength="90"  />
        </div>
        <!-- No. de folio fiscal -->
        <div class="col-md-2 mb-3">
            <x-label for="factura" class="mt-3" :value="__('No. de Folio Fiscal')" />
            <x-input id="factura" placeholder=" Ingrese el No. de folio fiscal" class="form-control" type="text" name="factura" :value="old('factura')" style="text-transform:uppercase;"   tabindex="9" maxlength="20" required autofocus />
        </div>
        <!-- No. de Inventario anterior  -->
        <div class="col-md-3 mb-3">
            <x-label for="NoInventarioAnterior" class="mt-3" :value="__('No. de Inventario Anterior')" />
            <x-input id="NoInventarioAnterior" placeholder="No. de Inventario" class="form-control" type="text" name="NoInventarioAnterior" maxlength="130" :value="old('NoInventarioAnterior')" style="text-transform:uppercase;"  tabindex="10"  />
        </div>
    </div>
    <div class="form-row">
        <!-- Mejoras al Equipo -->
        <div class="col-md-4  mb-3">
            <x-label for="MejorasEquipo" class="mt-3" :value="__('Mejoras al Equipo')" />
            <textarea class="form-control" name="MejorasEquipo" style="text-transform:uppercase;"  maxlength="700" :value="old('MejorasEquipo')"  tabindex="11" ></textarea>
        </div>
        <!--Visualizando Caracteristicas-->
        <div class="col-md-4  mb-3">
            <x-label for="caracteristicas" class="mt-3" :value="__('Características')" />
            <textarea class="form-control" name="Caracteristicas" style="text-transform:uppercase;"   maxlength="700"  tabindex="12" ></textarea>
        </div>
        <!--Visualizando Observaciones-->
        <div class="col-md-4  mb-3">
            <x-label for="observaciones" class="mt-3" :value="__('Observaciones')" />
            <textarea class="form-control" name="Observaciones" style="text-transform:uppercase;"  maxlength="700"  :value="old('Observaciones')" tabindex="13" ></textarea>
        </div>

    </div>
    <!--Visualizando No. de Inventario Nuevo-->
    <div id="NumeroInventario" style="display:none;" class=" row justify-content-md-center  mt-3 mr-3">
        <x-label type="hidden" for="NumeroInventario" :value="__('No. de Inventario: ')" />
    </div>
    <div id="NumeroInventarioActual" style="display:none;" class=" row justify-content-md-center">
        <x-input class="form-control col-md-3 mb-3" tabindex="14" for="NumeroInventarioActual" />

    </div>
    <!--Guardar-->
    <div class="row justify-content-md-center">
        <a href="/Bienes" tabindex="15" class="btn  btn-outline-dark  mt-3 mb-3 col-sm-3 mr-3">Regresar</a>
        <button type="submit" tabindex="16" class="btn btn-outline-success mt-3 mb-3 col-sm-3 mr-3">Alta</button>
    </div>
</form>
<table id="bienes" class="table table-bordered  shadow-lg display nowrap   mt-4 " style="width:100%">
    <thead class="table-success text-black">
        <tr>
            <th scope="col">Eliminar</th>
            <th scope="col">Clasificación</th>
            <th scope="col">No. de serie </th>
            <th scope="col">No. de Folio Fiscal</th>
            <th scope="col">No. Inventario Anterior</th>
            <th scope="col">No. Inventario Actual</th>
            <th scope="col">Modelo</th>
            <th scope="col">Marca</th>
            <th scope="col">Estado</th>
            <th scope="col">Ubicación</th>
            <th scope="col">Fecha</th>
        </tr>
    </thead>
    <tbody  style="text-transform:uppercase;">

        @foreach($bienes as $bienes)
        @if($bienes->persona==Session::get('user') )
        @if($bienes->idR==Session::get('idResguardo'))
        <tr style="background-color : #C0E5C0">
            <td>
                <form action="{{route('BienesUtic.destroy', $bienes->id)}}" method="POST">
                    <!--token para verificar que el usuario autenticado es quien en realidad está haciendo la petición-->
                    @csrf
                    <!--Eliminar personal-->
                    @method('DELETE')
                    <a onclick="return confirm('¿Esta seguro que quiere eliminar este bien?')" data-toggle="tooltip" data-placement="right" title="Eliminar">
                        <input type="submit" class="btn btn-dark " value="Eliminar">
                    </a>
                </form>
            </td>
            <td>{{$bienes->concepto}}</td>
            <td>{{$bienes->NoSerie}}</td>
            <td>{{$bienes->factura}}</td>
            <td>{{$bienes->NoInventarioAnterior}}</td>
            <td>{{$bienes->ClaveBien}}</td>
            <td>{{$bienes->Modelo}}</td>
            <td>{{$bienes->nombre}}</td>
            <td>{{$bienes->Estado}}</td>
            <td>{{$bienes->ubicacion}}</td>
            <td>{{$bienes->created_at}}</td>
        </tr>
        @else
        <tr>
            <td></td>
            <td>{{$bienes->concepto}}</td>
            <td>{{$bienes->NoSerie}}</td>
            <td>{{$bienes->factura}}</td>
            <td>{{$bienes->NoInventarioAnterior}}</td>
            <td>{{$bienes->ClaveBien}}</td>
            <td>{{$bienes->Modelo}}</td>
            <td>{{$bienes->nombre}}</td>
            <td>{{$bienes->Estado}}</td>
            <td>{{$bienes->ubicacion}}</td>
            <td>{{$bienes->created_at}}</td>
        </tr>
        @endif
        @endif
        @endforeach
    </tbody>
</table>
@stop

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link href="../css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="../css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="../css/sweetalert2.min.css">
@stop

@section('js')
<script src="../js/sweetalert2.min.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/dataTables.bootstrap4.min.js"></script>
<script src="../js/jquery.validate.js"></script>
<script src="../js/select2.min.js"></script>

<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.js-example-basic-single').select2({
            theme: "classic"
        });
    });

    function Finalizar() {
        var persona = document.getElementById("personas_id").value;
        var tipoResguardo = document.getElementById("EstadoT").value;
        var personaAlmacen = document.getElementById("almacenNombre").value;

        if (persona && tipoResguardo != "ALMACEN") {
            window.location.href = "../ListaResguardanteUtic/" + persona;
        } else if (tipoResguardo == "ALMACEN") {
            window.location.href = "../ListaResguardanteAlmacenUtic/" + personaAlmacen;
        } else {
            swal({
                title: "Ups",
                text: "Selecciona el nombre del personal.",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: '#0F8971',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: "Cancelar"
            });
        }
    }
    $(document).ready(function() {
        $('#bienes').DataTable({
            "scrollX": true,
            "lengthMenu": [
                [5, 10, 50, -1],
                [5, 10, 50, "Todo"]
            ],
            "order": [
                [10, "desc"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }
        });
    });

      //Almacenar valores de persona y tipo de resguardo
      function TipoResguardo(sel) {
        var id = document.getElementById("almacen").value;
        var nombre = document.getElementById("almacenNombre").value;
        if (sel.value == "ALMACEN") {
            $('#area').val(234); // Seleccionar el valor del area de la persona
            $('#personas_id').append('<option value="' + id + '" selected>' + nombre + '</option>');
            document.getElementById("Resguardante").value = id;
            $('#personas_id').prop('disabled', 'disabled');
            $('#Estado_id').prop('disabled', 'disabled');


        } else {
            $("#personas_id").find("option[value='']").select();
            $("#personas_id").find("option[value=" + id + "]").remove();
            $('#personas_id').removeAttr('disabled');
        }
        $('#area').trigger('change'); //Notifique a los componentes JS que el valor cambió

        document.getElementById("Tipo").value = sel.value;
    }
//seleccionando el area de la persona seleccionada
    document.getElementById('personas_id').onchange = function() {
        document.getElementById("Resguardante").value = document.getElementById("personas_id").value;
        var tipo = document.getElementById("Tipo").value;
        if (tipo == "PERMANENTE") {
            /* Referencia al option seleccionado */
            var Opcion = this.options[this.selectedIndex];
            /* Referencia a los atributos data de la opción seleccionada */
            var area = Opcion.dataset;
            $('#area').val(area.codigo); // Seleccionar el valor del area de la persona
            $('#area').trigger('change'); //Notifique a los componentes JS que el valor cambió
        }

    }
    window.onload = function() {
        var tipo = document.getElementById("EstadoT").value;
        if (tipo) {
            $('#personas_id').prop('disabled', 'disabled'); //deshabilitar a la persona
            $('#EstadoT').prop('disabled', 'disabled'); //deshabilitar tipo de resguardo
        }
    }
</script>
@stop
@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">REPORTES DE EQUIPO DE CÓMPUTO Y ACCESORIOS</h1>
@stop

@section('content')

<form action="" method="POST" id="bien">
    <div class="card-header">
    </div>
    <table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">DOCUMENTOS</th>
      <th scope="col">DESCARGAR</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">BIENES  DE CÓMPUTO Y ACCESORIOS SOLO CON NÚMERO DE INVENTARIO </th>
      <td colspan="2"><a href="NoInventarioComputo" class="button btn btn-success" ALIGN="center">
                <i class="fas fa-file-excel  " aria-hidden="true" ></i>
                </a> </td>
    </tr>
    <tr>
      <th scope="row">BIENES  DE CÓMPUTO Y ACCESORIOS SOLO CON NÚMERO DE  CONTROL</th>
      <td colspan="2"><a href="inventarioControl" class="button btn btn-success" ALIGN="center">
                <i class="fas fa-file-excel  " aria-hidden="true" ></i>
                </a> </td>
    </tr>
    </tbody>
</table>

</form>

@stop

@section('css')
<link href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap5.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


<link src="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css"/>>

<link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet"/>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet"/>
@stop

@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js"></script>


<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script>
    $(document).ready(function() {
        $('#bienes').DataTable({
           responsive:true, 
           
            "scrollX": true,
            "lengthMenu": [
                [5, 10, 50, -1],
                [5, 10, 50, "Todo"]
            ],
            "order": [
                [10, "desc"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }
        });
    });


    </script>

@stop
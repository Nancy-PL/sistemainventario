<table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">Clasificación</th>
                <th scope="col">No. de serie </th>
                <th scope="col">No. de Folio Fiscal</th>
                <th scope="col">No. Inventario Anterior</th>
                <th scope="col">No. Inventario Actual</th>
                <th scope="col">Modelo</th>
                <th scope="col">Marca</th>
                <th scope="col">Estado</th>
                <th scope="col">Ubicación</th>
                <th scope="col">Características</th>
                <th scope="col">Observaciones</th>
                <th scope="col">Fecha de Baja</th>
                <th scope="col">Disponibilidad</th>
                <th scope="col">Usuario alta</th>
                <th scope="col">Fecha de Creación</th>
            </tr>
    </thead>
        <tbody>
        @foreach ($bienes as $biene)
               <tr>
                    <td>{{$biene->concepto}}</td>
                    <td>{{$biene->NoSerie}}</td>
                    <td>{{$biene->factura}}</td>
                    <td>{{$biene->NoInventarioAnterior}}</td>
                    <td>{{$biene->ClaveBien}}</td>
                    <td>{{$biene->Modelo}}</td>
                    <td>{{$biene->nombre}}</td>
                    <td>{{$biene->Estado}}</td>
                    <td>{{$biene->ubicacion}}</td>
                    <td>{{$biene->Caracteristicas}}</td>
                    <td>{{$biene->Observaciones}}</td>
                    <td>{{$biene->fecha_baja}}</td>
                    <td>{{$biene->Activo}}</td>
                    <td>{{$biene->name}}</td>
                    <td>{{$biene->created_at}}</td>
                </tr>
        @endforeach
        </tbody>
</table>

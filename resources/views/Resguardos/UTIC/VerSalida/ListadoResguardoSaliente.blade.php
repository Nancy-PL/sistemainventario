@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h3 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated"> BIENES DE CÓMPUTO Y ACCESORIOS SALIENTES</h3>
@stop

@section('content')
<div class="card-header">

</div>


<div class="form-row  justify-content-md-center ml-10 ">

    <nav class="navbar navbar-light float-center" style="width: 550px; height: 100px;">
        <form class="form-inline" >
            <x-label for="Referencia" class="mr-3" :value="__('Referencia:')" />    
            <textarea id="Referencia" placeholder="Ingrese la referencia"  style="width: 300px; height: 70px;" class="form-control mr-3" type="text" name="Referencia" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" onchange="Ref()" tabindex="1"  maxlength="800" required autofocus>{{Session::get('Referencia')}}</textarea>

                <!--buscar el resguardo de la persona-->
                <button class="btn btn-outline-success  float-right" type="submit">Buscar</button><br>
            </div>
        </form>
    </nav>
</div>
<!-- Listado de resguardo -->
<table style="text-transform:uppercase;" id="resguardo" class="table table-striped table-bordered shadow-lg display nowrap" style="width:100%">
    <thead class="table-success text-black table-sm">
        <tr>
            <th scope="col">Nombre del personal</th>
            <th scope="col">Clasificación</th>
            <th scope="col">Modelo</th>
            <th scope="col">No de serie </th>
            <th scope="col">No Inventario Anterior</th>
            <th scope="col">No Inventario</th>
            <th scope="col">Marca</th>
            <th scope="col">Estado</th>
            <th scope="col">Caracteristicas</th>
            <th scope="col">Fecha</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            @php $b=0 @endphp
            @foreach ($resguardo as $Resguardo)
            @if($Resguardo->concepto)
            @php $b=1 @endphp

            <td>{{ $Resguardo->Nombre}} {{ $Resguardo->ApellidoP}} {{ $Resguardo->ApellidoM}}</td>
            <td>{{ $Resguardo->concepto}}</td>
            <td>{{ $Resguardo->Modelo}}</td>
            <td>{{ $Resguardo->NoSerie}}</td>
            <td>{{ $Resguardo->NoInventarioAnterior}}</td>
            <td>{{ $Resguardo->ClaveBien}}</td>
            <td>{{ $Resguardo->nombre}}</td>
            <td>{{ $Resguardo->Estado}}</td>
            <td>{{ $Resguardo->Caracteristicas}}</td>
            <td>{{ $Resguardo->fechaAsignacion}}</td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
<div class="row justify-content-md-center ">
    @if($b==1)
    @foreach($resguardo as $resguardos)
        <h5>Imprimir</h5>
        <a href="Reporte-salida/{{$resguardos->idSalida}}/{{$resguardos->area_id}}" class="button ml-3">
        <i class="far fa-file-pdf fa-3x" aria-hidden="true" style="color:#ff0000; "></i>
    </a>
        @break
    @endforeach
    @endif

</div>
</form>
@stop

@section('css')
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/jquery.dataTables.min.css">
<link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="css/select2.min.css">

@stop

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>

<script>
    //<!--Estilos del select2-->
    $(document).ready(function() {
        $('.js-example-basic-single').select2({
            theme: "classic"
        });
    });

    $(document).ready(function() {
        $('#resguardo').DataTable({
            "scrollX": false,
            "lengthMenu": [
                [5, 10, 50, -1],
                [5, 10, 50, "All"]
            ],
            "order": [
                [9, "desc"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }

        });
    });

    function Finalizar() {
        var Referencias = document.getElementById("Referencias").value;
        if (persona) {
            window.location.href = "ReporteSalida/" + Referencias;
        } else {
            alert("Registre una salida");
        }
    }
</script>
@stop
@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height="80px">
</div>
<h3 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">RESGUARDO TEMPORAL DE BIENES EQUIPO DE CÓMPUTO Y ACCESORIOS</h3>
@stop



@section('content')

<div class="form-row  justify-content-md-center ml-5 ">

    <nav class="navbar navbar-light float-center" style="width: 900px;">
        <form class="form-inline">
           
            <div class="row justify-content-center ">
                <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->
                <select name="idPersona" id="idPersona" class="custom-select js-example-basic-single" tabindex="1" type="search" required>
                    <option value="" selected>Seleccionar nombre</option>
                    @foreach ($personas as $persona)
                    <option value="{{$persona->id}}" data-bien="{{$persona->id}}">{{$persona->Nombre}} {{$persona->ApellidoP}} {{$persona->ApellidoM}}</option>
                    @endforeach
                </select>
                <!--buscar el resguardo de la persona-->
                <button class="btn btn-outline-success  float-right" type="submit">Buscar</button><br>

            </div>
        </form>
    </nav>
</div>

<form action="" method="POST" id="bien">
    <div class="card-header">
        
    <div class="justify-content-center ml-5">
        @foreach($resguardo as $resguardos)
        <label for="personal" class="ml-5">{{ $resguardos->Nombre}} {{ $resguardos->ApellidoP}} {{ $resguardos->ApellidoM}}</label>
        @break
        @endforeach
    </div>
    </div> 
    <a href="Bienes/create" class="button">
        <i class="fa fa-plus-circle  fa-3x" aria-hidden="true" style="color:#3A3E3C; "></i>
    </a>
    <br>
    
    <!-- Listado de resguardo -->
    <table style="text-transform:uppercase;" id="resguardo" class="table table-striped table-bordered shadow-lg display nowrap" style="width:100%">
        <thead class="table-success text-black table-sm">
            <tr>
                <th scope="col">Clasificación</th>
                <th scope="col">Modelo</th>
                <th scope="col">No de serie </th>
                <th scope="col">No Inventario Anterior</th>
                <th scope="col">No Inventario</th>
                <th scope="col">Marca</th>
                <th scope="col">Estado</th>
                <th scope="col">Características</th>
                <th scope="col">Fecha</th>
            </tr>
        </thead>
        <tbody>
                @php $b=0 @endphp
                @foreach ($resguardo as $Resguardo)
                    @if($Resguardo->concepto)
                    @php $b=1 @endphp

                    <tr>
                    <td>{{ $Resguardo->concepto}}</td>
                    <td>{{ $Resguardo->Modelo}}</td>
                    <td>{{ $Resguardo->NoSerie}}</td>
                    <td>{{ $Resguardo->NoInventarioAnterior}}</td>
                    <td>{{ $Resguardo->ClaveBien}}</td>
                    <td>{{ $Resguardo->nombre}}</td>
                    <td>{{ $Resguardo->Estado}}</td>
                    <td>{{ $Resguardo->Caracteristicas}}</td>
                    <td>{{ $Resguardo->fechaAsignacion}}</td>
                    @endif
                    </tr>
                @endforeach
        </tbody>
    </table>
    <div class="row justify-content-md-center ">
        @foreach ($resguardo as $Resguardo)
        @if($b==1)
        <h5>Imprimir</h5>
            <a href="ReportePrestamo/{{$Resguardo->id}}" class="button ml-3">
            <i class="far fa-file-pdf fa-3x" aria-hidden="true" style="color:#ff0000; "></i>
            </a>
        @endif
        @break
        @endforeach
    </div>
</form>
@stop

@section('css')
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/jquery.dataTables.min.css">
<link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="css/select2.min.css">

@stop

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>

<script>
    //<!--Estilos del select2-->
    $(document).ready(function() {
        $('.js-example-basic-single').select2({
            theme: "classic"
        });
    });
    $(document).ready(function() {
        $('#resguardo').DataTable({
            "scrollX": false,
            "lengthMenu": [
                [5, 10, 50, -1],
                [5, 10, 50, "All"]
            ],
            "order": [
                [8, "desc"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }

        });
    });
</script>
@stop
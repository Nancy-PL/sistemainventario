@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">RESGUARDO TEMPORAL</h1>
@stop

@section('content')
<div class="card-header">
      <!-- Mensaje del No. de inventario o No. de control -->
      <div class="row">
        @if($message=Session::get('Ups'))
        <div class="col-12 alert-success alert-dismissable fade show" role="alert">
            <h5>Registro correcto</h5>
            <span>{{$message}}</span>
        </div>
        @endif
    </div>
    <!--Imprimiendo los valores de las validaciones-->
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $messages)
            <li>{{ $messages }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
<form method="POST" id="formTemporal" class="was-validated">

    <!--El csrf es un token se utiliza para verificar que el usuario autenticado es la persona que realmente realiza las solicitudes a la aplicación. -->
    @csrf
    <!--Establece la clave y el valor dados en la colección-->
    
    <!-- Nombre del personal -->
    <div class="row justify-content-md-center ">
        <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->
        <div class=" form-group">
            <x-label for="NombreA" class="mt-3 mr-3" :value="__('Nombre del personal:')" />
             <!--<select class="js-example-basic-single " name="persona_id" id ="persona_id" tabindex="1" required autofocus>
               <option value="" selected>Seleccionar Nombre</option>
                @foreach ($personas as $persona)
                <option value="{{$persona->id}}" @if($persona->id == $resguardo ) selected @endif>{{$persona->Nombre}} {{$persona->ApellidoP}} {{$persona->ApellidoM}}  </option>
                @endforeach
            </select>-->
            @foreach ($personas as $persona)
            @if($persona->id == $data[0]->personaId )
            <x-label   value="{{$persona->Nombre}} {{$persona->ApellidoP}} {{$persona->ApellidoM}}" />
            <input name="persona_id" id ="persona_id" class="mt-5 mr-3" type="hidden" value="{{$persona->id}}" />

            @endif
            @endforeach
        </div>
    </div>

    <!-- Número de auditoria o de comisión  -->
    <div class="form-row">
        <div class="col-md-3 mb-3">
            <x-label for="Numero" class="mt-3" :value="__('No. de orden de la auditoría o comisión')" />
            <input id="No_ordenC" placeholder=" Ingrese Número" value="{{$No_orden}}" class="form-control" type="text" name="No_ordenC" style="text-transform:uppercase;" maxlength="50" tabindex="1" required autofocus />
        </div>
        <input id="temporal" value="{{$id}}" class="form-control" type="text" name="temporal" style="text-transform:uppercase;" hidden readonly />
        
        <!-- Bandera para indicar sis equita un bien o se agrega  -->
        <input id="bandera" value="0" class="form-control" type="text" name="bandera"  hidden readonly />

        <!-- obtener fecha  De Orden  -->
        <div class="col-md-2 mb-3 ">
            <x-label for="fechaOrden" class="mt-3 mr-3" :value="__(' Fecha de Orden   ')" />
            <input type="date" id="fechaOrden" name="FechaOrden" tabindex="2" value="{{$fecha_orden}}" data-date-format="DD MMMM YYYY" required autofocus />
        </div>
        <!-- obtener fecha  De inicio  -->
        <div class="col-md-1 mb-3 mr-3">
            <x-label for="FechaInicio" class="mt-3 mr-3 " :value="__(' Fecha de Inicio  ')" />
            <input type="date" id="FechaInicio" name="FechaInicio" tabindex="3" value="{{$fecha_inicio}}" data-date-format="DD MMMM YYYY" required autofocus />
        </div>
        <!-- obtener fecha  FechaDevolucion-->
        <div class="col-md-2 mb-3">
            <x-label for="FechaDevolucion" class="mt-3 mr-3 " :value="__('Fecha de devolución')" />
            <input type="date" id="FechaDevolucion" data-date-format="DD MMMM YYYY" value="{{$fecha_devolucion}}" name="FechaDevolucion" tabindex="4" required autofocus />
        </div>
        <!-- obtener fecha de vencimiento  -->
        <div class="col-md-2 mb-3">
            <x-label for="FechaVencimiento" class="mt-3 mr-3" :value="__('Fecha de Vencimiento:')" />
            <input type="date" id="FechaVencimiento" data-date-format="DD MMMM YYYY" value="{{$fecha_vencimiento}}" name="FechaVencimiento" tabindex="5" required autofocus />
        </div>
        <!-- Observaciones -->
        <div class="col-md-5 mb-4">
            <x-label for="Observaciones" class="mt-3" :value="__('Observaciones')" />
            <textarea id="Observaciones" placeholder=" Ingrese las observaciones" class="form-control" type="text" name="Observaciones" :value="old('Observaciones')" style="text-transform:uppercase;" value="" tabindex="6" maxlength="800"> {{$sol}}</textarea>
        </div>
        
        <div class="col-md-5 mb-4">
            <button class="btn btn-outline-success ml-3 mt-5" tabindex="8" type="button" onclick="agregar()">Agregar bienes</button>
            <button class="btn btn-outline-success ml-3 mt-5" tabindex="8" type="button" onclick="quitar()">Quitar bienes</button>
            <button class="btn btn-outline-success  ml-3 mt-5 RTemporal" tabindex="7" type="button" onclick="Guardar()">Guardar</button>
            <a href="../ReportePrestamo/{{$ResgID}}" class="button ml-3 mt-3">
                <i class="far fa-file-pdf fa-3x" aria-hidden="true" style="color:#ff0000; "></i>
            </a>
        </div>
        
    </div>
    <!-- Notas-->
            <h3  id="notas" ><center><FONT COLOR="red">DESELECCIONAR EL BIEN QUE DESEA QUITAR</FONT></center></h3>
    <!--Bienes seleccionados-->
    <input class="bien" type="hidden" id="bienes" name="bienes" value="">
    <!-- Listado de Bienes -->
    <table style="text-transform:uppercase;" id="bienesRT" class="table yajra-datatable">
            <thead class="table-success text-black table-sm">
            <tr>
                <th scope="col">Elegir</th>
                <th scope="col">Profesión</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido Paterno</th>
                <th scope="col">Apellido Materno</th>
                <th scope="col">Clasificación</th>
                <th scope="col">Modelo</th>
                <th scope="col">No. de serie </th>
                <th scope="col">No. Inventario Anterior</th>
                <th scope="col">No. Inventario</th>
                <th scope="col">Marca</th>
                <th scope="col">Estado</th>
                <th scope="col">Características</th>
                <th scope="col">Fecha Asignación</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>


</form>
@stop

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link href="../css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="../css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="../css/sweetalert2.min.css">
@stop

@section('js')
<script src="../js/sweetalert2.min.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/dataTables.bootstrap4.min.js"></script>
<script src="../js/jquery.validate.js"></script>
<script src="../js/select2.min.js"></script>
<script>
     // In your Javascript (external .js resource or <> tag)
     $(document).ready(function() {
        $('.js-example-basic-single').select2({
            theme: "classic"
        });
    });
    var table=$('.yajra-datatable').DataTable();
    // Paginación y estilo de la tabla área mediante ajax
    aux =document.getElementById("temporal").value;
        console.log(aux);
        table.destroy();
        url ="{{ route('get-ResguardoTemp1',$resguardo) }}";
          table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            orderCellsTop: true,
            fixedHeader: true,
            "scrollX": true,
            "order": [
                [13, "desc"]
            ],

            "language": {
                "src": "../js/Spanish.json"
            },
            ajax: url,
            columns: [{
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true ,
                    
                },
                {
                    data: 'Profesion',
                    name: 'Profesion',
                },
                {
                    data: 'Nombre',
                    name: 'Nombre',
                },
                {
                    data: 'ApellidoP',
                    name: 'ApellidoP'
                },
                {
                    data: 'ApellidoM',
                    name: 'ApellidoM'
                },
                {
                    data: 'concepto',
                    name: 'concepto'
                },
                {
                    data: 'Modelo',
                    name: 'Modelo'
                },
                {
                    data: 'NoSerie',
                    name: 'NoSerie'
                },
                {
                    data: 'NoInventarioAnterior',
                    name: 'NoInventarioAnterior'
                },
                {
                    data: 'ClaveBien',
                    name: 'ClaveBien'
                },
                {
                    data: 'nombre',
                    name: 'nombre'
                },
                {
                    data: 'Estado',
                    name: 'Estado'
                },
                {
                    data: 'Caracteristicas',
                    name: 'Caracteristicas'
                },
                {
                    data: "fechaAsignacion",
                    name: "fechaAsignacion",
                    "render": function(data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        if(month<10){
                            return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
                        }else{
                            return date.getDate() + "/" +  month + "/" + date.getFullYear();
                        }
                    }
                },
            ]
        });
    
        //buscar por encabezado
        $(document).ready(function() {
            $('#bienesRT thead tr').clone(true).appendTo('#bienesRT thead');
            $('#bienesRT thead tr:eq(1) th').each(function(i) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Buscar ' + title + '" />');

                $('input', this).on('keyup change', function() {
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            });
        });

    function agregar() {
        bienes = [];
        document.getElementById("bandera").value = 1;
        document.getElementById("notas").innerHTML  = "";

        
              url ="{{ route('get-ResguardoTemp2',$resguardo) }}";
              table.destroy();
                table = $('.yajra-datatable').DataTable({
                  processing: true,
                  serverSide: true,
                  orderCellsTop: true,
                  fixedHeader: true,
                  "scrollX": true,
                  "order": [
                      [13, "desc"]
                  ],
      
                  "language": {
                      "src": "../js/Spanish.json"
                  },
                  ajax: url,
                  columns: [{
                          data: 'action',
                          name: 'action',
                          orderable: true,
                          searchable: true ,
                          
                      },
                      {
                          data: 'Profesion',
                          name: 'Profesion',
                      },
                      {
                          data: 'Nombre',
                          name: 'Nombre',
                      },
                      {
                          data: 'ApellidoP',
                          name: 'ApellidoP'
                      },
                      {
                          data: 'ApellidoM',
                          name: 'ApellidoM'
                      },
                      {
                          data: 'concepto',
                          name: 'concepto'
                      },
                      {
                          data: 'Modelo',
                          name: 'Modelo'
                      },
                      {
                          data: 'NoSerie',
                          name: 'NoSerie'
                      },
                      {
                          data: 'NoInventarioAnterior',
                          name: 'NoInventarioAnterior'
                      },
                      {
                          data: 'ClaveBien',
                          name: 'ClaveBien'
                      },
                      {
                          data: 'nombre',
                          name: 'nombre'
                      },
                      {
                          data: 'Estado',
                          name: 'Estado'
                      },
                      {
                          data: 'Caracteristicas',
                          name: 'Caracteristicas'
                      },
                      {
                          data: "fechaAsignacion",
                          name: "fechaAsignacion",
                          "render": function(data) {
                              var date = new Date(data);
                              var month = date.getMonth() + 1;
                              if(month<10){
                                  return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
                              }else{
                                  return date.getDate() + "/" +  month + "/" + date.getFullYear();
                              }
                          }
                      },
                  ]
              })
          }
          function quitar() {
            bienes = [];
            document.getElementById("bandera").value = 0;
            document.getElementById("notas").innerHTML = "<center><FONT COLOR='red'>DESELECCIONAR EL BIEN QUE DESEA QUITAR</FONT></center>";

              url ="{{ route('get-ResguardoTemp1',$resguardo) }}";
              table.destroy();
                 table = $('.yajra-datatable').DataTable({
                  processing: true,
                  serverSide: true,
                  orderCellsTop: true,
                  fixedHeader: true,
                  "scrollX": true,
                  "order": [
                      [13, "desc"]
                  ],
      
                  "language": {
                      "src": "../js/Spanish.json"
                  },
                  ajax: url,
                  columns: [{
                          data: 'action',
                          name: 'action',
                          orderable: true,
                          searchable: true ,
                          
                      },
                      {
                          data: 'Profesion',
                          name: 'Profesion',
                      },
                      {
                          data: 'Nombre',
                          name: 'Nombre',
                      },
                      {
                          data: 'ApellidoP',
                          name: 'ApellidoP'
                      },
                      {
                          data: 'ApellidoM',
                          name: 'ApellidoM'
                      },
                      {
                          data: 'concepto',
                          name: 'concepto'
                      },
                      {
                          data: 'Modelo',
                          name: 'Modelo'
                      },
                      {
                          data: 'NoSerie',
                          name: 'NoSerie'
                      },
                      {
                          data: 'NoInventarioAnterior',
                          name: 'NoInventarioAnterior'
                      },
                      {
                          data: 'ClaveBien',
                          name: 'ClaveBien'
                      },
                      {
                          data: 'nombre',
                          name: 'nombre'
                      },
                      {
                          data: 'Estado',
                          name: 'Estado'
                      },
                      {
                          data: 'Caracteristicas',
                          name: 'Caracteristicas'
                      },
                      {
                          data: "fechaAsignacion",
                          name: "fechaAsignacion",
                          "render": function(data) {
                              var date = new Date(data);
                              var month = date.getMonth() + 1;
                              if(month<10){
                                  return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
                              }else{
                                  return date.getDate() + "/" +  month + "/" + date.getFullYear();
                              }
                          }
                      },
                  ]
              });
              
            }

    var bienes = [];
    $(document).on('change', '.casilla', function() {
        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz
            bienes[bienes.length] = $(this).val();
        } else {
            // Si no está marcado, elimine el valor de la matriz
            var index = bienes.indexOf($(this).val());
            if (index > -1) {
                bienes.splice(index, 1);
            }
        }
    });

    $(document).on('change', '.header-casilla', function() {
        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz           
            $('.casilla').each(function() {
                bienes[bienes.length] = $(this).val();
            });
        } else {
            // Si no está marcado, elimine el valor de la matriz
            $('.casilla').each(function() {
                var index = bienes.indexOf($(this).val());
                if (index > -1) {
                    bienes.splice(index, 1);
                }
            });
        }
    });
    $(document).ready(function() {

    $('.casilla').click(function() {
        var $this = $(this),
            fila = $this.closest('tr'),
            tbody = $this.closest('tbody')
        if ($this.is(':checked')) {
            fila.prependTo(tbody);
        } else {
            fila.appendTo(tbody);
        }
    });
    });
  
    function Guardar() {
        document.getElementById("bienes").value = bienes;

        if(!document.getElementById("bienes").value){
            var data = table.rows().nodes();
            data.each(function (value, index) {
            var valor = value.cells[0].children[0].value;
            var check = value.cells[0].children[0].checked;
            if (check)
            {
                bienes.push(valor);
            }            
        }); 

        }
        document.getElementById("bienes").value = bienes;

        if ($("#formTemporal").valid()) {
            var formData = new FormData(document.getElementById("formTemporal"));

            $.ajax({
                url: " {{ route('prueba') }}",
                type: "post",
                dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(res) {

                    if (res == "1") {
                        alert("Actualizado con exito");
                        window.location.reload(); 

                    }
                    else{
                        alert("ocurrio un error");
                    }

                },
                error: function(xhr, status) {
                    console.log("error");
                    console.log(xhr);
                    console.log(status);
                },
                complete: function(xhr, status) {

                }
            })
        } else {
            alert("falta acompletar campos");
        }
    }

</script>
@stop
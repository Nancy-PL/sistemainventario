@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h3 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated"> BIENES DE CÓMPUTO Y ACCESORIOS SALIENTES</h3>
@stop

@section('content')
<div class="card-header">
@foreach($Salidas as $Resguardo)
        <label for="personal" class="ml-5">Referencia: {{$Resguardo->referencia}}</label>
        @break
        @endforeach
</div>

<!-- Listado de resguardo -->
<table style="text-transform:uppercase;" id="resguardo" class="table table-striped yajra-datatable">
    <thead class="table-success text-black table-sm">
        <tr>
          
            <th scope="col">Nombre</th>
            <th scope="col">Clasificación</th>
            <th scope="col">Modelo</th>
            <th scope="col">No de serie </th>
            <th scope="col">No Inventario Anterior</th>
            <th scope="col">No Inventario</th>
            <th scope="col">Marca</th>
            <th scope="col">Estado</th>
            <th scope="col">Características</th>
            <th scope="col">Fecha</th>
        </tr>
    </thead>
    <tbody>
        
    <tr>
        @php $b=0; @endphp
        @foreach ($Salidas as $Resguardo)
        @php $b=1; @endphp           
            <td>{{ $Resguardo->Nombre}} {{ $Resguardo->ApellidoP}} {{ $Resguardo->ApellidoM}}</td>
            <td>{{ $Resguardo->concepto}}</td>
            <td>{{ $Resguardo->Modelo}}</td>
            <td>{{ $Resguardo->NoSerie}}</td>
            <td>{{ $Resguardo->NoInventarioAnterior}}</td>
            <td>{{ $Resguardo->ClaveBien}}</td>
            <td>{{ $Resguardo->nombre}}</td>
            <td>{{ $Resguardo->Estado}}</td>
            <td>{{ $Resguardo->Caracteristicas}}</td>
            <td>{{ $Resguardo->fechaAsignacion}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</form>
@stop


@section('css')
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/dataTables.bootstrap4.min.css">

@stop

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    // Paginación y estilo de la tabla área mediante ajax
    $(function() {
        var table = $('.yajra-datatable').DataTable({
               
                "scrollX": true,
                "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "All"]
            ],
            "order": [
                [0, "desc"]
            ],

            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
           
        });

    
    });
</script>
@stop
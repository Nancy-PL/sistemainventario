@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">SALIDA DE EQUIPOS</h1>


@stop
@section('content')

<div class="card-header">
    <!-- Mensaje del No. de inventario o No. de control -->
    <div class="row">
        @if($message=Session::get('Listo'))
        <div class="col-12 alert-success alert-dismissable fade show" role="alert">
            <h5>Registro correcto</h5>
            <span>{{$message}}</span>
        </div>
        @endif
    </div>
    <!--Imprimiendo los valores de las validaciones-->
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $messages)
            <li>{{ $messages }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @foreach($resguardos as $resguardo)
    <label for="personal" class="ml-5">{{ $resguardo->Nombre}} {{ $resguardo->ApellidoP}} {{ $resguardo->ApellidoM}}</label>
    @break
    @endforeach

</div>
<!-- Nombre del personal -->
<!--Select de la lista de personal-->
<nav class="navbar navbar-light " style="width: 300px;">
    <form class="form-inline">
        <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->
        <div class=" form-group">
            <div class="row ">
                <select class="js-example-basic-single col-md-4" name="areaa" id="areaa" onchange="idcambiaA()" required>
                    <option value="" selected>Seleccionar Área</option>
                    @foreach ($areas as $area)
                    <option value="{{$area->id}}" @if($area->id == $idArea) selected @endif>{{$area->Nombre}}</option>
                    @endforeach
                </select>
                <button class="btn btn-outline-success mr-5" id="AreaB" type="submit">Buscar</button><br>

                <select class="js-example-basic-single " onchange="idcambia()" id="resguardante" name="resguardante" type="search">
                    <option value="" selected>Seleccionar nombre del resguardante</option>
                    @foreach ($personas as $persona)
                    <option value="{{$persona->id}}" data-bien="{{$persona->area_id}}">{{$persona->Nombre}} {{$persona->ApellidoP}} {{$persona->ApellidoM}}</option>
                    @endforeach
                </select>

                <textarea style="display:none;" id="Referencias" class="form-control" value="" type="text" name="Referencias">{{$Referencia}}</textarea>
                <input type="hidden" id="dates" value="{{$FechaVencimiento}}" name="dates" />
                <input type="hidden" id="Notificacion" value="{{$FechaNotificacion}}" name="Notificacion" />
                <input type="hidden" id="ids" value="{{$id}}" name="ids" />
                <input type="hidden" id="area" value="{{$idArea}}" name="area" />
                <input type="hidden" id="nombre" value="{{$nombre}}" name="nombre" />

            </div>
            <!--buscar el resguardo de la persona-->
            <button class="btn btn-success ml-3" type="submit">Buscar</button><br>

        </div>
    </form>
</nav>
<form method="POST" action="SalidaEquipo" class="was-validated">
    <!--El csrf es un token se utiliza para verificar que el usuario autenticado es la persona que realmente realiza las solicitudes a la aplicación. -->
    @csrf
    <!--Bienes seleccionados-->
    <input class="bien" type="hidden" id="bienes" name="bienes" vagit addlue="">
    <input type="hidden" id="id" value="{{Session::get('id')}}" name="id" />

    <div class="form-row">
        <!-- Referencia  -->
        <div class="col-md-4 mb-3">
            <x-label for="Referencia" class="mt-3" :value="__('Referencia:')" />
            <textarea id="Referencia" placeholder="Ingrese la referencia" class="form-control" type="text" name="Referencia" style="text-transform:uppercase;" maxlength="270" onchange="Ref()" tabindex="1" required autofocus>{{Session::get('Referencia')}}</textarea>
        </div>
        <!-- Fecha del sistema-->
        <div class="col-md-2 mb-3 ml-4 mt-4">
            <x-label for="start" class="mt-3">Fecha:</x-label>
            <input type="date" id="FechaNotificacion" value="{{Session::get('FechaNotificacion')}}" name="FechaNotificacion" onchange="date()" tabindex="1" required autofocus />
            <input type="hidden" id="usuarioname" value="{{Session::get('nombre')}}" name="usuarioname" tabindex="1" />

        </div>
        <!-- obtener fecha de vencimiento  -->
        <div class="col-md-3 ml-3 mt-4">
            <x-label for="start" class="mt-3">Fecha de Vencimiento: </x-label>
            <input type="date" id="FechaVencimiento" value="{{Session::get('FechaVencimiento')}}" name="FechaVencimiento" onchange="date()" tabindex="2" required autofocus />
        </div>
    </div>



    <!-- Listado de Bienes -->
    <table id="bienesR" class="table table-bordered  shadow-lg display nowrap   mt-4 " style="width:100%">
        <thead class="table-success text-black">
            <tr>
                <th scope="col"><label><input type="checkbox" id="checkTodos" /></th>
                <th scope="col">Clasificación</th>
                <th scope="col">Modelo</th>
                <th scope="col">No. de serie </th>
                <th scope="col">No. Inventario Anterior</th>
                <th scope="col">No. Inventario</th>
                <th scope="col">Marca</th>
                <th scope="col">Estado</th>
                <th scope="col">Características</th>
                <th scope="col">Fecha Asignación</th>
            </tr>
        </thead>
        <tbody style="text-transform:uppercase;">
            <tr>
                @foreach ($resguardos as $resguardos)
                @if($resguardos->concepto)
                @php $valor=0 @endphp
                @foreach ($Salidas as $Salida)
                @if ($Salida->bien_id == $resguardos->bien_id)
                @php $valor=1 @endphp
                @endif
                @endforeach

                @if($valor>=0)
                <td data-order="0">
                    <input class="casilla" type="checkbox" name="biens[]" value="{{$resguardos->bien_id}}">
                    @csrf
                </td>
                <td>{{ $resguardos->concepto}}</td>
                <td>{{ $resguardos->Modelo}}</td>
                <td>{{ $resguardos->NoSerie}}</td>
                <td>{{ $resguardos->NoInventarioAnterior}}</td>
                <td>{{ $resguardos->ClaveBien}}</td>
                <td>{{ $resguardos->nombre}}</td>
                <td>{{ $resguardos->Estado}}</td>
                <td>{{ $resguardos->Caracteristicas}}</td>
                <td>{{$resguardos->fechaAsignacion}}</td>
                @endif
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="row justify-content-md-center ">
        <button class="btn btn-success  salida" type="submit">Agregar</button>
    </div>
    <div class="row justify-content-md-center ">
        <a onclick="Finalizar();" class="btn btn-outline-success mt-3">Finalizar</a>
    </div>
</form>
@stop


@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/sweetalert2.min.css">
@stop

@section('js')
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/select2.min.js"></script>
<!--Estilos del select2-->
<script>
    // In your Javascript (external .js resource or <> tag)
    $(document).ready(function() {
        $('.js-example-basic-single').select2({
            theme: "classic"
        });
     
    });

    $(document).ready(function() {
        var tableB = $('#bienesR').DataTable({
            "scrollX": true,
            "order": [
                [9, "desc"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }

        });

        $(document).ready(function() {

            $('.casilla').click(function() {
                var $this = $(this),
                    fila = $this.closest('tr'),
                    tbody = $this.closest('tbody')
                if ($this.is(':checked')) {
                    fila.prependTo(tbody);
                } else {
                    fila.appendTo(tbody);
                }
            });
        });
        //seleccionar todos
        $("#checkTodos").change(function() {
            $(':checkbox', tableB.rows().nodes()).prop('checked', this.checked);
            bienes = [];
            if ($(this).is(':checked')) {
                // si está marcado agregar a la matriz           
                $('.casilla', tableB.rows().nodes()).each(function() {
                    bienes[bienes.length] = $(this).val();
                });
            } else {
                // Si no está marcado, elimine el valor de la matriz
                $('.casilla', tableB.rows().nodes()).each(function() {
                    var index = bienes.indexOf($(this).val());
                    if (index > -1) {
                        bienes.splice(index, 1);
                    }
                });
            }
        });

    });

    function Finalizar() {
        var id = document.getElementById("id").value;
        var area = document.getElementById("area").value;

        if (id) {
            window.location.href = "finalizar/" + id + '/' + area;
        } else {
            swal({
                title: "Ups",
                text: "Primero inserte una salida.",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: '#0F8971',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: "Cancelar"
            });
        }
    }

    function Ref() {
        var ref = document.getElementById("Referencia").value;
        document.getElementById("Referencias").value = ref;
    }

    function date() {
        var dateS = document.getElementById("FechaVencimiento").value;
        document.getElementById("dates").value = dateS;
        var dateN = document.getElementById("FechaNotificacion").value;
        document.getElementById("Notificacion").value = dateN;
    }

    function cambiaArea(sel) {
        if (sel.value) {
            $('#resguardante').removeAttr('disabled');

        } else {
            $('#resguardante').prop('disabled', 'disabled');

        }
    }

    function idcambia() {
        var id = document.getElementById("id").value;
        document.getElementById("ids").value = id;

        var ref = document.getElementById("Referencia").value;
        document.getElementById("Referencias").value = ref;

        var dateS = document.getElementById("FechaVencimiento").value;
        document.getElementById("dates").value = dateS;

        var dateN = document.getElementById("FechaNotificacion").value;
        document.getElementById("Notificacion").value = dateN;
    }

    function idcambiaA() {
        var id = document.getElementById("areaa").value;
        document.getElementById("area").value = id;
    }


    window.onload = function() {
        var id = document.getElementById("ids").value;
        var ref = document.getElementById("Referencias").value;
        var dateS = document.getElementById("dates").value;
        var dateN = document.getElementById("Notificacion").value;
        var area = document.getElementById("area").value;
        var nombre = document.getElementById("nombre").value;

        if (ref) {
            document.getElementById("Referencia").value = ref;
        }
        if (dateS) {
            document.getElementById("FechaVencimiento").value = dateS;
        }
        if (dateN) {
            document.getElementById("FechaNotificacion").value = dateN;

        }
        if (id) {
            document.getElementById("id").value = id;
        }
        if (area != "") {
            $('#areaa').prop('disabled', 'disabled'); //deshabilitar tipo de resguardo
            $('#AreaB').prop('disabled', 'disabled'); //deshabilitar tipo de resguardo
        }
        if (nombre != null || nombre != '') {
            document.getElementById("usuarioname").value = nombre;
        }else{
            document.getElementById("usuarioname").value = "";
        }
    }

    var bienes = [];
    $(document).on('change', '.casilla', function() {
        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz
            bienes[bienes.length] = $(this).val();
        } else {
            // Si no está marcado, elimine el valor de la matriz
            var index = bienes.indexOf($(this).val());
            if (index > -1) {
                bienes.splice(index, 1);
            }
        }
    });

    $(document).on('change', '.header-casilla', function() {
        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz           
            $('.casilla').each(function() {
                bienes[bienes.length] = $(this).val();
            });
        } else {
            // Si no está marcado, elimine el valor de la matriz
            $('.casilla').each(function() {
                var index = bienes.indexOf($(this).val());
                if (index > -1) {
                    bienes.splice(index, 1);
                }
            });
        }
    });

    // al dar click en submit asignar los valores del array se envian al formulario
    $('.salida').click(function() {
        var bien = '';
        // leer bienes usando for loop
        // alert(bienes);
        document.getElementById("bienes").value = bienes;
    });
</script>
@stop
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">

    <title>Resguardo Temporal</title>
    <style>
        .page-break {
            page-break-inside: avoid;
        }
    </style>
    <style>
        /*estilos de pagina*/
        @page {
            margin: 100px 50px;
        }

        body {
            margin-top: 50px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            /** Extra estilos **/
            font-family: Arial;
            font-size: 12px;
            color: black;
            line-height: 5px;
        }


        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            height: 50px;
            color: black;
            line-height: 35px;

        }

        /*Estilos de la tabla de bienes salientes */
        #resguardo {
            font-family: Arial;
            border-collapse: collapse;
            line-height: 100%;
            height: auto;

        }

        #resguardo th {
            padding-top: 3px;
            padding-bottom: 3px;
            font-size: 7pt;
            text-align: left;
            background-color: #b4b4b4;
            color: #000;
            font-weight: normal;

        }

        #resguardo td {
            font-size: 8pt;
            font-weight: bold;

        }

        #resguardo td,
        #resguardo th {
            border: 1px solid #000;
            padding: 5px;
        }

        /*Estilos de la tabla del resguardo temporal */
        #ResguardoTemp {
            font-family: arial narrow;
            ;
            border-collapse: collapse;
            line-height: 100%;
            height: auto;

        }

        #ResguardoTemp th {
            padding-top: 3px;
            padding-bottom: 3px;
            font-size: 8pt;
            text-align: left;
            background-color: #b4b4b4;
            color: #000;
            font-weight: normal;

        }

        #ResguardoTemp td,
        #ResguardoTemp th {
            border: 1px solid #000;
            padding: 5px;
            font-size: 8pt;

        }

        #ResguardoTemp tfoot {
            font-family: arial narrow;
            border-collapse: collapse;
            font-size: 10pt;
            font-weight: bold;
            padding-top: 5px;
            padding-bottom: 5px;
            text-align: left;
            background-color: #b4b4b4;
            color: #000;
        }

        #ResguardoTemp td,
        th {
            padding: 5px;
        }

        /*Estilos de la tabla de firmas de autorización */
        #atentamente {
            font-family: arial narrow;
            border-collapse: collapse;
            font-size: 8pt;
            height: auto;
            border-spacing: 0px 0px;
            font-weight: normal;

        }

        #atentamente td,
        #atentamente th {
            border: 0px solid #fff;
            padding: 0px;
            border-top: 0px;
            border-right: 0px;
            border-left: 0px;
        }

        #atentamente th,
        td {
            text-align: center;
        }

        /**estilos de encabezado */
        p.encabezado {
            font-family: arial narrow;
            text-align: center;
            font-size: 10pt;
            line-height: 0pt;

        }

        /**estilos de encabezado */
        p.encabezadoA {
            font-family: Arial;
            text-align: center;
            font-size: 12pt;
            line-height: 1pt;

        }

        b.adobe {
            font-family: adobe garamond;
            font-size: 14pt;
            line-height: 1pt;
        }

        /**estilos de parrafos */
        p.centrado {
            padding-top: 0px;
            font-family: Arial;
            line-height: 1pt;
            text-align: center;
            font-size: 12px;
        }

        p.derecha {
            margin-top: 20px;
            font-family: Arial;
            line-height: 1.5pt;
            text-align: right;
            font-size: 12pt;
        }

        p.fecha {
            font-family: Arial;
            line-height: 1.5pt;
            text-align: right;
            font-size: 11pt;
        }

        p.justificar {
            font-family: Arial;
            line-height: 10pt;
            text-align: justify;
            font-size: 10pt;
        }

        p {
            font-family: Arial;
            font-size: 12px;
        }
    </style>
</head>

<body>
    <header>
        <p class="centrado">
            <img src="images/osfe.JPG" align="center" width="600"><br>
        <p class="encabezadoA"><b>UNIDAD DE TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIONES</b></p>
        <p class="encabezado"><b>RESGUARDO TEMPORAL PARA PRESTAMOS DE EQUIPOS</b></p>
        </p>
    </header>
    <footer>
        <script type="text/php">
            if ( isset($pdf) ) {
            $pdf->page_script('
                $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                $pdf->text(270, 820, "$PAGE_NUM / $PAGE_COUNT", $font, 10);
            ');
        }
    	</script>
    </footer>
    <main>
        <section>
            @foreach ($temporal as $Resguardo_temporal)
            <p class="derecha">Folio No: <b class="adobe"> {{$Resguardo_temporal->folio}}</b>
            </p>
            @break
            @endforeach
            <!--fecha-->
            @php
            $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            @endphp

            <p class="fecha">Fecha: {{date ('d')}} de {{$meses[date('n')-1]}} de {{date ('Y')}}</p>
            <!--Compromiso-->
            <p class="justificar">Recibí del departamento de Servicios Informáticos el siguiente equipo de cómputo, el cual me ha sido asignado temporalmente, por lo que me comprometo a utilizarlo exclusivamente para auxiliarme en las actividades que me han sido encomendadas, así como protegerlo de todo daño lógico y físico al que pueda estar expuesto y entregarlo en la fecha acordada, en las mismas condiciones en que me fue entregado.</p>

        </section>
        <section>
            <!-- Listado de resguardo -->
            <table align="center" id="resguardo" style="text-transform: uppercase;">
                <thead>
                    <tr>
                        <th>EQUIPO</th>
                        <th>MARCA</th>
                        <th>MODELO</th>
                        <th>SERIE</th>
                        <th>CARACTERÍSTICAS</th>
                        <th>INVENTARIO</th>
                    </tr>
                </thead>
                <tbody>
                    @php $bandera=0;
                    $i=1;
                    @endphp
                    @foreach ($resguardo as $Resguardo)
                    <tr>
                        <td>{{ $Resguardo->concepto}}</td>
                        <td>{{ $Resguardo->nombre}}</td>
                        <td>{{ $Resguardo->Modelo}}</td>
                        <td>{{ $Resguardo->NoSerie}}</td>
                        <td>{{ $Resguardo->Caracteristicas}}</td>
                        <td>{{ $Resguardo->NoInventarioAnterior}} / {{ $Resguardo->ClaveBien}}</td>
                    </tr>
                    @endforeach
                </tbody>
                <!--Información de Resguardo temporal-->
            </table><br>
        </section>
        <section>
            <table class="page-break" align="center" style="text-transform: uppercase;" id="ResguardoTemp">
                <thead>
                    <tr>
                        <th>NO. DE ORDEN DE AUDITORÍA O COMISIÓN</th>
                        <th>FECHA DE LA ORDEN</th>
                        <th>FECHA DE INICIO</th>
                        <th>FECHA DE VENCIMIENTO</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($temporal as $Resguardo_temporal)
                    <tr>
                        <td><b>{{$Resguardo_temporal->No_orden}}</b></td>
                        <td><b>{{date('d-m-Y', strtotime($Resguardo_temporal->fecha_orden))}}</b></td>
                        <td><b>{{date('d-m-Y', strtotime($Resguardo_temporal->fecha_inicio))}}</b></td>
                        <td><b>{{date('d-m-Y', strtotime($Resguardo_temporal->fecha_vencimiento))}}</b></td>
                    </tr>
                    @break
                    @endforeach
                </tbody>
                <tfoot>

                    <tr>
                        <th colspan="4"> <b>FECHA DE DEVOLUCIÓN ACORDADA: {{date('d-m-Y', strtotime($Resguardo_temporal->fecha_devolucion))}} </b></th>
                    </tr>

                </tfoot>
            </table>
        </section>
        <section>
            <!--Observaciones-->
            @foreach ($temporal as $Resguardo_temporal)
            <p class="justificar">Observaciones:<b> {{$Resguardo_temporal->Observaciones}}</b></p>
            @break
            @endforeach
            <p class="justificar"><i>
                    Queda bajo mi responsabilidad el uso adecuado del equipo bajo mi resguardo, así como el comunicar de manera inmediata cualquier contingencia; así mismo, en caso de renuncia o baja, quedo comprometido a devolver el equipo a la Unidad de Tecnologías de Información y Comunicaciones. En caso de no hacerlo, asumiré las responsabilidades a que haya lugar en términos de la Ley de Responsabilidades de los Servidores Públicos del Estado y Municipios.
                    <i />
            </p>
        </section>
        <!--Atentamente-->
        <section>
            <table class="page-break" id="atentamente" style="text-transform: uppercase;" align="center">
                <thead>
                    <tr>
                        <th scope="col">RECIBIÓ</th>
                        <th scope="col">ENTREGÓ</th>
                        <th scope="col">REVISÓ</th>
                        <th scope="col">AUTORIZÓ</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><br><br> <br></td>
                    </tr>
                    <tr>
                        @for ($i = 0; $i < 4; $i++) <td>_________________________</td>
                            @endfor
                    </tr>
                    <tr>
                        @foreach ($resguardo as $resguardo)
                        <td valign="top"><b>{{$resguardo->Profesion}} {{$resguardo->Nombre}} {{$resguardo->ApellidoP}} {{$resguardo->ApellidoM}}</b></td>
                        @break
                        @endforeach
                        @foreach ($Elaboro as $Elaboro)
                        <td valign="top"><b>{{$Elaboro->Profesion}} {{$Elaboro->Nombre}} {{$Elaboro->ApellidoP}} {{$Elaboro->ApellidoM}}</b></td>
                        @break
                        @endforeach
                        @foreach ($admiSI as $area)
                        <td valign="top"><b>{{$area->Responsable}}</b>
                            <br>{{$area->Cargo}}
                        </td>
                        @break
                        @endforeach
                        @foreach ($TitularTI as $area)
                        <td valign="top"><b>{{$area->Responsable}}</b>
                            <br>{{$area->Cargo}}
                        </td>
                        @break
                        @endforeach
                    </tr>
                </tbody>
            </table>
        </section>
    </main>
</body>

</html>

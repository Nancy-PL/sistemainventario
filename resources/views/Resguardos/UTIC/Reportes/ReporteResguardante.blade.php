<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>Resguardo Equipo de Computo y Accesorios</title>
    <style>
        .page-break {
            page-break-inside: avoid;
        }
    </style>
    <style>
        /*estilos de pagina*/
        @page {
            margin: 100px 50px;
        }

        body {
            margin-top: 30px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            /** Extra estilos **/
            font-family: Helvetica;
            font-size: 12px;
            color: black;
            line-height: 5px;
        }


    

        footer {
            position: fixed;
            bottom: -30px;
            left: 0px;
            right: 0px;
            height: 50px;
            color: black;
            line-height: 35px;

        }

         /*Estilos de la tabla de bienes salientes */
         #resguardo {
            font-family: Helvetica;
            border-collapse: collapse;
            font-size: 7pt;
            /* margin-top: 12px;*/
        }

        #resguardo th {
            text-align: left;
            background-color: #b4b4b4;
            color: #000;
        }

        #resguardo td,
        #resguardo th {
            border: 1px solid #000;
            padding: 5px;
            border-collapse: collapse;

        }
        /*Estilos de la tabla del resguardo temporal */
        #ResguardoTemp {
            font-family: Helvetica;
            border-collapse: collapse;
            font-size: 8px;
            line-height: 50%;

        }

        #ResguardoTemp th,
        td {
            font-family: Helvetica;
            border-collapse: collapse;
            padding-top: 5px;
            padding-bottom: 3px;
            text-align: left;
            color: #000;
            border: 5px solid #000;

        }

        #ResguardoTemp tfoot {
            padding-top: 5px;
            padding-bottom: 5px;
            text-align: left;
            background-color: #b4b4b4;
            color: #000;
        }

        #ResguardoTemp td,
        th {
            padding: 5px;
        }

        /*Estilos de la tabla de firmas de autorización */
        #atentamente {
            font-family: Helvetica;
            border-collapse: collapse;
            font-size: 6pt;
            height: auto;
            border-spacing: 0px 0px;
        }

        #atentamente td,
        #atentamente th {
            border: 0px solid #fff;
            padding: 0px;
            padding-left: 15px;
            padding-right: 15px;
            border-top: 0px;
            border-right: 0px;
            border-left: 0px;
        }

        #atentamente th,
        td {
            text-align: center;
        }

        /**estilos de parrafos */
        p.centrado {
            padding-top: 0px;
            font-family: Helvetica;
            line-height: 9pt;
            text-align: center;
            font-size: 9pt;
        }

        p.derecha {
            margin-top: -20px;
            font-family: Helvetica;
            line-height: 1pt;
            text-align: right;
            font-size: 9pt;
        }

        p.justificar {
            margin-top: -10px;
            font-family: Helvetica;
            text-align: justify;
            font-size: 9pt;

        }

        /*estilo img */
        p.encabezadoimg {
            margin-top: -20px;
            margin-left: 0px;
            text-align: left;
        }

        /**estilos de encabezado */
        p.encabezadoA {
            margin-top: 0px;
            margin-left: 120px;
            font-family: Helvetica;
            text-align: left;
            font-size: 9pt;
            line-height: 1pt;

        }

        /**estilos de encabezado */
        p.encabezado {
            margin-top: 0px;
            margin-left: 120px;
            font-family: Helvetica;
            text-align: left;
            font-size: 7pt;
            line-height: 1pt;

        }

        td.derecha {
            font-family: Helvetica;
            line-height: 9pt;
            font-size: 9pt;
            text-align: left;
        }
        tr.derecha {
            margin: 20px;
        }

        b.derecha {
            padding-top: 10px;
            font-family: Helvetica;
            font-size: 9pt;
            text-align: left;

        }
    </style>
</head>

<body>
    <header>
        <p class="encabezadoimg"><img src="images/logotipoOSFE.png" align="left" width="100">
        <p class="encabezadoA"><b>ÓRGANO SUPERIOR DE FISCALIZACIÓN DEL ESTADO DE OAXACA</b>
        <p class="encabezado">UNIDAD DE TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIONES
        <p class="encabezado">DEPARTAMENTO DE SERVICIOS INFORMÁTICOS
        <p class="encabezado">HOJA DE CONTROL INTERNO
            <hr>
    </header>
    <footer>
        <script type="text/php">
        if ( isset($pdf) ) {
            $pdf->page_script('
                $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                $pdf->text(270, 820, " $PAGE_NUM / $PAGE_COUNT", $font, 7);
            ');
        }
    	</script>
    </footer>
    <main class="mains">
        <section>
            <!--fecha-->
            @php
            $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            @endphp

            <p class="derecha">{{date ('d')}} de {{$meses[date('n')-1]}} de {{date ('Y')}}</p><br>
            @foreach ($resguardos as $Resguardo)
            <p class="derecha">No.: <b>R-{{$Resguardo->id}}</b></p>
            @break
            @endforeach
        </section>
        <section>
            <p class="justificar">
                Recibí del departamento de Servicios Informáticos el siguiente equipo de cómputo, el cual me ha sido asignado para auxiliarme en las actividades designadas por lo que me comprometo a utilizarlo para lo antes mencionado, así como protegerlo de todo daño lógico y físico al que pueda estar expuesto.
            </p>
        </section>
        <section>
            <!-- Listado de resguardo -->
            <table id="resguardo" align="center"  style="text-transform: uppercase;" >
                <thead>
                    <tr>
                        <th>EQUIPO</th>
                        <th>MARCA</th>
                        <th>MODELO</th>
                        <th>No. SERIE</th>
                        <th>No. INVENTARIO</th>
                        <th>CARACTERÍSTICAS</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i=0;  $AreaR=""; 
                    @endphp
                    @foreach ($resguardos as $Resguardo)
                    <tr>
                    @php $AreaR=$Resguardo->area;  $i=$i+1; @endphp
                        <td>{{ $Resguardo->concepto}}</td>
                        <td>{{ $Resguardo->nombre}}</td>
                        <td>{{ $Resguardo->Modelo}}</td>
                        @if($Resguardo->NoSerie)
                        <td>{{ $Resguardo->NoSerie}}</td>
                        @else
                        <td>S/N</td>
                        @endif
                        @if($Resguardo->NoInventarioAnterior!='S/N' && $Resguardo->NoInventarioAnterior!='s/n' && $Resguardo->NoInventarioAnterior!='SN' && $Resguardo->NoInventarioAnterior!='sn')
                        <td>  {{ $Resguardo->ClaveBien}}</td>
                        @else
                        <td>{{ $Resguardo->NoInventarioAnterior}} {{ $Resguardo->ClaveBien}}</td>
                        @endif
                        <td>{{ $Resguardo->Caracteristicas}} ESTADO {{ $Resguardo->Estado}} ({{date('d-m-Y', strtotime($Resguardo->fechaAsignacion))}})</td>
                    </tr>
                    @endforeach
                    <tr class="derecha">
                        <td class="derecha" colspan="6">Para su uso en:<b class="derecha"> {{$AreaR}}</b></td>
                    </tr>
                </tbody>
            </table>
        </section>
        <section>
            <p class="centrado"><i><b>Recibo hoja de recomendaciones para prolongar la vida util de la batería de la laptop</i></b> </p>
            <!--Información de Resguardo temporal-->
        </section>
        <section>
            <p class="justificar page-break">
                Queda bajo mi responsabilidad el uso adecuado del equipo bajo mi resguardo, así como el comunicar de manera inmediata
                cualquier contingencia; así mismo, en caso de renuncia o baja, quedo comprometido a devolver el equipo a la Unidad de
                Tecnologías de Información y Comunicaciones en las mismas condiciones en que me fue entregado. En caso de no hacerlo,
                asumiré las responsabilidades a que haya lugar en términos de la Ley de Responsabilidades Administrativas del Estado y
                Municipios de Oaxaca.
            </p>
            <!--Atentamente-->
            <table id="atentamente" class="page-break"  style="text-transform: uppercase;"  align="center">
                <tbody>
                    <tr>
                        <td><br><br><br> </td>
                    </tr>

                    <tr>
                        @for ($i = 0; $i < 3; $i++)
                         <td>__________________________________</td>
                            @endfor
                    </tr>
                    <tr>
                        @foreach ($resguardos as $resguardos)
                        <td><i>{{$resguardos->Profesion}} {{$resguardos->Nombre}} {{$resguardos->ApellidoP}} {{$resguardos->ApellidoM}}</i></td>
                        @break
                        @endforeach
                        @foreach ($Elaboro as $Elaboro)
                        <td><i>{{$Elaboro->Profesion}} {{$Elaboro->Nombre}} {{$Elaboro->ApellidoP}} {{$Elaboro->ApellidoM}}</i></td>
                        @break
                        @endforeach
                        <td><i>{{ $areas->Responsable}}</i></td>
                    </tr>
                    <tr>
                        <td><b>RECIBE EQUIPO</b></td>
                        <td><b>ENTREGA EQUIPO</b></td>
                        <td><b>Vo.Bo.</b></td>
                    </tr>
                </tbody>
            </table>
        </section>
    </main>
</body>

</html>

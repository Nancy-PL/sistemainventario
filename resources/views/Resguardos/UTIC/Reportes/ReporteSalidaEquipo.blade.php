<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>Salida de Equipos</title>
    <style>
        /*estilos de pagina*/
        @page {
            margin: 80px 35px;
        }

        body {
            margin-top: 110px;
        }

        header {
            position: fixed;
            top: -70px;
            left: 0px;
            right: 0px;
            /** Extra estilos **/
            font-family: Arial;
            font-size: 12px;
            color: black;
            line-height: 1.2px;
        }

       

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            height: 50px;
            color: black;
            text-align: center;
            line-height: 5px;
        }
          /*Estilos de la tabla de bienes salientes */
          #resguardo {
            width: 700;
            padding: auto;
            font-family: Arial;
            border-collapse: collapse;
            line-height: 100%;
            
           /* height: auto;*/

        }
        /* Persona*/
        .personaS  {
            padding-top: 5px;
            padding-bottom: 5px;
            font-size: 8pt;
            text-align: left;
            background-color: #b4b4b4;
            color: #000;
            margin: 50px;
        }
        /**Resguardo saliente */
        #resguardo th {
            padding-top: 0px;
            padding-bottom: 0px;
            font-size: 8pt;
            text-align: left;
            background-color: #b4b4b4;
            color: #000;
        }
        #resguardo td {
            font-size: 10pt;

        }

        #resguardo td, tr {
            font-size: 10pt;
        }

        #resguardo td,
        #resguardo th {
            border: 1px solid #000;
            padding: 5px;
        }

         /**persona */
          #personasS {
            padding: auto;
            font-family: Arial;
            border-collapse: collapse;
            line-height: 100%;
            width: 700;

           /* height: auto;*/

        }
         #personasS th {
            padding-top: 0px;
            padding-bottom: 0px;
            font-size: 8pt;
            text-align: left;
            background-color: #b4b4b4;
            color: #000;
        }
        #personasS td {
            font-size: 10pt;
            line-height: 20pt;

        }

        #personasS td, tr {
            font-size: 10pt;
            text-align: left;

        }

        #personasS td,
        #personasS th {
            border: 1px solid #000;
            padding: 5px;           
        }
        .colorP {
            background-color:#b4b4b4;
            page-break-inside: avoid;

        }

         /*Estilos de la tabla de firmas de autorización */
         #atentamente {
            font-family: arial narrow;
            border-collapse: collapse;
            font-size: 8pt;
            height: auto;
            border-spacing: 0px 0px;
            font-weight: normal;
            padding-top: 1px;
            padding-bottom: 1px;
            page-break-inside: avoid;

        }

        #atentamente td,
        #atentamente th {
            border: 0px solid #fff;
            padding: 0px;
            border-top: 0px;
            border-right: 0px;
            border-left: 0px;
            font-size: 8pt;

        }

        #atentamente th,
        td {
            text-align: center;
        }
        /*tabla encabezado*/
        #TablaEncabezado {
            width: 750;
            margin: 0px;
            border-collapse: collapse;
            margin-top: 15px;
        }
        #TablaEncabezado td,
        #TablaEncabezado th {
            padding: 0px;
            border-top: 20px;
            border-right: 0px;
            border-left: 0px;
        }
         /**estilos de encabezado */
         p.encabezadoA {
            font-family: Arial;
            text-align: center;
            font-size: 12pt;
            line-height: 1pt;

        }
      
        /**estilos de encabezado */
        p.encabezado {
            font-family: arial narrow;
            text-align: center;
            font-size: 10pt;
            line-height: 0pt;
        }
        p.fecha {
            font-family: arial narrow;
            text-align: right;
            font-size: 10pt;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        p.Fechas {
            font-family: arial narrow;
            text-align: left;
            font-size: 10pt;
            margin-left: 50px;
            line-height: 100%;
            margin-top: 0px;
            margin-bottom: 0px;


        }
        p.referencia {
            font-family: arial narrow;
            text-align: right;
            font-size: 10pt;
            line-height: 100%;
            border-collapse: collapse;

            margin-top: 0px;
            margin-bottom: 0px;

        }        
        /**estilos de parrafos */
        p.centrado {
            text-align: center;
            font-family: Arial;
            line-height: 50%;
            font-size: 12px;
        }

        p.right {
            text-align: right;
            font-family: Arial;
            line-height: 100%;
            font-size: 12px;
        }
        .page-break {
            page-break-inside: avoid;
        }
    </style>
</head>
<body>

    <header>
        <p class="centrado">
        <img src="images/osfe.JPG" align="center" width="600"><br>
        <p class="encabezadoA"><b>UNIDAD DE TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIONES</b><br>
        </p>
        <p class="encabezado">LISTA DE EQUIPOS QUE SERÁN UTILIZADOS PARA EL DESARROLLO DEL PROGRAMA ANUAL DE AUDITORÍAS, VISITAS E INSPECCIONES {{date ('Y')}}</p><br />&nbsp;<br />&nbsp;<br />&nbsp;
        @foreach ($Resguardo as $Resguardos)
        <table id="TablaEncabezado">
                <thead> 
                </thead>
                <tbody>
                    <tr>
                        <td><p  class="Fechas " width="150px"><b>Fecha de Notificación:</b> {{date('d-m-Y', strtotime($Resguardos->fecha_notificacion))}}</p>

                        </td>
                        <td>     <p class="fecha">Fecha: <b>{{date ('d-m-Y')}}</b></p>

                        </td>
                    </tr>
                    <tr>
                        <td><p width="150px"  class="Fechas"><b>Fecha de vencimiento:</b> {{date('d-m-Y', strtotime($Resguardos->fecha_vencimiento))}}</p>
                        </td>
                        <td><p class="referencia">Referencia:
                        <b>{{$Resguardos->referencia}}</b>
                        </td>
                    </tr>
                   
                </tbody>
            </table>
           @break
            @endforeach
        
    </header>
    <footer>
    <script type="text/php">
        if ( isset($pdf) ) {
            $pdf->page_script('
                $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                $pdf->text(370, 570, "Pág. $PAGE_NUM / $PAGE_COUNT", $font, 8);
            ');
        }
    	</script>
    </footer>
    <main>
            <section>
                 <!-- Listado de resguardo -->
            <table   id="personasS" align="center" style="text-transform: uppercase;">
                <thead >
                    <tr>
                        <th width="5%"></th>
                        <th width="55%">RESPONSABLE</th>
                        <th width="40%">FIRMA</th>
                    </tr>
                </thead>
                <tbody> 

                    <!--imprimir el resguardo -->
                    @php $ResgAnt=0; $NumPersona=0; 
                     $usuario =  array();
                     $existe =0;
                     @endphp
                    @foreach ($Resguardo as $Resguardos)
                        <!--contar personas por bien-->
                        @if($Resguardos->idP!=$ResgAnt)
                          @php $conta=0; $NumPersona= $NumPersona+1;
                           array_push($usuario,$Resguardos->idP);
                           @endphp
                           
                        @foreach ($Resguardo as $Resguards)
                            @if($Resguards->idP==$Resguardos->idP)
                                @php $conta=$conta+1;
                               @endphp
                            @endif
                        @endforeach
                        @php $existe=0;
                               @endphp
                        @foreach ($usuario as $us)
                            @if($Resguards->idP==$us)
                                @php $existe=$existe+1;
                               @endphp
                            @endif
                        @endforeach
                        @endphp
                        @if($existe<=1)
                        {
                            <tr>
                        <td >{{$NumPersona}}   </td>
                        <td >{{$Resguardos->Profesion}} {{ $Resguardos->Nombre}} {{ $Resguardos->ApellidoP}} {{ $Resguardos->ApellidoM}} </td>
                        <td ></td>                 
                        </tr>
                        } @endif
                        @endif
                        @php $ResgAnt=$Resguardos->idP; @endphp
                        @php $existe=0;
                               @endphp
                    @endforeach

                </tbody>
            </table><br>
        
            </section>
           <section>
            <!-- Listado de resguardo -->
            <table   id="resguardo" align="center" style="text-transform: uppercase;">
                <thead >
                    <tr>
                        <th>EQUIPO</th>
                        <th>MODELO Y MARCA</th>
                        <th>SERIE</th>
                        <th>INVENTARIO</th>
                    </tr>
                </thead>
                <tbody> 
                    <!--imprimir el resguardo -->
                    @php $ResgAnt=0; $NumPersona=0; @endphp
                    @foreach ($Resguardo as $Resguardos)
                     <!--contar personas por bien-->
                     @if($Resguardos->idP!=$ResgAnt)
                          @php $conta=0; $NumPersona= $NumPersona+1; @endphp
                          <tr>
                                <td class="colorP" colspan="4" >{{$Resguardos->Profesion}} {{ $Resguardos->Nombre}} {{ $Resguardos->ApellidoP}} {{ $Resguardos->ApellidoM}}</td>

                         </tr>
                        @foreach ($Resguardo as $Resguards)
                            @if($Resguards->idP==$Resguardos->idP)
                                @php $conta=$conta+1; @endphp
                            @endif
                        @endforeach
                        @endif

                    <tr>                        
                        <td>{{ $Resguardos->concepto}}</td>
                        <td>{{ $Resguardos->Modelo}} {{ $Resguardos->nombre}}</td>
                        <td>{{ $Resguardos->NoSerie}}</td>
                        <td>{{ $Resguardos->NoInventarioAnterior}}/ {{ $Resguardos->ClaveBien}}</td>
                        @php $ResgAnt=$Resguardos->idP; @endphp

                    </tr>
                    
                    @endforeach
                </tbody>  
            </table>
        </section>
        <!-- Atentamente  -->
        <section >
            <br>
            <table style="text-transform: uppercase;" id="atentamente "  align="center">
                <thead>
                    <tr>
                        <th scope="col">AUTORIZÓ</th>
                        <th scope="col">REVISÓ</th>
                        <th scope="col">Vo. Bo.</th>
                        <th scope="col">Vo. Bo.</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><br><br></td>
                    </tr>
                    <tr>
                        @for ($i = 0; $i < 4; $i++) <td>_________________________________</td>
                            @endfor
                    </tr>

                    <tr>
                        @foreach ($areas as $area)
                        @if($area->id==$idArea)
                        <td valign="top">{{ $area->Responsable}}<br>
                        {{ $area->Cargo}}</td>
                        @endif
                        @endforeach
                        @foreach ($areas as $area)
                        @if($area->Cargo=="JEFE DE DEPARTAMENTO DE RECURSOS MATERIALES")
                        <td valign="top">{{ $area->Responsable}}<br>
                        {{ $area->Cargo}}</td>
                        @endif
                        @endforeach
                        @foreach ($areas as $area)
                        @if($area->Cargo=="TITULAR DE LA UNIDAD DE TECNOLOGIAS DE LA INFORMACION Y COMUNICACIONES")
                        <td valign="top">{{ $area->Responsable}}<br>
                        {{ $area->Cargo}}</td>
                        @endif
                        @endforeach
                        @foreach($areas as $area)
                        @if($area->Cargo=="TITULAR DE LA UNIDAD DE ADMINISTRACIÓN")
                        <td valign="top">{{ $area->Responsable}}<br>
                        {{ $area->Cargo}}</td>
                        @endif  
                        @endforeach
                    </tr>
                   
                </tbody>
            </table>

        </section>
    </main>
</body>

</html>
@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">ASIGNACIÓN DE UN EQUIPO DE CÓMPUTO Y ACCESORIOS</h1>

<small><h4 class="d-flex justify-content-center mb-4 mt-100 " style="color:#008000 " class="was-validated">LISTA DE BIENES NO ASIGNADOS</h4></small>
@stop

@section('content')
<div class="card-header">
    <!--Imprimiendo los valores de las validaciones-->
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $messages)
            <li>{{ $messages }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
<!--Mensaje-->
<div class="row">
    @if($message=Session::get('Ups'))
    <div class="col-12 alert-danger alert-dismissable fade show" role="alert">
        <h5>El bien no esta disponible:</h5>
        <span>{{$message}}</span>
    </div>
    @endif
</div>
<form id="form" method="POST" action="../AsignacionBienUTIC">
    <!--El csrf es un token se utiliza para verificar que el usuario autenticado es la persona que realmente realiza las solicitudes a la aplicación. -->
    @csrf
    <!-- Nombre del personal -->
    <div class="row justify-content-md-center ">
        <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->
        <div class=" form-group">
            <x-label for="NombreA" class="mt-5 mr-3" :value="__('Nombre completo del personal')" />

            <select class="js-example-basic-single " name="persona_id" required>
                <option value="" selected>Seleccionar Nombre</option>
                @foreach ($personas as $persona)
                <option value="{{$persona->id}}">{{$persona->Nombre}} {{$persona->ApellidoP}} {{$persona->ApellidoM}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <!--Bienes seleccionados-->
    <input class="bienes" type="hidden" id="bienes" name="bienes" value="">
    <!-- Listado de Bienes -->
    <table style="text-transform:uppercase;" id="bienesTable" class="table table-striped yajra-datatable">
        <thead class="table-success text-black table-sm">
            <tr>
            <th scope="col"><label><input type="checkbox" id="checkTodos"/></label></th>
            
                <th scope="col">Clasificación</th>
                <th scope="col">Modelo</th>
                <th scope="col">No de serie </th>
                <th scope="col">No Inventario Anterior</th>
                <th scope="col">No Inventario</th>
                <th scope="col">Marca</th>
                <th scope="col">Estado</th>
                <th scope="col">Características</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <div class="row justify-content-md-center ">

    <button class="btn btn-outline-success ml-3 asignar" type="submit">Asignar</button>
    </div>
</form>
@stop

@section('css')
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" href="../css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../css/select2.min.css">

@stop

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>

<script>
    //<!--Estilos del select2-->
    $(document).ready(function() {
        $('.js-example-basic-single').select2({
            theme: "classic"
        });
    });
    // Paginación y estilo de la tabla área mediante ajax
    $(function() {
        var tableB = $('.yajra-datatable').DataTable({
            processing: true,
           // serverSide: true,
            "order": [
                [1, "desc"]
            ],

            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            ajax: "{{ route('get-AsignacionBienUtic') }}",
            columns: [{
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true

                },
                {
                    data: 'concepto',
                    name: 'concepto'
                },
                {
                    data: 'Modelo',
                    name: 'Modelo'
                },
                {
                    data: 'NoSerie',
                    name: 'NoSerie'
                },
                {
                    data: 'NoInventarioAnterior',
                    name: 'NoInventarioAnterior'
                },
                {
                    data: 'ClaveBien',
                    name: 'ClaveBien'
                },
                {
                    data: 'nombre',
                    name: 'nombre'
                },
                {
                    data: 'Estado',
                    name: 'Estado'
                },
                {
                    data: 'Caracteristicas',
                    name: 'Caracteristicas'
                },

            ]
        });
    //seleccionar todos
        $("#checkTodos").change(function () {
            $(':checkbox', tableB.rows().nodes()).prop('checked', this.checked);
                bienes=[];
                if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz           
            $('.checkboxClass',tableB.rows().nodes()).each(function() {
                bienes[bienes.length] = $(this).val();
            });
        } else {
            // Si no está marcado, elimine el valor de la matriz
            $('.checkboxClass',tableB.rows().nodes()).each(function() {
                var index = bienes.indexOf($(this).val());
                if (index > -1) {
                    bienes.splice(index, 1);
                }
            });
        }   
        });

    });
    
    var bienes = [];
    $(document).on('change', '.checkboxClass', function() {
        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz
            bienes[bienes.length] = $(this).val();
        } else {
            // Si no está marcado, elimine el valor de la matriz
            var index = bienes.indexOf($(this).val());
            if (index > -1) {
                bienes.splice(index, 1);
            }
        }
    });

    $(document).on('change', '.header-checkbox', function() {

        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz           
            $('.checkboxClass').each(function() {
                bienes[bienes.length] = $(this).val();
            });
        } else {
            // Si no está marcado, elimine el valor de la matriz
            $('.checkboxClass').each(function() {
                var index = bienes.indexOf($(this).val());
                if (index > -1) {
                    bienes.splice(index, 1);
                }
            });
        }
    });

    // al dar click en submit asignar los valores del array se envian al formulario
    $('.asignar').click(function() {
        var bien = '';
        // leer bienes usando for loop
       // alert(bienes);
        document.getElementById("bienes").value = bienes;
    });
</script>
@stop
@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h3 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated"> BIENES DE CÓMPUTO Y ACCESORIOS SALIENTES</h3>
@stop

@section('content')
<div class="card-header">
</div>
<div class="card card-success card-outline mr-2 col-lg-10 center" aling="center">
                <div class="card-header">
                    <h5 class="card-title">Salida de Equipos</h5>
                </div>
                <div class="card-body">
                    <p class="card-text">Vencimiento próximo de salidas</p>
                    <!-- Listado de resguardo -->
                    <table id="resguardo" class="table table-striped Salida">
                        <thead class="table-outline-success text-black table-sm">
                            <tr>
                                <th width="60%"scope="col">Referencia</th>
                                <th  scope="col">Fecha De Notificacion</th>
                                <th scope="col">Fecha De Vencimiento</th>
                                <th scope="col">Detalle</th>
                                <th scope="col">Entregar</th>

                            </tr>
                        </thead>
                        <tbody  style="text-transform:uppercase;">
                        </tbody>
                    </table>
                </div><!-- /.card -->
            </div>
            @stop

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
@stop

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    
// Paginación y estilo de la tabla área mediante ajax
$(function() {
            var table = $('.Salida').DataTable({
                processing: true,
                serverSide: true,
                bInfo: false,
                "order": [
                    [2, "desc"]
                ],
                "lengthMenu": [
                    [10, 20, 50, -1],
                    [10, 20, 50, "All"]
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                },
                ajax: "{{ route('get-getSalidaEquipoRed') }}",
                columns: [{
                        data: 'referencia',
                        name: 'referencia'
                    },
                    {
                        data: "fecha_notificacion",
                        name: "fecha_notificacion",
                        "render": function(data) {
                            var date = new Date(data);
                            var month = date.getMonth() + 1;
                            var dia=date.getDate() +1;
                            return dia + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
                        }
                    },
                    {
                        data: "fecha_vencimiento",
                        name: "fecha_vencimiento",
                        "render": function(data) {
                            var date = new Date(data);
                            var month = date.getMonth() + 1;
                            var dia=date.getDate() +1;

                            return dia + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
                        }
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'entregar',
                        name: 'entregar',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

        });
    </script>
    @stop
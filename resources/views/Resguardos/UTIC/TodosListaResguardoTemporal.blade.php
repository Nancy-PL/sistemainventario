@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
  <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h3 class="d-flex justify-content-center mb-4 mt-100" align="center" style="color:#008000" class="was-validated">LISTADO DE RESGUARDO TEMPORAL DE BIENES EQUIPO DE CÓMPUTO Y ACCESORIOS</h3>
@stop



@section('content')
<div class="card-header">
   <!-- Mensaje del No. de inventario o No. de control -->
   <div class="row">
        @if($message=Session::get('Ups'))
        <div class="col-12 alert-success alert-dismissable fade show" role="alert">
            <span>{{$message}}</span>
        </div>
        @endif
    </div>
</div>

<!-- Listado de resguardo -->
<table style="text-transform:uppercase;" id="resguardo" class="table table-bordered yajra-datatable" >
  <thead class="table-success text-black">
    <tr>
      <th scope="col">Estado</th>
      <th scope="col">Nombre</th>
      <th scope="col">Referencia</th>
      <th scope="col">Fecha de Vencimiento</th>
      <th scope="col">Editar</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>

@stop

@section('css')
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/jquery.dataTables.min.css">
<link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">

@stop

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>


<script>
  // Paginación y estilo de la tabla área mediante ajax
  $(function() {
    var table = $('.yajra-datatable').DataTable({
      processing: true,
      serverSide: true,
      "order": [
        [4, "desc"]
      ],

      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      },
      ajax: "{{ route('get-getResguardoTemp') }}",
      columns: [
        {
          data: 'Activo',
          name: 'Activo',
          "render": function(data) {
           if(data==1){
             return '<span class="badge badge-success">En curso</span>';
           }
            else{
              return '<span class="badge badge-danger">Entregados</span>';
            }
          }
        },
        {
          data:"Nombres",
          name: 'Nombres',
        },
        {
          data: 'No_orden',
          name: 'No_orden'
        },

        {
          data: "fecha_vencimiento",
          name: "fecha_vencimiento",
          "render": function(data) {
            var date = new Date(data);
            var month = date.getMonth() + 1;
            if(month<10){
              return date.getDate() + "/" +(month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
            }else{
              return date.getDate() + "/" + month+ "/" + date.getFullYear();
            }
          }
        },

        {
            data: 'action',
            name: 'action',
            orderable: true,
            "render": function(data) {
                return data;
            }
        },
      ]
    });

  });
</script>
@stop

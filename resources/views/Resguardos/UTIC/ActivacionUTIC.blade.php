@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">ACTIVACIÓN DE RESGUARDO DEL PERSONAL</h1>
@stop



@section('content')
<div class="card-header">
    <!--Imprimiendo los valores de las validaciones-->
    @if($message=Session::get('Listo'))
            <div class="col-12 alert-success alert-dismissable fade show" role="alert">
                <h5>La operación se completó correctamente</h5>
                <span>{{$message}}</span>
            </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $messages)
            <li>{{ $messages }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    </div>
<table id="resguardo" class="table table-bordered  shadow-lg display nowrap   mt-4 " style="width:100%">
    <thead class="table-success text-black"  >

        <tr>
            <th>Activar</th>
            <th scope="col">Profesión</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido Paterno</th>
            <th scope="col">Apellido Materno</th>
            <th scope="col">Fecha de Activación</th>
            <th scope="col">Fecha de Entrega</th>
            
        </tr>
    </thead>
    <tbody style="text-transform:uppercase;">
        <tr>
        @php $b=0; $Ant=0; @endphp
            @foreach ($Resguardo as $resguardos)
            @if($resguardos->idP != $Ant)
            @php $b=1; $Ant=$resguardos->idP; @endphp
            @else
            @php $b=0; @endphp
            @endif

            @if($b==1)
            <td data-order="0">
                       <input  type="radio" id="resguardos" name="resguardos" value="{{$resguardos->resguardo_id}}" required>
                    @csrf
                </td>
            <td>{{$resguardos->Profesion}}</td>
            <td>{{$resguardos->Nombre}}</td>
            <td>{{$resguardos->ApellidoP}}</td>
            <td>{{$resguardos->ApellidoM}}</td>
            <td>{{$resguardos->creacion}}</td>
            <td>{{$resguardos->cancelacion}}</td>
            
            </tr>
            @endif
            @endforeach
    </tbody>
</table>

<a onclick="Activar();" class="btn btn-outline-success mt-3">Activar</a>
@stop

@section('css')
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/jquery.dataTables.min.css">
<link rel="stylesheet" href="css/dataTables.bootstrap5.min.css">

@stop

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap5.min.js') }}"></script>


<script>
    $(document).ready(function() {
         $('#resguardo').DataTable({
            "scrollX": true,
            "order": [[ 6, "desc" ]],

            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }

            });        
    });
    function Activar(){
        var persona = $('input[name="resguardos"]:checked').val();
        
        if (persona) {
            window.location.href = "ActivacionResguardoUtic/"+persona;
        } else {
            alert("Selecciona a un personal");
        }
    }

</script>
@stop

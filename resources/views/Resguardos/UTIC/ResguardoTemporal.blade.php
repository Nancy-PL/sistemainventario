@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">RESGUARDO TEMPORAL</h1>
@stop

@section('content')
<div class="card-header">

    <!--Imprimiendo los valores de las validaciones-->
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $messages)
            <li>{{ $messages }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
<form method="POST" action="../ResguardoTemporal" class="was-validated">
    <!--El csrf es un token se utiliza para verificar que el usuario autenticado es la persona que realmente realiza las solicitudes a la aplicación. -->
    @csrf
    <!-- Nombre del personal -->
    <div class="row justify-content-md-center ">
        <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->
        <div class=" form-group">
            <x-label for="NombreA" class="mt-5 mr-3" :value="__('Nombre del personal a Asignar')" />

            <select class="js-example-basic-single " name="persona_id" tabindex="1" required autofocus>
                <option value="" selected>Seleccionar Nombre</option>
                @foreach ($personas as $persona)
                <option value="{{$persona->id}}" @if($persona->id == Session::get('Resguardo')) selected @endif>{{$persona->Nombre}} {{$persona->ApellidoP}} {{$persona->ApellidoM}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <!-- Número de auditoria o de comisión  -->
    <div class="form-row">
        <div class="col-md-4 mb-3">
            <x-label for="Numero" class="mt-3" :value="__('No. de orden de la auditoría o comisión')" />
            <x-input id="Numero" placeholder=" Ingrese Número" class="form-control" type="text" name="Numero" :value="old('Numero')" style="text-transform:uppercase;" value=""   maxlength="50" tabindex="2" required autofocus />
        </div>
        <!-- obtener fecha  De Orden  -->
        <div class="col-md-2 mb-3">
            <x-label for="fechaOrden" class="mt-3" :value="__('Fecha de Orden')" />
            <input type="date" id="fechaOrden" name="FechaOrden" tabindex="3" data-date-format="DD MMMM YYYY" value="<?php echo date('Y-m-d'); ?>" required autofocus />
        </div>
        <!-- obtener fecha  De inicio  -->
        <div class="col-md-2 mb-3">
            <x-label for="FechaInicio" class="mt-3" :value="__('Fecha de Inicio')" />
            <input type="date" id="FechaInicio" name="FechaInicio" tabindex="3" data-date-format="DD MMMM YYYY" required autofocus />
        </div>
        <!-- obtener fecha  FechaDevolucion-->
        <div class="col-md-2 mb-3">
            <x-label for="FechaDevolucion" class="mt-3" :value="__('Fecha de devolución')" />
            <input type="date" id="FechaDevolucion" data-date-format="DD MMMM YYYY" name="FechaDevolucion" tabindex="4" required autofocus />
        </div>
        <!-- obtener fecha de vencimiento  -->
        <div class="col-md-2 mb-3">
            <x-label for="FechaVencimiento" class="mt-3" :value="__('Fecha de Vencimiento:')" />
            <input type="date" id="FechaVencimiento" data-date-format="DD MMMM YYYY" name="FechaVencimiento" tabindex="5" required autofocus />
        </div>
        <!-- Observaciones -->
        <div class="col-md-5 mb-4">
            <x-label for="Observaciones" class="mt-3" :value="__('Observaciones')" />
            <textarea id="Observaciones" placeholder=" Ingrese las observaciones" class="form-control" type="text" name="Observaciones" :value="old('Observaciones')" style="text-transform:uppercase;" value=""   tabindex="6" maxlength="800"></textarea>
        </div>
    </div>
    
    <!--Bienes seleccionados-->
    <input class="bien" type="hidden" id="bienes" name="bienes" value="">

    <!-- Listado de Bienes -->
    <table style="text-transform:uppercase;" id="bienesEditRT" class="table yajra-datatable">
        <thead class="table-success text-black table-sm">
            <tr>
                <th scope="col">Elegir</th>
                <th scope="col">Profesión</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido Paterno</th>
                <th scope="col">Apellido Materno</th>
                <th scope="col">Clasificación</th>
                <th scope="col">Modelo</th>
                <th scope="col">No. de serie </th>
                <th scope="col">No. Inventario Anterior</th>
                <th scope="col">No. Inventario</th>
                <th scope="col">Marca</th>
                <th scope="col">Estado</th>
                <th scope="col">Características</th>
                <th scope="col">Fecha Asignación</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <div class="row justify-content-md-center ">

        <button class="btn btn-outline-success ml-3 RTemporal" tabindex="7" type="submit">Asignar</button>
    </div>

</form>
@stop

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link href="../css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="../css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="../css/sweetalert2.min.css">
@stop

@section('js')
<script src="../js/sweetalert2.min.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/dataTables.bootstrap4.min.js"></script>
<script src="../js/jquery.validate.js"></script>
<script src="../js/select2.min.js"></script>
<script>
    // In your Javascript (external .js resource or <> tag)
    $(document).ready(function() {
        $('.js-example-basic-single').select2({
            theme: "classic"
        });
    });

    // Paginación y estilo de la tabla área mediante ajax
    $(document).ready(function() {

        var tableB = $('.yajra-datatable').DataTable({
                processing: true,
             //   serverSide: true,
                orderCellsTop: true,
                fixedHeader: true,
                "scrollX": true,
            "order": [
                [13, "desc"]
            ],   "lengthMenu": [
                    [10, 30,50, -1],
                    [10, 30,50, "All"]
                ],
            "language": {
                "src": "../js/Spanish.json"
            },
            ajax: "{{ route('get-ResguardoTemp') }}",
            columns: [{
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
                {
                    data: 'Profesion',
                    name: 'Profesion',
                },
                {
                    data: 'Nombre',
                    name: 'Nombre',
                },
                {
                    data: 'ApellidoP',
                    name: 'ApellidoP'
                },
                {
                    data: 'ApellidoM',
                    name: 'ApellidoM'
                },
                {
                    data: 'concepto',
                    name: 'concepto'
                },
                {
                    data: 'Modelo',
                    name: 'Modelo'
                },
                {
                    data: 'NoSerie',
                    name: 'NoSerie'
                },
                {
                    data: 'NoInventarioAnterior',
                    name: 'NoInventarioAnterior'
                },
                {
                    data: 'ClaveBien',
                    name: 'ClaveBien'
                },
                {
                    data: 'nombre',
                    name: 'nombre'
                },
                {
                    data: 'Estado',
                    name: 'Estado'
                },
                {
                    data: 'Caracteristicas',
                    name: 'Caracteristicas'
                },
                {
                    data: "fechaAsignacion",
                    name: "fechaAsignacion",
                    "render": function(data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        if (month<10){
                            return date.getDate() + "/" +(month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
                        }else{
                            return date.getDate() + "/" + month + "/" + date.getFullYear();
                        }
                    }
                },
            ]
            
        });
    

        $(document).ready(function() {

            $('.casilla').click(function() {
                var $this = $(this),
                    fila = $this.closest('tr'),
                    tbody = $this.closest('tbody')
                if ($this.is(':checked')) {
                    fila.prependTo(tbody);
                } else {
                    fila.appendTo(tbody);
                }
            });
        });
        //seleccionar todos
        $("#checkTodos").change(function() {
        $(':checkbox', tableB.rows().nodes()).prop('checked', this.checked);
        bienes = [];
        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz           
            $('.casilla', tableB.rows().nodes()).each(function() {
                bienes[bienes.length] = $(this).val();
            });
        } else {
            // Si no está marcado, elimine el valor de la matriz
            $('.casilla', tableB.rows().nodes()).each(function() {
                var index = bienes.indexOf($(this).val());
                if (index > -1) {
                    bienes.splice(index, 1);
                }
            });
        }
    });

        //buscar por encabezado
    $(document).ready(function() {
                $('#bienesRT thead tr').clone(true).appendTo( '#bienesRT thead' );
                $('#bienesRT thead tr:eq(1) th').each( function (i) {
                    var title = $(this).text();
                    $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
            
                    $( 'input', this ).on( 'keyup change', function () {
                        if ( table.column(i).search() !== this.value ) {
                            table
                                .column(i)
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );  
            } );
    });
    var bienes = [];
    $(document).on('change', '.casilla', function() {
        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz
            bienes[bienes.length] = $(this).val();
        } else {
            // Si no está marcado, elimine el valor de la matriz
            var index = bienes.indexOf($(this).val());
            if (index > -1) {
                bienes.splice(index, 1);
            }
        }
    });

    $(document).on('change', '.header-casilla', function() {
        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz           
            $('.casilla').each(function() {
                bienes[bienes.length] = $(this).val();
            });
        } else {
            // Si no está marcado, elimine el valor de la matriz
            $('.casilla').each(function() {
                var index = bienes.indexOf($(this).val());
                if (index > -1) {
                    bienes.splice(index, 1);
                }
            });
        }
    });
    // al dar click en submit asignar los valores del array se envian al formulario
    $('.RTemporal').click(function() {
        var bien = '';
        // leer bienes usando for loop
        //alert(bienes);
        document.getElementById("bienes").value = bienes;
    });
</script>
@stop
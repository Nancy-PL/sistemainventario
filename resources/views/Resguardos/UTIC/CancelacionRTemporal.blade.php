@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h3 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">CANCELACIÓN DE RESGUARDO TEMPORAL</h3>
<h3 class="d-flex justify-content-center mb-3 mt-100" style="color:#008000" class="was-validated">Equipo de cómputo y accesorios</h3>

@stop


@section('content')
<!--Errores de validaciones-->
<div class="card-header">
    
    <!--Imprimiendo los valores de las validaciones-->
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $messages)
            <li>{{ $messages }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    @if($message=Session::get('UPS'))
    <div class="col-12 alert-success alert-dismissable fade show" role="alert">
        <h5>Operación exitosa</h5>
        <span>{{$message}}</span>

    </div>
    @endif
</div>

<!-- Mensaje del No. de inventario o No. de control -->
<div class="row">
    <!--Select de la lista de personal-->
    <nav class="navbar navbar-light float-center ml-3" style="width: 900px;">
        <form class="form-inline">
            <div class="row justify-content-md-center ">
                <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->

                <select class="custom-select js-example-basic-single" name="resguardante" type="search" tabindex="1" required>
                    <option value="" selected>Seleccionar resguardante temporal</option>
                    @foreach ($personas as $persona)
                    <option value="{{$persona->id}}" data-bien="{{$persona->id}}">{{$persona->Nombre}} {{$persona->ApellidoP}} {{$persona->ApellidoM}}</option>
                    @endforeach
                </select>
            <!--buscar el resguardo de la persona-->                
            <button class="btn btn-outline-success  float-right" type="submit">Buscar</button><br>

            </div>
        </form>
    </nav>
</div>


<!--Lista de resguardo de la persona selecionada-->
<form action="CancelarResguardoTemp" method="POST">
    <!--token para verificar que el usuario autenticado es quien en realidad está haciendo la petición-->
    @csrf
    <!--Bienes seleccionados-->
    <input class="bien" type="hidden" id="bienes" name="bienes" value="">

    <table id="biensRT" class="table table-bordered  shadow-lg display nowrap   mt-4 " style="width:100%">
        <thead class="table-success text-black">

            <tr>
                <th scope="col"><label><input type="checkbox" id="checkTodos"/></th>
                <th scope="col">Clasificación</th>
                <th scope="col">Modelo</th>
                <th scope="col">No. de serie </th>
                <th scope="col">No. Inventario Anterior</th>
                <th scope="col">No. Inventario Actual</th>
                <th scope="col">Marca</th>
                <th scope="col">Estado</th>
                <th scope="col">Características</th>
                <th scope="col">Observaciones</th>
                <th scope="col">Persona</th>
                <th scope="col">Fecha de resguardo</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                @foreach ($resguardos as $resguardos)
                @if($resguardos->concepto)
                <td>
                    <input class="casilla" type="checkbox" name="biens[]" value="{{$resguardos->bien_id}}">
                    
                </td>
                <td style="text-transform:uppercase;" >{{ $resguardos->concepto}}</td>
                <td style="text-transform:uppercase;">{{ $resguardos->Modelo}}</td>
                <td style="text-transform:uppercase;">{{ $resguardos->NoSerie}}</td>
                <td style="text-transform:uppercase;">{{ $resguardos->NoInventarioAnterior}}</td>
                <td style="text-transform:uppercase;">{{ $resguardos->ClaveBien}}</td>
                <td style="text-transform:uppercase;">{{ $resguardos->nombre}}</td>
                <td style="text-transform:uppercase;">{{ $resguardos->Estado}}</td>
                <td style="text-transform:uppercase;">{{ $resguardos->Caracteristicas}}</td>
                <td style="text-transform:uppercase;">{{ $resguardos->Observaciones}}</td>
                <td style="text-transform:uppercase;">{{ $resguardos->Nombre}} {{ $resguardos->ApellidoP}} {{ $resguardos->ApellidoM}}</td>
                <td style="text-transform:uppercase;">{{$resguardos->fechaAsignacion}}</td>
                @endif

            </tr>
            @endforeach
        </tbody>
    </table>

    <!-- Listado de personas -->
    <div class="row justify-content-md-center ">
        <h5 class="mt-50" class="row justify-content-md-center">Persona a Asignar</h5>
    </div>
    <table id="personal" class="table table-bordered  shadow-lg display nowrap   mt-4 " style="width:100%">
        <thead class="table-success text-black">

            <tr>
                <th scope="col">Profesión</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido Paterno</th>
                <th scope="col">Apellido Materno</th>
                <th scope="col">Fecha de ingreso</th>
                <th scope="col">Seleccionar</th>

            </tr>

        </thead>
        <tbody>
            <!-- Imprimir mediante la consulta en la tabla del personal desde la bd-->
            @foreach ($personas as $persona)
            <tr>
                <td style="text-transform:uppercase;">{{$persona->Profesion}}</td>
                <td style="text-transform:uppercase;" >{{$persona->Nombre}}</td>
                <td style="text-transform:uppercase;">{{$persona->ApellidoP}}</td>
                <td style="text-transform:uppercase;">{{$persona->ApellidoM}}</td>
                <td style="text-transform:uppercase;">{{$persona->created_at}}</td>
                <td>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="persona_id" value="{{$persona->id}}" required>
                    </div>
                    @csrf
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <!--Submit Reasignar resguardo-->
    <div class="row justify-content-md-center ">
        <button type="submit" class="btn btn-dark mt-3 mb-3 col-sm-3 mr-3 Cancelar" class="d-flex justify-content-center">Reasignar</button>
    </div>

</form>
@stop





@section('css')
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/dataTables.bootstrap5.min.css">
<link rel="stylesheet" href="css/select2.min.css">

@stop


@section('js')
<script src="{{ asset('js/jquery-3.5.1.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>



<script>
var obj = $('.js-example-basic-single').data('autocomplete');
obj && (obj._renderItem = function(){

});
    // select 2 para seleccionar nombre del resguardante
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
    // Paginación y estilo de la tabla personal
    $(document).ready(function() {
        $('#personal').DataTable({
            //   "scrollX": true,
            "lengthMenu": [
                [5, 10, 50, -1],
                [5, 10, 50, "All"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "order": [
                [4, "desc"]
            ],
        });
       
    });
    // Paginación y estilo de la tabla bienes
    $(document).ready(function() {
        var tableB= $('#biensRT').DataTable({
            "scrollX": true,
            "lengthMenu": [
                [5, 10, 50, -1],
                [5, 10, 50, "All"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "order": [
                [11, "desc"]
            ],
        });
        
         //seleccionar todos
         $("#checkTodos").change(function () {
                    $(':checkbox', tableB.rows().nodes()).prop('checked', this.checked);
                    bienes = [];
                    if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz           
            $('.casilla', tableB.rows().nodes()).each(function() {
                bienes[bienes.length] = $(this).val();
            });
        } else {
            // Si no está marcado, elimine el valor de la matriz
            $('.casilla', tableB.rows().nodes()).each(function() {
                var index = bienes.indexOf($(this).val());
                if (index > -1) {
                    bienes.splice(index, 1);
                }
            });
        }
            });
    });
    $('.casilla').click(function() {
        var $this = $(this),
            fila = $this.closest('tr'),
            tbody = $this.closest('tbody')
        if ($this.is(':checked')) {
            fila.prependTo(tbody);
        } else {
            fila.appendTo(tbody);
        }
    });
    var bienes = [];
    $(document).on('change', '.casilla', function() {
        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz
            bienes[bienes.length] = $(this).val();
        } else {
            // Si no está marcado, elimine el valor de la matriz
            var index = bienes.indexOf($(this).val());
            if (index > -1) {
                bienes.splice(index, 1);
            }
        }
    });

    $(document).on('change', '.header-casilla', function() {

        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz           
            $('.casilla').each(function() {
                bienes[bienes.length] = $(this).val();
            });
        } else {
            // Si no está marcado, elimine el valor de la matriz
            $('.casilla').each(function() {
                var index = bienes.indexOf($(this).val());
                if (index > -1) {
                    bienes.splice(index, 1);
                }
            });
        }
    });

    // al dar click en submit asignar los valores del array se envian al formulario
    $('.Cancelar').click(function() {
        var bien = '';
        // leer bienes usando for loop
        //alert(bienes);
        document.getElementById("bienes").value = bienes;
    });
</script>
@stop
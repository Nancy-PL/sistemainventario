@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="../images/logotipoOSFE.png" width=" 150px" height="80px">
</div>
<h3 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">RESGUARDO TEMPORAL DE BIENES EQUIPO DE CÓMPUTO Y ACCESORIOS</h3>
@stop



@section('content')
<form action="" method="POST" id="bien">
    <div class="card-header">

    <div class="justify-content-center ml-5">
        @foreach($resguardo as $resguardos)
        <label for="personal" class="ml-5">{{ $resguardos->Nombre}} {{ $resguardos->ApellidoP}} {{ $resguardos->ApellidoM}}</label>
        @break
        @endforeach
    </div>
    </div>
    <a href="../Bienes/create" class="button">
        <i class="fa fa-plus-circle  fa-3x" aria-hidden="true" style="color:#3A3E3C; "></i>
    </a>
    <br>
    <div class="row">
        @if($message=Session::get('Listo'))
        <div class="col-12 alert-success alert-dismissable fade show" role="alert">
            <h5>El número del bien es:</h5>
            <span>{{$message}}</span>
        </div>
        @endif
    </div>

    <!-- Listado de resguardo -->
    <table style="text-transform:uppercase;" id="resguardo" class="table table-striped table-bordered shadow-lg display nowrap" style="width:100%">
        <thead class="table-success text-black table-sm">
            <tr>
                <th scope="col">Clasificación</th>
                <th scope="col">Modelo</th>
                <th scope="col">No de serie </th>
                <th scope="col">No Inventario Anterior</th>
                <th scope="col">No Inventario</th>
                <th scope="col">Marca</th>
                <th scope="col">Estado</th>
                <th scope="col">Características</th>
                <th scope="col">Fecha</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                @foreach ($resguardo as $Resguardo)
                <td>{{ $Resguardo->concepto}}</td>
                <td>{{ $Resguardo->Modelo}}</td>
                <td>{{ $Resguardo->NoSerie}}</td>
                <td>{{ $Resguardo->NoInventarioAnterior}}</td>
                <td>{{ $Resguardo->ClaveBien}}</td>
                <td>{{ $Resguardo->nombre}}</td>
                <td>{{ $Resguardo->Estado}}</td>
                <td>{{ $Resguardo->Caracteristicas}}</td>
                <td>{{ $Resguardo->fechaAsignacion}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="row justify-content-md-center ">

        <a href="../ResguardoTemporal/create" class="btn  btn-outline-dark  mt-3 mb-3 col-sm-3 mr-3">Regresar</a>
        <a href="../ReportePrestamo/{{$id}}" class="button ml-3">
            <i class="far fa-file-pdf fa-3x" aria-hidden="true" style="color:#ff0000; "></i>
        </a>
    </div>
</form>
@stop



@section('css')
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/dataTables.bootstrap4.min.css">

@stop

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    $(document).ready(function() {
        $('#resguardo').DataTable({
            "scrollX": true,
            "lengthMenu": [
                [5, 10, 50, -1],
                [5, 10, 50, "All"]
            ],
            "order": [
                [8, "desc"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }

        });
    });
</script>
@stop

@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')

<h1 class="mt-50" class="row justify-content-md-center">Cancelacion de Resguardos</h1>
@stop

@section('content')
<!-- Listado de personas -->
<table style="text-transform:uppercase;" id="personal" class="table table-bordered  shadow-lg display nowrap   mt-4 " style="width:100%">
    <thead class="table-success text-black"  >

    <tr>
            <th scope="col">Clasificación</th>
            <th scope="col">ubicacion</th>
            <th scope="col">Modelo</th>
            <th scope="col">No de serie </th>
            <th scope="col">No Inventario Anterior</th>
            <th scope="col">No Inventario Actual</th>
            <th scope="col">Marca</th>
            <th scope="col">Estado</th>
            <th scope="col">Caracteristicas</th>
            <th scope="col">Observaciones</th>
            <th scope="col">Status</th>
            <th scope="col">Mejoras al equipo</th>
            <th scope="col">Seleccionar</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            @foreach ($Resguardo as $resguardos)
                <td>{{ $resguardos->concepto}}</td>
                <td>{{ $resguardos->Modelo}}</td>
                <td>{{ $resguardos->NoSerie}}</td>
                <td>{{ $resguardos->NoInventarioAnterior}}</td>
                <td>{{ $resguardos->ClaveBien}}</td>
                <td>{{ $resguardos->nombre}}</td>
                <td>{{ $resguardos->Estado}}</td>
                <td>{{ $resguardos->Caracteristicas}}</td>
                <td>{{ $resguardos->Observaciones}}</td>
                <td>{{ $resguardos->Status}}</td>
                <td>{{ $resguardos->MejorasEquipo}}</td>
                <td>
                <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                <label class="form-check-label" for="flexCheckChecked">
                    Checked checkbox
                </label>
                </div>
                @csrf
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<button type="submit" class="btn btn-outline-success mt-3" class="d-flex justify-content-center">Cancelar</button>
<table id="personal" class="table table-bordered  shadow-lg display nowrap   mt-4 " style="width:100%">
    <thead class="table-success text-black"  >

        <tr>
            <th scope="col">Profesion</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido Paterno</th>
            <th scope="col">Apellido Materno</th>
            <th scope="col">Area</th>
            <th scope="col">Fecha de ingreso</th>
            <th scope="col">Seleccionar</th>
            
        </tr>
        
    </thead>
    <tbody>
        <!-- Imprimir mediante la consulta en la tabla del personal desde la bd-->
        @foreach ($Resguardo as $resguardos)
        <tr>
            <td>{{$resguardos->Profesion}}</td>
            <td>{{$resguardos->Nombre}}</td>
            <td>{{$resguardos->ApellidoP}}</td>
            <td>{{$resguardos->ApellidoM}}</td>
            <td>{{$resguardos->NombreA}}</td>
            <td>{{$resguardos->created_at}}</td>
            <td>
                <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                <label class="form-check-label" for="flexCheckChecked">
                    Checked checkbox
                </label>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<button type="submit" class="btn btn-outline-success mt-4" class="d-flex justify-content-center">Reasignar</button>
@stop

@section('css')
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap5.min.css">
@stop

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    // Paginación y estilo de la tabla personal
    $(document).ready(function() {
        $('#personal').DataTable({
            //"scrollX": true
            "lengthMenu":[[5,10,50,-1],[5,10,50,"All"]]         
        });
    });
</script>
@stop
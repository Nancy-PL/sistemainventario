@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="/images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">ENTREGA DE EQUIPOS SALIENTES </h1>
@stop
@section('content')
<div class="card-header">

    <!--Imprimiendo los valores de las validaciones-->
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $messages)
            <li>{{ $messages }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @foreach($resguardosSalida as $resguardo)
    <label for="personal" class="ml-5">{{ $resguardo->Nombre}} {{ $resguardo->ApellidoP}} {{ $resguardo->ApellidoM}}</label>
    @break
    @endforeach
</div>

<!-- Nombre del personal -->
<!--Select de la lista de personal-->
<nav class="navbar navbar-light" style="width: 300px;">
    <form class="form-inline">
        <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->
        <div class="form-group">
            <div class="row">
                <select class="js-example-basic-single" name="resguardante" type="search" required>
                    <option value="" selected>Seleccionar nombre del resguardante</option>
                    @foreach ($personas as $persona)
                    <option value="{{$persona->id}}" data-bien="{{$persona->id}}">{{$persona->Nombre}} {{$persona->ApellidoP}} {{$persona->ApellidoM}}</option>
                    @endforeach
                </select>
            </div>
            <!--buscar el resguardo de la persona-->
            <button class="btn btn-success ml-3" type="submit">Buscar</button>
        </div>
    </form>
</nav>

<form method="POST" action="/EntregaEquipos" class="was-validated">
    <!--El csrf es un token se utiliza para verificar que el usuario autenticado es la persona que realmente realiza las solicitudes a la aplicación. -->
    @csrf
    <div class="form-row">

        <!-- Fecha del sistema-->
        <div class="col-md-2 mb-3 ml-4 mt-4">
            <x-label for="start" class="mt-3">Fecha de Entrega: </x-label>
            <x-label for="FechaEntrega" id="FechaEntrega" name="FechaEntrega" class="text-success mt-3">{{date ('d/m/Y')}}</x-label>
        </div>

    </div>
    
    <!--Bienes seleccionados-->
    <input class="bien" type="hidden" id="bienes" name="bienes" value="">
    <!-- Listado de Bienes -->
    <table id="bienesE" class="table table-bordered  shadow-lg display nowrap   mt-4 " style="width:100%">
        <thead class="table-success text-black">
            <tr>
                <th scope="col"><label><input type="checkbox" id="checkTodos"/></th>
                <th scope="col">Clasificación</th>
                <th scope="col">Modelo</th>
                <th scope="col">No de serie </th>
                <th scope="col">No Inventario Anterior</th>
                <th scope="col">No Inventario</th>
                <th scope="col">Marca</th>
                <th scope="col">Estado</th>
                <th scope="col">Características</th>
                <th scope="col">Fecha Asignación</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                @php $ResguardoAnterior = 0; @endphp
                @foreach ($resguardosSalida as $resguardos)
                @if($resguardos->concepto)
                <!--leyendo persona-->
                <td data-order="0">
                    <input class="casilla" type="checkbox" name="biens[]" value="{{$resguardos->bien_id}}">
                    @csrf
                </td>
                <td>{{ $resguardos->concepto}}</td>
                <td>{{ $resguardos->Modelo}}</td>
                <td>{{ $resguardos->NoSerie}}</td>
                <td>{{ $resguardos->NoInventarioAnterior}}</td>
                <td>{{ $resguardos->ClaveBien}}</td>
                <td>{{ $resguardos->nombre}}</td>
                <td>{{ $resguardos->Estado}}</td>
                <td>{{ $resguardos->Caracteristicas}}</td>
                <td>{{$resguardos->fechaAsignacion}}</td>

            </tr>
            @endif
            @endforeach
        </tbody>
    </table>
    <div class="row justify-content-md-center ">
        <x-label lass="mt-3">Personal a asignar</x-label>
    </div>
    <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->
    <div class="row justify-content-md-center ">
        <select class="js-example-basic-single" name="resguardanteNuevo" type="search" required>
            <option value="" selected>Seleccionar nombre del resguardante</option>
            @foreach ($personas as $persona)
            <option value="{{$persona->id}}" data-bien="{{$persona->id}}">{{$persona->Nombre}} {{$persona->ApellidoP}} {{$persona->ApellidoM}}</option>
            @endforeach
        </select>
    </div>
    <div class="row justify-content-md-center mt-3">
        <button class="btn btn-outline-success ml-3 Entrega" type="submit">Entregar Equipo</button>
    </div>
    <br>

</form>
@stop

@section('css')
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap5.min.css">
<link rel="stylesheet" href="/css/select2.min.css">

@stop

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>


<!--Estilos del select2-->
<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.js-example-basic-single').select2({
            theme: "classic"
        });
    });

    $(document).ready(function() {
        var tableB=$('#bienesE').DataTable({
            "scrollX": true,
            "order": [
                [9, "desc"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }

        });
        $(document).ready(function() {

            $('.casilla').click(function() {
                var $this = $(this),
                    fila = $this.closest('tr'),
                    tbody = $this.closest('tbody')
                if ($this.is(':checked')) {
                    fila.prependTo(tbody);
                } else {
                    fila.appendTo(tbody);
                }
            });

        });
      //seleccionar todos
      $("#checkTodos").change(function () {
                    $(':checkbox', tableB.rows().nodes()).prop('checked', this.checked);
                    bienes = [];
                    if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz           
            $('.casilla', tableB.rows().nodes()).each(function() {
                bienes[bienes.length] = $(this).val();
            });
        } else {
            // Si no está marcado, elimine el valor de la matriz
            $('.casilla', tableB.rows().nodes()).each(function() {
                var index = bienes.indexOf($(this).val());
                if (index > -1) {
                    bienes.splice(index, 1);
                }
            });
        }
            });
       
    });
        
    
    var bienes = [];
    $(document).on('change', '.casilla', function() {
        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz
            bienes[bienes.length] = $(this).val();
        } else {
            // Si no está marcado, elimine el valor de la matriz
            var index = bienes.indexOf($(this).val());
            if (index > -1) {
                bienes.splice(index, 1);
            }
        }
    });

    $(document).on('change', '.header-casilla', function() {

        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz           
            $('.casilla').each(function() {
                bienes[bienes.length] = $(this).val();
            });
        } else {
            // Si no está marcado, elimine el valor de la matriz
            $('.casilla').each(function() {
                var index = bienes.indexOf($(this).val());
                if (index > -1) {
                    bienes.splice(index, 1);
                }
            });
        }
    });

    // al dar click en submit asignar los valores del array se envian al formulario
    $('.Entrega').click(function() {
        var bien = '';
        // leer bienes usando for loop
       // alert(bienes);
        document.getElementById("bienes").value = bienes;
    });
</script>
@stop
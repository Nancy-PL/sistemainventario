<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>Resguardo General</title>
    <style>
        .page-break {
            page-break-inside: avoid;
        }
    </style>
    <style>
        /*estilos de pagina*/
        @page {
            margin: 70px;
        }

        body {
            margin-top: 60px;
        }

        header {
            position: fixed;
            top: -35px;
            left: 0px;
            right: 0px;
            bottom: 10px;
            /** Extra estilos **/
            font-family: Helvetica;
            font-size: 0px;
            color: black;
            line-height: 1pt;
        }


        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            height: 50px;
            color: black;
            text-align: center;
            line-height: 35px;
        }

        /*Estilos de la tabla de bienes salientes */
        #resguardo {
            padding-top: 0px;
            font-family: Helvetica;
            border-collapse: collapse;
            font-size: 6pt;
            /* margin-top: 12px;*/
        }
        #resguardo th {
            text-align: center;
            background-color: #b4b4b4;
            color: #000;
        }

        #resguardo td,
        #resguardo th {
            border: 1px solid #000;
            padding: 2.0px;
        }
/*Estilos de la tabla de fecha */
        #fecha {

            font-family: Helvetica;
            width: 800;
            padding-top: 0px;
        }
        /*Estilos de la tabla de firmas de autorización */
        #atentamente {
            font-family: Helvetica;
            border-collapse: collapse;
            font-size: 6pt;
            width: 600;
        }

        #atentamente td,
        #atentamente th {
            border: 0px solid #fff;
            padding: 0px;
        }

        #atentamente th,
        td {
            padding-top: 0px;
            padding-bottom: 0px;
            text-align: center;
        }

        /*alinear tabla izq */
        td.derecha {
            font-family: Helvetica;
            font-size: 7pt;
            text-align: left;
        }

        /**estilos de parrafos */
        p.centrado {
            padding-top: 12px;
            font-family: Helvetica;
            line-height: 1pt;
            text-align: center;
            font-size: 9pt;
        }

        p.izq {
            padding-top: 5px;
            font-family: Helvetica;
            line-height: 1pt;
            text-align: left;
            font-size: 9pt;
        }

        p.justificar {
            font-family: Helvetica;
            text-align: justify;
            font-size: 8pt;
            line-height: 10pt;

        }

        /*estilo img */
        p.encabezadoimg {
            margin-top: -20px;
            margin-left: 0px;
            text-align: left;
        }

        /**estilos de encabezado */
        p.encabezadoA {
            margin-top: 0px;
            margin-left: 120px;
            font-family: Helvetica;
            text-align: left;
            font-size: 9pt;
            line-height: 1pt;

        }

        /**estilos de encabezado */
        p.encabezado {
            margin-top: 0px;
            margin-left: 120px;
            font-family: Helvetica;
            text-align: left;
            font-size: 7pt;
            line-height: 1pt;

        }

        .alinearder {
            float: left;
            font-family: Helvetica;
            text-align: left;
            font-size: 7pt;
            line-height: 1pt;
        }

        .alinearizq {

            font-family: Helvetica;
            text-align: left;
            font-size: 7pt;
            line-height: 2pt;
        }
    </style>
</head>

<body>
    <header>
        <p class="encabezadoimg"><img src="images/logotipoOSFE.png" align="left" width="100">
        <p class="encabezadoA"><b>ÓRGANO SUPERIOR DE FISCALIZACIÓN DEL ESTADO DE OAXACA</b>
        <p class="encabezado">UNIDAD DE ADMINISTRACIÓN
        <p class="encabezado">DEPARTAMENTO DE RECURSOS MATERIALES
            <hr>
        <section>

            <p class="centrado"> <b> <br><br>RESGUARDO DE BIENES MUEBLES</b> </p>
             <!--fecha-->
             <table id="fecha">
                <thead>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            @php
                            $meses =
                            array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                            @endphp
                            @foreach ($resguardos as $Resguardo)
                            <p class="alinearizq"><b>ÁREA DE ADSCRIPCIÓN: </b>{{$Resguardo->area}}</p>
                        </td>
                        <td>
                            <p class="alinearder"> No.: R-{{@$NoResguardo->NoResguardo}}
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                            @break
                            @endforeach
                        </td>
                        <td>
                            <p class="alinearder">Oaxaca de Juárez a <b>{{date ('d')}} de {{$meses[date('n')-1]}} de
                                    {{date ('Y')}}</b></p>

                        </td>
                    </tr>

                </tbody>
            </table>
        </section>

    </header>
    <footer>
        <hr>
        <script type="text/php">
            if ( isset($pdf) ) {
            $pdf->page_script('
                $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                $pdf->text(770, 570, " $PAGE_NUM / $PAGE_COUNT", $font, 7);
            ');
        }
    	</script>
    </footer>


    <main class="mains">
        <section>
            <!-- Listado de resguardo -->

            <table id="resguardo" align="center" width="100%">
                <thead>
                    <tr>
                        <th width="3%">N/P</th>
                        <th width="11%" >CLASIFICACIÓN</th>
                        <th width="12%">No. INVENTARIO</th>
                        <th width="8%">No. SERIE</th>
                        <th width="11%">MARCA/MODELO</th>
                        <th  width="25%">CARACTERÍSTICAS</th>
                        <th width="5%">ESTADO FÍSICO</th>
                        <th>FACTURA</th>
                        <th  width="20%">OBSERVACIONES</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i=0;
                    @endphp
                    @foreach ($resguardos as $Resguardo)
                    <tr>
                        @php $i=$i+1; @endphp
                        <td>{{$i}}</td>
                        <td>{{ $Resguardo->concepto}}</td>
                        <td>{{ $Resguardo->ClaveBien}}</td>
                        @if($Resguardo->NoSerie)
                        <td>{{ $Resguardo->NoSerie}}</td>
                        @else
                        <td>S/N</td>
                        @endif
                        <td>{{ $Resguardo->nombre}} / {{ $Resguardo->Modelo}}</td>
                        <td style="text-align:left;">{{ strtoupper($Resguardo->Caracteristicas)}}</td>
                        <td>{{ $Resguardo->Estado}}</td>
                        @if(trim($Resguardo->factura) == "S/F")
                        <td></td>
                        @elseif(trim($Resguardo->factura) != "S/F")
                        <td>{{ $Resguardo->factura}}</td>
                        @else
                        <td>S/N</td>
                        @endif
                        @if($Resguardo->Observaciones=='SIN OBSERVACIONES' &&$Resguardo->ubicacion=='SIN UBICACIÓN')
                        <td></td>
                        @endif
                        @if($Resguardo->Observaciones!='SIN OBSERVACIONES' &&$Resguardo->ubicacion=='SIN UBICACIÓN')
                        <td style="text-align:left;">{{ $Resguardo->Observaciones}} </td>
                        @endif
                        @if($Resguardo->Observaciones=='SIN OBSERVACIONES' &&$Resguardo->ubicacion!='SIN UBICACIÓN')
                        <td style="text-align:left;">{{ $Resguardo->ubicacion}} </td>
                        @endif

                        @if($Resguardo->Observaciones==' SIN OBSERVACIONES ' &&$Resguardo->ubicacion==' SIN UBICACIÓN ')
                        <td></td>
                        @endif
                        @if($Resguardo->Observaciones!=' SIN OBSERVACIONES ' &&$Resguardo->ubicacion==' SIN UBICACIÓN ')
                        <td style="text-align:left;">{{ $Resguardo->Observaciones}} </td>
                        @endif
                        @if($Resguardo->Observaciones==' SIN OBSERVACIONES ' &&$Resguardo->ubicacion!=' SIN UBICACIÓN ')
                        <td style="text-align:left;">{{ $Resguardo->ubicacion}} </td>
                        @endif


                        @if($Resguardo->Observaciones=='	SIN OBSERVACIONES	' &&$Resguardo->ubicacion=='	SIN UBICACIÓN	')
                        <td></td>
                        @endif
                        @if($Resguardo->Observaciones!='	SIN OBSERVACIONES	' &&$Resguardo->ubicacion=='	SIN UBICACIÓN	')
                        <td style="text-align:left;">{{ $Resguardo->Observaciones}} </td>
                        @endif
                        @if($Resguardo->Observaciones=='	SIN OBSERVACIONES	' &&$Resguardo->ubicacion!='	SIN UBICACIÓN	')
                        <td>{{ $Resguardo->ubicacion}} </td>
                        @endif
                        @if($Resguardo->Observaciones!=' SIN OBSERVACIONES ' &&$Resguardo->ubicacion!=' SIN UBICACIÓN '
                        && $Resguardo->Observaciones!='SIN OBSERVACIONES' &&$Resguardo->ubicacion!='SIN UBICACIÓN'
                        && $Resguardo->Observaciones!='	SIN OBSERVACIONES	' &&$Resguardo->ubicacion!='	SIN UBICACIÓN	' )
                        <td style="text-align:left;">{{ $Resguardo->Observaciones}} {{ $Resguardo->ubicacion}} </td>
                        @endif

                    </tr>
                    @endforeach
                    <tr>
                        <td class="alinearizq" colspan="9"> <br><br> <b> Total:{{$i}}</b> </td>
                    </tr>
                </tbody>
                <p></p>
                <br><br>
                <p></p>
   
            </table>
            </section>
                <section class="page-break" >
                <p class="justificar page-break" style=" line-height: 95%;  width: 99%;"><br>
                    El Órgano Superior de Fiscalización del Estado de Oaxaca a través de la Unidad de Administración,
                    con fundamento en lo dispuesto por los artículos 82 fracción III y 83 de la Ley de Fiscalización
                    Superior y Rendición de
                    cuentas para el Estado de Oaxaca, y 19 fracciones IV y V del Reglamento Interior del Órgano Superior
                    de Fiscalización del Estado de Oaxaca, entrega y pone bajo mi resguardo los bienes muebles antes
                    descritos, en las
                    condiciones en que se detallan; por lo que me comprometo a conservarlos y darles el uso para el que
                    están destinados, utilizándolos exclusivamente en las tareas y funciones que desempeño en la
                    institución, informando de
                    manera inmediata al Departamento de Recursos Materiales-Inventarios, la pérdida, robo o extravío de
                    los bienes asignados; así como la renuncia, baja o cambio de adscripción del suscrito, obligándome
                    en este último caso a
                    la entrega inminente de los bienes entregados bajo resguardo. En caso de incumplimiento a lo aquí
                    manifestado el Órgano Superior de Fiscalización del Estado de Oaxaca podrá proceder conforme a lo
                    dispuesto en el título
                    tercero de las faltas administrativas de los servidores públicos y actos de particulares vinculados
                    con faltas administrativas graves, de la Ley de Responsabilidades Administrativas del Estado y
                    Municipios de Oaxaca.
                </p>
            <!--Atentamente-->
            <table id="atentamente" class="page-break"  align="center" style="text-transform: uppercase; width: 100%;;">
                <thead>
                    <tr>
                        <th scope="col">RESGUARDANTE</th>
                        <th scope="col">ELABORÓ</th>
                        <th scope="col">Vo. Bo.</th>
                        <th scope="col">AUTORIZÓ</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><br><br><br></td>
                    </tr>
                    <tr>
                        @for ($i = 0; $i < 4; $i++) <td>___________________________</td>
                            @endfor
                    </tr>
                    <tr>
                        @foreach ($resguardos as $resguardo)
                        <td><i><b>{{$resguardo->Profesion}} {{$resguardo->Nombre}}
                                    {{$resguardo->ApellidoP}} {{$resguardo->ApellidoM}}</b></i> </td>
                        @break
                        @endforeach
                        @foreach ($Elaboro as $Elaboro)
                        <td><i><b>{{$Elaboro->Profesion}} {{$Elaboro->Nombre}} {{$Elaboro->ApellidoP}}
                                    {{$Elaboro->ApellidoM}}</b></i></td>
                        @break
                        @endforeach
                        @foreach ($areas as $area)
                        @if($area->Cargo=="JEFE DE DEPARTAMENTO DE RECURSOS MATERIALES")
                        <td><i><b>{{ $area->Responsable}}</b></i></td>
                        @endif
                        @endforeach
                        @foreach ($areas as $area)
                        @if($area->Cargo=="TITULAR DE LA UNIDAD DE ADMINISTRACIÓN")
                        <td><i><b>{{ $area->Responsable}}</b></i></td>
                        @endif
                        @endforeach
                    </tr>
                    <tr>
                        @if($Cargo)
                        <td>{{$Cargo->Cargo}}</td>
                        @else<td></td>
                        @endif
                        <td></td>
                        @foreach ($areas as $area)
                        @if($area->Cargo=="JEFE DE DEPARTAMENTO DE RECURSOS MATERIALES")
                        <td valign="top">{{ $area->Cargo}}</td>
                        @endif
                        @endforeach
                        @foreach ($areas as $area)
                        @if($area->Cargo=="TITULAR DE LA UNIDAD DE ADMINISTRACIÓN")
                        <td valign="top">{{ $area->Cargo}}</td>
                        @endif
                        @endforeach
                    </tr>
                </tbody>
            </table>
        </section>
    </main>
</body>

</html>

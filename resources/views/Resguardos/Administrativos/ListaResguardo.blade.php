@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">LISTA DE RESGUARDOS DE BIENES MUEBLES</h1>
@stop

@section('content')

<div class="card-header">
    <div class="row justify-content-md-right">
        <a href="Resguardo" class="btn btn-success float-right">Generar Reporte</a>
    </div>
</div>

<a href="AsignacionBien/create" class="fa fa-plus-circle  fa-3x  mb-1"   style="color:#3A3E3C; "></a>
<table id="bienes" class="table table-bordered  yajra-datatable" style="width:100%">
    <thead class="table-success text-black">
        <tr>
            <th scope="col">Profesión</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido Paterno</th>
            <th scope="col">Apellido Materno</th>
            <th scope="col">No. Inventario Actual</th>
            <th scope="col">No. Inventario Anterior</th>
            <th scope="col">Clasificación</th>
            <th scope="col">Marca</th>
            <th scope="col">Modelo</th>
            <th scope="col">No. de Serie </th>
            <th scope="col">No.Folio Fiscal</th>
            <th scope="col">Estado</th>
            <th scope="col">Ubicación</th>
            <th scope="col">Características</th>
            <th scope="col">Observaciones</th>
            <th scope="col">Mejoras al equipo</th>
            <th scope="col">Fecha de Asignación</th>
            
        </tr>
    </thead>
    <tbody  style="text-transform:uppercase;">
      
    </tbody>
</table>
@stop

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">

    @stop

    @section('js')
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/dataTables.bootstrap4.min.js"></script>
    <script src="js/jquery.validate.js"></script>

<script>
    // Paginación y estilo de la tabla área mediante ajax
    $(function() {
        var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                orderCellsTop: true,
                fixedHeader: true,
                "scrollX": true,
            "order": [
                [16, "desc"]
            ],
            bInfo: false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            ajax: "{{ route('get-ResguardoAdmi') }}",
            columns: [{
                    data: 'Profesion',
                    name: 'Profesion'
                },
                {
                    data: 'Nombre',
                    name: 'Nombre'
                },
                {
                    data: 'ApellidoP',
                    name: 'ApellidoP'
                },
                {
                    data: 'ApellidoM',
                    name: 'ApellidoM'
                },
                {
                    data: 'ClaveBien',
                    name: 'ClaveBien'
                },
                {
                    data: 'NoInventarioAnterior',
                    name: 'NoInventarioAnterior',
                    "render": function(data){
                        if (!data){
                           return 'S/N';
                        }
                        else{
                            return data;
                        }
                    }
                },
                {
                    data: 'concepto',
                    name: 'concepto'
                },
                {
                    data: 'nombre',
                    name: 'nombre'
                },
                {
                    data: 'Modelo',
                    name: 'Modelo',
                    "render": function(data){
                        if (!data){
                           return 'S/M';
                        }
                        else{
                            return data;
                        }
                    }
                },
                {
                    data: 'NoSerie',
                    name: 'NoSerie',
                    "render": function(data){
                        if (!data){
                           return 'S/N';
                        }
                        else{
                            return data;
                        }
                    }
                },
                {
                    data: 'factura',
                    name: 'factura',
                    "render": function(data){
                        if (!data){
                           return 'S/F';
                        }
                        else{
                            return data;
                        }
                    }
                },
                {
                    data: 'Estado',
                    name: 'Estado'
                }, 
                {
                    data: 'ubicacion',
                    name: 'ubicacion',
                    "render": function(data){
                        if (!data){
                           return 'SIN UBICACIÓN';
                        }
                        else{
                            return data;
                        }
                    }
                },
                {
                    data: 'Caracteristicas',
                    name: 'Caracteristicas',
                    "render": function(data){
                        if (!data){
                           return 'SIN CARACTERISTICAS';
                        }
                        else{
                            return data;
                        }
                    }
                },
                {
                    data: 'Observaciones',
                    name: 'Observaciones',
                    "render": function(data){
                        if (!data){
                           return 'SIN OBSERVACIONES';
                        }
                        else{
                            return data;
                        }
                    }
                },
                {
                  
                    data: 'MejorasEquipo',
                    name: 'MejorasEquipo',
                    "render": function(data){
                        if (!data){
                           return 'NINGUNA';
                        }
                        else{
                            return data;
                        }
                    }
                },
                {
                    data: "fechaAsignacion",
                    name: "fechaAsignacion",
                    "render": function(data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return date.getDate() + "/" +(month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
                    }
                },
            ]
        });
    //buscar por encabezado
    $(document).ready(function() {
                $('#bienes thead tr').clone(true).appendTo( '#bienes thead' );
                $('#bienes thead tr:eq(1) th').each( function (i) {
                    var title = $(this).text();
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            
                    $( 'input', this ).on( 'keyup change', function () {
                        if ( table.column(i).search() !== this.value ) {
                            table
                                .column(i)
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );  
            } );
    });
</script>
@stop

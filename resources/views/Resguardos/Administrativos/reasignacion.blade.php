@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h3 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">CANCELACIÓN Y REASIGNACIÓN DE RESGUARDOS MUEBLES</h3>
@stop
@section('content')
<!--Errores de validaciones-->
<div class="card-header">
    <!--Imprimiendo los valores de las validaciones-->
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $messages)
            <li>{{ $messages }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
<!-- Mensaje del No. de inventario o No. de control -->
<div class="row">
    @if($message=Session::get('UPS'))
    <div class="col-12 alert-success alert-dismissable fade show" role="alert">
        <h5>Reasignación de bienes exitosa</h5>
        <span>{{$message}}</span>

    </div>
    @endif
</div>
</div>


<div class="form-row  justify-content-md-center ml-5 ">

    <nav class="navbar navbar-light float-center" style="width: 900px;">
        <form class="form-inline">
        @foreach ($areas as $area)
        <input type="hidden" id="almacen" value="{{$area->id}}" name="almacen" />
        <input type="hidden" id="almacenNombre" value="{{$area->Responsable}}" name="almacenNombre" />
        @break
        @endforeach
            <div class="row justify-content-center ">
                <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->           
              
                <div class="form-group mr-3">
                    <select onchange="TipoResguardo(this)" class="custom-select js-example-basic-single " name="EstadoT" id="EstadoT" tabindex="5" required>
                            <option value="PERMANENTE"  selected>PERMANENTE</option>
                            <option value="ALMACEN" @if( "ALMACEN" == $TipoResguardo) selected @endif>ALMACÉN </option>
                        </select>
                </div>

      
                <select name="resguardanteid" id="resguardanteid" class="custom-select js-example-basic-single" tabindex="1" type="search"  style="text-transform:uppercase;" required>
                        <option value="" selected>Seleccionar nombre</option>
                        @foreach ($personas as $persona)
                        <option value="{{$persona->id}}" style="text-transform:uppercase;" data-bien="{{$persona->id}}" @if( $persona->id == $nombre) selected @endif>{{$persona->Nombre}} {{$persona->ApellidoP}} {{$persona->ApellidoM}}</option>
                        @endforeach
                    </select>

                    
                    <!--buscar el resguardo de la persona-->
                    <button class="btn btn-outline-success  float-right" type="submit">Buscar</button><br>                
            </div>
        </form>
    </nav>
</div>

<!--Lista de resguardo de la persona selecionada-->
<form action="reasignacion" method="POST">
    <!--Select de la lista de personal-->
<!--Reesguardos seleccionados-->
<input class="casillas" type="hidden" id="resguardos" name="resguardos" value="">
<input  type="hidden" id="Tipo" name="Tipo" value="">

    <table id="bienes" class="table table-bordered  shadow-lg display nowrap   mt-4 " style="width:100%">
        <thead class="table-success text-black">

            <tr>
                <th scope="col"><label><input type="checkbox" id="checkTodos"/>Elegir</th>
                <th scope="col">persona</th>
                <th scope="col">Clasificación</th>
                <th scope="col">Modelo</th>
                <th scope="col">No de serie </th>
                <th scope="col">No Factura fiscal</th>
                <th scope="col">No Inventario Anterior</th>
                <th scope="col">No Inventario Actual</th>
                <th scope="col">Marca</th>
                <th scope="col">Estado</th>
                <th scope="col">Características</th>
                <th scope="col">Observaciones</th>
                <th scope="col">Fecha de asignación</th>
            </tr>
        </thead>
        <tbody style="text-transform:uppercase;">
            <tr>
                @foreach ($resguardos as $resguardos)
                @if($resguardos->concepto)
                <td><input class="bienResguardo" type="checkbox" name="resguard[]" value="{{$resguardos->bien_id}}">
                    @csrf
                </td>
                <td>{{ $resguardos->Nombre}} {{ $resguardos->ApellidoP}} {{ $resguardos->ApellidoM}}</td>
                <td>{{ $resguardos->concepto}}</td>
                <td>{{ $resguardos->Modelo}}</td>
                <td>{{ $resguardos->NoSerie}}</td>
                <td>{{ $resguardos->factura}}</td>
                <td>{{ $resguardos->NoInventarioAnterior}}</td>
                <td>{{ $resguardos->ClaveBien}}</td>
                <td>{{ $resguardos->nombre}}</td>
                <td>{{ $resguardos->Estado}}</td>
                <td>{{ $resguardos->Caracteristicas}}</td>
                <td>{{ $resguardos->Observaciones}}</td>
                <td>{{$resguardos->fechaAsignacion}}</td>
                <input type="hidden" name="resguardante" value="{{$resguardos->persona_id}}">
                @endif

            </tr>
            @endforeach
        </tbody>
    </table>

    <!-- Listado de personas -->
    <div class="row justify-content-md-center ">
        <h5 class="mt-50" class="row justify-content-md-center">Persona a Asignar</h5>
    </div>
    <table id="personal" class="table table-bordered   yajra-datatable" class="center">
        <thead class="table-success text-black">
            <tr>
               <th scope="col">Seleccionar</th>
                <th scope="col">Profesion</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido Paterno</th>
                <th scope="col">Apellido Materno</th>
                <th scope="col">Fecha de ingreso</th>
            </tr>

        </thead>
        <tbody style="text-transform:uppercase;">
        </tbody>
    </table>
    <!--baja del personal-->
    <div class="row justify-content-md-center ">
        <label><input class="casilla" id="casilla" type="checkbox" class="ml-5 float-right" name="baja" value=""> Dar de baja al personal</label>
    </div>
    <!-- motivo de la baja -->
    <div id="motivo" style="display:none;" class=" row justify-content-md-center mt-3 mr-3">
        <x-label type="hidden" for="motivo" class="mt-3" :value="__('Motivo de la baja')" />
    </div>
    <div id="Motivos" style="display:none;" class=" row justify-content-md-center mt-3 mr-3">
        <x-input id="Motivo" placeholder="Motivo de la baja" class="form-control col-sm-5" type="text" name="Motivo" style=" text-transform:uppercase;" value="" onkeyup="javascript:this.value=this.value.toUpperCase();" tabindex="5" maxlength="250"  required />
    </div>
    <!--Submit Reasignar resguardo-->
    <div class="row justify-content-md-center ">
        <button type="submit" class="btn btn-dark mt-3 mb-3 col-sm-3 mr-3 reasignar" class="d-flex justify-content-center">Reasignar</button>
    </div>

</form>
@stop
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/sweetalert2.min.css">
@stop

@section('js')
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/select2.min.js"></script>


<script>
 // select 2 para seleccionar nombre del resguardante
       
 $(document).ready(function() {
        $('.js-example-basic-single').select2();
        $(this).select2({
            width: 400
        }); // this does not work...

    });
    // Paginación y estilo de la tabla área mediante ajax
    $(function() {
        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            "scrollX": false,
            "order": [
                [5, "desc"]
            ],

            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            ajax: "{{ route('get-reasignacionAdmi') }}",
            columns: [
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
                {
                    data: 'Profesion',
                    name: 'Profesion'
                },
                {
                    data: 'Nombre',
                    name: 'Nombre'
                },

                {
                    data: 'ApellidoP',
                    name: 'ApellidoP'
                },
                {
                    data: 'ApellidoM',
                    name: 'ApellidoM'
                },
                {
                    data: "created_at",
                    name: "created_at",
                    "render": function(data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return date.getDate() + "/" +(month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
                    }
                },
            ]
        });

    });
    // Paginación y estilo de la tabla bienes
    $(document).ready(function() {
        var tableB = $('#bienes').DataTable({
            "scrollX": true,
            "lengthMenu": [
                [5, 10, 50, -1],
                [5, 10, 50, "All"]
            ],
            "order": [
                [12, "desc"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }
        });
    //Checked de bienes
    var bienes = [];
    $(document).on('change', '.bienResguardo', function() {
        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz
            bienes[bienes.length] = $(this).val();
        } else {
            // Si no está marcado, elimine el valor de la matriz
            var index = bienes.indexOf($(this).val());
            if (index > -1) {
                bienes.splice(index, 1);
            }
        }
    });

    $(document).on('change', '.header-bienResguardo', function() {

        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz           
            $('.bienResguardo').each(function() {
                bienes[bienes.length] = $(this).val();
            });
        } else {
            // Si no está marcado, elimine el valor de la matriz
            $('.bienResguardo').each(function() {
                var index = bienes.indexOf($(this).val());
                if (index > -1) {
                    bienes.splice(index, 1);
                }
            });
        }
    });
    //seleccionar todos
    $("#checkTodos").change(function () {
                $(':checkbox', tableB.rows().nodes()).prop('checked', this.checked);
                bienes = [];

                if ($(this).is(':checked')) {
                    // si está marcado agregar a la matriz           
                    $('.bienResguardo', tableB.rows().nodes()).each(function() {
                        bienes[bienes.length] = $(this).val();
                    });
                } else {
                    // Si no está marcado, elimine el valor de la matriz
                    $('.bienResguardo', tableB.rows().nodes()).each(function() {
                        var index = bienes.indexOf($(this).val());
                        if (index > -1) {
                            bienes.splice(index, 1);
                        }
                     });
                }     
        });
   
    // al dar click en submit asignar los valores del array se envian al formulario
    $('.reasignar').click(function() {
        var bien = '';
        // leer bienes usando for loop
       // alert(bienes);
        document.getElementById("resguardos").value = bienes;
    });
        $(document).ready(function() {

            $('.casillas').click(function() {
                var $this = $(this),
                    fila = $this.closest('tr'),
                    tbody = $this.closest('tbody')
                if ($this.is(':checked')) {
                    fila.prependTo(tbody);
                } else {
                    fila.appendTo(tbody);
                }
            });

        });
    });
    $('#casilla').on('change', function() {
        this.value = this.checked ? 1 : 0;
        if (this.value == "1") {
            divC = document.getElementById("motivo");
            divC.style.display = "";

            divT = document.getElementById("Motivos");
            divT.style.display = "";
            $('#Motivo').prop("required", true);

        } else {
            divC = document.getElementById("motivo");
            divC.style.display = "none";

            divT = document.getElementById("Motivos");
            divT.style.display = "none";
            $('#Motivo').removeAttr("required");
        }
    }).change();

    function TipoResguardo(sel) {
        var id = document.getElementById("almacen").value;
        var nombre = document.getElementById("almacenNombre").value;  
        var Tipo   =sel.value;  
        if(Tipo=="ALMACEN"){
            $('#resguardanteid').append('<option value="'+ id+'" selected>' +nombre+ '</option>');
            $('#resguardanteid').prop('disabled', 'disabled');

        }else{
            $("#resguardanteid").find("option[value='']").select();  
            $("#resguardanteid").find("option[value="+id+"]").remove();  
            $('#resguardanteid').removeAttr('disabled');
        }
    }
    window.onload = function() {
        var tipo=document.getElementById("EstadoT").value;
        document.getElementById("Tipo").value=tipo;
       
    }
</script>
@stop
@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">RESGUARDO GENERAL DE BIENES </h1>
@stop
@section('content')
<div class="card-header">
    @foreach($resguardos as $resguardo)
    <label for="personal" class="ml-5">{{ $resguardo->Nombre}} {{ $resguardo->ApellidoP}} {{ $resguardo->ApellidoM}}</label>
    @break
    @endforeach
</div>

<div class="row ml-3">

    <nav class=" ml-3 navbar navbar-light float-right" style="width: 400px;">
        <form class="form-inline">
            <div class="row justify-content-center ">
                <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->
                <div class=" form-group">
                    <select name="resguardante" id="resguardante" class=" custom-select js-example-basic-single" tabindex="1" type="search" required>
                        <option value="" selected>Seleccionar nombre</option>
                        @foreach ($personas as $persona)
                        <option value="{{$persona->id}}" data-bien="{{$persona->id}}">{{$persona->Nombre}} {{$persona->ApellidoP}} {{$persona->ApellidoM}}</option>
                        @endforeach
                    </select>
                    <!--buscar el resguardo de la persona-->
                    <button class="btn btn-outline-success  float-right" type="submit">Buscar</button><br>
                </div>
            </div>
        </form>
    </nav>
</div>

<!--Select de la lista de personal-->
<table id="bienes" class="table table-bordered  shadow-lg display nowrap   mt-4 " style="width:100%">
    <thead class="table-success text-black">

        <tr>
            <th scope="col">Clasificación</th>
            <th scope="col">Modelo</th>
            <th scope="col">No. de serie </th>
            <th scope="col">No. Factura fiscal</th>
            <th scope="col">No. Inventario Anterior</th>
            <th scope="col">No. Inventario Actual</th>
            <th scope="col">Marca</th>
            <th scope="col">Estado</th>
            <th scope="col">Características</th>
            <th scope="col">Observaciones</th>
            <th scope="col">Fecha de asignación</th>
        </tr>
    </thead>
    <tbody>
        @php $b=0; @endphp
        @foreach ($resguardos as $resguardos)
        @if($resguardos->concepto)
        @php $b=1; @endphp
        <tr>
            <td>{{ $resguardos->concepto}}</td>
            <td>{{ $resguardos->Modelo}}</td>
            @if($resguardos->NoSerie)
            <td>{{ $resguardos->NoSerie}}</td>
            @else
            <td>S/N</td>
            @endif
            @if($resguardos->factura)
            <td>{{ $resguardos->factura}}</td>
            @else
            <td>S/N</td>
            @endif
            <td>{{ $resguardos->NoInventarioAnterior}}</td>
            <td>{{ $resguardos->ClaveBien}}</td>
            <td>{{ $resguardos->nombre}}</td>
            <td>{{ $resguardos->Estado}}</td>
            <td>{{ $resguardos->Caracteristicas}}</td>
            <td>{{ $resguardos->Observaciones}}</td>
            <td>{{$resguardos->fechaAsignacion}}</td>
            <input type="hidden" name="resguardante" value="{{$resguardos->persona_id}}">
        </tr>
        @endif
        @endforeach
    </tbody>
</table>
<input type="hidden" id="id" value="{{$id}}" name="id" />
@if($b==1)
<!--Descargar reporte-->
<div class="row justify-content-md-center ">
    <h5>Imprimir</h5>
    <a   onclick="Finalizar();"  href="#" class="button ml-3">
        <i class="far fa-file-pdf fa-3x" aria-hidden="true" style="color:#ff0000; "></i>
    </a>
</div>

@endif

@stop
@section('css')
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/jquery.dataTables.min.css">
<link rel="stylesheet" href="css/dataTables.bootstrap5.min.css">
<link rel="stylesheet" href="css/sweetalert2.min.css">
<link rel="stylesheet" href="css/select2.min.css">


@stop

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>



<script>
    //Estilos del select2
    $(document).ready(function() {
        $('.js-example-basic-single').select2({
            theme: "classic"
        });
    });

    $(document).ready(function() {
        $('#bienes').DataTable({
            "scrollX": true,
            "order": [
                [10, "desc"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }

        });

    });

    function Finalizar() {
        var id = document.getElementById("id").value;
        if (id) {
            window.open("ReporteResguardoGral/" + id, '_blank');
         //   window.location.href = "/ReporteResguardoGral/" + id;
        } else {
            swal({
                title: "Ups",
                text: "Seleccione la persona y busque el resguardo",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: '#0F8971',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: "Cancelar"
            });
        }
    }
</script>
@stop
@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="../images/logotipoOSFE.png" width=" 150px" height="80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">Activación Resguardos Muebles</h1>
@stop



@section('content')
<div class="card-header">
    <!--Imprimiendo errores de la Validaciones de los campos--->
    <!--Imprimiendo los valores de las validaciones-->
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $messages)
            <li>{{ $messages }}</li>
            @endforeach
        </ul>
    </div>
    @endif
     <!--Imprimiendo los valores de las validaciones-->
     <div class="col-12 alert-success alert-dismissable fade show" role="alert">
         <h5>Resguardo Activado</h5>
        @php
             $b = 0;
        @endphp        
        
         @foreach($resguardo as $R)
         @php
             $b = 1;
        @endphp        
        @break
        @endforeach
        @if($b==1)
        <span>Seleccione los bienes para activar.</span>   
        @endif
        @if($b==0)
        <span>Pero no hay bienes disponibles para activar.</span>
        @endif
    </div>
    </div>
</div>
<form method="POST" action="../Activacion">
    <!--token para verificar que el usuario autenticado es quien en realidad está haciendo la petición-->
    @csrf
    <!--Bienes seleccionados-->
    <input type="hidden" name="resguardos" id="resguardos" value="">

    <!-- Listado de Bienes -->
    <table id="resguardo" class="table table-striped table-bordered shadow-lg display nowrap" style="width:100%">
        <thead class="table-success text-black table-sm">
            <tr>
                <th scope="col"><label><input type="checkbox" id="checkTodos"/></th>
                <th scope="col">Resguardante</th>
                <th scope="col">Clasificación</th>
                <th scope="col">Modelo</th>
                <th scope="col">No. de serie </th>
                <th scope="col">No. Inventario Anterior</th>
                <th scope="col">No. Inventario</th>
                <th scope="col">Marca</th>
                <th scope="col">Estado</th>
                <th scope="col">Características</th>
                <th scope="col">Fecha de asignación</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                @foreach ($resguardo as $resguardo)
                <td data-order="0">
                    <input class="bienResguardo" type="checkbox" name="resguards[]" value="{{$resguardo->id_detail}}">
                    @csrf
                </td>
                <td> {{ $resguardo->Nombre}} {{ $resguardo->ApellidoP}} {{ $resguardo->ApellidoM}}</td>
                <td>{{ $resguardo->concepto}}</td>
                <td>{{ $resguardo->Modelo}}</td>
                <td>{{ $resguardo->NoSerie}}</td>
                <td>{{ $resguardo->NoInventarioAnterior}}</td>
                <td>{{ $resguardo->ClaveBien}}</td>
                <td>{{ $resguardo->nombre}}</td>
                <td>{{ $resguardo->Estado}}</td>
                <td>{{ $resguardo->Caracteristicas}}</td>
                <td>{{ $resguardo->fechaAsignacion}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="row justify-content-md-center ">
        @if($b==1)
        <button class="btn btn-outline-success ml-3 Activar" type="submit">Activar</button>
        @endif
        <a class="btn btn-outline-success ml-3" href="../Activacion/">Regresar</a>
    </div>
</form>
@stop

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link href="../css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="../css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="../css/sweetalert2.min.css">
@stop

@section('js')
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/dataTables.bootstrap4.min.js"></script>
<script src="../js/jquery.validate.js"></script>
<script src="../js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        var tableB=$('#resguardo').DataTable({
            "scrollX": true,
            "order": [
                [9, "desc"]
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }
        });
        
        //seleccionar todos
        $("#checkTodos").change(function () {
                    $(':checkbox', tableB.rows().nodes()).prop('checked', this.checked);
                    bienes = [];
                    if ($(this).is(':checked')) {
                        // si está marcado agregar a la matriz           
                        $('.bienResguardo', tableB.rows().nodes()).each(function() {
                            bienes[bienes.length] = $(this).val();
                        });
                    } else {
                        // Si no está marcado, elimine el valor de la matriz
                        $('.bienResguardo', tableB.rows().nodes()).each(function() {
                            var index = bienes.indexOf($(this).val());
                            if (index > -1) {
                                bienes.splice(index, 1);
                            }
                        });
                    }     
            });
    });

  
    var bienes = [];
    $(document).on('change', '.bienResguardo', function() {
        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz
            bienes[bienes.length] = $(this).val();
        } else {
            // Si no está marcado, elimine el valor de la matriz
            var index = bienes.indexOf($(this).val());
            if (index > -1) {
                bienes.splice(index, 1);
            }
        }
    });

    $(document).on('change', '.header-bienResguardo', function() {

        if ($(this).is(':checked')) {
            // si está marcado agregar a la matriz           
            $('.bienResguardo').each(function() {
                bienes[bienes.length] = $(this).val();
            });
        } else {
            // Si no está marcado, elimine el valor de la matriz
            $('.bienResguardo').each(function() {
                var index = bienes.indexOf($(this).val());
                if (index > -1) {
                    bienes.splice(index, 1);
                }
            });
        }
    });

    // al dar click en submit asignar los valores del array se envian al formulario
    $('.Activar').click(function() {
        var bien = '';
        // leer bienes usando for loop
        //alert(bienes);
        document.getElementById("resguardos").value = bienes;
    });
</script>
@stop
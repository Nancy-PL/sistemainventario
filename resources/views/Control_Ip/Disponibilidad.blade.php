@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">LISTADO DE DISPONIBILIDAD DE DIRECCIONES IP</h1>
@stop


@section('content')

<!-- Listado de  Areas -->
<div class="card-header">
</div>
<div class="float-left mb-50 mt-2">

</div>
@can('Areas.create')
<a href="Control_Ip/create" class="button">
</a>
@endcan
<div style="overflow-x:scroll;600px;">

    <table id="Direccionip" class="table table-bordered yajra-datatable " style="text-transform: uppercase;">
        <thead class="table-success text-black">
            <tr>
                <th>Ip</th>
                <th>Disponibilidad</th>
                <th>Inicia</th>
                <th>Vence</th>
            </tr>
        </thead>
        <tbody>
        @php $valor=0;  @endphp

            @foreach($data as $data)

            @if( $data->ip !=$valor)
                <tr>
                    <td>{{$data->ip}}</td>
                    @if( $data->estatus==1)
                        <td><span class="badge badge-success">Libre</span></td>
                    @else
                        <td><span class="badge badge-danger">Ocupado</span></td>
                    @endif

                    <td>{{$data->inicia}}</td>
                    <td>{{$data->vence}}</td>
                </tr>
                @endif
                @php $valor =  $data->ip; @endphp
            @endforeach
        </tbody>
    </table>
</div>
@stop

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" href="../css/dataTables.bootstrap4.min.css">
@stop

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>


<script>
// Paginación y estilo de la tabla área mediante ajax
$(function() {
    var table = $('.yajra-datatable').DataTable({
       // processing: true,
       // serverSide: true,
        "order": [
            [1, "asc"]
        ],

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        }
    });

});/*
    var table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        "order": [
            [1, "desc"]
        ],

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        ajax: "{{ route('getDisponibilidad-Ip') }}",
        columns: [{
                data: 'ip',
                name: 'ip'
            },
            {
                data: 'action',
                name: 'action'
            },
            {
                data: 'inicia',
                name: 'inicia'
            },
            {
                data: 'vence',
                name: 'vence'
            }, 
           
        ]
    });

});*/
</script>
@stop
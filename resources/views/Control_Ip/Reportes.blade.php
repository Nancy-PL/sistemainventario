@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">REPORTES DE CONTROL DE
    DIRECCIONES IP</h1>
@stop

@section('content')
<div class="mt-500">
    <nav class="ml-5 row justify-content-md-center" style="width: 500px;  ">
        <form class="form-inline ml-5" name="formularioA">
            <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->
            <div class=" form-group ml-5 mt-5  ">
                <div class="row justify-content-md-center ml-5  mt-3 ">
                    <x-label class="mr-3" :value="__('Reporte por:')" />
                    <select class="js-example-basic-single col-md-8 ml-5" name="filtro" id="filtro" onchange="cambia()"
                        required>
                        <option value="" selected>Seleccionar </option>
                        <option value="5">General</option>
                        <option value="1">Reporte por área</option>
                        <option value="2">Reporte por Jefatura</option>
                        <option value="3">Reporte por Segmentación</option>
                        <option value="4">Temporal/Definitivo</option>

                    </select>
                    <!--    <button class="btn btn-outline-success mr-5" id="AreaB" type="submit">Buscar</button><br>-->
                </div>
                <div class="row justify-content-md-center ml-5 " id="seleccionarA" style="display:none">
                    <x-label class="mr-3" :value="__('Seleccionar Área:')" />
                    <select class="js-example-basic-single col-md-8 ml-5" name="area" id="area" required>
                        <option value="" selected> Seleccione el nombre del Área: </option>
                        @foreach ($areas as $area)
                        <option value="{{$area->id}}">{{$area->Nombre}}</option>
                        @endforeach
                    </select>
                    <!--    <button class="btn btn-outline-success mr-5" id="AreaB" type="submit">Buscar</button><br>-->
                </div>
                <div class="row justify-content-md-center ml-5 " id="Selectjefatura" style="display:none">
                    <div class="row justify-content-md-center">
                        <x-label :value="__('Seleccionar jefatura: ')" />
                    </div>
                    <select class="js-example-basic-single col-md-8 ml-5" name="jefatura" id="jefatura" required>
                        <option value="" selected>Seleccione el nombre de la jefatura </option>
                        @foreach ($jefatura as $jefatura)
                        <option value="{{$jefatura->id}}">{{$jefatura->Nombre}}</option>
                        @endforeach
                    </select>
                </div>
                <!-- Segmentacion -->
                <div class="row justify-content-md-center ml-5" id="SelectSegmentacion" style="display:none">
                    <x-label class="mt-3" :value="__('Segmentación:')" />
                    <div class="row col-md-5">
                        <input id="Segmentacion" placeholder="Segmentacion" class="form-control  mr-3" type="text"
                            name="Segmentacion" :value="old('Segmentacion')" style="text-transform:uppercase;" value=""
                            tabindex="3" maxlength="16" required autofocus />
                    </div>
                </div>
                <div class="row justify-content-md-center ml-5 " id="seleccionarD" style="display:none">
                    <x-label class="mr-3" :value="__('Seleccionar:')" />
                    <select class="js-example-basic-single col-md-8 ml-5" name="Definitivo" id="Definitivo" required>
                        <option value="1">DEFINITIVO</option>
                        <option value="2">TEMPORAL</option>
                    </select>
                    <!--    <button class="btn btn-outline-success mr-5" id="AreaB" type="submit">Buscar</button><br>-->
                </div>
                <a onclick="idcambiaA()" class="button btn btn-success ml-5 mr-5" ALIGN="center"><i
                        class="fas fa-file-excel  " aria-hidden="true"></i></a>
            </div>


        </form>
    </nav>
</div>


@stop




@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/sweetalert2.min.css">
@stop

@section('js')
<script src="js/sweetalert2.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/select2.min.js"></script>
<script>
$(document).ready(function() {
    $(document).ready(function() {
        $('.js-example-basic-single').select2({
            theme: "classic"
        });
    });

    $('#bienes').DataTable({
        responsive: true,

        "scrollX": true,
        "lengthMenu": [
            [5, 10, 50, -1],
            [5, 10, 50, "Todo"]
        ],
        "order": [
            [10, "desc"]
        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        }
    });
});

function cambia() {
    var id = document.getElementById("filtro").value;
    if (id == 1) {
        //activar
        document.getElementById("seleccionarA").style.display = "block";
        $('#area').prop("required", true);

        //desactivar 
        $('#jefatura').removeAttr("required");
        $('#Segmentacion').removeAttr("required");
        $('#Definitivo').removeAttr("required");
        document.getElementById("seleccionarD").style.display = "none";
        document.getElementById("Selectjefatura").style.display = "none";
        document.getElementById("SelectSegmentacion").style.display = "none";

        document.getElementById("jefatura").value='';
        document.getElementById("Segmentacion").value='';
        document.getElementById("Definitivo").value='';


    } else if (id == 2) {
        //activar
        document.getElementById("Selectjefatura").style.display = "block";
        $('#jefatura').prop("required", true);

        //desactivar 
        document.getElementById("seleccionarA").style.display = "none";
        document.getElementById("SelectSegmentacion").style.display = "none";
        document.getElementById("seleccionarD").style.display = "none";
        $('#Definitivo').removeAttr("required");
        $('#area').removeAttr("required");
        $('#Segmentacion').removeAttr("required");

        document.getElementById("area").value='';
        document.getElementById("Segmentacion").value='';
        document.getElementById("Definitivo").value='';

    } else if (id == 3) {
        //activar
        document.getElementById("SelectSegmentacion").style.display = "block";
        $('#jefatura').prop("required", true);

        //desactivar 
        document.getElementById("seleccionarA").style.display = "none";
        document.getElementById("Selectjefatura").style.display = "none";
        document.getElementById("seleccionarD").style.display = "none";
        $('#Definitivo').removeAttr("required");
        $('#area').removeAttr("required");
        $('#Segmentacion').removeAttr("required");
    } else if (id == 4) {
        //activar
        document.getElementById("seleccionarD").style.display = "block";
        $('#Definitivo').prop("required", true);

        //desactivar 
        document.getElementById("SelectSegmentacion").style.display = "none";
        document.getElementById("seleccionarA").style.display = "none";
        document.getElementById("Selectjefatura").style.display = "none";
        $('#area').removeAttr("required");
        $('#Segmentacion').removeAttr("required");
        $('#jefatura').removeAttr("required");

        document.getElementById("area").value='';
        document.getElementById("Segmentacion").value='';
        document.getElementById("jefatura").value='';
    }else if (id == 5) {
        //desactivar 
        document.getElementById("SelectSegmentacion").style.display = "none";
        document.getElementById("seleccionarD").style.display = "none";
        document.getElementById("seleccionarA").style.display = "none";
        document.getElementById("Selectjefatura").style.display = "none";
        $('#area').removeAttr("required");
        $('#Segmentacion').removeAttr("required");
        $('#jefatura').removeAttr("required");

        document.getElementById("area").value='';
        document.getElementById("Segmentacion").value='';
        document.getElementById("jefatura").value='';
        document.getElementById("Definitivo").value='';

    }

}

function idcambiaA() {
    var id = document.getElementById("area").value;

    document.formularioA.submit()

}
</script>

@stop
@extends('adminlte::page')

@section('title', 'OSFE')



@section('content_header')
<div class="float-right mb-50">
    <img src="../images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">Control de dirección
    Ip</h1>
@stop


@section('content')
<div class="row justify-content-md-center">
    <div class="card w-50">
        <div class="card-header" align="center">
            <h1 class="card-title " style="color:#008000">Registrar</h1><br>
            <!--Imprimiendo los valores de las validaciones-->
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <p>Corrige los siguientes errores:</p>
                <ul>
                    @foreach ($errors->all() as $messages)
                    <li>{{ $messages }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <!--Imprimiendo los valores de las validaciones-->
            @if($message=Session::get('UPS'))
                    <div class="col-12 alert-danger alert-dismissable fade show" role="danger">
                        <span>{{$message}}</span>
                    </div>
            @endif
        </div>
        <!--Select de la lista de areas-->
      <!--  <div class="row justify-content-md-center">
            <x-label class="ML-5 mt-5" :value="__('Área')" />
        </div>
        <nav class=" row justify-content-md-center"  >
            <form class="form-inline" name="formularioA">-->
                <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->
               <!--  <div class=" form-group ">
                    <div class="row justify-content-md-center ">
                        <select class="js-example-basic-single col-md-8 ml-5" name="areaa" id="areaa" onchange="idcambiaA()"
                          required>
                            <option value="" selected>Seleccionar Área</option>
                            @foreach ($areas as $area)
                            <option value="{{$area->id}}"  @if( $area->id == $idArea) selected
                        @endif>{{$area->Nombre}}</option>
                            @endforeach
                        </select>-->
                        <!--    <button class="btn btn-outline-success mr-5" id="AreaB" type="submit">Buscar</button><br>-->
               <!--      </div>
                </div>
            </form>
        </nav>-->
        <!-- Dar de Alta un Control_Ip -->
        <form method="POST" action="../Control_Ip" class="was-validated">

            <!--token para verificar que el usuario autenticado es quien en realidad está haciendo la petición-->
            @csrf
<!--            <input type="hidden" id="area" value="{{$idArea}}" name="area" /> -->
            <!-- Jefatura -->
<!--              <div class="row justify-content-md-center">
                <x-label for="Jefatura" class="mt-3" :value="__('Jefatura')" />
            </div>
            <div class="row justify-content-md-center">
                <select name="Jefatura" id="Jefatura" class="custom-select js-example-basic-single col-md-5"
                   tabindex="1" >
                    <option value="">Seleccionar Jefatura</option>
                    @foreach ($jefatura as $jefatura)
                    <option value="{{$jefatura->id}}">{{$jefatura->Nombre}}</option>
                    @endforeach
                </select>
            </div>-->
            <!-- Nombre -->
            <div class="row justify-content-md-center">
                <x-label class="mt-3" :value="__('Personal')" />
            </div>
            <div class="row justify-content-md-center">
                <select style="text-transform:uppercase;" class="custom-select js-example-basic-single col-md-5"
                    id="personas_id" :value="old('personas_id')" name="personas_id" tabindex="2" required autofocus>
                    <option style="text-transform:uppercase;" value="" data-codigo="" selected>Seleccionar nombre del
                        personal</option>
                    @foreach ($personas as $personas)
                    <option style="text-transform:uppercase;" value="{{$personas->id}}"
                        data-codigo="{{$personas->area_id}}" >{{$personas->Profesion}} {{$personas->Nombre}} {{$personas->ApellidoP}}
                        {{$personas->ApellidoM}}</option>
                    @endforeach
                </select>
            </div>
            <!-- Ip -->
            <div class="row justify-content-md-center">
                <x-label for="Dirección IP" class="mt-3" :value="__('Dirección IP')" />
            </div>
            <div class="row justify-content-md-center">
                <x-input id="ip" placeholder="Dirección IP " class="form-control col-sm-5 " type="text" name="ip"
                    :value="old('ip')" style="text-transform:uppercase;" value="" tabindex="3"  maxlength="15" onchange="idcambiaIp({{$ip}})"  required autofocus />
            </div>
              <!-- No. memo -->
              <div class="row justify-content-md-center">
                <x-label  class="mt-3" :value="__('No. de Memo')" />
            </div>
            <div class="row justify-content-md-center">
                <x-input id="memo" placeholder=" No. memo " class="form-control col-sm-5 " type="text" name="memo"
                    :value="old('memo')" style="text-transform:uppercase;" value="" tabindex="3"  maxlength="50"   required autofocus />
            </div>
             <!-- Estado -->
             <!--<div class="row justify-content-md-center">
                <x-label class="mt-3" :value="__('Estado')" />
            </div>
            <div class="row justify-content-md-center">
                <select style="text-transform:uppercase;" class="form-control col-md-5"
                    id="estado"   name="estado" tabindex="5" required>
                    <option style="text-transform:uppercase;" value="" data-codigo="" selected>Seleccionar estado</option>
                    <option style="text-transform:uppercase;" value="1">Libre</option>
                    <option style="text-transform:uppercase;" value="2">Ocupado</option>

                </select>
            </div>-->
            <!-- DEFINITIVO / TEMPORAL -->
            <div class="row justify-content-md-center">
                <x-label class="mt-3" :value="__('Estado')" />
            </div>
            <div class="row justify-content-md-center">
                <select style="text-transform:uppercase;" class="form-control col-md-5" onchange="Estadocambia()"
                    id="estado"   name="estado" tabindex="4"  required>
                    <option style="text-transform:uppercase;" value="" data-codigo="" selected>Seleccionar</option>
                    <option style="text-transform:uppercase;" value="1">Definitivo</option>
                    <option style="text-transform:uppercase;" value="2">Temporal</option>

                </select>
            </div>
             <!-- nivel-->
             <div class="row justify-content-md-center">
                <x-label class="mt-3" :value="__('Nivel de jerarquía')" />
            </div>
            <div class="row justify-content-md-center">
                <select style="text-transform:uppercase;" class="form-control col-md-5" 
                    id="nivel"   name="nivel" tabindex="5"  required autofocus>
                    <option style="text-transform:uppercase;" value="" data-codigo="" selected>Seleccionar</option>
                    <option style="text-transform:uppercase;" value="1">Titular</option>
                    <option style="text-transform:uppercase;" value="2">SubAuditores</option>
                    <option style="text-transform:uppercase;" value="3">Directores</option>
                    <option style="text-transform:uppercase;" value="4">Jefe de Departamento</option>
                    <option style="text-transform:uppercase;" value="5">Auditores</option>

                </select>
            </div>
            <!-- Fecha Inicio -->
            <div class=" row justify-content-md-center">
                <x-label  class="mt-3" :value="__('Fecha de inicio')" />
            </div>
            <div class="row justify-content-md-center">
                <input id="FechaInicio"  class="form-control col-sm-5 "  type="datetime-local"  
                    name="FechaInicio"   style="text-transform:uppercase;" value="<?php echo date("Y-m-d\TH:i:s");?>" tabindex="6" required autofocus />
            </div>

             <!-- Fecha Final -->
             <div class=" row justify-content-md-center">
                <x-label id="VenceT" name="VenceT" class="mt-3" :value="__('Vence')" />
            </div>
            <div class="row justify-content-md-center">
                <x-input id="Vence" class="form-control col-sm-5 "  type="datetime-local"
                    name="Vence"  style="display:none" style="text-transform:uppercase;" value="" tabindex="7"  />
            </div>
            <!-- Guardar -->
            <div class="row justify-content-md-center">
                <button type="submit" class="btn btn-success mt-3 mb-3 col-sm-3" tabindex="8" >Guardar</button>
            </div>
        </form>
    </div>
</div>
@stop


@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link href="../css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="../css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="../css/sweetalert2.min.css">
@stop

@section('js')
<script src="../js/sweetalert2.min.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/dataTables.bootstrap4.min.js"></script>
<script src="../js/jquery.validate.js"></script>
<script src="../js/select2.min.js"></script>
<script>
// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2({
        theme: "classic"
    });
});

function idcambiaIp(ip) {
    var ip2 = document.getElementById("ip").value;
    var b=0;
    ip.forEach( function(valor, indice, array) {
        if(valor.ip==ip2){
            b=1;
        }
    });
    if(b==1){
        alert("Dirección ip ocupado");

    }

    

}
function Estadocambia() {
    var estado = document.getElementById("estado").value;
  
    if (estado ==2) {
        document.getElementById("VenceT").style.display = "block";
        document.getElementById("Vence").style.display = "block";
        $('#Vence').prop("required", true);

      } else {
        document.getElementById("VenceT").style.display = "none";
        document.getElementById("Vence").style.display = "none";
        $('#Vence').removeAttr("required");

      }

}
</script>

@stop
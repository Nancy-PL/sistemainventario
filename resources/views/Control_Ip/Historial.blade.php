@extends('adminlte::page')

@section('title', 'OSFE')



@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">Historial de dirección
    IP</h1>
@stop


@section('content')
<div class="row justify-content-md-center">
    <!--Imprimiendo los valores de las validaciones-->
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $messages)
            <li>{{ $messages }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <!--Imprimiendo los valores de las validaciones-->
    @if($message=Session::get('UPS'))
    <div class="col-12 alert-danger alert-dismissable fade show" role="danger">
        <span>{{$message}}</span>
    </div>
    @endif

    <!--Select de la lista de areas-->
    <nav class=" row justify-content-md-center  mt-5">
        <form class="form-inline" name="formularioA">
            <!--Llamas a un select haciendo la consulta en la bd para requerir los datos del  nombre completode la tabla persona -->
            <div class=" form-group ">
                <div class="row justify-content-md-center ">
                    <x-label class="  mr-3" :value="__('Persona')" />

                    <select class="js-example-basic-single col-md-8 ml-5 ml-5 " name="persona" id="persona" required>
                        <option value="" selected>Seleccionar persona</option>
                        @foreach ($personas as $personas)
                        <option value="{{$personas->id}}" @if( $personas->id == $personasId) selected
                            @endif>{{$personas->Nombre}} {{$personas->ApellidoP}} {{$personas->ApellidoM}}</option>
                        @endforeach
                    </select>
                    <button class="btn btn-outline-success ml-5 mr-5 " id="AreaB" type="submit">Buscar</button><br>
                </div>
            </div>
        </form>
    </nav>
</div>
<div style="overflow-x:scroll;600px;">

    <table id="Direccionip" class="table table-bordered yajra-datatable " style="text-transform: uppercase;">
        <thead class="table-success text-black">
            <tr>
                <th>Ip</th>
                <th>No. Memo</th>
                <th>Nombre del personal</th>
                <th>Área</th>
                <th>Jefatura</th>
                <th>Estado</th>
                <th>Disponibilidad</th>
                <th>Inicia</th>
                <th>Vence</th>

            </tr>
        </thead>
        <tbody>
            @if( $personasId)
            @foreach($control as $data)
            <tr>
                <td>{{$data->ip}}</td>
                <td>{{$data->No_memo}}</td>
                <td>{{$data->NombreP}}</td>
                <td>{{$data->NombreA}}</td>
                <td>{{$data->NombreJ}}</td>
                @if( $data->Definitivo==1)
                <td>Definitivo</td>
                @else
                <td>Temporal</td>
                @endif
                @if( $data->estatus==1)
                <!--<td><span class="badge badge-success">Libre</span></td>-->
                <td></td>
                @else
                <td><span class="badge badge-danger">Ocupado</span></td>
                @endif

                <td>{{$data->inicia}}</td>
                <td>{{$data->vence}}</td>
            </tr>
            @endforeach
            @endif

        </tbody>
    </table>
</div>
@stop


@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/sweetalert2.min.css">
@stop

@section('js')
<script src="js/sweetalert2.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/select2.min.js"></script>
<script>
$(function() {
    var table = $('.yajra-datatable').DataTable({
        // processing: true,
        // serverSide: true,
        "order": [
            [7, "asc"]
        ],

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        }
    });

});
// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2({
        theme: "classic"
    });
});

function idcambiaA() {
    var id = document.getElementById("areaa").value;
    document.getElementById("area").value = id;
    document.formularioA.submit()

}
</script>

@stop
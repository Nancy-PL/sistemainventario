@extends('adminlte::page')

@section('title', 'OSFE')

@section('content_header')
<div class="float-right mb-50">
    <img src="images/logotipoOSFE.png" width=" 150px" height=" 80px">
</div>
<h1 class="d-flex justify-content-center mb-4 mt-100" style="color:#008000" class="was-validated">LISTADO DE DIRECCIONES
    IP</h1>
@stop


@section('content')

<!-- Listado de  Areas -->
<div class="card-header">
</div>
<div class="float-left mb-50 mt-2">

</div>
@can('Areas.create')
<a href="Control_Ip/create" class="button">
    <i class="fa fa-plus-circle  fa-3x ml-2" style="color:#3A3E3C; "></i>
</a>
@endcan
<div style="overflow-x:scroll;600px;">

    <table id="Direccionip" class="table table-bordered yajra-datatable" style="text-transform: uppercase;">
        <thead class="table-success text-black">
            <tr>
                <th>Ip</th>
                <th>No. Memo</th>
                <th>Nombre del personal</th>
                <th>Área</th>
                <th>Jefatura</th>
                <th>Estado</th>
                <th>Disponibilidad</th>
                <th>Inicia</th>
                <th>Vence</th>
                <th>Editar</th>
                <th>Baja</th>

            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
@stop

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/jquery.dataTables.min.css">
<link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
@stop

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>


<script>
// Paginación y estilo de la tabla área mediante ajax
$(function() {
    var table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        "order": [
            [7, "desc"]
        ],

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        ajax: "{{ route('get-Ip') }}",
        columns: [{
                data: 'ip',
                name: 'ip'
            },
            {
                data: 'No_memo',
                name: 'No_memo'
            },
            {
                data: 'NombreP',
                name: 'NombreP'
            },
            {
                data: 'NombreA',
                name: 'NombreA'
            },
            {
                data: 'NombreJ',
                name: 'NombreJ'
            },
            {
                data: "Definitivo",
                name: "Definitivo",
                "render": function(data) {
                    if (data == 1) {
                        return "Definitivo";
                    } else {
                        return "Temporal";
                    }
                }
            },
            {
                data: 'action2',
                name: 'action2',
                orderable: false,
                searchable: false,
                "render": function(data) {
                    return data;



                }

            },
            {
                data: "inicia",
                name: "inicia",
                /*  "render": function(data) {
                      var date = new DateTime(data);
                      var month = date.getMonth() + 1;
                      return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear()+date.get;
                  }*/


            },
            {
                data: "vence",
                name: "vence",
                /*  "render": function(data) {
                     
                      var date = new DateTime(data);
                      var month = date.getMonth() + 1;
                      if (data==null){
                          return " ";
                      }else{
                      return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
                      }
                  }*/


            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
                "render": function(data) {
                    return data;



                }

            },
            {
                data: 'baja',
                name: 'baja',
                orderable: false,
                searchable: false,
                "render": function(data) {
                    return data;



                }

            },
        ]
    });

});
</script>
@stop
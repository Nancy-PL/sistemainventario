<?php

namespace App\Http\Controllers;

use App\Http\Requests\BienesEditRequestUtic;
use App\Http\Requests\BienesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Datatables;
use App\Models\Bienes;
use App\Models\Persona;
use App\Models\marca;
use App\Models\HistorialBien;
use App\Models\clasificacion;
use App\Models\Area;
use App\Models\resguardo_detail;
use App\Models\Resguardo;
use Carbon\Carbon;
use App\Models\DetalleBien;
use App\Models\Historial_Inventario;
use App\Models\MejorasEquipo;
use App\Models\NoResguardo;

class BienesUticController extends Controller
{
     //protección de rutas
     public function __construct()
     {
         $this->middleware('can:BienesUtic.index')->only('index');
         $this->middleware('can:BienesUtic.create')->only('create');
         $this->middleware('can:BienesUtic.edit')->only('edit');
         $this->middleware('can:BienesUtic.destroy')->only('destroy');
     }
    //Llamando listado de bienes
    public function index()
    {
        return view('Bienes.UTIC.index');
    }

    //consulta de Bienes Utic para el listado
    public function getBienesUtic(Request $request)
    {
        if ($request->ajax()) {
            $data = Bienes::bienes("SI")->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '<a href="BienesUtic/' . $row->id . '/edit"class="fa  fa-edit  fa-lg" ALIGN="center"style="color:#3A3E3C; "></a> <a href="BienesUtic/' . $row->id . '"class="fa  fa-eye  fa-2x" ALIGN="center"style="color:#3A3E3C; "></a> ';
                    return $actionBtn;
                })
                ->addIndexColumn()
                ->addColumn('UserAlta', function ($row) {
                    if($row->name==null){
                        $UserAlta = $row->usuario_alta;
                    }else{
                        $UserAlta = $row->name;
                    }
                    return $UserAlta;
                })
                ->rawColumns(['action','UserAlta'])
                ->make(true);
        }
    }

    //Llamar vista create para la creación de un nuevo bien
    public function create()
    {
        $personas = Persona::all()->where('Activo', '1');
        $areas = Area::all()->where('Nombre','!=','ADMON SISTEMAS');;
        $Marca = marca::select('nombre', 'id')->get();
        $bienes =  Bienes::bienesHoyUtic()->get();
        $clasificacions = clasificacion::all()->where('Departamento', '=', 'SI');
        return view('Bienes.UTIC.create')->with('marcas', $Marca)->with('bienes', $bienes)->with('clasificacions', $clasificacions)->with('areas', $areas)->with('personas', $personas);
    }

     //Calculando el número de  control segun la clasificación dada
     protected function NoControl($clasificacion_id)
     {
         $clasificacion = clasificacion::find($clasificacion_id);
         $clave=$clasificacion->clave;
         $long=mb_strlen($clave)+5;

         if ($clasificacion->NoInventario == 1) {
           return 0;
        } else {
             $contadorBien = DB::table('bienes')
             ->select('ClaveBien')
             ->where('ClaveBien', 'like', '%'.$clave.'-'.'%')
             ->whereRaw('LENGTH(ClaveBien) = ?',$long)
             ->count();

             $contadorBien = $contadorBien + 1;
             $contador = str_pad($contadorBien, 4, "0", STR_PAD_LEFT);
             $ClaveBien = $clasificacion->clave . '-' . ($contador);
         }

         return $ClaveBien;
     }
    //Calculando el número de inventario o No de control segun la clasificación dada
    protected function ClaveBien($letra_id, $clasificacion_id)
    {
        $clasificacion = clasificacion::find($clasificacion_id);
        $clave=$clasificacion->clave;
        $areas  = Area::find($letra_id);


        if ($clasificacion->NoInventario == 1) {
            $long=mb_strlen($clave)+11;

            $contadorBien = DB::table('bienes')
            ->select('ClaveBien')
            ->where('ClaveBien', 'like', '%-'.$clave.'%')
            ->whereRaw('LENGTH(ClaveBien) = ?',$long)
            ->count();

            $contadorBien = $contadorBien + 1;
            $contador = str_pad($contadorBien, 4, "0", STR_PAD_LEFT);
            $ClaveBien = "OSFE-" . $areas->letra . "-" . $clasificacion->clave . ($contador);
        } else {
            $long=mb_strlen($clave)+5;

            $contadorBien = DB::table('bienes')
            ->select('ClaveBien')
            ->where('ClaveBien', 'like', '%'.$clave.'-'.'%')
            ->whereRaw('LENGTH(ClaveBien) = ?',$long)
            ->count();

            $contadorBien = $contadorBien + 1;
            $contador = str_pad($contadorBien, 4, "0", STR_PAD_LEFT);
            $ClaveBien = $clasificacion->clave . '-' . ($contador);
        }

        return $ClaveBien;
    }
    public function ValidarPersonaPermanente($id)
    {
        $resguardos = DB::table('resguardos')
            ->select('resguardos.*')
            ->where('persona_id', '=', $id)
            ->where('tipo', '=', 'PERMANENTE')
            ->get();

        foreach ($resguardos as $resguardo) {
            if ($resguardos) {
                return $resguardo->id;
            } else {
                return 0;
            }
        }
    }
    //validar si almacen tiene bienes a su resguardo
    public function ValidarPersonaAlmacen($id)
    {
        $resguardos = DB::table('resguardos')
            ->select('resguardos.*')
            ->where('persona_id', '=', $id)
            ->where('tipo', '=', 'ALMACEN')
            ->get();

        foreach ($resguardos as $resguardo) {
            if ($resguardos) {
                return $resguardo->id;
            } else {
                return 0;
            }
        }
    }
    //buscar personal de almacen
    public function almacen($nombre2)
    {

        // separando el nombre del almacenista
        $prof = explode(".", $nombre2);
        $contaprof = count(explode(".", $nombre2)) - 1;

        $nombre = explode(" ", $prof[$contaprof]);
        $conta = count(explode(" ", $nombre2));

        if ($conta > 5) {
            $nombres = $nombre[1] . " " . $nombre[2] . " " . $nombre[3];
            $Apellido = $nombre[4];
            $Apellidos = $nombre[5];
        } elseif ($conta > 4) {
            $nombres = $nombre[1] . " " . $nombre[2];
            $Apellido = $nombre[3];
            $Apellidos = $nombre[4];
        } else {
            $nombres = $nombre[1];
            $Apellido = $nombre[2];
            $Apellidos = $nombre[3];
        }

        $personaalmacen = Persona::buscarAlmacen($nombres, $Apellido, $Apellidos)->get();
        $idPersona=0;
        foreach ($personaalmacen as $personaalmacen) {
            $idPersona = $personaalmacen->id;
        }
        if($idPersona!=0){
            return $idPersona;
        }else{
            return 0;
        }
    }

    //Insertar un nuevo bien
    public function store(BienesRequest $request)
    {
        //leer datos y generar la clave bien
        $clasificacion = $request->get('Clasificacion_id');
        $area = $request->get('area');
        $TipoResguardo = $request->get('Tipo');
        $idPersona = $request->get('Resguardante');
        $Resguardante_id = 0;
        $NoInventario ="";

        if ($TipoResguardo == "ALMACEN") {
            $nombre2 = $request->get('almacenNombre');
            $idPersona = $this->almacen($nombre2);
            if($idPersona==0){
                return back()->with('Ups', "El almacenista no esta registrado como personal");

            }
            $NoInventario = $this->NoControl($clasificacion);

            $Resguardante_id = $this->ValidarPersonaAlmacen($idPersona);
        } else {
            $Resguardante_id = $this->ValidarPersonaPermanente($idPersona);
        }
        if ($TipoResguardo != "ALMACEN") {
            $NoInventario = $this->ClaveBien($area, $clasificacion);
        }
        $fecha = Carbon::now();
        $clasificacion = clasificacion::find($clasificacion);
        //Nuevo bien Administrativo
        $bienes = new Bienes();

        $bienes->Modelo = $request->get('Modelo');
        if ($TipoResguardo == "ALMACEN") {
            $bienes->Estado = 'BUENO';
        }else{
            $bienes->Estado = $request->get('Estado');
        }
        $bienes->Caracteristicas = $request->get('Caracteristicas');
        $bienes->Observaciones = $request->get('Observaciones');
        $bienes->Status = '0';
        if($NoInventario!=0){
            $bienes->ClaveBien = $NoInventario;
        }
        $bienes->Departamento = 'SI';
        $bienes->Activo = '1';
        $bienes->Clasificacion_id = $request->get('Clasificacion_id');
        $bienes->marca_id = $request->get('marca_id');
        $bienes->areas_id = $area;
        $bienes->save();
        $idBien = $bienes->id;

        $inv=$request->get('NoInventarioAnterior');

        //Nuevo inventario Anterior
        if( $inv){
            $InventarioAnt = new Historial_Inventario();
            $InventarioAnt->NoInventarioAnterior =  $inv;
            $InventarioAnt->bienes_id = $idBien;
            $InventarioAnt->save();
        }
        //Nuevo No de serie
        $Detalle_Bien = new DetalleBien();
        $Detalle_Bien->NoSerie = $request->get('NoSerie');
        $Detalle_Bien->ubicacion = strtoupper($request->get('Ubicacion'));
        $Detalle_Bien->factura = $request->get('factura');
        $Detalle_Bien->bienes_id = $idBien;
        $Detalle_Bien->save();

        $mejora= $request->get('MejorasEquipo');
        if($mejora){
            //Nuevo mejora
        $mejoras = new MejorasEquipo();
        $mejoras->MejorasEquipo =$mejora;
        $mejoras->bienes_id = $idBien;
        $mejoras->save();
        }
        //Registrar el bien en historial
        $HistorialBien = new HistorialBien();
        $HistorialBien->fecha_alta = $fecha;
        $HistorialBien->usuario_alta = auth()->user()->id;
        $HistorialBien->bien_id = $idBien;
        $HistorialBien->save();

        //Asignar el bien al personal seleccionado
        $resguardo_detail = new resguardo_detail();

        if ($Resguardante_id == 0) {
            $Resguardo = new Resguardo();
            $Resguardo->tipo = $TipoResguardo;
            $Resguardo->persona_id = $idPersona;
            $Resguardo->SI = "1";
            $Resguardo->UA = "0";
            $Resguardo->activo = 1;
            $Resguardo->save();
            $Resguardante_id =  $Resguardo->id;

       } else {
            $resguardo = Resguardo::find($Resguardante_id);
            $resguardo->activo = 1;
            $resguardo->SI = "1";
            $resguardo->save();
        }

        $resguardo_detail->resguardo_id = $Resguardante_id;
        $resguardo_detail->fechaAsignacion = $fecha;
        $resguardo_detail->bien_id = $bienes->id;
        $resguardo_detail->activo = 1;
        $resguardo_detail->User = auth()->user()->id;
        $resguardo_detail->save();
        $bienes->Status = 1;
        $bienes->update();


        if ($TipoResguardo == "PERMANENTE") {
            if ($clasificacion->NoInventario == 1) {
                if (!$Detalle_Bien->NoSerie) {
                    return back()->with('Listo', " Operación correcta,  se le asigno el número de inventario: " . $NoInventario)->with('Resguardo', $idPersona)->with('user', auth()->user()->id)->with('tipoR', $TipoResguardo)->with('idResguardo', $resguardo_detail->id)->with('Areaid', $area);
                } else {
                    return back()->with('Listo', "Al bien con número de serie: " . $Detalle_Bien->NoSerie . " se le asigno el número de inventario: " . $NoInventario)->with('Resguardo', $idPersona)->with('user', auth()->user()->id)->with('tipoR', $TipoResguardo)->with('idResguardo', $resguardo_detail->id)->with('Areaid', $area);
                }
            } else {
                if (!$Detalle_Bien->NoSerie) {
                    return back()->with('Listo', " Operación correcta,  se le asigno el número de control: " . $NoInventario)->with('Resguardo', $idPersona)->with('user', auth()->user()->id)->with('tipoR', $TipoResguardo)->with('idResguardo', $resguardo_detail->id)->with('Areaid', $area);
                } else {
                    return back()->with('Listo', "Al bien con número de serie: " . $Detalle_Bien->NoSerie . " se le asigno el número de control: " . $NoInventario)->with('Resguardo', $idPersona)->with('user', auth()->user()->id)->with('tipoR', $TipoResguardo)->with('idResguardo', $resguardo_detail->id)->with('Areaid', $area);
                }
            }
        } else {

            if ($NoInventario != 0) {
                if (!$Detalle_Bien->NoSerie) {
                    return back()->with('Listo', " Operación correcta,  se le asigno el número de control: " . $NoInventario)->with('Resguardo', $idPersona)->with('user', auth()->user()->id)->with('tipoR', $TipoResguardo)->with('idResguardo', $resguardo_detail->id)->with('Areaid', $area);
                } else {
                    return back()->with('Listo', "Al bien con número de serie: " . $Detalle_Bien->NoSerie . " se le asigno el número de control: " . $NoInventario)->with('Resguardo', $idPersona)->with('user', auth()->user()->id)->with('tipoR', $TipoResguardo)->with('idResguardo', $resguardo_detail->id)->with('Areaid', $area);
                }
            }

            if (!$Detalle_Bien->NoSerie) {
                return back()->with('Listo', " Operación correcta")->with('Resguardo', $idPersona)->with('user', auth()->user()->id)->with('tipoR', $TipoResguardo)->with('idResguardo', $resguardo_detail->id)->with('Areaid', $area);
            } else {
                return back()->with('Listo', "Se dio de alta el bien con número de serie: " . $Detalle_Bien->NoSerie)->with('Resguardo', $idPersona)->with('user', auth()->user()->id)->with('tipoR', $TipoResguardo)->with('idResguardo', $resguardo_detail->id)->with('Areaid', $area);
            }
        }
    }
    //Mostrar vista de editar bienes
    public function edit($id)
    {

        $bienes = Bienes::find($id);
        $detalle_biens = DetalleBien::wherebienes_id($id)->first();
        $historial__inventarios = Historial_Inventario::wherebienes_id($id)->first();
        $mejoras_equipos = MejorasEquipo::wherebienes_id($id)->first();
        $Marca = marca::select('nombre', 'id')->get();
        $clasificacions = clasificacion::all();
        return view('Bienes.UTIC.edit', compact('detalle_biens','historial__inventarios','mejoras_equipos'))->with('marcas', $Marca)->with('bienes', $bienes)->with('clasificacions', $clasificacions)->with('id', $id);
    }
    //Detalle
    public function show($id)
    {

        $bienes = Bienes::detalleBien($id)->get();

        return view('Bienes.UTIC.DetalleBien', compact('bienes'));
    }
    //actualizando  bienes desde la bd
    public function update(BienesEditRequestUtic $request, $id)
    {

        $bienes = Bienes::findOrFail($id);
        $InventarioAnt = Historial_Inventario::where('bienes_id','=',$id)->get();
        $idInv=0;
        foreach($InventarioAnt as $Inventario){
            $idInv=$Inventario->id;
            break;
        }
        if($request->get('NoInventarioAnterior')!='S/N'){

        $this->validate($request, [
            'NoInventarioAnterior' =>"nullable",
            'NoSerie' => 'nullable|string|max:100',
            'Modelo' => 'required|string|max:100',
            'Estado' => 'required|string|max:10',
            'factura' => 'required|string|max:25',
            'marca_id' => 'required',
        ]);
    }else{

        $this->validate($request, [
            'NoInventarioAnterior' =>"nullable",
            'NoSerie' => 'nullable|string|max:100',
            'Modelo' => 'required|string|max:100',
            'Estado' => 'required|string|max:10',
            'factura' => 'required|string|max:25',
            'marca_id' => 'required',
        ]);
    }

        $bienes->Modelo = $request->get('Modelo');
        $bienes->Estado = $request->get('Estado');
        $bienes->Caracteristicas = $request->get('Caracteristicas');
        $bienes->Observaciones = $request->get('Observaciones');
        $bienes->marca_id = $request->get('marca_id');
        $bienes->update();
        $inv=$request->get('NoInventarioAnterior');

        //Nuevo inventario Anterior
        if($idInv==0){
            $InventarioAnt = new Historial_Inventario();

        }else{
            $InventarioAnt = Historial_Inventario::findOrFail($idInv);
        }

        if($inv){
            $InventarioAnt->NoInventarioAnterior =  $inv;
            $InventarioAnt->bienes_id = $id;
            $InventarioAnt->save();
        }
        //Nuevo No de serie
        $Detalle_Bien = DetalleBien::where('bienes_id','=',$id)->get();
        $idDet=0;
        foreach($Detalle_Bien as $Detalle_Bien){
            $idDet=$Detalle_Bien->id;
            break;
        }
        //Nuevo inventario Anterior
        if($idDet==0){
            $mejoras = new DetalleBien();

        }else{
            $Detalle_Bien = DetalleBien::findOrFail($idDet);
        }

        $Detalle_Bien->NoSerie = $request->get('NoSerie');
        $Detalle_Bien->ubicacion = strtoupper($request->get('Ubicacion'));
        $Detalle_Bien->factura = $request->get('factura');
        $Detalle_Bien->bienes_id = $id;
        $Detalle_Bien->save();

        $mejora= $request->get('MejorasEquipo');
        $mejoras = MejorasEquipo::where('bienes_id','=',$id)->get();
        $idM=0;
        foreach($mejoras as $mejoras){
            $idM=$mejoras->id;
            break;
        }

        if($mejora){
            //Nuevo mejora
            if($idM==0){
                $mejoras = new MejorasEquipo();

            }else{
                $mejoras = MejorasEquipo::findOrFail($idM);
            }

        $mejoras->MejorasEquipo =$mejora;
        $mejoras->bienes_id = $id;
        $mejoras->save();
        }

        return redirect('/BienesUtic');
    }

    public function lista($id)
    {
        $resguardo = Resguardo::listaUtic($id)->get();
        return view('Bienes.UTIC.ListadoResguardo', compact('resguardo'));
    }
    public function listaAlmacen($nombreP)
    {
        // separando el nombre del almacenista
        $id= $this->almacen($nombreP);
        $resguardo = Resguardo::listaAlmacen($id)->get();
        return view('Bienes.UTIC.ListadoResguardo', compact('resguardo'));
    }
    //verificar la existencia de un bien al resguardo en caso de no haber se cancela
    public function  verificarExistenciaResguardo($id)
    {
        $verificar = resguardo_detail::where('resguardo_id', $id)->where('Activo', '=', 1)->get();

        foreach ($verificar as $verificar) {
            return 1;
        }
        //Cancelar resguardo
        $resguardoAnterior = Resguardo::find($id);
        $resguardoAnterior->activo = 0;
        $resguardoAnterior->delete_at = Carbon::now();
        $resguardoAnterior->update();
        return 0;
    }
    public function destroy($id)
    {
        //verificar Existencia de resguardos  si no tiene bienes entonces se desactiva
        $resguardo_detail = resguardo_detail::where('bien_id', $id)->get();
        foreach ($resguardo_detail as $resguardo_detail) {
            $idResguardo = $resguardo_detail->resguardo_id;
            break;
        }

        $bienes = Bienes::find($id);
        $clavebien = $bienes->ClaveBien;
        $bienes->delete();

        //verificar existencia de bienes al resguardo activos
        $this->verificarExistenciaResguardo($idResguardo);
        $resguardo = Resguardo::find($idResguardo);
        $idPersona = $resguardo->persona_id;
        $TipoResguardo = $resguardo->tipo;

        //Persona Areas
        $persona=Persona::find($idPersona);
        $area = $persona->area_id;
        return back()->with('Listo', "Al bien con clave bien: " . $clavebien . " se eliminó correctamente. ")->with('Resguardo', $idPersona)->with('user', auth()->user()->id)->with('tipoR', $TipoResguardo)->with('idResguardo', $resguardo_detail->id)->with('Areaid',$area );
    }
}

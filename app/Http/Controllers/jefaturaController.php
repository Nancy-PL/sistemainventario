<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\JefaturaRequest;
use App\Http\Requests\EditarRequest;
use Yajra\DataTables\Datatables;
use App\Models\control_ip;
use App\Models\Persona;
use App\Models\Resguardo;
use App\Models\resguardo_detail;
use Faker\Provider\ar_JO\Person;
use App\Models\Area;
use App\Models\jefatura;

class jefaturaController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:SalidaEquipo.index')->only('index');
        $this->middleware('can:SalidaEquipo.index')->only('create');
        $this->middleware('can:SalidaEquipo.index')->only('edit');
    }
    /*Consulta para la vista del Listado de areas*/
    public function index()
    {
        return view('Catalogos.jefatura.index') ;    
    }

    /*Muestra la vista para dar de alta un area*/
    public function create(Request $request)
    { 
        //consultar jefatura de cierta area 
        $personas = Persona::all()->where('Activo', '1');
        $areas = Area::all();

        return view('Catalogos.jefatura.Registro')->with('personas', $personas)->with('areas', $areas);

    }


    /*Insertando un nuevo area en la bd*/
    public function store(JefaturaRequest $request)
    {
        $request->validated();
        $jefatura = new jefatura();
        $jefatura->Nombre = strtoupper($request->get('Nombre'));
        $jefatura->idPersona = $request->get('personas_id');
        $jefatura->idArea = $request->get('area');
   //     $jefatura->NivelJerarquia = $request->get('nivel');
        $jefatura->Usuario_Captura = auth()->user()->id;
        $jefatura->Fecha_Captura = date("Y-m-d\TH:i:s");
        $jefatura->save();

        return redirect('jefatura');
    }
    /*Mostrando la vista para Editar*/
    public function edit(Request $request, $id)
    {
        $jefatura = jefatura::find($id);
        $jefatura=json_decode($jefatura);
        $personas = Persona::all()->where('Activo', '1');
        $areas = Area::all();

        return view('Catalogos.jefatura.Editar',compact('personas','areas','jefatura'));
    }

    /*Actualizando un control*/
    public function update(JefaturaRequest $request, $id)
    {
        $jefatura = jefatura::findOrFail($id);
        $request->validated();
        $jefatura->Nombre = strtoupper($request->get('Nombre'));
        $jefatura->idPersona = $request->get('personas_id');
        $jefatura->idArea = $request->get('area');
        $jefatura->Usuario_Captura = auth()->user()->id;
       // $jefatura->NivelJerarquia = $request->get('nivel');
        $jefatura->update();
        return redirect('/jefatura');
    }
    //consulta de areas para el listado
    public function getjefatura(Request $request)
    {
        if ($request->ajax()) {
            $data = jefatura::jefaturas()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = ' <a href="jefatura/'.$row->id.'/edit" class="fa  fa-edit  fa-lg" ALIGN="center"style="color:#3A3E3C; "></a> ';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
}
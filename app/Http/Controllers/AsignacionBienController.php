<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bienes;
use App\Models\resguardo_detail;
use App\Http\Requests\AsignacionRequest;
use App\Models\Persona;
use App\Models\Area;
use App\Models\NoResguardo;
use Illuminate\Support\Facades\DB;
use App\Models\Resguardo;
use App\Models\User;
use Illuminate\Support\Carbon;
use Barryvdh\DomPDF\Facade as PDF;
use Yajra\DataTables\Datatables;

class AsignacionBienController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:AsignacionBien.index')->only('index');
        $this->middleware('can:AsignacionBien.create')->only('create');
    }
    //Listado de resguardos de bienes muebles
    public function index()
    {
        $Resguardo = Resguardo::resguardoActivo()->get();
        return view('Resguardos.Administrativos.ListaResguardo', compact('Resguardo'));
    }
    //Asignar un nuevo bien
    public function create()
    {

        $personas = Persona::all()->where('Activo', '1');
        return view('Resguardos.Administrativos.AsignacionBien', compact('personas', $personas));
    }

    //consulta de Asignacion Bien Admi para el listado
    public function getAsignacionBienAdmi(Request $request)
    {

        if ($request->ajax()) {
            $data =  Bienes::bienDisponibleUA()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '<input class="checkboxClass" type="checkbox" id="biens" name="biens[]" value="' . $row->id . '">
                    ';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    //consulta de Resguardo Utic para el listado de resguardos Activos
    public function getResguardoAdmi(Request $request)
    {
        if ($request->ajax()) {
            $data =  Resguardo::ResguardoActivoAdmi()->get();
            return Datatables::of($data)->make(true);
        }
    }


    //Validar existencia de personas con resguardo
    public function ValidarPersona($id)
    {
        $resguardos = DB::table('resguardos')
            ->select('resguardos.*')
            ->where('persona_id', '=', $id)
            ->where('UA', '=', '1')
            ->where('tipo', '=', 'PERMANENTE')
            ->get();

        foreach ($resguardos as $resguardo) {
            if ($resguardos) {
                return $resguardo->id;
            } else {
                return 0;
            }
        }
    }

    //Asignacion de un bien a resguardantes
    public function store(AsignacionRequest $request)
    {
        
        $request->validated();
        $Biens = $request->get('bienes');
        $idPersona = $request->get('persona_id');




        // separando el nombre del almacenista
        $Bien = explode(",", $Biens);
        
        foreach ($Bien as $bien) {
                $fecha = Carbon::now();
                $bienes = Bienes::find($bien);
                $resguardo_detail = new resguardo_detail();
                $Resguardante_id = $this->ValidarPersona($idPersona);

                if ($Resguardante_id == 0) {
                    $Resguardo = new Resguardo();
                    $Resguardo->tipo = "PERMANENTE";
                    $Resguardo->persona_id = $idPersona;
                    $Resguardo->UA = "1";
                    $Resguardo->activo = 1;
                    $Resguardo->save();
                    $Resguardante_id =  $Resguardo->id;
                }

                $resguardo_detail->resguardo_id = $Resguardante_id;
                $resguardo_detail->fechaAsignacion = $fecha;
                $resguardo_detail->bien_id = $bien;
                $resguardo_detail->activo = 1;
                $resguardo_detail->User = auth()->user()->id;
                $resguardo_detail->save();

                $bienes->Status = 1;
                $bienes->update();
            
        }
        return redirect('/ListaAsignados/' . $Resguardante_id);
    }
    //Listado de resguardos especificos bienes muebles
    public function lista($id)
    {
        $resguardo = Resguardo::listaResguardo($id)->get();
        return view('Resguardos.Administrativos.ListadoResguardo', compact('resguardo'));
    }
    //Consultar resguardos de resguardantes para generar reporte
    public function resguardo(Request $request)
    {
        $personas = Persona::all()->where('Activo', 1);
        $id = $request->get('resguardante');
        $resguardos = Resguardo::buscarporPers($id)->get();
        return view('Resguardos.Administrativos.ResguardoporResguardante', compact('personas', 'resguardos', 'id'));
    }

    //Descargar reporte  de bienes muebles
    public function downloadResguardoPDF($id)
    {

        $resguardos = Resguardo::buscarporPers($id)->get();
        $areas = Area::reporteResguardo()->get();
        $Elaboro = User::personaEnSesion(auth()->user()->id)->get();
        
        foreach($resguardos as $resguardo){
            $Nombre=$resguardo->Nombre.' '.$resguardo->ApellidoP.' '.$resguardo->ApellidoM;
            break;
        }
        $Cargo=$this->BuscarCargo($Nombre);
        $NoResguardo=NoResguardo::where('persona_id',$id)->first();

        $pdf = PDF::loadView('Resguardos.Administrativos.Reportes.ReporteResguardante', compact('resguardos', 'areas', 'Elaboro','Cargo','NoResguardo'));
        return $pdf->setPaper('a4', 'landscape')
            ->stream('Resguardo Administrativo.pdf');
    }
    //Vista para descargar reporte general para generar reporte
    public function ReporteGeneral(Request $request)
    {
        $personas = Persona::all()->where('Activo', 1);
        $id = $request->get('resguardante');
        $NoResguardo=NoResguardo::where('persona_id',$id)->first();
        $resguardos = Resguardo::resguardoGralPorPers($id)->get();
        return view('Resguardos.Administrativos.ResguardoGeneralporResguardante', compact('personas', 'resguardos', 'id','NoResguardo'));
    }
        //buscar personal que tenga cargos
        public function BuscarCargo($Nombre)
        {
        
            $Cargo = Area::buscarCargo($Nombre)->first();
            return $Cargo;
        }
    //Descargar reporte  de bienes muebles y Equipo de Computo
    public function downloadResguardoGeneralPDF($id)
    {
        $resguardos = Resguardo::resguardoGralPorPers($id)->get();
        $areas = Area::reporteResguardo()->get();
        foreach($resguardos as $resguardo){
            $Nombre=$resguardo->Nombre.' '.$resguardo->ApellidoP.' '.$resguardo->ApellidoM;
            break;
        }
        $Cargo=$this->BuscarCargo($Nombre);

        $Elaboro = User::personaEnSesion(auth()->user()->id)->get();
        $NoResguardo=NoResguardo::where('persona_id',$id)->first();

        $pdf = PDF::loadView('Resguardos.Administrativos.Reportes.ReporteGeneralResguardante', compact('resguardos', 'areas', 'Elaboro','Cargo','NoResguardo'));
        return $pdf->setPaper('a4', 'landscape')
            ->stream('Resguardo Administrativo.pdf');
    }
}
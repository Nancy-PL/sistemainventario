<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\Control_IpRequest;
use App\Http\Requests\EditarRequest;
use Yajra\DataTables\Datatables;
use App\Models\control_ip;
use App\Models\Persona;
use App\Models\Resguardo;
use App\Models\resguardo_detail;
use Faker\Provider\ar_JO\Person;
use App\Models\Area;
use App\Models\jefatura;

class HistorialIpController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:SalidaEquipo.index')->only('index');
        $this->middleware('can:SalidaEquipo.index')->only('create');
        $this->middleware('can:SalidaEquipo.index')->only('edit');
    }
    /*Consulta para la vista del Listado*/
    public function index(Request $request)
    {
        $fecha=date("Y-m-d H:i:s");
        $data = control_ip::ControlTemp($fecha)->get();
        $personas = Persona::all()->where('Activo', '1');
        $personasId=$request->get('persona');
        $control = control_ip::ControlHistorial($personasId)->get();

        foreach ($data as $controlVence){
        $control_ip = control_ip::findOrFail($controlVence->id);
        if ($control_ip){
            $control_ip->estado = 2;
            $control_ip->estatus = 1;
            $control_ip->fechaActualizacion = $control_ip->fechaActualizacion;
            $control_ip->update();
        }
        
        }
        return view('Control_Ip.Historial',compact('personas','personasId','control'));

        
    }
 
}
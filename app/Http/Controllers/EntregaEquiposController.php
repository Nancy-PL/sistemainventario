<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Resguardo;
use App\Models\Persona;
use App\Models\resguardo_detail;
use App\Models\resguardo_temporal;
use App\Models\salidaEquipo;
use App\Models\Detallesalida;
use App\Http\Requests\EntradaEquipoRequest;
use Carbon\Carbon;

class EntregaEquiposController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:EntregaEquipo.index')->only('index');
    }
    //vista paa entregar equipos utic
    public function index(Request $request)
    {

        $personas = Persona::all()->where('Activo', '1');
        //consultar bienes de un resguardante
        $nombre = $request->get('resguardante');
        $resguardosSalida = Resguardo::buscarporResguardoSalida($nombre)->get();
        return view('Resguardos.UTIC.EntregaEquipos')->with('personas', $personas)->with('resguardosSalida', $resguardosSalida);
    }
    //validar existencia de una persona
    public function ValidarPersona($id)
    {
        $resguardos = DB::table('resguardos')
            ->select('resguardos.*')
            ->where('persona_id', '=', $id)
            ->where('SI', '=', '1')
            ->where('tipo', '=', 'PERMANENTE')
            ->get();

        foreach ($resguardos as $resguardo) {
            if ($resguardos) {
                return $resguardo->id;
            } else {
                return 0;
            }
        }
    }
    //verificar existencia de bienes salientes sin entregar
    public function verificarBien($id)
    {
        $resguardo = Detallesalida::where('salida_equipos_id', $id)->where('Activo', 1)->get();
        foreach ($resguardo as $resguardo) {
            if ($resguardo) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    //verificar existencia de bienes al resguardo de una persona
    public function verificarResguardo($id)
    {
        $verificar = resguardo_detail::where('resguardo_id', $id)->where('Activo', '=', 1)->get();

        foreach ($verificar as $verificar) {
            return 1;
        }
        //Cancelar resguardo
        $resguardoAnterior = Resguardo::find($id);
        $resguardoAnterior->activo = 0;
        $resguardoAnterior->delete_at = Carbon::now();
        $resguardoAnterior->update();

        $resguardoTemporal = resguardo_temporal::where('resguardo_id', $id)->where('Activo', '=', 1)->get();
        foreach ($resguardoTemporal as $resguardoTemporal) {
            $resguardoTemporal->Activo = 0;
            $resguardoTemporal->update();
        }
        return 0;
        
    }
    //Registrar entrega de equipos utic
    public function store(EntradaEquipoRequest $request)
    {
        $request->validated();
        $Biens = $request->get('bienes');
        $resguardante = $request->get('resguardanteNuevo');
        $fecha = Carbon::now();
        $salidaEquipo_id = 0;
        $nuevoResguardante_id = 0;
        $resguardo_id = 0;
        // separando el bienes 
        $Bien = explode(",", $Biens);
        //registrar salida de bienes seleccionados
        foreach ($Bien as $bien) {
             
            //se registra la entrada del bien saliente de equipos
            $detalleSalida = Detallesalida::where('bien_id', $bien)->get();
            foreach ($detalleSalida as $detalleSalida) {
                $detalleSalida->Activo = 0;
                $salidaEquipo_id = $detalleSalida->salida_equipos_id;
                $detalleSalida->update();
            }

            //Asignar nuevo resguardante
            $nuevoResguardante_id = $this->ValidarPersona($resguardante);

            //En caso de ser el primer resguardo de la persona se crea un nuevo resguardo
            if ($nuevoResguardante_id == 0) {
                $Resguardo = new Resguardo();
                $Resguardo->tipo = "PERMANENTE";
                $Resguardo->persona_id = $resguardante;
                $Resguardo->SI = "1";
                $Resguardo->activo = 1;
                $Resguardo->save();
                $nuevoResguardante_id = $Resguardo->id;
            } else {
                $resguardo = Resguardo::find($nuevoResguardante_id);
                $resguardo->activo = 1;
                $resguardo->save();
            }
            //desactivar resguardo anterior y cancelando
            $resguardo_detailAnt = resguardo_detail::where('bien_id', $bien)->where('Activo', 1)->get();
            $personaAnterior=0;
            foreach ($resguardo_detailAnt as $resguardo_detailAnt) {
                $personaAnterior = $resguardo_detailAnt->resguardo_id;
                $resguardo_id = $resguardo_detailAnt->resguardo_id;

                break;
            }
                if ($personaAnterior!= $resguardante) {
                    $resguardo_detailAnt->fechaCancelacion = $fecha;
                    $resguardo_detailAnt->Activo = 0;
                    $resguardo_detailAnt->update();
                }
                if ($personaAnterior!= $resguardante) {
                //reasgnacion al nuevo resguardante
                $resguardo_detail = new resguardo_detail();
                $resguardo_detail->resguardo_id = $nuevoResguardante_id;
                $resguardo_detail->Activo = 1;
                $resguardo_detail->fechaAsignacion = $fecha;
                $resguardo_detail->bien_id = $bien;
                $resguardo_detail->ResguardoAnterior_id = $personaAnterior;
                $resguardo_detail->save();
                }
                 $this->verificarResguardo($resguardo_id);
           
        }
        //verificar si se entrego todo el bien saliente
        $salida = $this->verificarBien($salidaEquipo_id);
       
        if ($salida == 0) {
            //Desactivar salida y capturar fecha de devolucion
            $salidaEquipo = salidaEquipo::find($salidaEquipo_id);
            $salidaEquipo->Activo = 0;
            $salidaEquipo->fecha_Entrega = $request->get('FechaEntrega');
            $salidaEquipo->update();
        }


        return redirect('/ListaAsignadosUtic/' . $nuevoResguardante_id);
    }
    
}

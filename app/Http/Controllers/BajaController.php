<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\Control_IpRequest;
use App\Http\Requests\EditarRequest;
use Yajra\DataTables\Datatables;
use App\Models\control_ip;
use App\Models\Persona;
use App\Models\Resguardo;
use App\Models\resguardo_detail;
use Faker\Provider\ar_JO\Person;
use App\Models\Area;
use App\Models\jefatura;

class BajaController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:SalidaEquipo.index')->only('index');
        $this->middleware('can:SalidaEquipo.index')->only('create');
        $this->middleware('can:SalidaEquipo.index')->only('edit');
    }
    /*Consulta para la vista del Listado*/
    public function index()
    {
        $fecha=date("Y-m-d H:i:s");
        $data = control_ip::ControlTemp($fecha)->get();

        foreach ($data as $controlVence){
        $control_ip = control_ip::findOrFail($controlVence->id);
        if ($control_ip){
            $control_ip->estado = 2;
            $control_ip->estatus = 1;
            $control_ip->fechaActualizacion = $control_ip->fechaActualizacion;
            $control_ip->update();
        }
        
        }
        return view('Control_Ip.Baja');

        
    }
    public function create(Request $request)
    {  
        $fecha=date("Y-m-d H:i:s");
        $data = control_ip::ControlTemp($fecha)->get();

        foreach ($data as $controlVence){
        $control_ip = control_ip::findOrFail($controlVence->id);
        if ($control_ip){
            $control_ip->estado = 2;
            $control_ip->estatus = 1;
            $control_ip->fechaActualizacion = $control_ip->fechaActualizacion;
            $control_ip->update();
        }
        
        }
        $data = control_ip::ControlDisponibilidad()->get();

        return view('Control_Ip.Disponibilidad',compact('data'));

    }


    public function store(Request $request)
    {
    }
    /*Baja de control ip*/
    public function edit(Request $request, $id)
    {
        $control_ip = control_ip::findOrFail($id);
        $control_ip->estado = 2;
        $control_ip->estatus = 1;
        $control_ip->update();
        return redirect('/Control_Ip'); 
         
    }

    public function update(Control_IpRequest $request, $id)
    { 
         
    }

     
    public function getControl(Request $request)
    {
        
    }
     //consulta de areas para el listado
     public function getBajaControl(Request $request)
     {
         if ($request->ajax()) {
             $data = control_ip::ControlBaja()->get();
             return Datatables::of($data)
                 ->addIndexColumn()
                 ->addColumn('action', function($row){
                     $actionBtn = ' <a href="BajaIP/'.$row->id.'/edit"   ALIGN="center"style="color:#3A3E3C; "><span class="badge badge-danger">Baja</span></a> ';
                     return $actionBtn;
                 })
                 ->addIndexColumn()
                 ->addColumn('action2', function($row){
                     if($row->estatus==2){
                         $actionBtn = "Ocupado";
                     }else{
                         $actionBtn = "Libre";
                     }
                     return $actionBtn;
                 })
                 ->addIndexColumn()
                 ->addColumn('fechab', function($row){
                     if($row->vence){
                         $actionBtn = $row->vence;
                     }else{
                         $actionBtn = $row->fechaActualizacion;
                     }
                     return $actionBtn;
                 })
                
                 ->rawColumns(['action2'],['action'],['fechab'])
                 ->make(true);
         }
     }
     //consulta de disponibilidad de direcciones ip
     public function getDisponibilidad(Request $request)
     {
         if ($request->ajax()) {
             $data = control_ip::ControlDisponibilidad()->get();
             return Datatables::of($data)
                 ->addIndexColumn()
                 ->addColumn('action', function($row){
                      if($row->estatus==1  ){
                        $actionBtn = '<span class="badge badge-success">Libre</span>';
                        }else{
                            $actionBtn = '<span class="badge badge-danger">Ocupado</span>';
                        }
                        return $actionBtn;
                    
                     })
               
                 ->rawColumns(['action'])
                 ->make(true);
         }
     }
    
 
}
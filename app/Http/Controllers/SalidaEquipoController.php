<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Models\Resguardo;
use App\Models\Persona;
use Yajra\DataTables\Datatables;
use App\Models\salidaEquipo;
use App\Models\Detallesalida;
use App\Http\Requests\SalidaEquipoRequest;
use App\Models\Area;
use Carbon\Carbon;


class SalidaEquipoController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:SalidaEquipo.index')->only('index');
        $this->middleware('can:listadoSalida.listadoSalidas')->only('listadoSalidas');
    }
    //Bienes disponibles para salida de un resguardante 
    public function index(Request $request)
    {
        //consultar personas de cierta area 
        $idArea = $request->get('area');
        $personas = Persona::consultarPersonas($idArea)->get();

        //consultar area 
        $areas = Area::all()->where('Nombre', '!=', 'ADMON SISTEMAS');
        //consultar bienes de un resguardante
        $nombre = $request->get('resguardante');
        $Referencia = $request->get('Referencias');
        $FechaNotificacion = $request->get('Notificacion');
        $FechaVencimiento = $request->get('dates');
        $id = $request->get('ids');
 
        $resguardos = Resguardo::buscarporResguardoParaSalir($nombre)->get();
        $Salidas = Resguardo::BuscarporReferenciaSalidasBien($nombre)->get();
        return view('Resguardos.UTIC.SalidaEquipo', compact('personas', 'Salidas', 'resguardos', 'Referencia', 'FechaVencimiento', 'FechaNotificacion', 'areas', 'id', 'idArea', 'nombre'));
    }

    //registrar salida de equipos
    public function store(SalidaEquipoRequest $request)
    {
        $request->validated();
        $Biens = $request->get('bienes');
        $Referencia = $request->get('Referencia');
        $FechaVencimiento = $request->get('FechaVencimiento');
        $FechaNotificacion = $request->get('FechaNotificacion');
        $FechaSistema =  $request->get('FechaNotificacion');
        $resguardante =  $request->post('usuarioname');
        $id = $request->get('id');
        // separando el bienes 
        $Bien = explode(",", $Biens);
        //registrar salida de bienes seleccionados

        $array =  array();
        foreach ($Bien as $bien) {
            $resguardos = Resguardo::ValidarTemporal($resguardante, $bien)->get();
            foreach ($resguardos as $res) {
                $msj="El articulo con inventario :". $res->ClaveBien. " se encuentra asignado temporalmente a :". $res->Nombre." ".$res->ApellidoP." ".$res->ApellidoM." cambie y/o cancele el resguardo antes de continuar";
                if ($res) {
                    array_push($array, $msj);
                }
            }
        }
      /*  if (count($array) > 0) {
            return back()->withErrors($array)->with('Referencia', $Referencia)->with('FechaVencimiento', $FechaVencimiento)->with('FechaNotificacion', $FechaNotificacion)->with('id', $id);
           } else {
           
           foreach ($Bien as $bien) {
            //se crea una salida de equipos
            if (!$id) {
                $salidaEquipo = new salidaEquipo();
                $salidaEquipo->referencia = $Referencia;
                $salidaEquipo->fecha_notificacion = $FechaSistema;
                $salidaEquipo->fecha_vencimiento = $FechaVencimiento;
                $salidaEquipo->Activo = 1;
                $salidaEquipo->save();
                $id = $salidaEquipo->id;
            }
            //Se registran los bienes salientes
            $detalleSalida = new Detallesalida();
            $detalleSalida->salida_equipos_id = $id;
            $detalleSalida->bien_id = $bien;
            $detalleSalida->Activo = 1;
            $detalleSalida->save();
        }
        return  back()->with('Referencia', $Referencia)->with('FechaVencimiento', $FechaVencimiento)->with('FechaNotificacion', $FechaNotificacion)->with('id', $id)->with('Listo', "Salida registrada");
    
        }*/


        foreach ($Bien as $bien) {
            //se crea una salida de equipos
            if (!$id) {
                $salidaEquipo = new salidaEquipo();
                $salidaEquipo->referencia = $Referencia;
                $salidaEquipo->fecha_notificacion = $FechaSistema;
                $salidaEquipo->fecha_vencimiento = $FechaVencimiento;
                $salidaEquipo->Activo = 1;
                $salidaEquipo->save();
                $id = $salidaEquipo->id;
            }
            //Se registran los bienes salientes
            $detalleSalida = new Detallesalida();
            $detalleSalida->salida_equipos_id = $id;
            $detalleSalida->bien_id = $bien;
            $detalleSalida->Activo = 1;
            $detalleSalida->save();
        }
        return  back()->with('Referencia', $Referencia)->with('FechaVencimiento', $FechaVencimiento)->with('FechaNotificacion', $FechaNotificacion)->with('id', $id)->with('Listo', "Salida registrada");



    }
    //lista de equipos salientes por salida
    public function lista($id, $area)
    {
        $resguardo = Resguardo::buscarporReferenciaSalida($id)->get();
        return view('Resguardos.UTIC.ListadoResguardoSaliente', compact('resguardo', 'id', 'area'));
    }
    //lista de equipos salientes por salida
    public function listas(Request $request)
    {
        $Referencia = $request->get('Referencia');
        $resguardo = Resguardo::buscarporNombSalida($Referencia)->get();
        return view('Resguardos.UTIC.VerSalida.ListadoResguardoSaliente', compact('resguardo'));
    }
    //Salida de todos los quipos salientes
    public function listadoSalidas($id)
    {
      //  $Salidas = Resguardo::buscarporReferenciaSalidasId($id)->get();
      $Salidas = Resguardo::buscarporReferenciaSalida($id)->get();

        return view('Resguardos.UTIC.ListadoResguardosSalientes', compact('Salidas'));
    }
    //consulta de Salida de Equipo Bien Utic  listado
    public function getSalidaEquipo(Request $request, $id)
    {
        if ($request->ajax()) {
            $data = Resguardo::buscarporReferenciaSalidasId($id)->get();
            return Datatables::of($data)->make(true);
        }
    }





    //Salida de todos los quipos salientes
    public function getSalidaEquipoRed(Request $request)
    {

        if ($request->ajax()) {
            $data = salidaEquipo::BuscarporSalidas()->get();
            return Datatables::of($data)

                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = ' <a href="ListadoSalida/' . $row->id . '"class="fa  fa-eye  fa-2x" ALIGN="center"style="color:#3A3E3C; "></a> ';
                    return $actionBtn;
                })
                ->addIndexColumn()
                ->addColumn('entregar', function ($row) {
                    $actionBtn = ' <a href="SalidaEquipo/' . $row->id . '"class="fa fa-minus-circle fa-2x" ALIGN="center"style="color:#3A3E3C; "></a> ';
                    return $actionBtn;
                })
                ->rawColumns(['action', 'entregar'])
                ->make(true);
        }
    }
    // Desactivar salida
    public function show($id)
    {
        $Biensaliente = Detallesalida::where('salida_equipos_id', $id)->get();
        $salida = salidaEquipo::find($id);

        foreach ($Biensaliente as $Biensaliente) {
            $Biensaliente->Activo = 0;
            $Biensaliente->update();
        }
        $salida->Activo = 0;
        $salida->update();
        return redirect('ListadoSalidas');
    }

    //Salida de todos los quipos salientes
    public function listadoSalidass()
    {
        return view('Resguardos.UTIC.SalidaReducido');
    }


    public function listaSalidas($id)
    {

        $resguardo = salidaEquipo::Detalle($id)->get();
        return view('Resguardos.UTIC.ListadoResguardosSalientes', compact('resguardo'));
    }



    //Descargar reporte de salida de equipos
    public function downloadSalidaPDF($id, $idArea)
    {
        $Resguardo = Resguardo::buscarporReferenciaSalida($id)->get();
        $areas = Area::buscarAreaReporteSalida($idArea)->get();
        $pdf = PDF::loadView('Resguardos.UTIC.Reportes.ReporteSalidaEquipo', compact('Resguardo', 'areas', 'idArea'));
        return $pdf->setPaper('a4', 'landscape')
            ->stream('Salida Equipos.pdf');
    }
}

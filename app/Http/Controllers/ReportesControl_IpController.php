<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\Control_IpRequest;
use App\Http\Requests\EditarRequest;
use Yajra\DataTables\Datatables;
use App\Models\control_ip;
use App\Models\Persona;
use App\Models\Resguardo;
use App\Models\resguardo_detail;
use Faker\Provider\ar_JO\Person;
use App\Models\Area;
use App\Models\jefatura;
use App\Exports\ControlAreaExport;
use App\Exports\ControlJefaturaExport;
use App\Exports\ControlSegmentacionExport;
use App\Exports\ControlDefinitivoExport;
use App\Exports\ControlGeneralExport;
use Maatwebsite\Excel\Facades\Excel;

class ReportesControl_IpController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:SalidaEquipo.index')->only('index');
        $this->middleware('can:SalidaEquipo.index')->only('create');
        $this->middleware('can:SalidaEquipo.index')->only('edit');
    }
    
    
    /*Consulta para la vista del Listado*/
    public function index(Request $request)
    {
        $idArea = $request->get('area');
        $idjefatura = $request->get('jefatura');
        $Segmentacion = $request->get('Segmentacion');
        $Definitivo = $request->get('Definitivo');
        $filtro = $request->get('filtro');

        $areas = Area::all()->where('Nombre', '!=', 'ADMON SISTEMAS');
        $jefatura = jefatura::all();
       
        // Reporte por área
        if($idArea){
             // in controller
            ob_end_clean(); // this
            ob_start(); // and this
            return Excel::download(new  ControlAreaExport($idArea), ' Control_IP_Area.xlsx');

        // Reporte por Jefatura
        }else if($idjefatura){
             // in controller
            ob_end_clean(); // this
            ob_start(); // and this
            return Excel::download(new  ControlJefaturaExport($idjefatura), ' Control_IP_Jefatura.xlsx');
        
        //Segmentación
        }else if($Segmentacion ){
            // in controller
            ob_end_clean(); // this
            ob_start(); // and this
            return Excel::download(new  ControlSegmentacionExport($Segmentacion), ' Control_IP_Segmentación.xlsx');
        //Segmentación
        }else if($Definitivo ){
            // in controller
            ob_end_clean(); // this
            ob_start(); // and this
            return Excel::download(new  ControlDefinitivoExport($Definitivo), ' Control_IP_Definitivo.xlsx');
        }else if($filtro ==5){
             // in controller
             ob_end_clean(); // this
             ob_start(); // and this
             return Excel::download(new  ControlGeneralExport(), ' Control_IP_General.xlsx');
        }

        return view('Control_Ip.Reportes')->with('areas', $areas)->with('jefatura', $jefatura);

       
    }

    /*Muestra la vista para dar de alta */
    public function create(Request $request)
    { 
      
    }


    /*Insertando un nuevo en la bd*/
    public function store(Control_IpRequest $request)
    {
      
    }
    /*Mostrando la vista para Editar */
    public function edit(Request $request, $id)
    {
      
    }

    /*Actualizando un control*/
    public function update(Control_IpRequest $request, $id)
    {
     
    }
        
 
}
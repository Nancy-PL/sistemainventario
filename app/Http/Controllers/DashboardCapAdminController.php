<?php

namespace App\Http\Controllers;


use Yajra\DataTables\Datatables;
use Illuminate\Http\Request;
use App\Models\Bienes;

class DashboardCapAdminController extends Controller
{
   
   
     //Obtener datos para el home de los ultimos bienes agregados en 3 dias
    public function getHistorialBienes(Request $request)
    {
        if ($request->ajax()) {
            $data = Bienes::bienesHistorialHoy()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '<a href="/Bienes/' . $row->id . '/edit"class="fa  fa-edit  fa-lg" ALIGN="center"style="color:#3A3E3C; "></a> <a href="/Bienes/' . $row->id . '"class="fa  fa-eye  fa-2x" ALIGN="center"style="color:#3A3E3C; "></a> ';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
     //Obtener datos para la tabla SI de los ultimos 3 dias
     public function getHistorialBienesSI(Request $request)
     {
         if ($request->ajax()) {
             $data = Bienes::bienesHistorialSIHoy()->get();
             return Datatables::of($data)
                 ->addIndexColumn()
                 ->addColumn('action', function ($row) {
                     $actionBtn = '<a href="/Bienes/' . $row->id . '/edit"class="fa  fa-edit  fa-lg" ALIGN="center"style="color:#3A3E3C; "></a> <a href="/Bienes/' . $row->id . '"class="fa  fa-eye  fa-2x" ALIGN="center"style="color:#3A3E3C; "></a> ';
                     return $actionBtn;
                 })
                 ->rawColumns(['action'])
                 ->make(true);
         }
     }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Datatables;
use App\Models\Bienes;
use App\Models\Resguardo;


class HistorialBienController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:HistorialBienesUTIC.indexSI')->only('indexSI');
        $this->middleware('can:HistorialBien.index')->only('index');



    }
    /*  Vista del historial      */
    public function index(Request $request)
    {
        $Nombre=$request->get('Noserie');
        $Bienes=Bienes::bienesHistorialUTIC($Nombre,'UA')->get();

        return view('Bienes.Administrativos.HistorialBien',compact('Bienes'));
    }
    //Obtener datos para la tabla UA
    public function getHistorialBienes(Request $request)
    {
        if ($request->ajax()) {
            $data = Bienes::bienesHistorial("UA")->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '';
                    if($row->ActivoR==1 && $row->ActivoB==1){
                    $actionBtn = '<span class="badge badge-success">Activo</span>';
                    }else{
                        $actionBtn = '<span class="badge badge-danger">Inactivo</span>';
                    }
                    return $actionBtn;
                    
                })
                ->addIndexColumn()
                ->addColumn('nombreCompleto', function ($row) {
                    $nombreCompleto = $row->Nombre.' '.$row->ApellidoP.' '.$row->ApellidoM;
                    return $nombreCompleto;
                })
                ->addIndexColumn()
                ->addColumn('UserAlta', function ($row) {
                    if($row->name==null){
                        $UserAlta = $row->usuario_alta;
                    }else{
                        $UserAlta = $row->name;
                    }
                    return $UserAlta;
                })
                ->rawColumns(['UserAlta'])
                ->addIndexColumn()
                ->addColumn('UsuarioBaja', function ($row) {
                    if($row->userBaja==null){
                        $Userbaja = $row->usuario_baja;
                    }else{
                        $Userbaja = $row->userBaja;
                    }
                    return $Userbaja;
                })
                ->rawColumns(['UsuarioBaja'])
                ->rawColumns(['action','nombreCompleto','UserAlta','UsuarioBaja'])
                ->make(true);
        }
    }
    /*  Vista del historial  SI    */
    public function indexSI(Request $request)
    {
        $Nombre=$request->get('Noserie');
        $Bienes=Bienes::bienesHistorialUTIC($Nombre,'SI')->get();
        
        return view('Bienes.UTIC.HistorialBienUTIC', compact('Bienes'));

    }
    //Obtener datos para la tabla SI
    public function getHistorialBienesSI(Request $request)
    {
        if ($request->ajax()) {
            $data = Resguardo::bienesHistorialSI()->get();
            return Datatables::of($data)
            ->addIndexColumn()  
            ->addColumn('action', function ($row) {
                $actionBtn = '';
                if($row->ActivoR==1 && $row->ActivoB==1){
                    $actionBtn = '<span class="badge badge-success">Activo</span>';
                }else{
                    $actionBtn = '<span class="badge badge-danger">Inactivo</span>';
                }
                return $actionBtn;
                
            })
            ->addIndexColumn()
            ->addColumn('nombreCompleto', function ($row) {
                $nombreCompleto = $row->Nombre.' '.$row->ApellidoP.' '.$row->ApellidoM;
                return $nombreCompleto;
            })
            ->addIndexColumn()
            ->addColumn('UserAlta', function ($row) {
                if($row->name==null){
                    $UserAlta = $row->usuario_alta;
                }else{
                    $UserAlta = $row->name;
                }
                return $UserAlta;
            })
            ->rawColumns(['UserAlta'])
            ->addIndexColumn()
            ->addColumn('UsuarioBaja', function ($row) {
                if($row->userBaja==null){
                    $Userbaja = $row->usuario_baja;
                }else{
                    $Userbaja = $row->userBaja;
                }
                return $Userbaja;
            })
            ->rawColumns(['UsuarioBaja'])
            ->rawColumns(['action','nombreCompleto','UserAlta','UsuarioBaja'])
            ->make(true);
        }
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;


use App\Models\Bienes;
use App\Exports\ControlComputoExport;
use App\Exports\TotalExport;
use App\Exports\BienesExport;
use App\Exports\BienesUticExport;
use App\Exports\AdmiResguardoExport;
use App\Exports\AreaResguardoExport;
use App\Exports\NoControlExport;
use App\Exports\NoInventarioComputoExport;
use App\Exports\ClasificacionImprimirExport;
use App\Exports\AreaImprimirExport;
use App\Exports\UsuariosExport;
use App\Exports\BajaComputoExport;
use App\Exports\BajaMueblesExport;
use App\Exports\MueblesGeneralExport;
use App\Exports\ComputoGeneralExport;
use Maatwebsite\Excel\Facades\Excel;



class ReportesController extends Controller
{
     //protección de rutas
     public function __construct()
     {
         $this->middleware('can:ReportesUTIC.reportesUTIC')->only('reportesUTIC');
     }
    //Vista para descargar reportes
    public function index()
    {
        $bienes = bienes::all();
        return view('Bienes.Reportes')->with('bienes', $bienes);
    }
    //En esta función se esta llamando un nuevo control Export haciendo referencia a las listas
    //de bienes para bienes informaticos 
    public function ControlComputo()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new ControlComputoExport, 'BienesInformaticos.xlsx');
    }



    public function reportes()
    {
        $bienes = bienes::all();
        return view('Bienes.Administrativos.Reportes.Reportes', compact('bienes'));
    }
    public function reportesUTIC()
    {
        $bienes = bienes::all();
        return view('Bienes.UTIC.Reportes.ReportesUTIC', compact('bienes'));
    }


    public function export()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new BienesExport, 'ListaBienes.xlsx');
    }
    public function BienesComputo()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new BienesUticExport, 'ListaBienesComputo.xlsx');
    }

    public function  AdmiResguardo()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new  AdmiResguardoExport, 'ResguardosMuebles.xlsx');
    }
    public function AreaResguardo()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new  AreaResguardoExport, 'AreaResguardo.xlsx');
    }

    public function  NoControl()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new  NoControlExport, ' NoControlBienes.xlsx');
    }
    public function  TotalExport()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new  TotalExport, ' NoInventario.xlsx');
    }
    public function  NoInventarioComputo()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new  NoInventarioComputoExport, ' NoInventariocomputo.xlsx');
    }

    public function  ClasificaciónImprimir()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new  ClasificacionImprimirExport, 'Clasificacion.xlsx');
    }
    public function  AreaImprimir()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new  AreaImprimirExport, 'Áreas.xlsx');
    }
    public function  computo()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new  TotalExport, 'BienesComputo.xlsx');
    }
    public function  UsuarioImprimir()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new UsuariosExport, 'ListaUsuarios.xlsx');
    }

    public function  BajaMuebles()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new BajaMueblesExport, 'MueblesBajas.xlsx');
    }

    public function   BajaComputo()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new  BajaComputoExport, 'BajaComputo.xlsx');
    }
    public function   MueblesGeneral()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new  MueblesGeneralExport, 'MueblesGeneral.xlsx');
    }
    public function  ComputoGeneral()
    {

        // in controller
        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new  ComputoGeneralExport, 'ComputoGeneral.xlsx');
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Resguardo;
use Datatables;


class DatatableController extends Controller
{
    public function resguardo()
    {
        $resguardo = Resguardo::select('id', 'tipo', 'Departamento', 'activo')->get();

      return datatables()->of($resguardo)->toJson();
    }
}

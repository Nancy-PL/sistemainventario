<?php

namespace App\Http\Controllers;
use Yajra\DataTables\Datatables;
use Illuminate\Http\Request;
use App\Models\resguardo_temporal;
use Illuminate\Support\Facades\DB;
use App\Models\Persona;


class ListaTodoRTemporalController extends Controller
{
     //protección de rutas
     public function __construct()
     {
         $this->middleware('can:ResguardosTemporales.index')->only('index');
         //$this->middleware('can:ResguardosTemporales.edit')->only('edit');
     }
    //lista de resguardos temporales
    public function index()
    {
        return view('Resguardos.UTIC.TodosListaResguardoTemporal');
    }
    //consulta de Los Resguardos temporales para el listado
    public function getResguardoTemp(Request $request)
    {
        if ($request->ajax()) {
           // $data = resguardo_temporal::resguardoTemporal()->distinct('personas.id')->get();
           $data = Persona::resguardoTemporal()->distinct('personas.id')->get();


            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('Nombres', function($row){
                $Nombres = $row->Profesion.' '.$row->Nombre.' '.$row->ApellidoP.' '.$row->ApellidoM;
                return $Nombres;
            })

            ->addColumn('action', function ($row) {
                if($row->Activo == 1){
                    return $action= '<a href="Restemp/' . $row->resguardoId . '" class="fa  fa-edit  fa-lg" ALIGN="center"style="color:#3A3E3C; "></a> ';
                }
            })

            ->rawColumns(['Nombres','action'])
            ->make(true);
        }
    }

    //Metodo de edición de campos en la tabla de clasificación y tipo bien
    public function edit($id)
    {
    }
}

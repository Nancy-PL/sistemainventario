<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Resguardo;
use Illuminate\Support\Facades\DB;
use App\Models\Persona;
use App\Models\resguardo_detail;
use Carbon\Carbon;
use App\Models\Area;
use App\Models\Bienes;
use App\Models\clasificacion;
use App\Http\Requests\ReasignacionRequest;
use App\Models\BajaPersonal;
use Yajra\DataTables\Datatables;


class ReasignacionResguardoUticController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:ReasignacionUtic.index')->only('index');
    }
 //listado de reasignacion
 public function index(Request $request)
 {
     $personas = Persona::latest()->where('Activo', '1')->get();
     $areas = Area::almacen()->get();
     $TipoResguardo = $request->get('EstadoT');
     //consultar bienes de un resguardante
     $nombre = $request->get('resguardanteid');
     $almacen = Area::almacen()->get();
     if ($TipoResguardo == "ALMACEN") {
         $nombre = $request->get('almacenNombre');
         $nombre = $this->almacen($nombre);
         if($nombre==0){
             return back()->with('Ups', "El almacenista no esta registrado como personal")->with( 'nombre' ,$nombre )->with( 'TipoResguardo' ,$TipoResguardo );

         }

     }
     $resguardos = Resguardo::buscarpor($nombre, $TipoResguardo,'SI')->get();

        return view('Resguardos.UTIC.reasignacion', compact('personas', 'resguardos', 'areas'))->with( 'nombre' ,$nombre )->with( 'TipoResguardo' ,$TipoResguardo );
    }
    //consulta de Reasignacion Bien Utic para el listado
    public function getReasignacionUtic(Request $request)
    {
        if ($request->ajax()) {
            $data =  Persona::latest()->where('Activo', '1')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = ' <input class="form-check-input ml-5" type="radio" name="persona_id"  value="' . $row->id . '" required>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
     //buscar personal de almacen
     public function almacen($nombre2)
     {
 
         // separando el nombre del almacenista
         $prof = explode(".", $nombre2);
         $contaprof = count(explode(".", $nombre2)) - 1;
 
         $nombre = explode(" ", $prof[$contaprof]);
         $conta = count(explode(" ", $nombre2));
 
         if ($conta > 5) {
             $nombres = $nombre[1] . " " . $nombre[2] . " " . $nombre[3];
             $Apellido = $nombre[4];
             $Apellidos = $nombre[5];
         } elseif ($conta > 4) {
             $nombres = $nombre[1] . " " . $nombre[2];
             $Apellido = $nombre[3];
             $Apellidos = $nombre[4];
         } else {
             $nombres = $nombre[1];
             $Apellido = $nombre[2];
             $Apellidos = $nombre[3];
         }
 
         $personaalmacen = Persona::buscarAlmacen($nombres, $Apellido, $Apellidos)->get();
         $idPersona=0;
         foreach ($personaalmacen as $personaalmacen) {
             $idPersona = $personaalmacen->id;
         }
         if($idPersona!=0){
             return $idPersona;
         }else{
             return 0;
         }
     }
  

  //validar existencia de una persona
  public function ValidarPersona($id)
  {
      $resguardos = DB::table('resguardos')
          ->select('resguardos.*')
          ->where('persona_id', '=', $id)
          ->where('tipo', '=', 'PERMANENTE')
          ->get();
      foreach ($resguardos as $resguardo) {
          if ($resguardos) {
              return $resguardo->id;
          } else {
              return 0;
          }
      }
  }

    //verificar la existencia de un bien al resguardo en caso de no haber se cancela
    public function  verificarExistenciaResguardo($id)
    {
        $resguardosActivos = resguardo_detail::resguardosActivos($id,'SI')->first();
        $resguardoAnterior = Resguardo::find($id);
        if(!$resguardosActivos){
            $resguardoAnterior->SI = "0";
            if($resguardoAnterior->UA==0 ){
              $resguardoAnterior->activo = 0;
          }
          $resguardoAnterior->update();

         }



        $verificar = resguardo_detail::where('resguardo_id', $id)->where('Activo', '=', 1)->get();

        foreach ($verificar as $verificar) {
            return 1;
        }
        
        //Cancelar resguardo
        $resguardoAnterior->activo = 0;
        $resguardoAnterior->delete_at = Carbon::now();
        $resguardoAnterior->update();
        return 0;
    }
     //Calculando el número de inventario o No de control segun la clasificación dada 
     protected function ClaveBien($letra_id, $clasificacion_id)
     {
         $clasificacion = clasificacion::find($clasificacion_id);
         $clave=$clasificacion->clave;
         $areas  = Area::find($letra_id);
 
 
         if ($clasificacion->NoInventario == 1) {
             $long=mb_strlen($clave)+11;
 
             $contadorBien = DB::table('bienes')
             ->select('ClaveBien')
             ->where('ClaveBien', 'like', '%-'.$clave.'%')
             ->whereRaw('LENGTH(ClaveBien) = ?',$long)
             ->count();
 
             $contadorBien = $contadorBien + 1;
             $contador = str_pad($contadorBien, 4, "0", STR_PAD_LEFT);
             $ClaveBien = "OSFE-" . $areas->letra . "-" . $clasificacion->clave . ($contador);
         } else {
             return 0;
         }
 
         return $ClaveBien;
     }
     //reasignando resguardo
     public function store(ReasignacionRequest $request)
     {
         $resguardante = $request->get('resguardante');
         $biens = $request->get('resguardos');
         $persona_id = $request->get('persona_id');
         $TipoResguardo = $request->get('Tipo');
         $fecha = Carbon::now();
         $nuevoResguardante_id = 0;
         $resguardanteAnterior = 0;
 
         if($resguardante!=$persona_id){
         //obteniendo los id de resguardo y verificando existencia
         $resguardante_id = $this->ValidarPersona($resguardante);
         $nuevoResguardante_id = $this->ValidarPersona($persona_id);
 
         // separando los bienes
         $bienes = explode(",", $biens);
         if($TipoResguardo=="ALMACEN"){
             $Person = Persona::find($persona_id);
             $area=$Person->area_id;

         }
          //reasignacion de bienes
          foreach ($bienes as $bien) {
            //Asignar No Inventario a los bienes de almacen
            if($TipoResguardo=="ALMACEN"){
                $Bienes = Bienes::find($bien);
                $clasificacion=$Bienes->Clasificacion_id;
                $NoInv=$this->ClaveBien($area, $clasificacion);

                if($NoInv!=0){
                    $Bienes->ClaveBien=$NoInv;

                }
                $Bienes->update();
            }

          //En caso de ser el primer resguardo de la persona se crea un nuevo resguardo
          if ($nuevoResguardante_id == 0) {
            $Resguardo = new Resguardo();
            $Resguardo->tipo = "PERMANENTE";
            $Resguardo->persona_id = $persona_id;
            $Resguardo->SI = "1";
            $Resguardo->activo = 1;
            $Resguardo->save();
            $nuevoResguardante_id = $Resguardo->id;
        } else {
            $resguardo = Resguardo::find($nuevoResguardante_id);
            $resguardo->activo = 1;
            $resguardo->SI = "1";
            $resguardo->save();
        }
          //desactivar resguardo anterior si el bien estaba al resguardo de alguien y cancelar
          $resguardo_detailAnt = resguardo_detail::where('bien_id', $bien)->get();
          foreach ($resguardo_detailAnt as $resguardo_detailAnt) {
              $resguardo_detailAnt->fechaCancelacion = $fecha;
              $resguardo_detailAnt->Activo = 0;
              $resguardo_detailAnt->update();
              $resguardanteAnterior = $resguardo_detailAnt->resguardo_id;
          }
          //reasgnacion al nuevo resguardante
          $resguardo_detail = new resguardo_detail();
          $resguardo_detail->resguardo_id = $nuevoResguardante_id;
          $resguardo_detail->Activo = 1;
          $resguardo_detail->fechaAsignacion = $fecha;
          $resguardo_detail->bien_id = $bien;
          $resguardo_detail->ResguardoAnterior_id = $resguardante_id;
          $resguardo_detail->User = auth()->user()->id;
          $resguardo_detail->save();
      }


             //verificar existencia de bienes al resguardo activos
             $resguardo = $this->verificarExistenciaResguardo($resguardanteAnterior);
            
            }
                //Baja del personal 
                $baja = $request->get('baja');
                if ($baja == 1) {
                    //si la persona a la que se le asigna el bien es la misma se le niega la baja
                    if ($resguardante == $persona_id) {
                        return back()->with('UPS', "Reasignacion correcta pero no es posible dar de baja a la persona que se le asigno el bien");
                    }
                    $persona = Persona::find($resguardante);
                    //Validar que el usuario no tenga ningun resguardo
                    $resguardoActivo = DB::table('resguardos')
                        ->select('persona_id')
                        ->where('persona_id', '=', $resguardante)
                        ->where('activo', '=', '1')
                        ->groupBy('persona_id')
                        ->count();
                    if ($resguardoActivo > 0) {
                        return back()->with('UPS', "No se pudo dar de baja al personal " . $persona->Nombre . " " . $persona->ApellidoP . " " . $persona->ApellidoM . " porque cuenta con un resguardo activo de bienes muebles.");
                    }
                    $user = DB::table('users')
                        ->select('persona_id')
                        ->where('persona_id', '=', $resguardante)
                        ->where('Activo', '=', 1)
                        ->groupBy('persona_id')
                        ->count();
                    //si el personal cuenta con un usuario se le niega la baja
                    if ($user > 0) {
                        return back()->with('UPS', "No se pudo dar de baja al personal " . $persona->Nombre . " " . $persona->ApellidoP . " " . $persona->ApellidoM . " porque cuenta con un usuario del sistema activo.");
                    } else {
                        $persona->Activo = 0;
                        $persona->update();
                        //dar de baja al personal
                        $bajaPersona = new BajaPersonal();
                        $bajaPersona->Motivo = $request->get('Motivo');
                        $bajaPersona->UsuarioBaja = auth()->user()->id;
                        $bajaPersona->Fecha = $fecha;
                        $bajaPersona->persona_id = $resguardante;
                        $bajaPersona->save();
                        return redirect('/ListaAsignadosUtic/' . $nuevoResguardante_id);
                    }
                }
                if ($nuevoResguardante_id == 0) {
                    return redirect('/reasignacion');
                } else {
                    return redirect('/ListaAsignadosUtic/' . $nuevoResguardante_id);
                }
            }
        }
        

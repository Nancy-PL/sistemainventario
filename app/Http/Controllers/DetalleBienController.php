<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\BienesRequest;
use App\Models\Bienes;
use App\Models\marca;
use App\Models\Area;
use App\Models\BienesAdministrativos;
use App\Models\clasificacion;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\split;

class DetalleBienController extends Controller
{
      //protección de rutas
      public function __construct()
      {
          $this->middleware('can:DetalleBien.index')->only('index');
         
      }
    
    public function index()
    {
        $bienes = DB::table('bienes')
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('bienes_administrativos', 'bienes_administrativos.bien_id', '=', 'bienes.id')
            ->select('marcas.*', 'clasificacions.*', 'bienes_administrativos.*', 'bienes.*')
            ->where('bienes.Departamento','=','UA')
            ->get();
        //return view('Bienes.Administrativos.index')->with('bienes',$bienes)->with('areas', $Area)->with('marcas',$marcas)->with('clasificacions',$clasificacions);
        return view('Bienes.Administrativos.DetalleBien', compact('bienes'));
        // $DetalleBien = Bienes::all();
        //return view('Bienes.Administrativos.DetalleBien')->with('Bienes',  $DetalleBien);
        
    }

  
   //Insertar un nuevo bien
   public function store(BienesRequest $request)
   {
       $clasificacion=$request->get('Clasificacion_id');
       $letra=$request->get('area');
       $NoInventario=$this->ClaveBien($letra,$clasificacion);

       $bienes = new Bienes();
       $BienesAdministrativos = new Bienes();

       $bienes->Modelo = $request->get('Modelo');
       $bienes->NoSerie = $request->get('NoSerie');
       $bienes->Estado = $request->get('Estado');
       $bienes->NoInventarioAnterior = $request->get('NoInventarioAnterior');
       $bienes->Caracteristicas = $request->get('Caracteristicas');
       $bienes->Observaciones = $request->get('Observaciones');
       $bienes->Status = '1';
       $bienes->ClaveBien =$NoInventario;
       $bienes->Departamento = 'UA';
       $bienes->MejorasEquipo = 'NINGUNA';
       $bienes->Activo = '1';
       $bienes->Clasificacion_id = $request->get('Clasificacion_id');
       $bienes->marca_id = $request->get('marca_id');
       $bienes->save();

       $idBien = $bienes->id;
       $BienesAdministrativos->ubicacion = $request->get('Ubicacion');
       $BienesAdministrativos->factura = $request->get('factura');
       $BienesAdministrativos->bien_id = $idBien;
       $BienesAdministrativos->save();

       return redirect('/DetalleBien');
   }

    
    public function show($id)
    {
        //
    }

}
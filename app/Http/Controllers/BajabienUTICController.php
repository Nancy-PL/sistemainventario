<?php

namespace App\Http\Controllers;

use App\Http\Requests\BajaBienRequest;
use Illuminate\Http\Request;
use App\Models\HistorialBien;
use Carbon\Carbon;
use App\Models\Bienes;
use Yajra\DataTables\Datatables;
use App\Models\baja_bienes;
use App\Models\resguardo_detail;
use App\Models\Resguardo;
use App\Models\clasificacion;


class BajabienUTICController extends Controller
{
     //protección de rutas
     public function __construct()
     {
         $this->middleware('can:BajaBienUtic.index')->only('index');
         $this->middleware('can:ListabajaUtic.listaUtic')->only('listaUtic');
     }
    //vista de la lista de bienes Utic
    public function index()
    {
        $Bienes = resguardo_detail::bienesUticActivos()->get();

        return view('Bienes.UTIC.BajaBien', compact ('Bienes'));
    }
    //consulta de Bienes disponibles para el listado de baja
    public function getBajaBienesUtic(Request $request)
    {
        if ($request->ajax()) {
            $data = resguardo_detail::bienesUticActivos()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn =  '<input class="casilla" type="radio" name="bienes" value="' . $row->id . '" required>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    //verificar la existencia de un bien al resguardo en caso de no haber se cancela
    public function  verificarExistenciaResguardo($id)
    {
        $verificar = resguardo_detail::where('resguardo_id', $id)->where('Activo', '=', 1)->get();
        foreach ($verificar as $verificar) {
            return 1;
        }
        //Cancelar resguardo
        $resguardoAnterior = Resguardo::find($id);
        $resguardoAnterior->activo = 0;
        $resguardoAnterior->delete_at = Carbon::now();
        $resguardoAnterior->update();
        return 0;
    }
    //Dar de baja bienes
    public function store(BajaBienRequest $request)
    {
        $bien_id = $request->get('bienes');
        $fecha = Carbon::now();
        $EdoBuenos="";
        // separando el nombre del almacenista
        $Bien = explode(",", $bien_id);
        
        foreach ($Bien as $bien) {

            $bienes = Bienes::find($bien);

            if ($bienes->Estado == "MALO") {
                    //consultar bien y desactivarlo
                $bienes->activo = 0;
                $bienes->Status = 0;
                $bienes->update();
                $Noserie = $bienes->NoSerie;
                //registrar los motivos de la baja del bien
                $baja = new baja_bienes();
                $baja->Motivo_baja = $request->get('Motivo');
                $baja->Observaciones = $request->get('Observaciones');
                $baja->bienes_id  = $bien;
                $baja->FechaBaja  = $fecha;
                $baja->save();
                //Registrar el historial de baja del bien
                $HistorialBien =  HistorialBien::where('bien_id', $bien)->get();
                foreach ($HistorialBien as $HistorialBien) {
                    $HistorialBien->fecha_baja = $fecha;
                    $HistorialBien->usuario_baja = auth()->user()->id;
                    $HistorialBien->update();
                }

                //desactivar resguardo anterior y cancelando
                $resguardo_detailAnt = resguardo_detail::where('bien_id', $bien)->get();
                foreach ($resguardo_detailAnt as $resguardo_detailAnt) {
                    $resguardo_detailAnt->fechaCancelacion = $fecha;
                    $resguardo_detailAnt->Activo = 0;
                    $resguardo_detailAnt->update();
                    $resguardanteAnterior = $resguardo_detailAnt->resguardo_id;
                }
                //verificar existencia de bienes al resguardo activo
                $this->verificarExistenciaResguardo($resguardanteAnterior);
            } else {
                $c=clasificacion::find( $bienes->Clasificacion_id);

                if($c->NoInventario==1){
                    $EdoBuenos=$EdoBuenos."\n".$c->concepto." con número de inventario: ".$bienes->ClaveBien." Estado: ".$bienes->Estado.', ';
                }else{
                    $EdoBuenos=$EdoBuenos."\n".$c->concepto." con número de control: ".$bienes->ClaveBien." Estado: ".$bienes->Estado.', ';
                }
        }
    }
    if($EdoBuenos){
        return back()->with('UPS', "No es posible dar de baja porque los estados correspondientes no son malos: " . $EdoBuenos);

    }


        return back()->with('listo', "Operación correcta");
    }
    public function listaUtic()
    {
       
        return view('Bienes.UTIC.ListaBajaBienUtic');
    }

    //consulta de Bienes disponibles para el listado de baja
    public function getListaBajaUtic(Request $request)
    {
        if ($request->ajax()) {
            $data = Bienes::ListaBaja("SI")->get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('UserAlta', function ($row) {
                if($row->name==null){
                    $UserAlta = $row->usuario_alta;
                }else{
                    $UserAlta = $row->name;
                }
                return $UserAlta;
            })
            ->rawColumns(['UserAlta'])
            ->make(true);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HistorialBien;
use Carbon\Carbon;
use App\Models\Bienes;
use App\Models\baja_bienes;
use App\Models\resguardo_detail;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\BajaBienRequest;;
use App\Models\clasificacion;

use App\Models\Resguardo;
use Yajra\DataTables\Datatables;

class BajabienController extends Controller
{
    public function index()
    {
        return view('Bienes.Administrativos.BajaBien');
    }

      //protección de rutas
      public function __construct()
      {
          $this->middleware('can:BajaBien.index')->only('index');
       
      }

    //consulta de Bienes disponibles para el listado de baja
    public function getBajaBienes(Request $request)
    {
        if ($request->ajax()) {
            $data = Bienes::BajaBienes("UA")->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn =  '<input class="checkboxClass" type="checkbox" name="biens[]" value="' . $row->id . '" >';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }


    //verificar la existencia de un bien al resguardo en caso de no haber se cancela
    public function  verificarExistenciaResguardo($id)
    {
        $verificar = resguardo_detail::where('resguardo_id', $id)->where('Activo', '=', 1)->get();
        foreach ($verificar as $verificar) {
            return 1;
        }
        //Cancelar resguardo
        $resguardoAnterior = Resguardo::find($id);
        $resguardoAnterior->activo = 0;
        $resguardoAnterior->delete_at = Carbon::now();
        $resguardoAnterior->update();
        return 0;
    }
    //Dar de baja bienes
    public function store(BajaBienRequest $request)
    {

        $bien_id = $request->get('bienes');
        $fecha = Carbon::now();
        $EdoBuenos="";
        // separando el nombre del almacenista
        $Bien = explode(",", $bien_id);
        
        foreach ($Bien as $bien) {
            $bienes = Bienes::find($bien);

            if ($bienes->Estado == "MALO") {
                $bienes->activo = 0;
                $bienes->Status = 0;
                $bienes->update();
                //registrar los motivos de la baja del bien
                $baja = new baja_bienes();
                $baja->Motivo_baja = $request->get('Motivo');
                $baja->Observaciones = $request->get('Observaciones');
                $baja->FechaBaja  = $fecha;
                $baja->bienes_id  = $bien;
                $NoSerie =  $bienes->NoSerie;
                $baja->save();
                //Registrar el historial de baja del bien
                $HistorialBien =  HistorialBien::where('bien_id', $bien_id)->get();
                foreach ($HistorialBien as $HistorialBien) {
                    $HistorialBien->fecha_baja = $fecha;
                    $HistorialBien->usuario_baja = auth()->user()->id;
                    $HistorialBien->update();
                }
                //desactivar resguardo anterior y cancelando
                $resguardo_detail =  resguardo_detail::where('bien_id', $bien_id)->get();
                foreach ($resguardo_detail as $resguardo_detail) {
                    $resguardo_detail->fechaCancelacion = $fecha;
                    $resguardo_detail->Activo = 0;
                    $resguardo_detail->update();
                    $resguardanteAnterior = $resguardo_detail->resguardo_id;
                }
                //verificar existencia de bienes al reaguardo activos
                $this->verificarExistenciaResguardo($resguardanteAnterior);
            } else {
                $c=clasificacion::find( $bienes->Clasificacion_id);

                if($c->NoInventario==1){
                    $EdoBuenos=$EdoBuenos.", \n".$c->concepto." con número de inventario: ".$bienes->ClaveBien." Estado: ".$bienes->Estado;
                }else{
                    $EdoBuenos=$EdoBuenos.", \n".$c->concepto." con número de control: ".$bienes->ClaveBien." Estado: ".$bienes->Estado;
                }        
        }
    }
    if($EdoBuenos){
        return back()->with('UPS', "No es posible dar de baja porque los estados correspondientes no son malos: " . $EdoBuenos);

    }
    return back()->with('listo', "Operación correcta");

    }
    
    public function lista()
    {
        return view('Bienes.Administrativos.ListaBajaBien');
    }

    //consulta de Bienes disponibles para el listado de baja
    public function getListaBaja(Request $request)
    {
        if ($request->ajax()) {
            $data = Bienes::ListaBaja('UA')->get();
            return Datatables::of($data)
            ->addIndexColumn()
                ->addColumn('UserAlta', function ($row) {
                    if($row->name==null){
                        $UserAlta = $row->usuario_alta;
                    }else{
                        $UserAlta = $row->name;
                    }
                    return $UserAlta;
                })
                ->rawColumns(['UserAlta'])
            
            ->make(true);
        }
    }

}

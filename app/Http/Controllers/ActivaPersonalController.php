<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Persona;

class ActivaPersonalController extends Controller
{
      //protección de rutas
      public function __construct()
      {
          $this->middleware('can:ActivarPersonas.index')->only('index');
    
  
      }
    public function index()
    {

        return view('personas.ActivarPersonas');
    }

    //baja del personal
    public function store(Request $request)
    {
        $resguardante = $request->get('Persona');
        $persona = Persona::find($resguardante);
        $persona->Activo = 1;
        $persona->update();
        return back()->with('Listo', " El personal con nombre " . $persona->Nombre . " " .$persona->ApellidoP . " " . $persona->ApellidoM . " se activo correctamente.");
       
    }

}

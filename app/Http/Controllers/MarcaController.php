<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\MarcaRequest;
use App\Http\Requests\EditMarcaRequest;
use App\Models\marca;
use Illuminate\Routing\Route;

class MarcaController extends Controller
{
      //protección de rutas
      public function __construct()
      {
          $this->middleware('can:Marcas.index')->only('index');
          $this->middleware('can:Marcas.edit')->only('edit');
  
      }

    //Mostrando el listado de Marcas
    public function index()
    {
        $Marca = marca::all();
        return view('Catalogos.Marcas.Marcas')->with('marcas', $Marca);
    }

    //Agregando nuevo elemento en marca
    public function store(MarcaRequest $request)
    {
        $request->validated();
        $Marcas = new marca();
        $Marcas->nombre = $request->get('Marca');
        $Marcas->save();
        return redirect('/Marcas');
    }


    //Mostrando la vista de Editar marca de la marca seleccionada
    public function edit($id)
    {
        $Marcas = marca::find($id);
        return view('Catalogos.Marcas.EditarMarca')->with('marcas', $Marcas);
    }

    //Actualizando marca 
    public function update(EditMarcaRequest $request, $id)
    {
        $Marcas = marca::findOrFail($id);
          //Haciendo validaciones del  concepto de la tabla clasificacions
        $this->validate($request, [
            'Marca' =>"required|unique:marcas,nombre,$id"
        ]);
        $Marcas->nombre = $request->get('Marca');
        $Marcas->save();
        return redirect('/Marcas');
    }
}

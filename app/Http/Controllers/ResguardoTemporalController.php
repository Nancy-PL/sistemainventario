<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bienes;
use App\Models\resguardo_detail;
use App\Models\Persona;
use App\Models\resguardo_temporal;
use Illuminate\Support\Facades\DB;
use App\Models\Resguardo;
use Illuminate\Support\Carbon;
use App\Http\Requests\ResguardoTemporalRequest;
use App\Http\Requests\resguardos_temporal;
use App\Http\Requests\resguardos_temporalbitacora;
use App\Models\Area;
use App\Models\User;
use Barryvdh\DomPDF\Facade as PDF;
use Yajra\DataTables\Datatables;
use App\Models\logueo;
use App\Models\resguardo_temporalbitacora;
use DateTime;



class ResguardoTemporalController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:ResguardoTemporal.create')->only('create');
    }
    public function index()
    {
    }
    //Vista para registrar un resguardo temporal
    public function create()
    {
        $personas = Persona::all()->where('Activo', '1');
        return view('Resguardos.UTIC.ResguardoTemporal', compact('personas'));
    }
    //consulta de Resguardo Temporal Disponible Bien Utic para el listado
    public function getResguardoTemporalDis(Request $request)
    {
        if ($request->ajax()) {
            $data =  Resguardo::buscarResguardoTemporalDisponible()->get();
            return Datatables::of($data)
                ->addIndexColumn()                                          
                ->addColumn('action', function ($row) {
                    $actionBtn = '<input class="casilla" type="checkbox" name="biens[]" value="' . $row->bien_id . '" >

                    ';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    //validar la existencia de un resguardo temporal de dicha persona
    public function ValidarPersona($id)
    {
        $resguardos = DB::table('resguardos')
            ->select('resguardos.*')
            ->where('persona_id', '=', $id)
            ->where('tipo', '=', 'TEMPORAL')
            ->get();
        foreach ($resguardos as $resguardo) {
            if ($resguardos) {
                return $resguardo->id;
            } else {
                return 0;
            }
        }
    }
    //validar la existencia de un resguardo temporal de dicha persona
    public function ComprobarResguardoT($id,$bien)
    {
        $resguardosT = DB::table('resguardo_detail')
            ->select('resguardo_detail.*')
            ->where('resguardo_id', '=', $id)
            ->where('bien_id', '=', $bien)
            ->where('Activo', '=', '1')
            ->get();
        foreach ($resguardosT as $resguardo) {
            return 0;
        }
        return 1;
        
    }
    
    //Validar existencia de un resguardo temporal
    public function ValidarResguardoTemp($id)
    {
        $resguardos = DB::table('resguardos_temporal')
            ->select('resguardo_id')
            ->where('id', '=', $id)
            ->where('Activo', '=', 1)
            ->get();
        foreach ($resguardos as $resguardo) {
            if ($resguardos) {
                return $resguardo->resguardo_id;
            } else {
                return 0;
            }
        }
    }
    //Calculando folio del resguardo temporal
    protected function Folio()
    {
        $contadorResguardo = resguardo_temporal::count();

        $contadorResguardo = $contadorResguardo + 1;
        $contadorResguardo = str_pad($contadorResguardo, 3, "0", STR_PAD_LEFT);
        $folio = "PFI" . '-' . ($contadorResguardo);

        return $folio;
    }
    //registrar resguardo temporal
    public function store(ResguardoTemporalRequest $request)
    {
        $request->validated();
        $Biens = $request->get('bienes');
        $idPersona = $request->get('persona_id');
        $FechaSistema = Carbon::now();
        $resguardoTemp_id = 0;

        // separando el bienes
        $Bien = explode(",", $Biens);
        //Registrar bienes del resguardo
        foreach ($Bien as $bien) {
            $bienes = Bienes::find($bien);
            $resguardo_detail = new resguardo_detail();
            $Resguardante_id = $this->ValidarPersona($idPersona);
            //Si el resguardante no cuenta con resguardo temporal se registra un nuevo
            if ($Resguardante_id == 0) {
                $Resguardo = new Resguardo();
                $Resguardo->tipo = "TEMPORAL";
                $Resguardo->persona_id = $idPersona;
                $Resguardo->SI = "1";
                $Resguardo->activo = 1;
                $Resguardo->save();
                $Resguardante_id =  $Resguardo->id;
            } else {
                //Si el resguardante cuenta con un resguardo temporal solo se activa el resguardo
                $Resguardo = Resguardo::find($Resguardante_id);
                $Resguardo->activo = 1;
                $Resguardo->update();
            }
            //desactivar resguardo anterior si el bien estaba al resguardo de alguien y cancelar
            $resguardo_detailAnt = resguardo_detail::where('bien_id', $bien)
               // ->leftjoin('resguardos', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->where('resguardo_detail.activo', '=', '1')
             //   ->where('resguardos.tipo', '=', 'TEMPORAL')
                ->get();
            $resguardanteAnterior = null;
            foreach ($resguardo_detailAnt as $resguardo_detailAnt) {
                $ResguardoAnts = resguardo_detail::find($resguardo_detailAnt->id);
                $ResguardoAnts->fechaCancelacion = $FechaSistema;
                $ResguardoAnts->Activo = 0;
                $ResguardoAnts->update();
                $resguardanteAnterior = $resguardo_detailAnt->resguardo_id;
            }
            //asignacion al nuevo resguardante
            $resguardo_detail = new resguardo_detail();
            $resguardo_detail->resguardo_id = $Resguardante_id;
            $resguardo_detail->Activo = 1;
            $resguardo_detail->fechaAsignacion = $FechaSistema;
            $resguardo_detail->bien_id = $bien;
            $resguardo_detail->ResguardoAnterior_id = $resguardanteAnterior;
            $resguardo_detail->save();
            $bienes->Status = 1;
            $bienes->save();

            $resguardo_temporal = $this->ValidarResguardoTemp($resguardoTemp_id);
            if ($resguardo_temporal == 0) {
                $resguardoTemp = new resguardo_temporal();
                $resguardoTemp->folio = $this->Folio();
                $resguardoTemp->fecha_orden = $request->get('FechaOrden');;
                $resguardoTemp->No_orden = $request->get('Numero');
                $resguardoTemp->fecha_inicio = $request->get('FechaInicio');
                $resguardoTemp->fecha_vencimiento = $request->get('FechaVencimiento');
                $resguardoTemp->fecha_devolucion = $request->get('FechaDevolucion');
                $resguardoTemp->Activo = 1;
                $resguardoTemp->resguardo_id = $resguardo_detail->resguardo_id;
                $resguardoTemp->Observaciones = $request->get('Observaciones');
                $resguardoTemp->save();
                $resguardoTemp_id = $resguardoTemp->id;
            }
        }
        return redirect('ListaResguardanteTempo/' . $Resguardante_id);
    }
    //lista de resguardos temporales de equipo de computo de una persona
    public function lista($id)
    {
        $resguardo = Resguardo::buscarResguardoTemporal($id)->get();
        return view('Resguardos.UTIC.ListadoResguardoTempo', compact('resguardo'), compact('id'));
    }
    //lista de resguardos temporales de equipo de computo de una persona
    public function listas(Request $request)
    {
        $personas = Persona::all()->where('Activo', '1');
        $id = $request->get('idPersona');
        $resguardo = Resguardo::buscarResguardoTemporalP($id)->get();
        return view('Resguardos.UTIC.VerSalida.ListadoResguardoTempo', compact('resguardo', 'personas'));
    }

    public function todos($id)
    {
        $resguardo = Resguardo::buscarTodos($id)->get();
        return view('Resguardos.UTIC.ListadoResguardo', compact('resguardo'));
    }
    //Descargar reporte de Prestamos de equipo (Resguardo Temporal)
    public function downloadPrestamoPDF($id)
    {
        $temporal =  DB::table('resguardos_temporal')
            ->where('resguardo_id', '=', $id)
            ->where('Activo', '=', 1)
            ->select("resguardos_temporal.*")
            ->get();
        $Elaboro = User::personaEnSesion(auth()->user()->id)->get();
        $resguardo = Resguardo::buscarResguardoTemporal($id)->get();
        $admiSI = Area::where('Nombre', "ADMON SISTEMAS")->get();
        $TitularTI = Area::where('Cargo', "TITULAR DE LA UNIDAD DE TECNOLOGIAS DE LA INFORMACION Y COMUNICACIONES")->get();

        $pdf = PDF::loadView(
            'Resguardos.UTIC.Reportes.ReportePrestamoEquipo',
            compact('resguardo', 'Elaboro', 'temporal', 'admiSI', 'TitularTI')
        );
        return $pdf->stream('Prestamo de Equipo.pdf');
    }


    public function editarresguardo(Request $request, $id)
    {

        $personas = Persona::all()->where('Activo', '1');

        $resguardo = $id;

        $data =  Resguardo::buscarResguardoTemporalDisponible1($id)->get();
        $informacion = Resguardo::buscarResguardoTemporalPersona($id)->get();
        $temporal = [];
  
        $sol = NULL;
        $No_orden = NULL;
        $fecha_orden = NULL;
        $fecha_inicio = NULL;
        $fecha_vencimiento = NULL;
        $fecha_devolucion = NULL;
        $folio = NULL;
        $id = NULL;


        if (count($informacion) > 0) {
            $temporal =  DB::table('resguardos_temporal')
                ->where('resguardo_id', '=', $informacion[0]->resguardo_id)
                ->where('Activo', '=', 1)
                ->select("resguardos_temporal.*")
                ->get();
            $sol = $temporal[0]->Observaciones;
            $No_orden = $temporal[0]->No_orden;
            $fecha_orden = $temporal[0]->fecha_orden;
            $fecha_inicio = $temporal[0]->fecha_inicio;
            $fecha_vencimiento = $temporal[0]->fecha_vencimiento;
            $fecha_devolucion = $temporal[0]->fecha_devolucion;
            $folio = $temporal[0]->folio;
            $id = $temporal[0]->id;
            $ResgID= $temporal[0]->resguardo_id;
        


        return view('Resguardos.UTIC.ResguardoTemporalEdit', compact(
            'personas',
            'resguardo',
            'sol',
            'No_orden',
            'fecha_inicio',
            'fecha_vencimiento',
            'fecha_devolucion',
            'folio',
            'id',
            'fecha_orden','data','ResgID'
        ));
    }
    else{
        $resgT = resguardo_temporal::find($resguardo);
        $resgT->Activo = 0;
        $idR=$resgT->resguardo_id;
        $resgT->update();
       
        if($idR){
            $resguardos = Resguardo::find($idR);
            $resguardos->activo = 0;
            $resguardos->update();
        }

        return back()->with('Ups', "Ocurrio un error al intentar consultar el resguardo, intentelo mas tarde");
  
    }
    }
 //consulta de Resguardo Temporal Disponible Bien Utic para el listado
 public function temporalesD($id)
 {

     if ($id != null || $id > 1) {   
         $data=  Resguardo::buscarResguardoTemporalDisponible()->get();
         //$data =  Resguardo::buscarResguardoTemporalDisponible1($id)->get();

         return Datatables::of($data)
             ->addIndexColumn()
             ->addColumn('action', function ($row) {
                    
                             $actionBtn = '<input  class="casilla ml-5" type="checkbox" name="biens[]" value="' . $row->bien_id . '">';
                         
                                        
                 return $actionBtn;
             })
             ->rawColumns(['action'])
             ->make(true);
     }
 }

    //consulta de Resguardo Temporal Disponible Bien Utic para el listado
    public function temporalesUSer($id)
    {

        if ($id != null || $id > 1) {   
            $data =  Resguardo::buscarResguardoTemporalDisponible1($id)->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                       
                                $actionBtn = '<input  class="casilla ml-5" type="checkbox" name="biens[]" value="' . $row->bien_id . '" checked>';                         
                                           
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
      //cancelar los bienes del resguardo Temporal en el editar
      public function CancelarResguardoT($Biens,$id)
      {
           //consultar  resguardo
           $resguardoT = resguardo_temporal::find($id);
           $resguardoId=$resguardoT->resguardo_id;

          // separando el bienes
        $Bien = explode(",", $Biens);
        if($Bien){
        //consultar bienes del resguardo
            $resguardosT = DB::table('resguardo_detail')
            ->select('resguardo_detail.*')
            ->where('resguardo_id', '=', $resguardoId)
            ->where('Activo', '=', '1')
            ->get();

            $b=0;
            //Detectar los bienes que se quitaron entre los bienes que se seleccionaron para editar 
            foreach ($resguardosT as $resguardo) {
                foreach ($Bien as $bien) {
                    if($resguardo->bien_id==$bien){
                        $b=1;
                        break;
                    }else{
                        $b=0;
                    }
                }
                //Desactivar bienes que ya no se seleccionaron
                if($b==0){
                    $DesactivarRT = resguardo_detail::find($resguardo->id);
                    $DesactivarRT->Activo=0;
                    $DesactivarRT->update();
                }
            }
            
            
        }
         
          return 1;
          
      }
    //editar resguardo temporal, bienes y atributos 
    public function prueba(Request $request)

    {

        $Biens               = $request->get('bienes');
        $temporal            = $request->get('temporal');
        $idPersona          = $request->get('persona_id');
        $FechaOrden          = $request->get('FechaOrden');
        $FechaInicio         = $request->get('FechaInicio');
        $FechaDevolucion     = $request->get('FechaDevolucion');
        $FechaVencimiento    = $request->get('FechaVencimiento');
        $Observaciones       = $request->get('Observaciones');
        $bandera               = $request->get('bandera');
        $No_orden            = $request->get('No_ordenC');
        $FechaSistema        = Carbon::now();
        $resguardoTemp_id    = 0;

        $Actualizacion                    =  resguardo_temporal::find($temporal);
        $Actualizacion->Observaciones     =  $Observaciones;
        $Actualizacion->No_orden           =  $No_orden;        
        $Actualizacion->fecha_orden       =  $FechaOrden;
        $Actualizacion->fecha_inicio      =  $FechaInicio;
        $Actualizacion->fecha_vencimiento =  $FechaVencimiento;
        $Actualizacion->fecha_devolucion  =  $FechaDevolucion;
        $Actualizacion->save();
        if($bandera==0){
            $this->CancelarResguardoT($Biens,$temporal);
         }else{
        // separando el bienes
        $Bien = explode(",", $Biens);
         //Registrar bienes del resguardo
         foreach ($Bien as $bien) {
            $bienes = Bienes::find($bien);
            $Resguardante_id = $this->ValidarPersona($idPersona);
           
            //desactivar resguardo anterior si el bien estaba al resguardo de alguien y cancelar
            $resguardo_detailAnt = resguardo_detail::where('bien_id', $bien)
                ->leftjoin('resguardos', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->where('resguardo_detail.activo', '=', '1')
                ->get();
            $resguardanteAnterior = null;
            foreach ($resguardo_detailAnt as $resguardo_detailAnt) {
                $resguardo_detailAnt->fechaCancelacion = $FechaSistema;
                $resguardo_detailAnt->Activo = 0;
                $resguardo_detailAnt->update();
                $resguardanteAnterior = $resguardo_detailAnt->resguardo_id;
            }
            //consultar  resguardo
           $resguardoT = resguardo_temporal::find($temporal);
           $resguardoId=$resguardoT->resguardo_id;

              //consultar bienes del resguardo
              $resguardosT = DB::table('resguardo_detail')
              ->select('resguardo_detail.*')
              ->where('resguardo_id', '=', $resguardoId)
              ->where('Activo', '=', '1')
              ->get();
  
              $b=0;
              //Detectar los bienes que se quitaron entre los bienes que se seleccionaron para editar 
              foreach ($resguardosT as $resguardo) {
                      if($resguardo->bien_id==$bien){
                          $b=1;
                          break;
                      }
                  }
                  //Desactivar bienes que ya no se seleccionaron
                  if($b==0){
                      
            //asignacion al nuevo resguardante
                $resguardo_detail = new resguardo_detail();
                $resguardo_detail->resguardo_id = $Resguardante_id;
                $resguardo_detail->Activo = 1;
                $resguardo_detail->fechaAsignacion = $FechaSistema;
                $resguardo_detail->bien_id = $bien;
                $resguardo_detail->ResguardoAnterior_id = $resguardanteAnterior;
                $resguardo_detail->User = auth()->user()->id;
                $resguardo_detail->save();
                $bienes->Status = 1;
                $bienes->save();
                }
        }
    }
        
         $res =  1;
         return $res;
       //return redirect('/ListaResguardoTemporal');
    }
    //no se ocupa solo se conserva por los cambios que se realizaron 
    public function prueba_editar(Request $request)
    {

        $temporal            = $request->get('temporal');
        $persona_id          = $request->get('persona_id');
        $FechaOrden          = $request->get('FechaOrden');
        $FechaInicio         = $request->get('FechaInicio');
        $FechaDevolucion     = $request->get('FechaDevolucion');
        $FechaVencimiento    = $request->get('FechaVencimiento');

        $Actualizacion                    =  resguardo_temporal::find($temporal);
        $Actualizacion->fecha_orden       =  $FechaOrden;
        $Actualizacion->fecha_inicio      =  $FechaInicio;
        $Actualizacion->fecha_vencimiento = $FechaDevolucion;
        $Actualizacion->fecha_devolucion  =  $FechaVencimiento;

        $logueo = logueo::sesionIniciada(auth()->user()->id)->get();

        $bitacora = new  resguardo_temporalbitacora();
        $bitacoraaux = new  resguardo_temporalbitacora();

        $bitacora->folio                     =   $Actualizacion->folio;
        $bitacora->fecha_orden               =   $Actualizacion->fecha_orden;
        $bitacora->No_orden                  =   $Actualizacion->No_orden;
        $bitacora->Observaciones             =   $Actualizacion->Observaciones;
        $bitacora->fecha_inicio              =   $Actualizacion->fecha_inicio;
        $bitacora->fecha_vencimiento         =   $Actualizacion->fecha_vencimiento;
        $bitacora->fecha_devolucion          =   $Actualizacion->fecha_devolucion;
        $bitacora->id_resguardoTemporal      =   $Actualizacion->id;

        DB::beginTransaction();
        // $Actualizacion->beginTransaction();
        if ($Actualizacion->save()) {
            //    $bitacora->begin();
            if ($bitacora->save()) {
                $bitacoraaux->folio                   =  $Actualizacion->folio;
                $bitacoraaux->fecha_orden             =  $Actualizacion->fecha_orden;
                $bitacoraaux->No_orden                =  $Actualizacion->No_orden;
                $bitacoraaux->Observaciones           =  $Actualizacion->Observaciones;
                $bitacora->fecha_inicio               =  $Actualizacion->fecha_inicio;
                $bitacoraaux->fecha_vencimiento       =  $Actualizacion->fecha_vencimiento;
                $bitacoraaux->fecha_devolucion        =  $Actualizacion->fecha_devolucion;
                $bitacoraaux->id_resguardoTemporal    =  $Actualizacion->id;
                $bitacoraaux->fecha_Actualizacion     = (new DateTime('NOW'))->format('Y-m-d H:i:s');
                $bitacoraaux->usuario                 = $logueo[0]->user_id;

                if ($bitacoraaux->save()) {
                    DB::commit();
                    $res =  1;
                } else {
                    DB::rollBack();
                    $res =  0;
                }
            } else {
                DB::rollBack();
                $res =  0;
            }
        } else {
            DB::rollBack();
            $res =  0;
        }

        return $res;
    }
}

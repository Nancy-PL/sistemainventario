<?php

namespace App\Http\Controllers;

use App\Models\Area;
use Illuminate\Http\Request;
//referencia al modelo de clasificación y tipoBien
use App\Models\clasificacion;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ClasificacionRequest;
use App\Http\Requests\ClasificacionEditRequest;
use Yajra\DataTables\Datatables;




//Creación de los metodos del control
class clasificacionController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:Clasificacion.index')->only('index');
        $this->middleware('can:Clasificacion.create')->only('create');
        $this->middleware('can:Clasificacion.edit')->only('edit');
    }
    //Hace referencia a la vista de ListaCapAdmi y  a las tabalas de clasificación y la de tipos de bienes
    public function index()
    {

        return view('Catalogos.Clasificacion.Administrativo.Lista');
    }


    public function getClasificacionAD(Request $request)
    {
        if ($request->ajax()) {
            //$data = clasificacion::latest()->get();
            $data = clasificacion::all()->where('activo', '1');
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                        $actionBtn = '<a href="Clasificacion/' . $row->id . '/edit" class="fa  fa-edit  fa-lg" ALIGN="center"style="color:#3A3E3C; "></a> ';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    //Creacion de una nueva clasificación haciendo referencia a las tablas de TipoBien y clasificación
    public function create()
    {
        $Area = Area::all();
        return view('Catalogos.Clasificacion.Administrativo.Alta')->with('areas', $Area);
    }

    //Inserta datos en las tablas correspondientes
    public function store(ClasificacionRequest $request)
    { //Haciendo validaciones del  concepto de la tabla clasificacions

        $request->validated();

        $Clasificacion = new clasificacion();
        $Clasificacion->clave = $request->get('Clave');
        $Clasificacion->concepto = $request->get('Concepto');
        $Clasificacion->activo = ('1');
        $Clasificacion->NoInventario = $request->get('NoInventario');
        $Clasificacion->Departamento =$request->get('Departamento');
        $Clasificacion->save();
        return redirect('/Clasificacion');
    }


    //Metodo de edición de campos en la tabla de clasificación y tipo bien
    public function edit($id)
    {
        $Area = Area::all();

        $NoInventario =['NoInventario' => ['value' => '0', 'name' => 'No de control'],['value' => '1', 'name' => 'No de inventario']];

        $Departamento =['Departamento' => ['value' => 'UA', 'name' => 'UA'],['value' => 'SI', 'name' => 'SI']];


        $Clasificacion = clasificacion::find($id);
        return view('Catalogos.Clasificacion.Administrativo.Editar')->with('clasificacions', $Clasificacion)->with('areas', $Area)->with('departamento', $Departamento)->with('noInventario', $NoInventario);
    }
    //Actualiza los datos en la tabla calsificación y tipo
    public function update(ClasificacionEditRequest $request, $id)
    {
        //Haciendo validaciones del  concepto de la tabla clasificacions
        $this->validate($request, [
            'Clave' => "required|unique:clasificacions,clave,$id",
            'Concepto' => "required|unique:clasificacions,concepto,$id"
        ]);

        $Clasificacion = clasificacion::find($id);
        $Clasificacion->clave = $request->get('Clave');
        $Clasificacion->concepto = $request->get('Concepto');
        $Clasificacion->activo = ('1');
        $Clasificacion->NoInventario = $request->get('NoInventario');
        $Clasificacion->Departamento =$request->get('Departamento');
        $Clasificacion->update();
        return redirect('/Clasificacion');
    }


    public function destroy($id)
    {
        //
    }
}

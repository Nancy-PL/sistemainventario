<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Resguardo;
use Illuminate\Support\Facades\DB;
use App\Models\Persona;
use App\Models\Bienes;
use App\Models\resguardo_detail;
use Carbon\Carbon;
use App\Http\Requests\CancelarTemporalRequest;
use App\Models\resguardo_temporal;

class CancelacionRTemporalController extends Controller
{
     //protección de rutas
     public function __construct()
     {
         $this->middleware('can:CancelarResguardoTem.index')->only('index');
     }
     //listado de resguardos temporales por persona
    public function index(Request $request)
    {
        $personas = Persona::all()->where('Activo', '1');
        //consultar bienes de un resguardante
        $nombre = $request->get('resguardante');
        $resguardos = Resguardo::BuscarporResguardoTemp($nombre)->get();

        return view('Resguardos.UTIC.CancelacionRTemporal')->with('personas', $personas)->with('resguardos', $resguardos);
    }

    //validar la existencia de un resguardo temporal de dicha persona
    public function ValidarPersonaRP($id)
    {
        $resguardos = DB::table('resguardos')
            ->select('resguardos.*')
            ->where('SI', '=', '1')
            ->where('persona_id', '=', $id)
            ->where('tipo', '=', 'PERMANENTE')
            ->get();
        foreach ($resguardos as $resguardo) {
            if ($resguardos) {
                return $resguardo->id;
            } else {
                return 0;
            }
        }
    }
    //verificar la existencia de un bien al resguardo en caso de no haber se cancela
    public function  verificarExistenciaResguardo($id)
    {
        $verificar = resguardo_detail::where('resguardo_id', $id)->where('Activo', '=', 1)->get();

        foreach ($verificar as $verificar) {
            return 1;
        }
        //Cancelar resguardo
        $resguardoAnterior = Resguardo::find($id);
        $resguardoAnterior->activo = 0;
        $resguardoAnterior->delete_at = Carbon::now();
        $resguardoAnterior->update();
        $resguardoTemporal = resguardo_temporal::where('resguardo_id', $id)->where('Activo', '=', 1)->get();
        foreach ($resguardoTemporal as $resguardoTemporal) {
            $resguardoTemporal->Activo = 0;
            $resguardoTemporal->update();
        }
        return 0;
    }

    public function store(CancelarTemporalRequest $request)
    {
        $request->validated();
        $Biens = $request->get('bienes');
        $idPersona = $request->get('persona_id');
        $FechaSistema = Carbon::now();
        $resguardanteAnterior = 0;

        // separando el bienes 
        $Bien = explode(",", $Biens);
        //Registrar bienes del resguardo
        // for_each
        foreach ($Bien as $bien) {
            $bienes = Bienes::find($bien);
            $resguardo_detail = new resguardo_detail();
            $Resguardante_id = $this->ValidarPersonaRP($idPersona);
            //Si el resguardante no cuenta con resguardo temporal se registra un nuevo
            if ($Resguardante_id == 0) {
                $Resguardo = new Resguardo();
                $Resguardo->tipo = "PERMANENTE";
                $Resguardo->persona_id = $idPersona;
                $Resguardo->SI = "1";
                $Resguardo->activo = 1;
                $Resguardo->save();
                $Resguardante_id =  $Resguardo->id;
            } else {
                //Si el resguardante cuenta con un resguardo temporal solo se activa el resguardo
                $Resguardo = Resguardo::find($Resguardante_id);
                $Resguardo->activo = 1;
                $Resguardo->update();
            }
            //desactivar resguardo anterior si el bien estaba al resguardo de alguien y cancelar
            $resguardo_detailAnt = resguardo_detail::where('bien_id', $bien)->get();
            foreach ($resguardo_detailAnt as $resguardo_detailAnt) {
                $resguardo_detailAnt->fechaCancelacion = $FechaSistema;
                $resguardo_detailAnt->Activo = 0;
                $resguardo_detailAnt->update();
                $resguardanteAnterior = $resguardo_detailAnt->resguardo_id;
            }
            //asignacion al nuevo resguardante
            $resguardo_detail = new resguardo_detail();
            $resguardo_detail->resguardo_id = $Resguardante_id;
            $resguardo_detail->Activo = 1;
            $resguardo_detail->fechaAsignacion = $FechaSistema;
            $resguardo_detail->bien_id = $bien;
            $resguardo_detail->ResguardoAnterior_id = $resguardanteAnterior;
            $resguardo_detail->save();
            $bienes->Status = 1;
            $bienes->save();
        }
        //verificar existencia de bienes al reaguardo activos
        $resguardo = $this->verificarExistenciaResguardo($resguardanteAnterior);

        //si el personal cuenta con un usuario se le niega la baja
        if ($resguardo == 1) {
            return back()->with('UPS', " Resguardo renovado ");
        } else {
            return back()->with('UPS', " Resguardo cancelado ");
        }
    }
}

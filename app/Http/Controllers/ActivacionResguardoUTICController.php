<?php

namespace App\Http\Controllers;

use App\Models\Resguardo;
use App\Models\Persona;
use App\Models\resguardo_detail;
use Carbon\Carbon;
use App\Http\Requests\ActivacionRequest;



class ActivacionResguardoUTICController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:ActivacionUtic.index')->only('index');
    }
    //listado de resguardos recien desactivados
    public function index()
    {
        $Resguardo = Resguardo::activacion('SI')->get();
        return view('Resguardos.UTIC.ActivacionUTIC', compact('Resguardo'));
    }


    public function activar($id)
    {

        //activando el resguardo
        $resguardo = Resguardo::find($id);
        $resguardo->activo = 1;
        $resguardo->save();
        $persona = Persona::find($resguardo->persona_id);
        $persona->Activo = 1;
        $persona->update();
        //consulta de los bienes anteriores al resguardo
        $resguardo = Resguardo::activacionR($id, "SI")->get();
         //consulta de los bienes anteriores al resguardo
         $b=0;
         foreach($resguardo as $r){
             $b=1;
             break;
         }
         if($b==0){
             return back()->with('Listo', "Resguardo Activado");
         }else{
            return view('Resguardos.UTIC.ResguardoActivo', compact('resguardo'));
        }
    }



    public function store(ActivacionRequest $request)
    {
        $request->validated();
        $BienesR = $request->get('resguardos');
        $fecha = Carbon::now();
        //reasignacion de bienes
        $resguardoID = 0;

        // separando el nombre del almacenista
        $resguardo = explode(",", $BienesR);

        foreach ($resguardo as $resguardo) {
            $resguardo_detailAnt = resguardo_detail::find($resguardo);
            //reasgnacion al nuevo resguardante
            $resguardo_detail = new resguardo_detail();
            $resguardo_detail->resguardo_id = $resguardo_detailAnt->ResguardoAnterior_id;
            $resguardo_detail->Activo = 1;
            $resguardo_detail->fechaAsignacion = $fecha;
            $resguardo_detail->bien_id = $resguardo_detailAnt->bien_id;
            $resguardo_detail->ResguardoAnterior_id = $resguardo_detailAnt->resguardo_id;
            $resguardo_detail->save();
            //desactivar resguardo anterior y cancelando
            $resguardo_detailAnt->fechaCancelacion = $fecha;
            $resguardo_detailAnt->Activo = 0;
            $resguardo_detailAnt->update();
            $resguardoID = $resguardo_detail->resguardo_id;
        }
        $resguardo = Resguardo::find($resguardoID);
        $id = $resguardo->persona_id;

       return  redirect("/ListaResguardanteUtic/" . $id);
        

    }
}

<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\AreaRequest;
use App\Http\Requests\EditarRequest;
use Yajra\DataTables\Datatables;
use App\Models\Area;
use App\Models\Persona;
use App\Models\Resguardo;
use App\Models\resguardo_detail;
use Faker\Provider\ar_JO\Person;

class AreaController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:Areas.index')->only('index');
        $this->middleware('can:Areas.create')->only('create');
        $this->middleware('can:Areas.edit')->only('edit');
    }
    /*Consulta para la vista del Listado de areas*/
    public function index()
    {
        return view('Catalogos.Area.ListaAreas');
    }

    /*Muestra la vista para dar de alta un area*/
    public function create()
    {
        return view('Catalogos.Area.Areas');
    }


    /*Insertando un nuevo area en la bd*/
    public function store(AreaRequest $request)
    {
        $request->validated();
        $Area = new Area();
        $Area->Nombre = $request->get('NombreA');
        $Area->Responsable = $request->get('ResponsableA');
        $Area->Cargo = $request->get('CargoA');
        $Area->letra = $request->get('LetraA');
        $Area->save();

        return redirect('/Areas');
    }
    /*Mostrando la vista para Editar un area*/
    public function edit($id)
    {
        $Area = Area::find($id);
        $Area=json_decode($Area);
        return view('Catalogos.Area.EditarArea',compact('Area'));
    }

    /*Actualizando un area*/
    public function update(EditarRequest $request, $id)
    {
        $Area = Area::findOrFail($id);
        $request->validated();
        $Area->Nombre = $request->get('NombreA');
        $Area->Responsable = $request->get('ResponsableA');
        $Area->Cargo = $request->get('CargoA');
        $Area->letra = $request->get('LetraA');
        $Area->update();
        return redirect('/Areas');
    }
    //consulta de areas para el listado
    public function getArea(Request $request)
    {
        if ($request->ajax()) {
            $data = Area::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = ' <a href="Areas/'.$row->id.'/edit" class="fa  fa-edit  fa-lg" ALIGN="center"style="color:#3A3E3C; "></a> ';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Persona;
use App\Models\Area;
use App\Http\Requests\PersonasRequest;
use Yajra\DataTables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Models\jefatura;

class PersonasController extends Controller
{
      //protección de rutas
      public function __construct()
      {
          $this->middleware('can:personas.index')->only('index');
          $this->middleware('can:personas.create')->only('create');
          $this->middleware('can:personas.edit')->only('edit');
  
      }
    /* Mostrando la vista de lista de personal y enviando la consulta de la tabla personas unida con la tabla areas*/
    public function index()
    {
        return view('personas.ListaPersonas');

    }
    //consulta personal para dar de baja
    public function getPersonass(Request $request)
    {


        if ($request->ajax()) {
            $data = Persona::BuscarPersona()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    $actionBtns = '<input type="radio" id="Persona" name="Persona" onclick="persona()" value="'.$row->id_persona.'" required>';
                    return $actionBtns;
                })
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '<a href="personas/'.$row->id.'/edit" class="fa  fa-edit  fa-2x" ALIGN="center"style="color:#3A3E3C; "></a>';
                    return $actionBtn;
                })
                ->rawColumns(['actions','action'])
                ->make(true);
        }
    }
//consulta personal para Activar
public function getPersonaActiva(Request $request)
{


    if ($request->ajax()) {
        $data = Persona::BuscarPersonaD()->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('actions', function ($row) {
                $actionBtns = '<input type="radio" id="Persona" name="Persona" onclick="persona()" value="'.$row->id_persona.'" required>';
                return $actionBtns;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
    //Mostrando la vista para dar de alta nuevo personal
    public function create()
    {
        $Area = Area::all()->where('Nombre','!=','ADMON SISTEMAS');
        $Jefatura = jefatura::all();
        $personas = Persona::latest()->where('Activo', '1')->get();

        return view('personas.personas')->with('areas', $Area)->with('personas', $personas)->with('Jefatura', $Jefatura);
    }

    //insertando nueva persona en la bd
    public function store(PersonasRequest $request)
    {
        $firstNameUniqueRule = Rule::unique('personas')->where('Nombre', request()->get('ApellidoP', ''));

        $rules = [
            'Nombre' => ['required', $firstNameUniqueRule],
            'ApellidoP' => 'required',
        ];
        $Persona = new Persona();

        $validator = Validator::make($request->all(), [
            'Nombre' => 'required|max:100',
            'ApellidoP'  => 'required|unique:personas,Nombre,NULL,id,ApellidoP,' . $request->Nombre . '|max:100',
        ]);

        $personaExistente = DB::table('personas')
        ->select('id')
        ->where('Nombre', 'like', "%".$request->get('Nombre')."%")
        ->where('ApellidoP', 'like', "%".$request->get('ApellidoP')."%")
        ->where('ApellidoM', 'like', "%".$request->get('ApellidoM')."%")
        ->where('Activo', '=', "1")
        ->groupBy('id')
        ->count();
        
        if($personaExistente){
            return back()->with('UPS', "Personal existente");
        }else{
            $Persona->Nombre =mb_strtoupper( $request->get('Nombre'));
            $Persona->ApellidoP = mb_strtoupper($request->get('ApellidoP'));
            $Persona->ApellidoM = mb_strtoupper($request->get('ApellidoM'));
            $Persona->Profesion = mb_strtoupper($request->get('Profesion'));
            $Persona->Activo = '1';
            $Persona->area_id = $request->get('area');
            $Persona->jefatura_id = $request->get('Jefatura');
            $Persona->save();
            return redirect('/personas');
        }
    }

    public function edit($id)
    {
        $Persona = Persona::find($id);
        $Area = Area::select('Nombre', 'id')->get();
        $Jefatura = jefatura::all();

        return view('personas.EditarPersonas')->with('persona', $Persona)->with('areas', $Area)->with('Jefatura', $Jefatura);
    }

   
    //Actualizando datos del personal
    public function update(PersonasRequest $request, $id)
    {
        $Persona = Persona::find($id);
        $Persona->Nombre =mb_strtoupper( $request->get('Nombre'));
        $Persona->ApellidoP =mb_strtoupper( $request->get('ApellidoP'));
        $Persona->ApellidoM =mb_strtoupper( $request->get('ApellidoM'));
        $Persona->Profesion = mb_strtoupper($request->get('Profesion'));
        $Persona->area_id = $request->get('area');
        $Persona->jefatura_id = $request->get('Jefatura');
        $Persona->save();
        return redirect('/personas');
    }

   
}

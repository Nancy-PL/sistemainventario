<?php

namespace App\Http\Controllers;

use Yajra\DataTables\Datatables;
use Illuminate\Http\Request;
use App\Models\resguardo_temporal;
use App\Models\logueo;
use App\Models\control_ip;
use App\Models\salidaEquipo;
class DashboardController extends Controller
{
    
     //protección de rutas
     public function __construct()
     {
        // $this->middleware('can:get-getsession.getsession')->only('getsession');
 
     }
    public function index()
    {
       
        //Administrador
        if (auth()->user()->roles_id == "1") {
            return view('/Home/dashboard');
        }
        //Capturista Administrativo
        elseif (auth()->user()->roles_id == "2") {
            return view('/Home/userAdmondash');
        }
        //Capturista UTIC
        elseif (auth()->user()->roles_id == "3") {
            return view('/Home/userUticdash');
        }
        //consulta de Los Resguardos temporales para el listado

    }
    //consulta de Salida de Equipo Bien Utic  listado
    public function getSalidaEquipo(Request $request)
    {
        if ($request->ajax()) {
            $data = salidaEquipo::buscarporSalidasAVencer()->get();
            return Datatables::of($data)->make(true);
        }
    }
    public function getResguardoVence(Request $request)
    {
        if ($request->ajax()) {
            $data = resguardo_temporal::resguardoTemporalVencimiento()->get();
            return Datatables::of($data)->make(true);
        }
    }
    public function getsession(Request $request)
    {
        if ($request->ajax()) {
            $data =logueo::logs()->get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $actionBtn = '';
                if($row->fin_login==NULL){
                $actionBtn = '<span class="badge badge-success">Activo</span>';
                }else{
                    $actionBtn = '<span class="badge badge-danger">Inactivo</span>';
                }
                return $actionBtn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }
    public function getProximosIp(Request $request)
    {
        $fecha=date("Y-m-d H:i:s");
        $data = control_ip::ControlTemp($fecha)->get();

        foreach ($data as $controlVence){
        $control_ip = control_ip::findOrFail($controlVence->id);
        if ($control_ip){
            $control_ip->estado = 2;
            $control_ip->estatus = 1;
            $control_ip->fechaActualizacion = $control_ip->fechaActualizacion;
            $control_ip->update();
        }
        
        }
        if ($request->ajax()) {
            $data =control_ip::IpAVencer()->get();
            return Datatables::of($data)->make(true);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UsuariosEditRequest;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Persona;
use App\Models\Area;
use App\Models\Role;
use Yajra\DataTables\Datatables;



class ListaUsuarioController extends Controller
{
      //protección de rutas
      public function __construct()
      {
          $this->middleware('can:ListaUsuario.index')->only('index');
          $this->middleware('can:Users.index')->only('index');
          $this->middleware('can:ListaUsuario.edit')->only('edit');
  
      }
    //Metodo listado 
    public function index()
    {
        return view('auth.ListaUsuario');
    }
    public function getUsers(Request $request)
    {
            if ($request->ajax()) {
                $data = User::buscar()->get();
                return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    $actionBtn = '<input type="radio" name="User" value="'.$row->UserId.'" required>';
                    return $actionBtn;
                })
                
                    ->addColumn('action', function($row){
                        $actionBtn = '<a href="ListaUsuario/'.$row->UserId.'/edit" class="fa  fa-edit  fa-lg" ALIGN="center"style="color:#3A3E3C; "></a> ';
                        return $actionBtn;
                    })
                    
                    ->rawColumns(['action','actions'])
                    ->make(true);
            }
    }
   
    public function edit($id)
    {
        $users = User::find($id);
        $Persona = Persona::select(DB::raw('CONCAT(Nombre," ", ApellidoP," ", ApellidoM) AS NombreCompleto'), 'id as Persona_id')->get();
        $Area = Area::select('Nombre', 'id')->get();
        $Roles = Role::select('name', 'id as Rol_id')->get();
        return view('auth.EditarUsuario')->with('users', $users)->with('persona', $Persona)->with('areas', $Area)->with('roles', $Roles);
    }
    public function store(Request $request)
    {
        $user_id = $request->get('User');
        $sesion=auth()->user()->id;
        if($sesion== $user_id){
            return back()->with('UPS', " Tiene sesión iniciada");
        }
        $user = User::find($user_id);
        $user->Activo = 0;
        $name=$user->name;
        $user->update();
        return back()->with('Listo', "El usuario ".$name." se dio de baja correctamente.");
    }

    public function update(UsuariosEditRequest $request, $id)
    {
        //Validación de datos
        $this->validate($request, [
            'name' => "required|unique:users,name,$id",
            'password' =>  [
                'required',
                'string',
                'min:8',             //debe tener al menos 8 caracteres de longitud
                'regex:/[a-z]/',      // debe contener al menos una letra minúscula
                'regex:/[A-Z]/',      // debe contener al menos una letra mayúscula 
                'regex:/[0-9]/',      // debe contener al menos un dígito
                'regex:/[@$-_*!%*#?.&]/', // debe contener un carácter especial
            ],
        ]);

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->roles_id = $request->get('role_id');
        $pass = $_POST['password'];
        $passHash = password_hash($pass, PASSWORD_BCRYPT);
        $user->password = $passHash;
        $user->roles()->sync($request->get('role_id'));

        $user->update();
        return redirect('ListaUsuario');
    }
}

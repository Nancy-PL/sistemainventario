<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Persona;
use Illuminate\Support\Facades\DB;
use App\Models\BajaPersonal;
use Carbon\Carbon;

class BajaPersonalController extends Controller
{
      //protección de rutas
      public function __construct()
      {
          $this->middleware('can:BajaPersonas.index')->only('index');
    
  
      }
    public function index()
    {

        return view('personas.ListaPersonas');
    }

    //baja del personal
    public function store(Request $request)
    {
        $resguardante = $request->get('Persona');
        $persona = Persona::find($resguardante);
        $fecha = Carbon::now();

        $this->validate($request, [
            'Motivos' => "required|max:300",
        ]);
        //Validar que el usuario no tenga ningun resguardo
        $resguardoActivo = DB::table('resguardos')
            ->select('persona_id')
            ->where('persona_id', '=', $resguardante)
            ->where('activo', '=', '1')
            ->groupBy('persona_id')     
            ->count();
        if ($resguardoActivo > 0) {
            return back()->with('UPS', "No se pudo dar de baja al personal " . $persona->Nombre . " " . $persona->ApellidoP . " " . $persona->ApellidoM . " porque cuenta con un resguardo activo.");
        }
        $user = DB::table('users')
            ->select('persona_id')
            ->where('persona_id', '=', $resguardante)
            ->where('Activo', '=', 1)
            ->groupBy('persona_id')
            ->count();
        //si el personal cuenta con un usuario se le niega la baja
        if ($user > 0) {
            return back()->with('UPS', "No se pudo dar de baja al personal " . $persona->Nombre . " " . $persona->ApellidoP . " " . $persona->ApellidoM . " porque cuenta con un usuario del sistema activo.");  
        } else {
            $persona->Activo = 0;
            $persona->update();
            //dar de baja al personal
            $bajaPersona = new BajaPersonal();
            $bajaPersona->Motivo = $request->get('Motivos');
            $bajaPersona->UsuarioBaja = auth()->user()->id;
            $bajaPersona->Fecha = $fecha;
            $bajaPersona->persona_id = $resguardante;
            $bajaPersona->save();

            return back()->with('Listo', " El personal con nombre " . $persona->Nombre . " " . $persona->ApellidoP . " " . $persona->ApellidoM . " se dio de baja correctamente.");
       }
    }

}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use App\Models\logueo;
use App\Providers\RouteServiceProvider;

class AuthenticatedSessionController extends Controller
{
    // metodo para la creación del login 
    public function create()
    {
        return view('auth.login');
    }

    //insertar datos
    public function store(LoginRequest $request)
    {
       
        $request->authenticate();
        $request->session()->regenerate();
        return redirect('/logueo');
    }

    //metodo para la eliminación 
    public function destroy(Request $request)
    {
        if(auth()->user()->id){
            $fecha = Carbon::now();
            $logueo = logueo::sesionIniciada(auth()->user()->id)->get();
    
            foreach($logueo as $logueo){
                $logueo->fin_login = $fecha;
                $logueo->update();
                break;
            }
            $request->session()->invalidate();
    
            Auth::logout();
            $request->session()->regenerateToken();
        }
      

        return redirect('../');
    }
}

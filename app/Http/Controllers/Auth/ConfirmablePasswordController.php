<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class ConfirmablePasswordController extends Controller
{
       public function show(Request $request)
    {
        return view('auth.confirm-password');
    }

    //metodo para insertar datos 
    public function store(Request $request)
    {
        if (! Auth::guard('web')->validate([
            'name' => $request->user()->name,
            'password' => $request->password,
        ])) {
            throw ValidationException::withMessages([
                'password' =>'la contraseña no coincide con el nombre de usuario',

            ]);
        }

        $request->session()->put('auth.password_confirmed_at', time());

        return redirect()->intended(RouteServiceProvider::HOME);
    }
}
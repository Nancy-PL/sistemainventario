<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
//llamas los modelos a utilizar 
use App\Models\Persona;
use App\Models\Area;
use App\Models\Role;
use App\Models\users_detail;
use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\UsuariosEditRequest;
use Illuminate\Support\Facades\DB;

class RegisteredUserController extends Controller
{
    protected $redirectTo = '../inventarios/';
    
   // protected $clase = 'Resgistro';
    //metodo para la creación de un nuevo usuario haciendo referencia a las tablas persona y areas 
    public function create()
    {
        $Persona = Persona::select(DB::raw('CONCAT(Nombre," ", ApellidoP," ", ApellidoM) AS NombreCompleto'), 'id')->get();
        $Area = Area::select('Nombre', 'id')->get();
        $Roles = Role::select('name', 'id')->get();

        return view('auth.register')->with('persona', $Persona)->with('areas', $Area)->with('roles', $Roles);
    }


    //Metodo de inyeccion de datos 
    public function store(RegisterUserRequest $request)
    {

        $this->validate($request, [
            'persona_id' => "required|unique:users,persona_id",
            'name' => "required|unique:users,name",
            'password' => [
                'required',
                'string',
                'min:8',             //debe tener al menos 8 caracteres de longitud
                'regex:/[a-z]/',      // debe contener al menos una letra minúscula
                'regex:/[A-Z]/',      // debe contener al menos una letra mayúscula 
                'regex:/[0-9]/',      // debe contener al menos un dígito
                'regex:/[@$-_*!%*#?.&]/', // debe contener un carácter especial
            ],
        ]);
        $user = new User();
        $user->name = $request->get('name');
        $user->password =  Hash::make($request->get('password'));
        $user->persona_id = $request->get('persona_id');
        $user->roles_id =$request->get('role_id');
        $user->Activo ='1';
        $user->save();
        $user->roles()->sync($request->get('role_id'));
        event(new Registered($user));

        return redirect('ListaUsuario');
    }
    public function destroy($request, $id)
    {
        $user_id=$request->get('User');

        $user=User::find($user_id);
        $user->Activo=0;
        $user->update();
        event(new Registered("Baja de usuario",$user));

        return redirect('/ListaUsuario');
    }
}

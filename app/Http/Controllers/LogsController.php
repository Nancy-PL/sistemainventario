<?php

namespace App\Http\Controllers;

use Yajra\DataTables\Datatables;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Support\Carbon;
use App\Models\User;
use App\Models\logueo;
use App\Providers\RouteServiceProvider;


class LogsController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:LogsUser.index')->only('index');
    }
    //Metodo listado 
    public function index(Request $request)
    {      
        $FechaInicio = $request->get('FechaInicio');
        $FechaFin =date( "Y-m-d",strtotime($request->get('FechaFin')."+1 days"));

        $activity = Activity::actividad($FechaInicio,$FechaFin)->get();
        return view('auth.LogsUser', compact('activity'));
    }
    //Metodo registro de inicio de logueo 
    public function login(Request $request)
    {
        $fecha = Carbon::now();
        $logueo = logueo::sesionIniciada(auth()->user()->id)->get();
        $Conectar = User::find(auth()->user()->id);
        $activo =  $Conectar->Activo;

        foreach ($logueo as $logueo) {
            $logueo->fin_login = $fecha;
            $logueo->update();
        }

        $logueo = new logueo();
        $logueo->inicio_login = $fecha;
        $logueo->user_id = auth()->user()->id;
        $logueo->save();
        $logueo = logueo::sesionIniciada(auth()->user()->id)->get();
       
        if ($activo == 1) {
            return redirect()->intended(RouteServiceProvider::HOME);
        } else {
            $request->session()->invalidate();
    
           // Auth::logout();
            $request->session()->regenerateToken();

            return back()->with('UPS', "El usuario ha sido de baja ");        
        }

    }
    //Metodo registro de fin de logueo 
    public function logout()
    {
        $fecha = Carbon::now();
        $logueo = logueo::find(auth()->id);
        $logueo->fin_login = $fecha;
        $logueo->update();
        return redirect('/');
    }
}

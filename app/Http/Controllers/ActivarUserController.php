<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Yajra\DataTables\Datatables;



class ActivarUserController extends Controller
{
      //protección de rutas
      public function __construct()
      {
          $this->middleware('can:ActivarUsuario.index')->only('index');
  
      }
    //Metodo listado 
    public function index()
    {
        return view('auth.ActivarUser');
    }
    //Obtener lista de usuarios dados de baja
    public function getUsers(Request $request)
    {
            if ($request->ajax()) {
                $data = User::buscarActivar()->get();
                return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    $actionBtn = '<input type="radio" name="User" value="'.$row->UserId.'" required>';
                    return $actionBtn;
                })
    
                    ->rawColumns(['actions'])
                    ->make(true);
            }
    }
   
  //Activar usuario
    public function store(Request $request)
    {
        $user_id = $request->get('User');
        $user = User::find($user_id);
        $user->Activo = 1;
        $name=$user->name;
        $user->update();
        return back()->with('Listo', "El usuario ".$name." se activo correctamente.");
    }

   
}

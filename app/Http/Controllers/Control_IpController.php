<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\Control_IpRequest;
use App\Http\Requests\EditarRequest;
use Yajra\DataTables\Datatables;
use App\Models\control_ip;
use App\Models\Persona;
use App\Models\Resguardo;
use App\Models\resguardo_detail;
use Faker\Provider\ar_JO\Person;
use App\Models\Area;
use App\Models\jefatura;
use Illuminate\Support\Facades\DB;

class Control_IpController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:SalidaEquipo.index')->only('index');
        $this->middleware('can:SalidaEquipo.index')->only('create');
        $this->middleware('can:SalidaEquipo.index')->only('edit');
    }
    /*Consulta para la vista del Listado*/
    public function index()
    {
        $fecha=date("Y-m-d H:i:s");
        $data = control_ip::ControlTemp($fecha)->get();

        foreach ($data as $controlVence){
        $control_ip = control_ip::findOrFail($controlVence->id);
        if ($control_ip){
            $control_ip->estado = 2;
            $control_ip->estatus = 1;
            $control_ip->fechaActualizacion = $control_ip->fechaActualizacion;
            $control_ip->update();
        }
        
        }
        return view('Control_Ip.index');

       
    }

    /*Muestra la vista para dar de alta */
    public function create(Request $request)
    { 
        //consultar jefatura de cierta area 
        $idArea = $request->get('areaa');
        $personas = Persona::all()->where('Activo', '1');
        $areas = Area::all()->where('Nombre', '!=', 'ADMON SISTEMAS');
        $jefatura = jefatura::consultarA($idArea)->get();
        $ip = DB::table('control_ip')
        ->select('ip')
        ->where('estatus', '=', '2')
        ->get();


        return view('Control_Ip.Registro')->with('personas', $personas)->with('areas', $areas)->with('jefatura', $jefatura)->with('idArea', $idArea)->with('ip', $ip);

    }


    /*Insertando un nuevo en la bd*/
    public function store(Control_IpRequest $request)
    {
        $fecha=date("Y-m-d H:i:s");
        $data = control_ip::ControlTemp($fecha)->get();

        foreach ($data as $controlVence){
        $control_ip = control_ip::findOrFail($controlVence->id);
        if ($control_ip){
            $control_ip->estado = 2;
            $control_ip->estatus = 1;
            $control_ip->fechaActualizacion = $control_ip->fechaActualizacion;
            $control_ip->update();
        }
        
        }
        $control_ipValida = control_ip::all()->where('ip', '=', $request->get('ip'))->where('estado', '=', '1');

        foreach($control_ipValida as $control_ipValida){
            return back()->with('UPS', "Dirección IP ocupado.");
        }

        $idP=$request->get('personas_id');
        $Persona = Persona::find($idP);

        $request->validated();
        $control_ip = new control_ip();
        $control_ip->ip = $request->get('ip');
        $control_ip->estatus = 2; //ocupado
        $control_ip->estado = 1;  //activo
        $control_ip->No_memo = $request->get('memo');
        $control_ip->NivelJerarquia = $request->get('nivel');
        $control_ip->Definitivo = $request->get('estado');
        $control_ip->inicia = $request->get('FechaInicio');
        $control_ip->vence = $request->get('Vence');
        $control_ip->fechaRegistro = date("Y-m-d\TH:i:s");
        $control_ip->U_captura = auth()->user()->id;
        $control_ip->idPersona = $request->get('personas_id');
        $control_ip->idArea = $Persona->area_id;
        $control_ip->idJefatura = $Persona->jefatura_id;
        $control_ip->save();

        return redirect('Control_Ip');
    }
    /*Mostrando la vista para Editar */
    public function edit(Request $request, $id)
    {
        $control_ip = control_ip::find($id);
        $control_ip=json_decode($control_ip);
        $idArea = $request->get('areaa');
        $personas = Persona::all()->where('Activo', '1');
        $areas = Area::all()->where('Nombre', '!=', 'ADMON SISTEMAS');
        $jefatura = jefatura::consultarA($idArea)->get();

        return view('Control_Ip.Editar',compact('control_ip','idArea','personas','areas','jefatura'));
    }

    /*Actualizando un control*/
    public function update(Control_IpRequest $request, $id)
    {
        $control_ipValida = control_ip::all()->where('ip', '=', $request->get('ip'))->where('estado', '=', '1')->where('id', '!=', $id);

        foreach($control_ipValida as $control_ipValida){
            return back()->with('UPS', "Dirección IP ocupado.");
        }

        $idP=$request->get('personas_id');
        $Persona = Persona::find($idP);

        $control_ip = control_ip::findOrFail($id);
        $request->validated();
        $control_ip->ip = $request->get('ip');
        $control_ip->estatus = 2;
        $control_ip->No_memo = $request->get('memo');
        $control_ip->Definitivo = $request->get('estado');
        $control_ip->inicia = $request->get('FechaInicio');
        $control_ip->vence = $request->get('Vence');
        $control_ip->fechaRegistro = date("Y-m-d\TH:i:s");
        $control_ip->U_captura = auth()->user()->id;
        $control_ip->idPersona = $request->get('personas_id');
        $control_ip->idArea =  $Persona->area_id;
        $control_ip->idJefatura = @$Persona->jefatura_id;
        $control_ip->update();
        return redirect('/Control_Ip');
    }
    //consulta de areas para el listado
    public function getControl(Request $request)
    {
        if ($request->ajax()) {
            $data = control_ip::Control()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = ' <a href="Control_Ip/'.$row->id.'/edit" class="fa  fa-edit  fa-lg" ALIGN="center"style="color:#3A3E3C; "></a> ';
                    return $actionBtn;
                })
                ->addIndexColumn()
                ->addColumn('action2', function($row){
                    if($row->estatus==2){
                        $actionBtn = "Ocupado";
                    }else{
                        $actionBtn = "Libre";
                    }
                    return $actionBtn;
                })
                ->addIndexColumn()
                ->addColumn('baja', function($row){
                    $actionBtn = ' <a href="BajaIP/'.$row->id.'/edit"   ALIGN="center"style="color:#3A3E3C; "><span class="badge badge-danger">Baja</span></a> ';
                    return $actionBtn;
                })
                ->rawColumns(['baja'],['action2'],['action'])
                ->make(true);
        }
    }
    
    
 
}
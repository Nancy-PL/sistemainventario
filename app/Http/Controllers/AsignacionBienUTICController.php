<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\AsignacionRequest;
use Barryvdh\DomPDF\Facade as PDF;
use Yajra\DataTables\Datatables;
use App\Models\resguardo_detail;
use App\Models\Resguardo;
use App\Models\Persona;
use App\Models\Bienes;
use App\Models\Area;
use App\Models\User;

class AsignacionBienUTICController extends Controller
{
    //protección de rutas
    public function __construct()
    {
        $this->middleware('can:ResguardosUtic.index')->only('index');
        $this->middleware('can:ResguardosUtic.create')->only('create');
        $this->middleware('can:ResguardosUtic.edit')->only('edit');
        $this->middleware('can:ResguardoPDFUtic.resguardo')->only('resguardo');
    }
    //Listado de resguardos de bienes Equipo
    public function index()
    {
        return view('Resguardos.UTIC.ListaResguardoUTIC');
    }
    //consulta de Resguardo Utic para el listado de resguardos Activos
    public function getResguardoUtic(Request $request)
    {
        if ($request->ajax()) {
            $data =  Resguardo::ResguardoActivoUtic()->get();
            return Datatables::of($data)->make(true);
        }
    }
    //Asignar un nuevo bien
    public function create()
    {
        $personas = Persona::all()->where('Activo', '1');
        return view('Resguardos.UTIC.AsignacionBienUTIC', compact('personas'));
    }

    //consulta de Asignacion Bien Utic para el listado
    public function getAsignacionBienUtic(Request $request)
    {
        if ($request->ajax()) {
            $data =  Bienes::bienDisponible("SI")->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '<input class="checkboxClass" type="checkbox" id="biens" name="biens[]" value="' . $row->id . '">
                    ';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
    //Validar existencia de personas con resguardo
    public function ValidarPersona($id)
    {
        $resguardos = DB::table('resguardos')
            ->select('resguardos.*')
            ->where('SI', '=', '1')
            ->where('persona_id', '=', $id)
            ->where('tipo', '=', 'PERMANENTE')
            ->get();

        foreach ($resguardos as $resguardo) {
            if ($resguardos) {
                return $resguardo->id;
            } else {
                return 0;
            }
        }
    }
    //Asignacion de un bien a resguardantes
    public function store(AsignacionRequest $request)
    {
        $request->validated();
        $Biens = $request->get('bienes');
        $idPersona = $request->get('persona_id');
        // separando el nombre
        $Bien = explode(",", $Biens);
        foreach ($Bien as $bien) {
            $fecha = Carbon::now();
            $bienes = Bienes::find($bien);
            $resguardo_detail = new resguardo_detail();
            $Resguardante_id = $this->ValidarPersona($idPersona);

            if ($Resguardante_id == 0) {
                $Resguardo = new Resguardo();
                $Resguardo->tipo = "PERMANENTE";
                $Resguardo->persona_id = $idPersona;
                $Resguardo->SI = "1";
                $Resguardo->activo = 1;
                $Resguardo->save();
                $Resguardante_id =  $Resguardo->id;
            }

            $resguardo_detail->resguardo_id = $Resguardante_id;
            $resguardo_detail->fechaAsignacion = $fecha;
            $resguardo_detail->bien_id = $bien;
            $resguardo_detail->activo = 1;
            $resguardo_detail->User = auth()->user()->id;
            $resguardo_detail->save();

            $bienes->Status = 1;
            $bienes->update();
        }
        return redirect('/ListaAsignadosUtic/' . $Resguardante_id);
    }
    //Listado de resguardos especificos bienes Equipo de computo y accesorios
    public function lista($id)
    {
        $resguardo = Resguardo::listaResguardoUtic($id)->get();
        return view('Resguardos.UTIC.ListadoResguardo', compact('resguardo'));
    }
    //Consultar resguardos de resguardantes para generar reporte
    public function resguardo(Request $request)
    {
        $personas = Persona::all()->where('Activo', 1);
        $id = $request->get('resguardante');
        $resguardos = Resguardo::buscarpersonaUtic($id)->get();
        $areas = Area::reporteResguardoUTIC()->get();
        return view('Resguardos.UTIC.ResguardoporResguardante', compact('personas', 'resguardos', 'areas', 'id'));
    }
    //Descargar reporte de salida de bienes de Equipo de computo y accesorio
    public function downloadResguardoPDF($id, $VoBo)
    {
        $areas = Area::find($VoBo);
        $resguardos = Resguardo::buscarpersonaUtic($id)->get();
        $Elaboro = User::personaEnSesion(auth()->user()->id)->get();
        $pdf = PDF::loadView('Resguardos.UTIC.Reportes.ReporteResguardante', compact('resguardos',  'Elaboro', 'areas'));
        return $pdf->stream('Resguardo UTIC.pdf');
    }
}

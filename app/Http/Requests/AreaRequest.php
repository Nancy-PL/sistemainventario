<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;


class AreaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {

        return [
            'NombreA' => ['required', 'string', 'max:150'],
            'ResponsableA' => 'required|string|max:150',
            'CargoA' => 'required|string|max:150',
            'LetraA' => ['required', 'string', 'max:150'],
        ];
    }
    public function messages()
    {
        return [
            'NombreA.required' => 'Nombre del área requerido',
            'NombreA.max' => 'Nombre del área demasiado largo',
            'ResponsableA.required' => 'Responsable requerido',
            'ResponsableA.max' => 'Responsable debe de ser máximo de 150 caracteres',
            'CargoA.required' => 'Cargo requerido',
            'CargoA.max' => 'Cargo máximo de 150 caracteres',
            'LetraA.max' => 'La letra debe de ser de uno a 5 caracteres',
            'LetraA.unique' => 'La letra ya existe',
        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

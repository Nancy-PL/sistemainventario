<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;


class PersonasRequest extends FormRequest
{
    public function authorize()
    {
        // return Auth::check();
        return true;
    }


    public function rules()
    {

        return [
            'Nombre' => 'required|max:30',
            'ApellidoP' => 'required|max:30',
            'ApellidoM' => 'required|max:30',
            'Profesion' => ['required', 'string', 'max:10'],

        ];
    }
    public function messages()
    {
        return [
            'Nombre.required' => 'Nombre requerido',
            'Nombre.max' => 'Nombre demasiado largo',
            'ApellidoP.required' => 'Apellido paterno requerido',
            'ApellidoP.max' => 'Apellido paterno demasiado largo',
            'ApellidoM.required' => 'Apellido materno requerido',
            'ApellidoM.max' => 'Apellido materno demasiado largo',
            'Profesion.required' => 'Profesión requerido',
            'Profesion.max' => 'Profesión debe de ser máximo de 10 caracteres',
        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

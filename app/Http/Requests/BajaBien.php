<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;

class BajaBien extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        
        return [
            'bienes' => ['required'],
           
        ];
    }
    public function messages()
    {
        return [
            'bienes.required' => 'Es necesario seleccionar un bien para asignar',
        ];
    }
    
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

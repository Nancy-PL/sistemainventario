<?php


namespace App\Http\Requests;



use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;

class RegisterUserRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    
    }

    
    public function rules()
    {
        return [
            'persona_id' => 'required','unique:users,persona_id',
            'name' =>[ 'required', 'string', 'max:30', 'unique:users,name,'],
            'password' => 'required|string|min:8|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$-_*!%*#?.&]/', 

           
          
            
        ];
    }
    public function messages()
    {
        return [
            'persona_id.required' => 'Persona requerido',
            'persona_id.unique' => 'Persona ya cuenta con usuario',
            'name.required' => 'Nombre de Usuario requerido',
            'name.max'    => 'Usuario máximo 30 caracteres',
            'name.unique' => 'Nombre existente',
            'password.required' => 'contraseña requerida', 
            'password.min' =>'Debe contener al menos 8 caracteres', 
                'required'=> 'contraseña requerida',
                'min' => 'Debe tener al menos 8 caracteres de longitud',
                'regex'=>'Debe contener letras Mayúsculas, Minúsculas y un dígito(No alfanuméricos)',
        ];
    }


    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

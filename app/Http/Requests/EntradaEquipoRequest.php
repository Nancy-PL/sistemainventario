<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;

class EntradaEquipoRequest extends FormRequest
{
   
    public function authorize()
    {
        return Auth::check();
    }
    public function rules()
    {
            
        return [
            'bienes' => 'required',

        ];
    }
    
    public function messages()
    {
        return [
            'bienes.required' => 'Seleccione los bienes para registrar',
        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

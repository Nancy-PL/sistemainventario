<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;

class ClasificacionRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check();
    }
    public function rules()
    {

        return [
            'Clave' => ['required', 'string', 'max:100'],
            'Concepto' =>  ['required', 'string', 'max:100', 'unique:clasificacions,concepto,'],
            'NoInventario' => 'required',
            'Departamento'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'Clave.required' => 'clave requerido',
            'Clave.max' => 'clave demasiado largo',
            'Concepto.required' => 'Concepto requerido',
            'Concepto.max' => 'Concepto debe de ser máximo de 100 caracteres',
            'NoInventario.required' => 'No. de Inventario requerido',
            'Departamento.required' => 'Departamento requerido',
        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

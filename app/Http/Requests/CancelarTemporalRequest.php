<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;

class CancelarTemporalRequest extends FormRequest
{   public function authorize()
    {
        return Auth::check();
    }
    public function rules()
    {
            
        return [
            'bienes' => 'required',
            'persona_id'=>'required'
        
        ];
    }
    
    public function messages()
    {
        return [
            'persona_id.required' => 'Seleccione a la persona a asignar',
            'bienes.required' => 'Seleccione los bienes para cancelar',
        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

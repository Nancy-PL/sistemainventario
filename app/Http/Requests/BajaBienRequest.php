<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;

class BajaBienRequest extends FormRequest
{ public function authorize()
    {
        return Auth::check();
    }

    
    public function rules()
    {
         
        return [
            'Motivo'=>'required|string|max:300',
            'Observaciones' => 'required|string|max:800',
            'bienes'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'Motivo' => [
                'required' => 'Motivo del Modelo requerido',
                'max' => 'Motivo demasiado largo',
            ],
            'Observaciones' => [
                'required' => 'Observaciones requerido',
                'max' => 'Observaciones demasiado largo',
            ],
            'bienes.required' => 'Seleccione por lo menos un bien'

        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

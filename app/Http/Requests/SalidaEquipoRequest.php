<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;

class SalidaEquipoRequest extends FormRequest
{

   
    public function authorize()
    {
        return Auth::check();
    }
    public function rules()
    {
            
        return [
            'Referencia' => 'required|string|max:800',
            'bienes' => 'required',
        
        ];
    }
    
    public function messages()
    {
        return [
            'Referencia.max' => 'La referencia es demasiado largo',
            'bienes.required' => 'Seleccione los bienes para registrar salida',
        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
    
}

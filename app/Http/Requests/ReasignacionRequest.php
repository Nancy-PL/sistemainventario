<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;

class ReasignacionRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check();
    }
    public function rules()
    {
            
        return [
            'Motivos' => 'string|max:300',
            'resguardos' => 'required',
        
        ];
    }
    
    public function messages()
    {
        return [
            'Motivos.max' => 'Motivo demasiado largo',
            'resguardos.required' => 'seleccione los bienes para asignar',
        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

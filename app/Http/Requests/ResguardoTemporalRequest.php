<?php

namespace App\Http\Requests;


use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;

class ResguardoTemporalRequest extends FormRequest
{
   
    public function authorize()
    {
        return Auth::check();
    }
    public function rules()
    {
            
        return [
            'Numero' => 'required|string|max:50',
            'bienes' => 'required',
        
        ];
    }
    
    public function messages()
    {
        return [
            'Numero.max' => 'El número de orden es demasiado largo',
            'bienes.required' => 'Seleccione los bienes para asignar',
        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

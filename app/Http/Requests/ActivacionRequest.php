<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;


class ActivacionRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }


    public function rules()
    {

        return [
            'resguardos' => ['required'],
           
        ];
    }
    public function messages()
    {
        return [
            'resguardos.required' => 'Es necesario seleccionar un bien para activar',
        ];
        }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

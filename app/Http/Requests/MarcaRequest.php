<?php

namespace App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;



class MarcaRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check();
    }


    public function rules()
    {
        return [
            'Marca' => ['required','string','max:40','unique:marcas,nombre,']
        ];
    }
    public function messages()
    {
        return [
                'Marca.required' => 'Nombre de la marca requerido',
                'Marca.max' => 'Nombre demasiado largo',
                'Marca.unique' => 'Marca existente',
        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

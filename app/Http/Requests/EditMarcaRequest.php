<?php


namespace App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Routing\Route;


class EditMarcaRequest extends FormRequest
{
    public function __construct(Route $route)
    {
    $this->route=$route;
    }

    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'Marca' => 'required','unique:marcas,nombre,'.$this->route('Marca')

        ];
    }
    public function messages()
    {
        return [
            'Marca.required' => 'Nombre de la marca requerido',
                'Marca.max' => 'Nombre demasiado largo',
                'Marca.unique' => 'Marca existente',
        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

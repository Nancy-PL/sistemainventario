<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;


class JefaturaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {

        return [
            'area' => ['required', 'string', 'max:15'],
            'personas_id' => 'required|string|max:15',
            'Nombre' => ['required', 'string', 'max:140'],


        ];
    }
    public function messages()
    {
        return [
            'area.required' => 'Nombre del área requerido',
            'area.max' => 'Nombre del área demasiado largo',
            'Nombre.required' => 'Nombre requerido',
            'Nombre.max' => 'Nombre debe de ser máximo de 45 caracteres',
            'personas_id.required' => 'Nombre de la persona requerido',
            'personas_id.max' => 'El nombre de la persona debe de tener máximo de 150 caracteres',
           
            

        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

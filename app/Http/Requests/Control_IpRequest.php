<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;


class Control_IpRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {

        return [
        //    'area' => ['required', 'string', 'max:15'],
           // 'Jefatura' => 'required|string|max:15',
            'personas_id' => 'required|string|max:15',
            'ip' => ['required', 'string', 'max:16'],
            'estado' => ['required', 'string', 'max:3'],
            'FechaInicio' => ['required'],
            'nivel' => ['required'],
            'memo' => ['required', 'string', 'max:50'],

        ];
    }
    public function messages()
    {
        return [
            'memo.required' => 'No. de memo  requerido',
            'memo.max' => 'No. de memo demasiado largo',
            'area.required' => 'Nombre del área requerido',
            'area.max' => 'Nombre del área demasiado largo',
            'Jefatura.required' => 'Jefatura requerido',
            'Jefatura.max' => 'Jefatura debe de ser máximo de 15 caracteres',
            'personas_id.required' => 'Nombre de la persona requerido',
            'personas_id.max' => 'El nombre de la persona debe de tener máximo de 150 caracteres',
            'ip.required' => 'Ip de la persona requerido',
            'estado.required' => 'Estado requerido',
            'FechaInicio.required' => 'Fecha de Inicio requerido',
            'nivel.required' => 'Nivel requerido',

        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

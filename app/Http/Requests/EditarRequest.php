<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;


class EditarRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check();
    }


    public function rules()
    {

        return [
            'NombreA' => 'required', 'string', 'max:150',
            'ResponsableA' => 'required|string|max:150',
            'CargoA' => 'required|string|max:150',
            'LetraA' => 'required', 'string', 'max:5', 'unique:areas,letra,{$this->areas->id}',
        ];
    }
    public function messages()
    {
        return [
            'NombreA.required' => 'Nombre requerido',
            'NombreA.max' => 'Nombre demasiado largo',
            'ResponsableA.required' => 'Responsable requerido',
            'ResponsableA.max' => 'Responsable debe de ser máximo de 150 caracteres',
            'CargoA.required' => 'Cargo requerido',
            'CargoA.max' => 'Cargo máximo de 150 caracteres',
            'LetraA.max' => 'La letra debe de ser de uno a 5 caracteres',
        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

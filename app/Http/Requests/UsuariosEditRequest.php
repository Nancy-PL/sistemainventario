<?php

namespace App\Http\Requests;


use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;

class UsuariosEditRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    
    }

    public function rules()
    {
        return [
            
            'name' =>'required', 'string', 'max:30', 'unique:users,name',
            'password' => 'required|string|min:8|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$-_*!%*#?.&]/', 
          
            
        ];
    }
    public function messages()
    {
        return [
            
            'name.required' => 'Nombre de Usuario requerido',
            'name.max'    => 'Usuario máximo 30 caracteres',
          
            'password.required' => 'Contraseña requerida', 
            'password.min' =>'Debe contener al menos 8 caracteres', 
                'required'=> 'contraseña requerida',
                'min' => 'Debe tener al menos 8 caracteres de longitud',
                'regex'=>'Debe contener letras Mayúsculas, Minúsculas y un dígito(No alfanuméricos)',
                
            
         
        ];
    }


    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
}

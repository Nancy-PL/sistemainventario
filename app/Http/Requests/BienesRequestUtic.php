<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;

class BienesRequestUtic extends FormRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    
    public function rules()
    {
         
        return [
            'Modelo' => 'required|string|max:100',
            //'Estado' => 'required|string|max:10',
             'factura' => 'required|string|max:25',
             'NoInventarioAnterior' => ['nullable','string','max:150'],
             'NoSerie' => 'nullable|string|max:100',
             'Clasificacion_id' => 'required',
             'marca_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'Modelo.required' => 'Nombre del Modelo requerido',
            'Modelo.max' => 'Nombre demasiado largo',
            'Modelo.unique' => 'Modelo existente',
            'NoSerie.unique' => 'Número de Serie existente',
            'Clasificacion_id.required' => 'Clasificación requerido',
            'marca_id.required' => 'Marca requerido',
            'factura.max' => 'Factura debe de tener máximo 25 caracteres',

        ];
    }
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Debes iniciar sesión');
    }
    
}

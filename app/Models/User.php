<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;
    use LogsActivity;

    protected $table = 'users';
    protected $guarded = array();
    protected $with = ['logueo'];

    protected $fillable = [
        'persona_id',
        'Activo',
        'name',
        'password',
        'roles_id',
    ];

    protected static $logName = "auth()->user()->id";

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['name', 'roles_id', 'persona_id','Activo']);
        // Chain fluent methods for configuration options
    }
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == "created") {
            return "Creación de un nuevo usuario";
        } else if ($eventName == "updated") {
            return "Actualización de un nuevo usuario";
        } else if ($eventName == "deleted") {
            return "Baja de un usuario";
        }else{
            return "{$eventName}";
        }
    }
    protected $hidden = [
        'password',
        'remember_token',
    ];


    public function sessions()
    {
        return $this->hasMany(logueo::class);
    }
    //busqueda scope de la persona en sesion
    public function scopePersonaEnSesion($query, $id)
    {
        if ($id) {
            return $query
                ->join('personas', 'users.persona_id', '=', 'personas.id')
                ->where('users.id', '=', $id)
                ->select('personas.Profesion', 'personas.Nombre', 'personas.ApellidoP', 'personas.ApellidoM');
        }
    }


    //busqueda scope de usuarios activos
    public function scopeBuscar($query)
    {
        return $query
            ->join('personas', 'users.persona_id', '=', 'personas.id')
            ->join('areas', 'areas.id', '=', 'personas.area_id')
            ->join('roles', 'users.roles_id', '=', 'roles.id')
            ->where('users.Activo', '=', '1')
            ->select('users.name', 'users.created_at', 'users.password', 'users.persona_id', 'users.id as UserId', 'personas.id as userP', 'personas.Profesion', 'personas.Nombre as NombreP', 'personas.ApellidoP', 'personas.ApellidoM', 'areas.Nombre as NombreA', 'roles.name as NombreRol', 'roles.id as roles_id');
    }

    //busqueda scope de usuarios desactivados
    public function scopeBuscarActivar($query)
    {
        return $query
            ->join('personas', 'users.persona_id', '=', 'personas.id')
            ->join('areas', 'areas.id', '=', 'personas.area_id')
            ->join('roles', 'users.roles_id', '=', 'roles.id')
            ->where('users.Activo', '=', '0')
            ->select('users.name', 'users.created_at', 'users.password', 'users.persona_id', 'users.id as UserId', 'personas.id as userP', 'personas.Profesion', 'personas.Nombre as NombreP', 'personas.ApellidoP', 'personas.ApellidoM', 'areas.Nombre as NombreA', 'roles.name as NombreRol', 'roles.id as roles_id');
    }
    protected $casts = [
        //'email_verified_at' => 'datetime',
        'name_verified_at' => 'datetime',
    ];
    //relacion uno a uno
    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }

    //relacion uno a muchos inversa
    public function Role()
    {
        return $this->belongsTo('App\Models\Role');
    }
    //relacion uno a muchos
    public function users_detail()
    {
        return $this->hasMany('App\Models\users_detail', 'user_id');
    }
    //relacion uno a muchos
    public function logueo()
    {
        return $this->hasMany('App\Models\logueo', 'user_id');
    }
}

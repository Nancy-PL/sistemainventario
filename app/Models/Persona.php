<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Persona extends Model
{
    use HasFactory;
    use LogsActivity;

    protected $fillable = [
        'Nombre',
        'ApellidoP',
        'ApellidoM',
        'Profesion',
        'Activo',
    ];
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['Nombre', 'ApellidoP', 'ApellidoM', 'Profesion','Activo']);
        // Chain fluent methods for configuration options
    }
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == "created") {
            return "Creación de una persona";
        } else if ($eventName == "updated") {
            return "Actualización de una persona";
        } else if ($eventName == "deleted") {
            return "Baja de una persona";
        }else{
            return "{$eventName}";
        }
    }


       //relación uno a uno 

       public function BajaPersonal()
       {
           return $this->hasOne('App/Models/BajaPersonal', 'persona_id');
       }

    public function Resguardo()
    {
        return $this->hasMany('App/Models/Resguardo');
    }
    //busqueda scope de persona
    public function scopeBuscarpor($query, $id)
    {
        if ($id) {
            return $query->where('personas.id', '=', $id);
        }
    }
    //Relacion uno a uno
    public function User()
    {
        return $this->hasOne('App\Models\User', 'persona_id');
    }
    //relacion uno a muchos inversa
    public function Area()
    {
        return $this->belongsTo('App\Models\Area');
    }
    
    //Consultar persona de almacen
    public function scopeBuscarAlmacen($query, $nombres, $Apellido, $Apellidos)
    {
        if ($nombres) {
            return  $query->where('Nombre', 'like', '%'.$nombres.'%')->where('ApellidoP', 'like',  '%'.$Apellido.'%')->where('ApellidoM', 'like', '%'.$Apellidos.'%')->select('personas.id as id');

        }
    }
        //Consultar personas de cierta area 
        public function scopeConsultarPersonas($query, $id)
        {
           if($id){
            return $query 
            ->where('personas.area_id', '=', $id)
            ->where('Activo', '1')
            ->select('personas.*');
           }
        }
//personas activas para dar de baja
        public function scopeBuscarPersona($query)
    {


        return $query->join('areas','areas.id','=','personas.area_id')
        ->join('jefatura','jefatura.id','=','personas.jefatura_id', 'left outer')
        ->select('personas.*','personas.id as id_persona','jefatura.Nombre as NombreJ','areas.Nombre as NombreA')
        ->where('personas.Activo','=','1');
    }
    //personas desactivadas para acctivar
        public function scopeBuscarPersonaD($query)
    {


        return $query->join('areas','areas.id','=','personas.area_id')
        ->select('personas.*','personas.id as id_persona','areas.Nombre as NombreA')
        ->where('personas.Activo','=','0');
    }
      //Resguardos  disponibles para resguardo temporal
      public function scopeResguardoTemporal($query)
      {
          return $query
             ->join('resguardos', 'resguardos.persona_id', '=', 'personas.id')
             ->join('resguardos_temporal', 'resguardos_temporal.resguardo_id', '=', 'resguardos.id')
              ->where('resguardos.tipo', '=', 'TEMPORAL')
              ->where('resguardos.SI', '=', '1')
              ->select('personas.*', 'resguardos_temporal.Activo', 'resguardos_temporal.fecha_vencimiento','resguardos_temporal.No_orden', 'resguardos_temporal.Observaciones','resguardos_temporal.id as resguardoId');
      }
}

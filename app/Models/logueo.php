<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class logueo extends Model
{
    protected $table = 'logueo';
    public $timestamps = false;
    protected $appends = ['expires_at'];

    use HasFactory;
    //relacion uno a muchos inversa
    public function User()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function isExpired()
    {
        $fecha = Carbon::now();
        $logueo = logueo::sesionIniciada(auth()->user())->get();

        foreach ($logueo as $logueo) {
            $logueo->fin_login = $fecha;
            $logueo->update();
            break;
        }
        return $this->last_activity < Carbon::now()->subMinutes(config('session.lifetime'))->getTimestamp();
    }

    public function getExpiresAtAttribute()
    {
        return Carbon::createFromTimestamp($this->last_activity)->addMinutes(config('session.lifetime'))->toDateTimeString();
    }
    //obtener session iniciada
    public function scopeSesionIniciada($query,$id)
    {
        if($id){
            return $query
            ->where('user_id', '=', $id)
            ->where('fin_login', '=', NULL)
            ->select('logueo.*');
        }
       
    }
  
    //obtener informacion de los usuarios logueados
    public function scopeLogs($query)
    {
            return $query
            ->join('users','users.id','=','logueo.user_id')
            ->whereDate('logueo.inicio_login', Carbon::today())
            ->select('logueo.*','users.name');
       
    }
}

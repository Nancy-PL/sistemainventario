<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class resguardo_temporalbitacora extends Model
{

    protected $table = 'resguardos_temporalbitacora';
    public $timestamps = false;
    protected $guarded = array();

    use HasFactory;
    use LogsActivity;


    protected $fillable = [
        'folio',
        'fecha_Orden',
        'No_Orden',
        'fecha_inicio',
        'fecha_vencimiento',
        'fecha_devolucion',
        'id_resguardoTemporal',
        'fecha_Actualizacion',
        'usuario',
    ];
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['folio', 'fecha_Orden', 'No_Orden','fecha_inicio','fecha_vencimiento','fecha_devolucion','id_resguardoTemporal','fecha_Actualizacion','usuario']);
    }
 
}

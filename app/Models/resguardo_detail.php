<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class resguardo_detail extends Model
{
   protected $table = 'resguardo_detail';
   public $timestamps = false;
   protected $guarded = array();

   use HasFactory;
   use LogsActivity;


   protected $fillable = [
       'fechaAsignacion',
       'fechaCancelacion',
       'Activo',
       'ResguardoAnterior_id',
       'bien_id',
       'resguardo_id',
   ];

   public function getActivitylogOptions(): LogOptions
   {
       return LogOptions::defaults()
           ->logOnly(['fechaAsignacion', 'fechaCancelacion', 'Activo','ResguardoAnterior_id','bien_id','resguardo_id']);
   }
   public function getDescriptionForEvent(string $eventName): string
   {
       if ($eventName == "created") {
           return "Asignacion de un nuevo bien";
       } else if ($eventName == "updated") {
           return "Actualizacion de resguardante de un nuevo bien";
       } else if ($eventName == "deleted") {
           return "Baja de un resguardo";
       }else{
           return "{$eventName}";
       }
   }
   //relación uno a muchos inversa
   public function resguardo_detail()
   {
      return $this->belongsTo(resguardo_detail::class);
   }
   public function Resguardo()
   {
      return $this->belongsTo('App/Models/Resguardo');
   }
   //relacion uno a muchos inversa
   public function Bienes()
   {
      return $this->belongsTo('App/Models/Bienes');
   }

   //Consulta bienes para baja
   public function  scopeBienesUticActivos($query)
   {
       return $query
           ->leftjoin('bienes', 'resguardo_detail.bien_id', '=', 'bienes.id')
           ->leftjoin('resguardos', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
           ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
           ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
           ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
           ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id','left outer')
           ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
           ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')  
           ->where('bienes.Departamento', 'like',  '%SI%')
           ->where('bienes.Activo', '=', '1')
           ->where('resguardo_detail.Activo', '=', '1')
           ->select('mejoras_equipos.*','detalle_biens.*','historial__inventarios.*','personas.*', 'resguardo_detail.*', 'marcas.*', 'clasificacions.*', 'bienes.*');
   }
   //Consulta reesguardo Utic activo
   public function  scopeResguardosActivos($query,$id,$depto)
   {
       if($id){
        return $query
        ->leftjoin('bienes', 'resguardo_detail.bien_id', '=', 'bienes.id')
        ->where('bienes.Departamento', 'like',  '%'.$depto.'%')
        ->where('bienes.Activo', '=', '1')
        ->where('resguardo_detail.Activo', '=', '1')
        ->where('resguardo_detail.resguardo_id', '=', $id)
        ->select('resguardo_detail.id');
       }
     
   }
}

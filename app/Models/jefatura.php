<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\DB;

class jefatura extends Model
{
    use HasFactory;
    use LogsActivity;
    protected $table = 'jefatura';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'idPersona',
        'idArea',
  ///      'NivelJerarquia',
        'Usuario_Captura',
        'Fecha_Captura',
        'Fecha_Actualizacion',

    ];
    ///funcion para obtener las actividades 
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['idPersona', 'idArea', 'Usuario_Captura','Fecha_Captura','Fecha_Actualizacion']);
    }
    //función para la descripción de los eventos que se hacen 
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == "created") {
            return "Creación de una nueva Jerarquia";
        } else if ($eventName == "updated") {
            return "Actualización de Jerarquia";
        } else if ($eventName == "deleted") {
            return "Baja de una Jerarquia";
        }else{
            return "{$eventName}";
        }
    }

    
 //Consultar jefaturas especificas
 public function scopeConsultarA($query, $id)
 {
    if($id){
     return $query 
     ->where('jefatura.idArea', '=', $id)
     ->select('jefatura.*');
    }
 }
 //Control lista en index
 public function scopeJefaturas($query)
 {
     if($query){
         return $query->join('personas', 'personas.id', '=', 'jefatura.idPersona')
         ->join('areas', 'areas.id', '=', 'jefatura.idArea')
    //     ->where('control_ip.estado', '=', '1')
         ->select(DB::raw('CONCAT(personas.Nombre," ", personas.ApellidoP," ", personas.ApellidoM) AS NombreP'),'areas.Nombre as NombreA','jefatura.*');
     }

 }
}

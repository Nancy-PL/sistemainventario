<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class control_ip extends Model
{
    use HasFactory;
    use LogsActivity;
    protected $table = 'control_ip';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'ip',
        'estatus',
        'No_Memo',
        'Definitivo',
        'inicia',
        'vence',
        'fechaRegistro',
        'fechaActualizacion',
        'U_captura',
        'idPersona',
        'idArea',

    ];
    ///funcion para obtener las actividades 
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['ip', 'estatus', 'No_Memo','Definitivo','inicia','vence','fechaRegistro','fechaActualizacion','U_captura','idPersona','idArea']);
    }
    //función para la descripción de los eventos que se hacen 
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == "created") {
            return "Creación de un nuevo control de ip";
        } else if ($eventName == "updated") {
            return "Actualización de control de ip";
        } else if ($eventName == "deleted") {
            return "Baja de un control de ip";
        }else{
            return "{$eventName}";
        }
    }

    
    //Control lista en index
    public function scopeControl($query)
    {
        if($query){
            return $query->join('personas', 'personas.id', '=', 'control_ip.idPersona')
            ->join('areas', 'areas.id', '=', 'control_ip.idArea')
            ->join('jefatura', 'jefatura.id', '=', 'control_ip.idJefatura','left outer')
            ->where('control_ip.estado', '=', '1')
            ->select(DB::raw('CONCAT(personas.Nombre," ", personas.ApellidoP," ", personas.ApellidoM) AS NombreP'),'areas.Nombre as NombreA','jefatura.Nombre as NombreJ',  'control_ip.*');
        }

    }
    //consultar si aun no vencen los ip temporales
    public function scopeControlTemp($query,$fecha)
    {
        if($fecha){
            return $query
         //   ->where('control_ip.estado', '=', '1')
            ->where('control_ip.Definitivo', '=', '2')
           // ->where('control_ip.vence', '>=', $fecha)
            ->whereBetween('control_ip.vence', [Carbon::now()->subDays(150), Carbon::now()])
            ->select('control_ip.id');
        }

    }
     //Consultar el control de ip por área para reportes de  excel 
     public function scopeBuscarIpArea($query, $id)
     {
         if ($id) {
             return  $query
             ->join('areas', 'areas.id', '=', 'control_ip.idArea')
             ->join('jefatura', 'jefatura.id', '=', 'control_ip.idJefatura', 'left outer')
             ->join('personas', 'personas.id', '=', 'control_ip.idPersona')
             ->where('control_ip.idArea', '=', $id)
             ->select('areas.Nombre as NombreA','jefatura.Nombre as NombreJ', 'control_ip.ip', 'control_ip.ip',DB::raw('CONCAT(personas.Nombre," ", personas.ApellidoP," ", personas.ApellidoM) AS NombreP'),'control_ip.estatus','control_ip.No_memo','control_ip.inicia','control_ip.vence','control_ip.Definitivo');
 
         }
     }
       //Consultar el control de ip por jefatura para reportes de  excel
       public function scopeBuscarIpJefatura($query, $id)
       {
           if ($id) {
               return  $query
               ->join('areas', 'areas.id', '=', 'control_ip.idArea')
               ->join('jefatura', 'jefatura.id', '=', 'control_ip.idJefatura', 'left outer')
               ->join('personas', 'personas.id', '=', 'control_ip.idPersona')
               ->where('control_ip.idJefatura', '=', $id)
               ->select('areas.Nombre as NombreA','jefatura.Nombre as NombreJ', 'control_ip.ip', 'control_ip.ip',DB::raw('CONCAT(personas.Nombre," ", personas.ApellidoP," ", personas.ApellidoM) AS NombreP'),'control_ip.estatus','control_ip.No_memo','control_ip.inicia','control_ip.vence','control_ip.Definitivo');
   
           }
       }
        //Consultar el control de ip por segmentación para reportes de excel
        public function scopeBuscarIpSeg($query, $Segmentacion)
        {
            if ($Segmentacion) {
                return  $query
                ->join('areas', 'areas.id', '=', 'control_ip.idArea')
                ->join('jefatura', 'jefatura.id', '=', 'control_ip.idJefatura', 'left outer')
                ->join('personas', 'personas.id', '=', 'control_ip.idPersona')
                ->where('control_ip.ip', 'like', '%'.$Segmentacion.'%')
                ->select('areas.Nombre as NombreA','jefatura.Nombre as NombreJ', 'control_ip.ip', 'control_ip.ip',DB::raw('CONCAT(personas.Nombre," ", personas.ApellidoP," ", personas.ApellidoM) AS NombreP'),'control_ip.estatus','control_ip.No_memo','control_ip.inicia','control_ip.vence','control_ip.Definitivo');
    
            }
        }
          //Consultar el control de ip por segmentación para reportes de excel
          public function scopeBuscarIpDefinitivo($query, $Definitivo)
          {
              if ($Definitivo) {
                  return  $query
                  ->join('areas', 'areas.id', '=', 'control_ip.idArea')
                  ->join('jefatura', 'jefatura.id', '=', 'control_ip.idJefatura', 'left outer')
                  ->join('personas', 'personas.id', '=', 'control_ip.idPersona')
                  ->where('control_ip.Definitivo', '=', $Definitivo)
                  ->select('areas.Nombre as NombreA','jefatura.Nombre as NombreJ', 'control_ip.ip', 'control_ip.ip',DB::raw('CONCAT(personas.Nombre," ", personas.ApellidoP," ", personas.ApellidoM) AS NombreP'),'control_ip.estatus','control_ip.No_memo','control_ip.inicia','control_ip.vence','control_ip.Definitivo');
      
              }
          }
            //Consultar el control de ip  para reportes general de excel
            public function scopeBuscarIpGral($query)
            {
                    return  $query
                    ->join('areas', 'areas.id', '=', 'control_ip.idArea')
                    ->join('jefatura', 'jefatura.id', '=', 'control_ip.idJefatura', 'left outer')
                    ->join('personas', 'personas.id', '=', 'control_ip.idPersona')
                    ->orderby('control_ip.idArea','asc')
                    ->orderby('control_ip.idJefatura','asc')
                    ->orderby('control_ip.NivelJerarquia','asc')
                    ->select('areas.Nombre as NombreA','jefatura.Nombre as NombreJ', 'control_ip.ip', 'control_ip.ip',DB::raw('CONCAT(personas.Nombre," ", personas.ApellidoP," ", personas.ApellidoM) AS NombreP'),'control_ip.estatus','control_ip.No_memo','control_ip.inicia','control_ip.vence','control_ip.Definitivo','control_ip.NivelJerarquia');
        
                
            }

              //obtener informacion de las proximas direcciones ip a vencer
            public function scopeIpAVencer($query)
            {
                return $query->join('personas', 'personas.id', '=', 'control_ip.idPersona')
                ->join('areas', 'areas.id', '=', 'control_ip.idArea')
                ->join('jefatura', 'jefatura.id', '=', 'control_ip.idJefatura','left outer')
                ->where('control_ip.estado', '=', '1')
                ->where('control_ip.Definitivo', '=', '2')
                ->whereBetween('vence', [Carbon::now()->subDays(10), Carbon::now()->addDays(7)])
                ->select(DB::raw('CONCAT(personas.Nombre," ", personas.ApellidoP," ", personas.ApellidoM) AS NombreP'),  'control_ip.*');
     
            
            }
            //Listado de control de direcciones ip dados de baja
            public function scopeControlBaja($query)
            {
                if($query){
                    return $query->join('personas', 'personas.id', '=', 'control_ip.idPersona')
                    ->join('areas', 'areas.id', '=', 'control_ip.idArea')
                    ->join('jefatura', 'jefatura.id', '=', 'control_ip.idJefatura','left outer')
                    ->where('control_ip.estado', '=', '2')
                    ->select(DB::raw('CONCAT(personas.Nombre," ", personas.ApellidoP," ", personas.ApellidoM) AS NombreP'),'areas.Nombre as NombreA','jefatura.Nombre as NombreJ',  'control_ip.*');
                }

            }
             //Listado de Disponibilidad control de direcciones ip  
             public function scopeControlDisponibilidad($query)
             {
                 if($query){ 
                     return $query->join('personas', 'personas.id', '=', 'control_ip.idPersona')
                     ->join('areas', 'areas.id', '=', 'control_ip.idArea')
                     ->join('jefatura', 'jefatura.id', '=', 'control_ip.idJefatura','left outer')
                     ->orderby('control_ip.ip', 'desc')
                     ->orderby('control_ip.fechaActualizacion', 'desc')
                     //->groupBy('control_ip.ip')
                     ->select(DB::raw('CONCAT(personas.Nombre," ", personas.ApellidoP," ", personas.ApellidoM) AS NombreP'),'areas.Nombre as NombreA','jefatura.Nombre as NombreJ',  'control_ip.*');
                    
                 }
 
             }
             
             //Listado de historial control de direcciones ip  
             public function scopeControlHistorial($query,$persona)
             {
                 if($persona){ 
                     return $query->join('personas', 'personas.id', '=', 'control_ip.idPersona')
                     ->join('areas', 'areas.id', '=', 'control_ip.idArea')
                     ->join('jefatura', 'jefatura.id', '=', 'control_ip.idJefatura','left outer')
                     ->orderby('control_ip.ip', 'desc')
                     ->orderby('control_ip.fechaActualizacion', 'desc')
                     ->where('control_ip.idPersona','=', $persona)
                     ->select(DB::raw('CONCAT(personas.Nombre," ", personas.ApellidoP," ", personas.ApellidoM) AS NombreP'),'areas.Nombre as NombreA','jefatura.Nombre as NombreJ',  'control_ip.*');
                    
                 }
 
             }
}
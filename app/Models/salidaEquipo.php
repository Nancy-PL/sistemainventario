<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class salidaEquipo extends Model
{
    use HasFactory;
    use LogsActivity;
    protected $table = 'salida_equipos';
    public $timestamps = false;
    protected $guarded = array(); 
 
    protected $fillable = [
        'referencia',
        'fecha_notificacion',
        'fecha_vencimiento',
        'fecha_Entrega',
        'Activo',        
    ];
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['referencia', 'fecha_notificacion', 'fecha_vencimiento','fecha_Entrega','Activo']);
    }
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == "created") {
            return "Asignacion de una salida de equipo";
        } else if ($eventName == "updated") {
            return "Renovacion de una salida de equipo";
        } else if ($eventName == "deleted") {
            return "Baja de una salida de equipo";
        }else{
            return "{$eventName}";
        }
    }
    //relación uno a muchos inversa
    public function Detallesalida()
    {
        return $this->hasMany('App/Models/Detallesalida', 'salida_equipos_id');
    }
    
    //busqueda scope de proximas salidas a vencer
    public function scopeBuscarporSalidasAVencer($query)
    {
        return $query
            ->whereBetween('fecha_vencimiento', [Carbon::now()->subDays(10), Carbon::now()->addDays(3)])
            ->where('Activo', '=', '1')
            ->select('salida_equipos.*');
    }

        
    //busqueda scope de proximas salidas a vencer
    public function scopeBuscarporSalidas($query)
    {
        return $query
            ->where('Activo', '=', '1')
            ->select('salida_equipos.*');
    }

    public function scopeDetalle($query, $id)
    {
        if ($id) {
            return $query
            ->where('Activo', '=', '1')
            ->where('salida_equipos.id', '=', $id) 
            ->select('salida_equipos.*');
        }
    }
}

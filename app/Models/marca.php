<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class marca extends Model
{
    use HasFactory;
    use LogsActivity;

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['nombre', 'created_at', 'updated_at']);
        // Chain fluent methods for configuration options
    }
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == "created") {
            return "Creación de una marca ";
        } else if ($eventName == "updated") {
            return "Actualización de  marca";
        } else if ($eventName == "deleted") {
            return "Baja de  marca";
        }else{
            return "{$eventName}";
        }
    }


    //relación uno a muchos 
    public function Bienes()
    {
        return $this->hasMany('App/Models/Bienes','marca_id');
    }
}

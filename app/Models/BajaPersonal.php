<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BajaPersonal extends Model
{
    protected $table = 'bajapersona';
    public $timestamps = false;
 
    use HasFactory;

    public function Persona(){
        return $this->belongsTo('App/Models/Persona');
    }
}

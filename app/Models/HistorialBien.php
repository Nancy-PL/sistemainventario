<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistorialBien extends Model
{
    protected $table = 'historial_biens';
    public $timestamps = false;

    use HasFactory;
    //relacion uno a muchos inversa
    public function Bienes()
    {
        return $this->belongsTo('App/Models/Bienes');
    }
   
}

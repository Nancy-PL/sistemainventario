<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Bienes extends Model
{
    use LogsActivity;
    use HasFactory;
    
    protected $table = 'bienes';
    protected $guarded = array();
    
    protected $fillable = [
        'Modelo',
        'Estado',
        'Caracteristicas',
        'Observaciones',
        'Status',
        'ClaveBien',
        'Departamento',
        'Clasificacion_id',
        'areas_id',
        'marca_id',
        'created_at',
        'Activo',

    ];
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly([ 'Modelo','Estado','Caracteristicas', 'Observaciones','Status','ClaveBien','Departamento','Clasificacion_id','areas_id', 'marca_id','created_at', 'Activo']);
        // Chain fluent methods for configuration options
    }
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == "created") {
            return "Creación de un nuevo bien";
        } else if ($eventName == "updated") {
            return "Modificación de un bien";
        } else if ($eventName == "deleted") {
            return "Baja de un bien";
        }else{
            return "{$eventName}";
        }
    }
    //relación uno a uno
    public function bienesAdministrativos()
    {
        return $this->hasOne('App/Models/BienesAdministrativos', 'bien_id');
    }
    //relación uno a uno

    public function baja_bienes()
    {
        return $this->hasOne('App/Models/baja_bienes', 'bienes_id');
    }
    //relación uno a muchos
    public function DetalleSalida()
    {
        return $this->hasMany('App/Models/DetalleSalida', 'bien_id');
    }
    //relacion uno a muchos
    public function resguardo_detail()
    {
        return $this->hasMany('App/Models/resguardo_detail');
    }
    //relacion uno a muchos
    public function HistorialBien()
    {
        return $this->hasMany('App/Models/HistorialBien', 'bien_id');
    }
    //relación uno a muchos inversa
    public function marca()
    {
        return $this->belongsTo('App/Models/marca');
    }

    //relación uno a muchos inversa
    public function clasificacion()
    {
        return $this->belongsTo('App/Models/clasificacion');
    }
    //Bienes mueble
    //Bienes del departamento UA
    public function scopeBienes($query, $dep)
    {
        if($dep){
            return $query->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id','left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id', 'left outer')
            ->where('bienes.Departamento', 'like',  '%'.$dep.'%')
            ->where('bienes.Activo', '=', '1')
            ->select('historial__inventarios.*','marcas.nombre',  'clasificacions.*', 'clasificacions.concepto', 'users.name','historial_biens.usuario_alta','detalle_biens.*', 'bienes.*');
        }

    }
    //Bien Para editar
     //Bienes del departamento UA
     public function scopeEditbien($query, $id)
     {
         if($id){
             return $query->where('bienes.id', '=', $id)
             ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id','left outer')
             ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
             ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
             ->select('detalle_biens.*','historial__inventarios.*', 'mejoras_equipos.MejorasEquipo','bienes.*');
         }

     }
    //Bienes dados de baja del  departamento UA
    //consulta para la lista de bienes dados de baja
   
      public function scopeListaBaja($query,$Depa)
    {
        if($Depa){
            return $query->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->leftjoin('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id')
            ->leftjoin('bajabienes', 'bajabienes.bienes_id', '=', 'bienes.id','left outer')
            ->leftjoin('users', 'historial_biens.usuario_alta', '=', 'users.id','left outer')
            ->leftjoin('users as userbaja', 'historial_biens.usuario_baja', '=', 'userbaja.id','left outer')
            ->leftjoin('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id','left outer')
            ->leftjoin('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
            ->leftjoin('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
            ->where('bienes.Departamento', 'like',  '%'.$Depa.'%')
            ->where('bienes.Activo', '=', '0')
            ->select('detalle_biens.*','historial__inventarios.*', 'mejoras_equipos.MejorasEquipo', 'clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto', 'bienes.*', 'users.name','userbaja.name as Userbaja','historial_biens.*', 'bajabienes.*', 'bajabienes.Observaciones as ObvervacionesBaja','marcas.nombre')
            ;
        }


    }
 
    //Bienes mueble
    //Bienes del departamento UA
    public function scopeBajaBienes($query,$Dep)
    {
        if($Dep){
            return $query->leftjoin('resguardo_detail', 'resguardo_detail.bien_id', '=', 'bienes.id')
            ->leftjoin('resguardos', 'resguardo_detail.resguardo_id', '=', 'resguardos.id','left outer')
            ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id','left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
            ->where('resguardos.tipo', '!=', 'TEMPORAL')
            ->where('bienes.Departamento', 'like',  '%'.$Dep.'%')
            ->where('bienes.Activo', '=', '1')
            ->where('resguardo_detail.Activo', '=', '1')

            ->select('mejoras_equipos.*','detalle_biens.*','historial__inventarios.*','personas.*', 'resguardo_detail.*', 'marcas.*', 'clasificacions.*', 'bienes.*');
        }

    }

    //BIENES UTIC

    //Consulta bienes para baja
    public function  scopeBienesUticActivos($query)
    {
        return $query
            ->leftjoin('resguardo_detail', 'resguardo_detail.bien_id', '=', 'bienes.id')
            ->leftjoin('resguardos', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
            ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id','left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
            ->where('bienes.Departamento', 'like',  '%SI%')
            ->where('bienes.Activo', '=', '1')
            ->select('mejoras_equipos.*','detalle_biens.*','historial__inventarios.*','personas.*', 'resguardo_detail.*', 'marcas.*', 'clasificacions.*', 'bienes.*');
    }

    //Bienes del departamento UA de cierto usuario que se insertaron este dia
    public function scopeBienesHoy($query)
    {


        return $query
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id')
            ->join('resguardo_detail', 'resguardo_detail.bien_id', '=', 'bienes.id')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id','left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
            ->select('mejoras_equipos.*','detalle_biens.*','historial__inventarios.*','marcas.nombre', 'clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto', 'bienes.*', 'users.id as persona', 'resguardo_detail.id as idR')
            ->where('bienes.Departamento', 'like',  '%UA%')
            ->whereDate('bienes.created_at', Carbon::today());
    }

    //Bienes del departamento UA de cierto usuario que se insertaron este dia
    public function scopeBienesHoyUtic($query)
    {


        return $query
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id')
            ->join('resguardo_detail', 'resguardo_detail.bien_id', '=', 'bienes.id')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id','left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
            ->select('mejoras_equipos.*','detalle_biens.*','historial__inventarios.*','marcas.nombre', 'clasificacions.concepto', 'bienes.*', 'users.id as persona', 'resguardo_detail.id as idR')
            ->where('bienes.Departamento', 'like',  '%SI%')
            ->whereDate('bienes.created_at', Carbon::today());
    }
    //mostrando detalles de cierto bien
    public function scopeDetalleBien($query, $id)
    {

        if ($id) {
            return $query
                ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id')
                ->join('users', 'historial_biens.usuario_alta', '=', 'users.id','left outer')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id','left outer')
                ->where('bienes.id', '=', $id)
                ->select('detalle_biens.*','historial__inventarios.*','marcas.nombre', 'clasificacions.concepto', 'users.name','mejoras_equipos.*','historial_biens.usuario_alta', 'bienes.*');
        }
    }
    //busqueda scope del bien mueble
    public function scopeBienDisponible($query,$Depa)
    {
        if($Depa){
            return $query
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id', 'left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id','left outer')
           // ->orwhere('resguardo_detail.bien_id')
            //->where('bienes.Status', '=', '0')
            ->where('bienes.Departamento', 'like',  '%'.$Depa.'%')
            ->where('bienes.Status', '=', '0')
            ->select('detalle_biens.*','historial__inventarios.*','mejoras_equipos.*','marcas.nombre', 'clasificacions.clave', 'clasificacions.concepto', 'bienes.*', 'users.name');
        }

    }

       //busqueda scope del bien mueble
       public function scopeBienDisponibleUA($query)
       {

               return $query
               ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
               ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
               ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
               ->join('users', 'historial_biens.usuario_alta', '=', 'users.id', 'left outer')
               ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
               ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
               ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id','left outer')
               ->join('resguardo_detail', 'resguardo_detail.bien_id', '=', 'bienes.id','left outer')

               ->orwhere('resguardo_detail.bien_id')
               //->where('bienes.Status', '=', '0')
               ->where('bienes.Departamento', 'like',  '%UA%')
               //->where('bienes.Status', '=', '0')
               ->select('detalle_biens.*','historial__inventarios.*','mejoras_equipos.*','marcas.nombre', 'clasificacions.clave', 'clasificacions.concepto', 'bienes.*', 'users.name');

       }
    //Bienes de Computo

    //Este scope  es para seleccionar solo los bienes que tienen no de inventario
    //es llamado por NoInventarioExport.php
    public function scopeNoInventario($query)
    {


        return $query->where('ClaveBien', 'like', '%OSFE%')
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id')
            ->select('historial_biens.fecha_baja', 'marcas.nombre', 'clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto', 'bienes.*', 'users.name');
    }

    //Historial de bienes UA
    public function scopeBienesHistorial($query, $Depa)
    {
        if($Depa){
            return $query
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id', 'left outer')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
            ->join('resguardo_detail', 'resguardo_detail.bien_id', '=', 'bienes.id', 'left outer')
            ->join('resguardos', 'resguardos.id', '=', 'resguardo_detail.resguardo_id', 'left outer')
            ->join('personas', 'personas.id', '=', 'resguardos.persona_id','left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id','left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
            ->join('users as usuarioBaja', 'historial_biens.usuario_baja', '=', 'usuarioBaja.id', 'left outer')
            ->where('bienes.Departamento', 'like',  '%'.$Depa.'%')
            ->where('bienes.Activo', '=', "1")
            ->select( 'clasificacions.concepto','detalle_biens.NoSerie','historial__inventarios.NoInventarioAnterior', 'users.name', 'usuarioBaja.name as userBaja', 'historial_biens.*', 'personas.*','resguardo_detail.fechaAsignacion','resguardo_detail.fechaCancelacion','resguardo_detail.Activo as ActivoR', 'bienes.*')->orderBy('fechaAsignacion','DESC');

        }

    }
    //Historial de bienes UA
    public function scopeBienesHistorialUTIC($query, $NoSerie,$depto)
    {

        if($NoSerie){
            return $query

            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id', 'left outer')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
            ->join('resguardo_detail', 'resguardo_detail.bien_id', '=', 'bienes.id', 'left outer')
            ->join('resguardos', 'resguardos.id', '=', 'resguardo_detail.resguardo_id', 'left outer')
            ->join('personas', 'personas.id', '=', 'resguardos.persona_id','left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id','left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
            ->join('users as usuarioBaja', 'historial_biens.usuario_baja', '=', 'usuarioBaja.id', 'left outer')
            ->orwhere('historial__inventarios.NoInventarioAnterior', 'like', '%'.$NoSerie.'%')
            ->orwhere('bienes.ClaveBien', 'like', '%'.$NoSerie.'%')
            ->orwhere('detalle_biens.NoSerie', 'like', '%'.$NoSerie.'%')

            ->select( 'clasificacions.concepto','detalle_biens.NoSerie','historial__inventarios.NoInventarioAnterior', 'users.name', 'usuarioBaja.name as userBaja', 'historial_biens.*', 'personas.*','resguardo_detail.fechaAsignacion','resguardo_detail.fechaCancelacion','resguardo_detail.Activo as ActivoR', 'bienes.*','bienes.Activo  as ActivoB')->orderBy('fechaAsignacion','DESC');

        }

    }
/******************************************************************************* */
     //Historial de bienes SI
     public function scopeBienesHistorialSI($query)
     {
             return $query
             ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
             ->leftjoin('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id')
             ->leftjoin('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id')
             ->leftjoin('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id')
             ->leftjoin('users', 'users.id', '=', 'historial_biens.usuario_alta','left outer')
             ->leftjoin('users as usuarioBaja', 'users.id', '=', 'historial_biens.usuario_baja','left outer')

             ->where('bienes.Departamento', 'like',  '%SI%')
             ->select( 'clasificacions.concepto','detalle_biens.*','historial__inventarios.NoInventarioAnterior','historial_biens.*','usuarioBaja.name as userBaja','users.name','bienes.*');


     }
    //Historial de bienes UA De los ultimos 3 dias
    public function scopeBienesHistorialHoy($query)
    {

        return $query
            ->whereBetween('fecha_alta', [Carbon::now()->subDays(3), Carbon::now()])
            ->orwhereBetween('fecha_baja', [Carbon::now()->subDays(3), Carbon::now()])
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id')
            ->join('users as usuarioBaja', 'historial_biens.usuario_baja', '=', 'usuarioBaja.id', 'left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id','left outer')
            ->where('bienes.Departamento', 'like',  '%UA%')
            ->select('clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto', 'detalle_biens.*','historial__inventarios.*','mejoras_equipos.*', 'bienes.*', 'users.name', 'usuarioBaja.name as userBaja', 'historial_biens.*');
    }
    //Historial de bienes SI
    public function scopeBienesHistorialSIHoy($query)
    {

        return $query
            ->whereBetween('fecha_alta', [Carbon::now()->subDays(3), Carbon::now()])
            ->orwhereBetween('fecha_baja', [Carbon::now()->subDays(3), Carbon::now()])
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id')
            ->join('users as usuarioBaja', 'historial_biens.usuario_baja', '=', 'usuarioBaja.id', 'left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id','left outer')
            ->where('bienes.Departamento', 'like',  '%SI%')
            ->select('clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto', 'detalle_biens.*','historial__inventarios.*','mejoras_equipos.*', 'bienes.*', 'users.name', 'usuarioBaja.name as userBaja', 'historial_biens.*');
    }
}

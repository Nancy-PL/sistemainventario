<?php

namespace App\Models;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    public $guarded = [];

    
    //relacion uno a muchos
    public function User(){
        return $this->hasMany('App\Models\User');
    }
}

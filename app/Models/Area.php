<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Spatie\Permission\Traits\HasRoles;

class Area extends Model
{
    use HasFactory;
    use LogsActivity;


    protected $fillable = [
        'Nombre',
        'Responsable',
        'Cargo',
        'letra',
    ];
    ///funcion para obtener las actividades 
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['Nombre', 'Responsable', 'Cargo','letra']);
        // Chain fluent methods for configuration options
    }
    //función para la descripción de los eventos que se hacen 
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == "created") {
            return "Creación de una nueva área";
        } else if ($eventName == "updated") {
            return "Actualización de área";
        } else if ($eventName == "deleted") {
            return "Baja de un área";
        }else{
            return "{$eventName}";
        }
    }

    //relacion uno a muchos
    public function Persona()
    {
        return $this->hasMany('App\Models\Persona', 'area_id');
    }
    //Consultar areas para salida de equipos
    public function scopeBuscarAreaReporteSalida($query, $id)
    {
        if ($id) {
            return $query
                ->where('areas.id', '=', $id)
                ->orwhere('areas.Cargo', '=', 'JEFE DE DEPARTAMENTO DE RECURSOS MATERIALES')
                ->orwhere('areas.Cargo', '=', 'TITULAR DE LA UNIDAD DE TECNOLOGIAS DE LA INFORMACION Y COMUNICACIONES')
                ->orwhere('areas.Cargo', '=', 'TITULAR DE LA UNIDAD DE ADMINISTRACIÓN')
                ->select('areas.*');
        }
    }
    //CONSULTAR TITULARES DE UTIC PARA AUTORIZAR EL REPORTE DE RESGURDO
    public function scopeReporteResguardoUTIC($query)
    {
        return $query
            ->where('letra', '=', 'E')
            ->select('Responsable', 'id');
    }
    //CONSULTAR EL RESPONSABLE DE ALMACEN
    public function scopeAlmacen($query)
    {
        return $query
            ->where('Nombre', '=', 'ALMACEN')
            ->select('areas.Responsable', 'areas.id');
    }
    //Consultar areas para el reporte de resguardo bien mueble
    public function scopeReporteResguardo($query)
    {
        return $query
            ->where('areas.Cargo', '=', 'JEFE DE DEPARTAMENTO DE RECURSOS MATERIALES')
            ->orwhere('areas.Cargo', '=', 'TITULAR DE LA UNIDAD DE ADMINISTRACIÓN')
            ->select('areas.*');
    }
    //Consultar persona de cargos
    public function scopeBuscarCargo($query, $nombres)
    {
        if ($nombres) {
            return  $query->where('Responsable', 'like', '%'.$nombres.'%')->select('areas.Cargo');

        }
    }
}

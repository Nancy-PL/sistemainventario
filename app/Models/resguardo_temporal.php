<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class resguardo_temporal extends Model
{

    protected $table = 'resguardos_temporal';
    public $timestamps = false;
    protected $guarded = array();

    use HasFactory;
    use LogsActivity;


    protected $fillable = [
        'folio',
        'fecha_Orden',
        'No_Orden',
        'fecha_inicio',
        'fecha_vencimiento',
        'fecha_devolucion',
        'Activo',
        'resguardo_id',
    ];
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['folio', 'fecha_Orden', 'No_Orden','fecha_inicio','fecha_vencimiento','fecha_devolucion','Activo','resguardo_id']);
    }
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == "created") {
            return "Asignacion de un nuevo resguardo temporal";
        } else if ($eventName == "updated") {
            return "Actualizacion de un resguardo temporal";
        } else if ($eventName == "deleted") {
            return "Baja de resguardo temporal";
        }else{
            return "{$eventName}";
        }
    }
    //relación uno a muchos inversa
    public function Resguardo()
    {
        return $this->belongsTo('App/models/Resguardo');
    }

    //Resguardos  disponibles para resguardo temporal
    public function scopeResguardoTemporal($query)
    {
        return $query
            ->leftjoin('resguardos', 'resguardos.id', '=', 'resguardos_temporal.resguardo_id')
            ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->where('resguardos.SI', '=', '1')
            ->where('resguardos.tipo', 'like',  '%TEMPORAL%')
            ->select('personas.*', 'resguardos_temporal.Activo', 'resguardos_temporal.fecha_vencimiento');
    }
    //Resguardos  temporales de los ultimos 3 dias para vencer
    public function scopeResguardoTemporalVencimiento($query)
    {
        return $query
            ->whereBetween('fecha_vencimiento', [Carbon::now()->subDays(10), Carbon::now()->addDays(3)])
            ->leftjoin('resguardos', 'resguardos.id', '=', 'resguardos_temporal.resguardo_id')
            ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->where('resguardos.tipo', '=', 'TEMPORAL')
            ->where('resguardos.SI', '=', '1')
            ->where('resguardos.Activo', '=', '1')
            ->where('resguardos_temporal.Activo', '=', '1')
            ->select('personas.*', 'resguardos_temporal.fecha_vencimiento');
    }

 
}

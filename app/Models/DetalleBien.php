<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleBien extends Model
{
    protected $table = 'detalle_biens';
    protected $fillable = [
        'NoSerie',
        'factura',
        'ubicacion',
        'bienes_id',
    ];
    use HasFactory;
}

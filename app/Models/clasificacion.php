<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Illuminate\Support\Facades\DB;

class clasificacion extends Model
{
    use HasFactory;
    use LogsActivity;

    protected $fillable = [
        'clave',
        'concepto',
        'NoInventario',
        'activo',
        'Departamento',
    ];


    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['clave', 'concepto', 'NoInventario', 'activo', 'Departamento']);
        // Chain fluent methods for configuration options
    }
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == "created") {
            return "Creación de una  clasificación";
        } else if ($eventName == "updated") {
            return "Actualización de  clasificación";
        } else if ($eventName == "deleted") {
            return "Baja de clasificación";
        }else{
            return "{$eventName}";
        }
    }


     //relación uno a muchos
     public function bienes()
     {
         return $this->hasMany('App/models/bienes','Clasificacion_id');
     }
}

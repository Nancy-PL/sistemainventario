<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detallesalida extends Model
{
    use HasFactory;
    //relación uno a muchos
    public function Bienes()
    {
        return $this->belongsTo('App/Models/Bienes');
    }
    //relación uno a muchos inversa
    public function salidaEquipo()
    {
        return $this->belongsTo('App/Models/salidaEquipo');
    }
}

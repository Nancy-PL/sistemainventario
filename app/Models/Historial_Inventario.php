<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historial_Inventario extends Model
{
    protected $table = 'historial__inventarios';

    use HasFactory;
}

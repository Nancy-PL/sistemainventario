<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Traits\HasRoles;

class Resguardo extends Model
{
    use HasFactory;
    use LogsActivity;

    protected $table = 'resguardos';
    protected $guarded = array();

    protected $fillable = [
        'tipo',
        'persona_id',
        'activo',
        'UA',
        'SI',

    ];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['tipo', 'UA','SI', 'persona_id', 'activo', 'update_at']);
        // Chain fluent methods for configuration options
    }
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == "created") {
            return "Creación de un nuevo resguardo";
        } else if ($eventName == "updated") {
            return "Actualización de un resguardo";
        } else if ($eventName == "deleted") {
            return "Baja de un resguardo";
        } else {
            return "{$eventName}";
        }
    }
    public function Persona()
    {
        return $this->belongsTo('App/Models/Persona');
    }
    //relación uno a muchos
    public function resguardo_detail()
    {
        return $this->hasMany('App/Models/resguardo_detail', 'resguardo_id');
    }
    //relación uno a uno

    public function resguardo_temporal()
    {
        return $this->hasOne('App/Models/resguardo_temporal');
    }
    //Todos los resguardos
    public function scopeTodoslosResguardos($query)
    {
        return $query->where('resguardos.tipo', 'not like', '%TEMPORAL%')
            ->join('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->join('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
            ->join('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('areas', 'areas.id', '=', 'personas.area_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id', 'left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->where('bienes.Activo', '=', '1')
            ->where('bienes.Status', '=', '1')
            ->where('resguardo_detail.Activo', '=', '1')
            ->select(DB::raw('CONCAT(personas.Profesion," ",personas.Nombre," ",personas.ApellidoP ," ",personas.ApellidoM) as full_name'), 'historial_biens.fecha_alta as fechita', 'areas.Nombre as NombreA', 'resguardo_detail.*', 'resguardo_detail.fechaAsignacion as fechaR', 'resguardo_detail.User', 'marcas.nombre', 'clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto', 'detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*','bienes.*', 'users.name', 'historial_biens.*');
    }

    //Este scope  es para seleccionar solo los bienes que tienen no de control
    //es llamado por NoControlResguardoExport.php
    public function scopeNoControl($query)
    {

        

        return $query->where('bienes.ClaveBien', 'not like', '%OSFE%')
            ->join('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->join('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id', 'left outer')
            ->join('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('areas', 'areas.id', '=', 'personas.area_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id', 'left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->where('bienes.Activo', '=', '1')
            ->where('bienes.Status', '=', '1')
            ->where('resguardo_detail.Activo', '=', '1')
            ->select(DB::raw('CONCAT(personas.Profesion," ",personas.Nombre," ",personas.ApellidoP ," ",personas.ApellidoM) as full_name'), 'historial_biens.fecha_alta as fechita', 'areas.Nombre as NombreA', 'resguardo_detail.*', 'resguardo_detail.fechaAsignacion as fechaR', 'resguardo_detail.User', 'marcas.nombre', 'clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto','detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'users.name', 'historial_biens.*');
    }

    //Este scope  es para seleccionar los bienes dados de baja de admin
    //es llamado por BajaMueblesExport.php
    public function scopeBajaComputo($query)
    {


        return $query
            ->join('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->join('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id', 'left outer')
            ->join('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id', 'left outer')
            ->join('areas', 'areas.id', '=', 'personas.area_id')
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
            ->join('bajabienes', 'bajabienes.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id', 'left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->select(DB::raw('CONCAT(personas.Profesion," ",personas.Nombre," ",personas.ApellidoP ," ",personas.ApellidoM) as full_name'), 'historial_biens.fecha_alta as fechita', 'areas.Nombre as NombreA', 'resguardo_detail.*',  'marcas.nombre', 'clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto','detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*',  'bienes.*', 'users.name', 'historial_biens.*', 'bajabienes.*', 'bajabienes.FechaBaja as fechabaja', 'bajabienes.Observaciones as obser', 'bajabienes.Motivo_baja as motivos', 'resguardo_detail.User')
            ->where('bienes.Departamento', 'like',  '%SI%')
            ->where('bienes.Activo', '=', '0');
    }
    //es llamado por BajaMueblesExport.php
    public function scopeBajaMuebles($query)
    {


        return $query
            ->join('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->join('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id', 'left outer')
            ->join('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id', 'left outer')
            ->join('areas', 'areas.id', '=', 'personas.area_id')
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
            ->join('bajabienes', 'bajabienes.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id', 'left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->select(DB::raw('CONCAT(personas.Profesion," ",personas.Nombre," ",personas.ApellidoP ," ",personas.ApellidoM) as full_name'),  'historial_biens.fecha_alta as fechita', 'areas.Nombre as NombreA', 'resguardo_detail.*',  'marcas.nombre', 'clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto','detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'users.name', 'historial_biens.*', 'bajabienes.*', 'bajabienes.Observaciones as obser', 'bajabienes.Motivo_baja as motivos', 'resguardo_detail.User', 'historial_biens.fecha_baja as baja')
            ->where('bienes.Departamento', 'like',  '%UA%')

            ->where('resguardo_detail.Activo', '=', '0')
            ->where('bienes.Activo', '=', '0');
    }

    //Este scope  es para seleccionar solo los bienes que tienen no de inventario
    //es llamado por NoInventarioExport.php
    public function scopeNoInventario($query)
    {


        return $query->where('ClaveBien', 'like', '%OSFE%')
            ->join('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->join('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id', 'left outer')
            ->join('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id', 'left outer')
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('areas', 'areas.id', '=', 'personas.area_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id', 'left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->where('bienes.Activo', '=', '1')
            ->where('bienes.Status', '=', '1')
            ->where('resguardo_detail.Activo', '=', '1')
            ->select(DB::raw('CONCAT(personas.Profesion," ",personas.Nombre," ",personas.ApellidoP ," ",personas.ApellidoM) as full_name'), 'historial_biens.fecha_alta as fechita', 'areas.Nombre as NombreA', 'resguardo_detail.*', 'resguardo_detail.fechaAsignacion as fechaR', 'resguardo_detail.User',  'marcas.nombre', 'clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto', 'detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*','bienes.*', 'users.name', 'historial_biens.*');
    }


    //Este scope  es para seleccionar solo los bienes UA que tienen no de inventario y su resguardante
    //es llamado por MueblesGeneralExport.php
    public function scopeMueblesGeneral($query)
    {
        return $query->where('ClaveBien', 'like', '%OSFE%')
            ->join('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->join('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id', 'left outer')
            ->join('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id', 'left outer')
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('areas', 'areas.id', '=', 'personas.area_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id', 'left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->where('bienes.Activo', '=', '1')
            ->where('bienes.Status', '=', '1')
            ->where('bienes.Departamento', 'like',  '%UA%')
            ->where('resguardo_detail.Activo', '=', '1')
            ->orderby('personas.id','asc')
            ->select(DB::raw('CONCAT(personas.Profesion," ",personas.Nombre," ",personas.ApellidoP ," ",personas.ApellidoM) as full_name'), 'historial_biens.fecha_alta as fechita', 'areas.Nombre as NombreA', 'resguardo_detail.*', 'resguardo_detail.fechaAsignacion as fechaR', 'resguardo_detail.User',  'marcas.nombre', 'clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto','detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'users.name', 'historial_biens.*');
    }


    //Este scope  es para seleccionar solo los bienes UA que tienen no de inventario y su resguardante
    //es llamado por ComputoGeneralExport.php
    public function scopeComputoGeneral($query)
    {


        return $query->where('ClaveBien', 'like', '%OSFE%')
            ->join('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->join('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id', 'left outer')
            ->join('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id', 'left outer')
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('areas', 'areas.id', '=', 'personas.area_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id', 'left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->where('bienes.Activo', '=', '1')
            ->where('bienes.Status', '=', '1')
            ->where('bienes.Departamento', 'like',  '%SI%')
            ->where('resguardo_detail.Activo', '=', '1')
            ->orderby('personas.id','asc')
            ->select(DB::raw('CONCAT(personas.Profesion," ",personas.Nombre," ",personas.ApellidoP ," ",personas.ApellidoM) as full_name'), 'historial_biens.fecha_alta as fechita', 'areas.Nombre as NombreA', 'resguardo_detail.*', 'resguardo_detail.fechaAsignacion as fechaR', 'resguardo_detail.User',  'marcas.nombre', 'clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto','detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'users.name', 'historial_biens.*');
    }



    //RESGUARDOS POR ÁREA
    public function scopeAreaResguardo($query)
    {
        return $query->where('resguardos.tipo', 'not like', '%TEMPORAL%')
            ->join('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->join('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
            ->join('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('areas', 'areas.id', '=', 'personas.area_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id', 'left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->where('bienes.Activo', '=', '1')
            ->where('bienes.Status', '=', '1')
            ->where('bienes.Departamento', 'like',  '%UA%')
            ->where('resguardo_detail.Activo', '=', '1')
            ->select(DB::raw('CONCAT(personas.Profesion," ",personas.Nombre," ",personas.ApellidoP ," ",personas.ApellidoM) as full_name'), 'historial_biens.fecha_alta as fechita', 'areas.Nombre as NombreA', 'resguardo_detail.*', 'resguardo_detail.fechaAsignacion as fechaR', 'resguardo_detail.User', 'marcas.nombre', 'clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto','detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'users.name', 'historial_biens.*')
            ->orderby('areas.Nombre','asc');
    }
    //bienes muebles
    //Todos los resguardos activos del bien mueble
    public function scopeResguardoActivo($query)
    {
        return $query
            ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
            ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
            ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->where('resguardos.tipo', 'like',  '%PERMANENTE%')
            ->where('bienes.Departamento', 'like',  '%UA%')
            ->where('bienes.Activo', '=', '1')
            ->where('bienes.Status', '=', '1')
            ->where('resguardo_detail.Activo', '=', '1')
            ->select('personas.*', 'resguardo_detail.*', 'bienes.*', 'marcas.*', 'clasificacions.*');
    }


    //busqueda scope del resguardo de bien Mueble
    public function scopeBuscarpor($query, $persona, $TipoResguardo, $Departamento)
    {

        if ($TipoResguardo) {
            return $query->where('personas.id', '=', $persona)
                ->where('resguardos.tipo', 'like',  '%' . $TipoResguardo . '%')
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->leftjoin('areas', 'areas.id', '=', 'personas.area_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('bienes.Departamento', 'like',  '%' . $Departamento . '%')
                ->where('bienes.Activo', '=', '1')
                ->where('resguardo_detail.Activo', '=', '1')
                ->select('areas.Nombre as area', 'personas.*', 'resguardo_detail.*', 'detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'marcas.*', 'clasificacions.*',  'resguardos.*');
        }
    }
    //busqueda scope del resguardo de bien Mueble de una persona
    public function scopeBuscarporPers($query, $persona)
    {

        if ($persona) {
            return $query->where('personas.id', '=', $persona)->where('ClaveBien', 'like', '%OSFE%')
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->leftjoin('areas', 'areas.id', '=', 'personas.area_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('bienes.Departamento', 'like',  '%UA%')
                ->where('bienes.Activo', '=', '1')
                ->where('resguardo_detail.Activo', '=', '1')
                ->select('areas.Nombre as area', 'personas.*', 'resguardo_detail.*', 'detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'marcas.*', 'clasificacions.*',  'resguardos.*')->orderby('clasificacions.concepto','asc');
        }
    }
    //Este scope  es para seleccionar solo los bienes que tienen no de inventario
    //es llamado por NoInventarioExport.php
    public function scopeNoInventarioComputo($query)
    {


        return $query->where('ClaveBien', 'like', '%OSFE%')
            ->join('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->join('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id', 'left outer')
            ->join('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id', 'left outer')
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('areas', 'areas.id', '=', 'personas.area_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id',  'left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->where('bienes.Activo', '=', '1')
            ->where('bienes.Status', '=', '1')
            ->where('resguardo_detail.Activo', '=', '1')
            ->where('bienes.Departamento', 'like',  '%SI%')
            ->select(DB::raw('CONCAT(personas.Profesion," ",personas.Nombre," ",personas.ApellidoP ," ",personas.ApellidoM) as full_name'), 'historial_biens.fecha_alta as fechita', 'areas.Nombre as NombreA', 'resguardo_detail.*', 'resguardo_detail.fechaAsignacion as fechaR', 'resguardo_detail.User',  'marcas.nombre', 'clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto','detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'users.name', 'historial_biens.*');
    }

    //Este scope  es para seleccionar solo los bienes que tienen no de control
    //es llamado por NoControlResguardoExport.php
    public function scopeControlComputo($query)
    {


        return $query->where('ClaveBien', 'not like', '%OSFE%')
            ->join('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->join('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id', 'left outer')
            ->join('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id', 'left outer')
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('areas', 'areas.id', '=', 'personas.area_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id', 'left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->where('bienes.Activo', '=', '1')
            ->where('bienes.Status', '=', '1')
            ->where('resguardo_detail.Activo', '=', '1')
            ->where('bienes.Departamento', 'like',  '%SI%')
            ->select(DB::raw('CONCAT(personas.Profesion," ",personas.Nombre," ",personas.ApellidoP ," ",personas.ApellidoM) as full_name'), 'historial_biens.fecha_alta as fechita', 'areas.Nombre as NombreA', 'resguardo_detail.*', 'resguardo_detail.fechaAsignacion as fechaR', 'resguardo_detail.User',  'marcas.nombre', 'clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto','detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'users.name', 'historial_biens.*');
    }



    //Listado de resguardos especificos bienes muebles
    public function scopelistaResguardo($query, $id)
    {
        if ($id) {

            return $query
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('resguardos.activo', '=', '1')
                ->where('resguardos.id', '=', $id)
                ->where('bienes.Activo', '=', '1')
                ->where('bienes.Departamento', 'like',  '%UA%')
                ->select('personas.*', 'detalle_biens.*','historial__inventarios.*','resguardo_detail.*', 'bienes.*', 'marcas.*', 'clasificacions.*',  'resguardos.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*');
        }
    }
    //listaResguardante UA
    public function scopeLista($query, $id)
    {
        if ($id) {
            return $query->where('personas.id', '=', $id)
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('resguardos.activo', '=', '1')
                ->where('bienes.Activo', '=', '1')
                ->where('bienes.Departamento', 'like',  '%UA%')
                ->select('personas.*', 'resguardo_detail.*',   'detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'marcas.*', 'clasificacions.*',  'resguardos.*');
        }
    }

    //listaResguardante Utic
    public function scopeListaUtic($query, $id)
    {
        if ($id) {
            return $query->where('personas.id', '=', $id)
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('resguardos.activo', '=', '1')
                ->where('bienes.Departamento', 'like',  '%SI%')
                ->where('resguardos.tipo', 'like',  '%PERMANENTE%')
                ->select('personas.*', 'resguardo_detail.*',  'detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'marcas.*', 'clasificacions.*',  'resguardos.*');
        }
    } //listaResguardante Utic
    public function scopeListaAlmacen($query, $id)
    {
        if ($id) {
            return $query->where('personas.id', '=', $id)
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('resguardos.activo', '=', '1')
                ->where('bienes.Activo', '=', '1')
                ->where('bienes.Departamento', 'like',  '%SI%')
                ->where('resguardos.tipo', 'like',  '%ALMACEN%')
                ->select('personas.*', 'resguardo_detail.*', 'detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'marcas.*', 'clasificacions.*',  'resguardos.*');
        }
    }

    public function scopeListaAlmacenAdmi($query, $id)
    {
        if ($id) {
            return $query->where('personas.id', '=', $id)
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('resguardos.activo', '=', '1')
                ->where('bienes.Departamento', 'like',  '%UA%')
                ->where('resguardos.tipo', 'like',  '%ALMACEN%')
                ->select('personas.*', 'resguardo_detail.*', 'detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'marcas.*', 'clasificacions.*',  'resguardos.*');
        }
    }
    //busqueda scope del resguardo bien de una personaequipo de computo y accesorios
    public function scopeBuscarpersonaUtic($query, $persona)
    {

        if ($persona) {
            return $query->where('personas.id', '=', $persona)
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->leftjoin('areas', 'areas.id', '=', 'personas.area_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('bienes.Departamento', 'like',  '%SI%')
                ->where('bienes.Activo', '=', '1')
                ->where('resguardo_detail.Activo', '=', '1')
                ->select('mejoras_equipos.*','detalle_biens.*','historial__inventarios.*','areas.Nombre as area', 'personas.*', 'resguardo_detail.*', 'bienes.*', 'marcas.*', 'clasificacions.*',  'resguardos.*')->orderby('clasificacions.concepto','asc')
                ;

        }
    }
    //Todos los resguardos activos del bien mueble
    public function scopeResguardoActivoUtic($query)
    {
        return $query
            ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
            ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
            ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            //->where('resguardos.tipo', '!=', 'TEMPORAL')
            ->where('bienes.Departamento', 'like',  '%SI%')
            ->where('resguardo_detail.Activo', '=', '1')
            ->where('bienes.Activo', '=', '1')
            ->select('personas.*', 'resguardo_detail.*', 'clasificacions.*', 'detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'marcas.*');
    }
    //Todos los resguardos activos del bien mueble
    public function scopeResguardoActivoAdmi($query)
    {
        return $query
            ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
            ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
            ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->where('resguardos.tipo', '!=', 'TEMPORAL')
            ->where('bienes.Departamento', 'like',  '%UA%')
            ->where('resguardo_detail.Activo', '=', '1')
            ->where('bienes.Activo', '=', '1')
            ->select('personas.*', 'resguardo_detail.*', 'detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'marcas.*', 'clasificacions.*');
    }
    //Listado de resguardos especificos bienes Utic
    public function scopelistaResguardoUtic($query, $id)
    {
        if ($id) {

            return $query
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('resguardos.activo', '=', '1')
                ->where('resguardos.id', '=', $id)
                ->where('bienes.Departamento', 'like',  '%SI%')
                ->select('personas.*', 'resguardo_detail.*', 'detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'marcas.*', 'clasificacions.*', 'resguardos.*');
        }
    }
    //Salidas de Bienes
    //busqueda scope para Salida de un bien equipo de computo resguardo temporal y permanente
    public function scopeBuscarporResguardoParaSalir($query, $persona)
    {
        if ($persona) {

            return $query->where('personas.id', '=', $persona)
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('bienes.Departamento', 'like',  '%SI%')
                ->where('bienes.Activo', '=', '1')
                ->where('resguardo_detail.Activo', '=', '1')
                ->select('detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*','resguardo_detail.*', 'bienes.*', 'bienes.id as bien_id', 'marcas.*', 'clasificacions.*', 'resguardos.*', 'personas.*');
        }
    }
    //busqueda scope del Salida de un bien equipo de computo y accesorios
    public function scopeBuscarporResguardoSalida($query, $persona)
    {
        if ($persona) {

            return $query->where('personas.id', '=', $persona)
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->leftjoin('detallesalidas', 'detallesalidas.bien_id', '=', 'resguardo_detail.bien_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('bienes.Departamento', 'like',  '%SI%')
                ->where('bienes.Activo', '=', '1')
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('detallesalidas.Activo', '=', '1')
                ->select('detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*','resguardo_detail.*', 'bienes.*', 'bienes.id as bien_id', 'marcas.*', 'clasificacions.*', 'resguardos.*', 'personas.*');
        }
    }
    //busqueda scope del Salida de un bien equipo de computo y accesorios de todaslas personas
    public function scopeBuscarporReferenciaSalida($query, $id)
    {
        if ($id) {
            $query2 = clone $query;


            $c1= $query
            ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
            ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
            ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->leftjoin('detallesalidas', 'detallesalidas.bien_id', '=', 'resguardo_detail.bien_id')
            ->leftjoin('salida_equipos', 'salida_equipos.id', '=', 'detallesalidas.salida_equipos_id')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->where('bienes.Departamento', 'like',  '%SI%')
           // ->where('resguardos.tipo','=','PERMANENTE')
            ->where('bienes.Activo', '=', '1')
            ->where('resguardo_detail.Activo', '=', '1')
            ->whereRaw('((select  count(*)  from resguardo_detail where bien_id= bienes.id  and activo=1))=1',)
            ->where('detallesalidas.Activo', '=', '1')
            ->where('detallesalidas.salida_equipos_id', '=', $id)
            ->orderby('personas.id')
            ->select('historial__inventarios.*','detalle_biens.*','resguardo_detail.*', 'bienes.*', 'bienes.id as bien_id', 'marcas.*', 'clasificacions.*','personas.id as idP','personas.*', 'salida_equipos.*', 'resguardos.*')->orderby('personas.id');
            $c2= $query2
            ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
            ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
            ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->leftjoin('detallesalidas', 'detallesalidas.bien_id', '=', 'resguardo_detail.bien_id')
            ->leftjoin('salida_equipos', 'salida_equipos.id', '=', 'detallesalidas.salida_equipos_id')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->where('bienes.Departamento', 'like',  '%SI%')
          //  ->where('resguardos.tipo','=','TEMPORAL')
            ->where('bienes.Activo', '=', '1')
            ->where('resguardo_detail.Activo', '=', '1')
            ->whereRaw('((select  count(*)  from resguardo_detail where bien_id= bienes.id  and activo=1))=2',)
            ->where('detallesalidas.Activo', '=', '1')
            ->where('detallesalidas.salida_equipos_id', '=', $id)
            ->orderby('personas.id')
            ->select('historial__inventarios.*','detalle_biens.*','resguardo_detail.*', 'bienes.*', 'bienes.id as bien_id', 'marcas.*', 'clasificacions.*','personas.id as idP','personas.*', 'salida_equipos.*', 'resguardos.*')->orderby('personas.id')
            ->union($c1) ->orderby('idP');
        
            return  $c2;
        }
    }

    //busqueda scope del Salida de un bien equipo de computo y accesorios de todaslas personas
    public function scopeBuscarporNombSalida($query, $nombre)
    {
        if ($nombre) {
            $query2 = clone $query;

            $c1= $query
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->leftjoin('detallesalidas', 'detallesalidas.bien_id', '=', 'resguardo_detail.bien_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->leftjoin('salida_equipos', 'salida_equipos.id', '=', 'detallesalidas.salida_equipos_id')
                ->where('salida_equipos.referencia', 'like', '%'.$nombre.'%')
                ->where('bienes.Departamento', 'like',  '%SI%')
                ->whereRaw('((select  count(*)  from resguardo_detail where bien_id= bienes.id  and activo=1))=1',)
                ->where('bienes.Activo', '=', '1')
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('detallesalidas.Activo', '=', '1')
                ->orderby('personas.id')
                ->select('detalle_biens.*','resguardo_detail.*','historial__inventarios.*','mejoras_equipos.*','bienes.*', 'bienes.id as bien_id', 'marcas.*', 'clasificacions.*', 'resguardos.*', 'salida_equipos.*', 'personas.*', 'salida_equipos.id as idSalida');
            
            $c2= $query2
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->leftjoin('detallesalidas', 'detallesalidas.bien_id', '=', 'resguardo_detail.bien_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->leftjoin('salida_equipos', 'salida_equipos.id', '=', 'detallesalidas.salida_equipos_id')
                ->where('salida_equipos.referencia', 'like', '%'.$nombre.'%')
                ->where('bienes.Departamento', 'like',  '%SI%')
                ->whereRaw('((select  count(*)  from resguardo_detail where bien_id= bienes.id  and activo=1))=2',)
                ->where('bienes.Activo', '=', '1')
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('detallesalidas.Activo', '=', '1')
                ->orderby('personas.id')
                ->select('detalle_biens.*','resguardo_detail.*','historial__inventarios.*','mejoras_equipos.*','bienes.*', 'bienes.id as bien_id', 'marcas.*', 'clasificacions.*', 'resguardos.*', 'salida_equipos.*', 'personas.*', 'salida_equipos.id as idSalida')
                ->union($c1);

       return  $c2;
       
            }
    } 
    //busqueda scope de todas las salidas
    public function scopeBuscarporReferenciaSalidas($query)
    {
        return $query
            ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
            ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
            ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->leftjoin('detallesalidas', 'detallesalidas.bien_id', '=', 'resguardo_detail.bien_id')
            ->leftjoin('salida_equipos', 'salida_equipos.id', '=', 'detallesalidas.salida_equipos_id')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->where('bienes.Departamento', 'like',  '%SI%')
            ->where('bienes.Activo', '=', '1')
            ->where('resguardo_detail.Activo', '=', '1')
            ->where('detallesalidas.Activo', '=', '1')
            ->orderby('personas.id')
            ->select('historial__inventarios.*','detalle_biens.*','resguardo_detail.*', 'bienes.*', 'bienes.id as bien_id', 'marcas.*', 'clasificacions.*','personas.*', 'salida_equipos.referencia', 'resguardos.*');
    }
    //busqueda scope de todas las salidas
    public function scopeBuscarporReferenciaSalidasId($query,$id)
    {

        if($id){
        return $query
            ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
            ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
            ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->leftjoin('detallesalidas', 'detallesalidas.bien_id', '=', 'resguardo_detail.bien_id')
            ->leftjoin('salida_equipos', 'salida_equipos.id', '=', 'detallesalidas.salida_equipos_id')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->where('bienes.Departamento', 'like',  '%SI%')
            ->where('bienes.Activo', '=', '1')
            ->where('resguardo_detail.Activo', '=', '1')
            ->where('salida_equipos.id', '=', $id)
            ->where('detallesalidas.Activo', '=', '1')
            ->select('historial__inventarios.*','detalle_biens.*','resguardo_detail.*', 'bienes.*', 'bienes.id as bien_id', 'marcas.*', 'clasificacions.*','personas.*', 'salida_equipos.referencia', 'resguardos.*');
        }
    }
    //buscar salidas de bien de una persona
    public function scopeBuscarporReferenciaSalidasBien($query, $nombre)
    {
        if ($nombre) {
            return $query
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->leftjoin('detallesalidas', 'detallesalidas.bien_id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('salida_equipos', 'salida_equipos.id', '=', 'detallesalidas.salida_equipos_id')
                ->where('bienes.Departamento', 'like',  '%SI%')
                ->where('resguardos.persona_id', '=', $nombre)
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('detallesalidas.Activo', '=', '1')
                ->select('detallesalidas.bien_id');
        }
    }
    //Resguardos temporales
    //busqueda scope del resguardo bien equipo de computo y accesorios
    public function scopeBuscarporResguardoTemp($query, $persona)
    {

        if ($persona) {

            return $query->where('personas.id', '=', $persona)
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('resguardos.tipo', '=', 'TEMPORAL')
                ->where('bienes.Departamento', 'like',  '%SI%')
                ->where('bienes.Activo', '=', '1')
                ->where('resguardo_detail.Activo', '=', '1')
                ->select('detalle_biens.*','historial__inventarios.*','mejoras_equipos.*',  'personas.*', 'resguardo_detail.*', 'bienes.*', 'bienes.id as bien_id', 'marcas.*', 'clasificacions.*', 'resguardos.*');
        }
    }
    //Imprimir todos los bienes de Equipo de Computo de un resguardo Temporal
    public function scopeBuscarTodos($query, $id)
    {
        if ($id) {
            return $query
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->leftjoin('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('resguardos.activo', '=', '1')
                ->where('resguardos.id', '=', $id)
                ->where('bienes.Departamento', 'like',  '%SI%')
                ->select('detalle_biens.*','historial__inventarios.*',  'personas.*', 'resguardo_detail.*', 'bienes.*', 'resguardos.*');
        }
    }
    //lista de resguardo temporal
    public  function scopeBuscarResguardoTemporal($query, $id)
    {
        if ($id) {
            return $query->where('resguardos.id', '=', $id)
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('resguardos.activo', '=', '1')
                ->where('bienes.Departamento', 'like',  '%SI%')
                ->where('resguardos.tipo', '=', 'TEMPORAL')
                ->select('detalle_biens.*','historial__inventarios.*','mejoras_equipos.*','personas.*', 'resguardo_detail.*', 'bienes.*', 'marcas.*', 'clasificacions.*', 'resguardos.*');
        }
    }
    //lista de resguardo temporal por persona
    public  function scopeBuscarResguardoTemporalP($query, $id)
    {
        if ($id) {
            return $query
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id','left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id','left outer')
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('resguardos.activo', '=', '1')
                ->where('bienes.Departamento', 'like',  '%SI%')
                ->where('resguardos.tipo', '=', 'TEMPORAL')
                ->where('personas.id', '=', $id)
                ->select('detalle_biens.*','historial__inventarios.*','personas.*', 'resguardo_detail.*', 'bienes.*', 'marcas.*', 'clasificacions.*', 'resguardos.*');
        }
    }

    //Resguardos  disponibles para resguardo temporal
    public function scopeBuscarResguardoTemporalDisponible($query)
    {
        return $query
            ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
            ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
            ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
            ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->where('resguardos.tipo', '!=', 'TEMPORAL')
            ->where('bienes.Departamento', 'like',  '%SI%')
            ->where('resguardo_detail.Activo', '=', '1')
            ->select('detalle_biens.*','historial__inventarios.*','mejoras_equipos.*','personas.*', 'resguardo_detail.*', 'resguardo_detail.id as detail_id', 'bienes.*', 'bienes.id as bien_id', 'marcas.*', 'clasificacions.*');
    }
    //busqueda scope del resguardo de bien Mueble y de equipo de computo de una persona
    public function scopeResguardoGralPorPers($query, $persona)
    {
        $prueba = clone $query;

        if ($persona) {
        $c=   $query->where('personas.id', '=', $persona)->where('ClaveBien', 'like', '%OSFE%')
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->leftjoin('areas', 'areas.id', '=', 'personas.area_id')
                ->where('bienes.Activo', '=', '1')
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('resguardos.tipo','=','PERMANENTE')
                ->select('areas.Nombre as area', 'personas.*', 'resguardo_detail.*', 'detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.MejorasEquipo', 'bienes.*', 'marcas.nombre', 'clasificacions.concepto',  'resguardos.*')
                ->orderby('bienes.Departamento','desc')->orderby('clasificacions.concepto','asc');
         /*      
           $c1= $prueba
              ->leftjoin('resguardos_temporal','resguardos_temporal.resguardo_id','resguardos.id')
              ->where('resguardos_temporal.Activo','=','1')
              ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
              ->where('resguardo_detail.Activo', '=', '1')

              ->leftjoin('resguardo_bien_persona', 'resguardo_bien_persona.id_bien', '=', 'resguardo_detail.bien_id')
              ->where('resguardo_bien_persona.id_persona','=',$persona)

              ->leftjoin('personas', 'personas.id', '=', 'resguardo_bien_persona.id_persona')

              ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
              ->where('ClaveBien', 'like', '%OSFE%')

              ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
              ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
              ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
              ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
              ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
              ->leftjoin('areas', 'areas.id', '=', 'personas.area_id')
              ->where('bienes.Activo', '=', '1')
              ->select('areas.Nombre as area', 'personas.*', 'resguardo_detail.*', 'detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.MejorasEquipo', 'bienes.*', 'marcas.nombre', 'clasificacions.concepto',  'resguardos.*')
              ->union($c)
                ;*/
                return   $c;
       
            }
    }

    public function scopeActivacion($query, $Departamento)
    {
        if ($Departamento) {
            return $query
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')->distinct('personas.id')
                ->where('resguardos.tipo', 'like',  '%PERMANENTE%')
//                ->where('resguardos.activo', '=', '0')
                ->where('personas.Activo', '=', '1')
                ->where('resguardos.'.$Departamento, '=',  '0')->orwhere('resguardos.'.$Departamento, '=',  'NULL')
                ->select('personas.*', 'personas.id as idP', 'resguardos.id as resguardo_id', 'resguardos.created_at as creacion', 'resguardos.updated_at as cancelacion');
        }
    }
    //consulta de los bienes anteriores al resguardo
    public function scopeActivacionR($query, $id, $Departamento)
    {
        if ($id) {
            return $query
                ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
                ->leftjoin('resguardo_detail', 'resguardo_detail.ResguardoAnterior_id', '=', 'resguardos.id')
                ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
                ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
                ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
                ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
                ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
                ->where('resguardo_detail.Activo', '=', '1')
                ->where('resguardo_detail.ResguardoAnterior_id', '=', $id)
                ->where('bienes.Departamento', 'like',  '%' . $Departamento . '%')
                ->select('personas.*', 'resguardo_detail.*', 'resguardo_detail.id as id_detail', 'detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'marcas.*', 'clasificacions.*',  'resguardos.*');
        }
    }

//Historial UTIC
 //Historial de bienes SI
 public function scopeBienesHistorialSI($query)
 {
         return $query
         ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
         ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
         ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
         ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
         ->leftjoin('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id')
         ->leftjoin('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id')
         ->leftjoin('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id')
         ->leftjoin('users', 'users.id', '=', 'historial_biens.usuario_alta','left outer')
         ->leftjoin('users as usuarioBaja', 'users.id', '=', 'historial_biens.usuario_baja','left outer')
         ->where('bienes.Departamento', 'like',  '%SI%')
         ->select( 'clasificacions.concepto','resguardo_detail.*','detalle_biens.*','historial__inventarios.NoInventarioAnterior','historial_biens.*','usuarioBaja.name as userBaja','users.name','bienes.*','bienes.Activo ActivoB');
 }

public function scopeValidarTemporal($query, $persona,$bien )
{
        return $query->where('resguardos.persona_id', '!=', $persona)
            ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
            ->leftjoin('bienes','bienes.id','resguardo_detail.bien_id')
            ->leftjoin('personas','personas.id','resguardos.persona_id')
            ->where('bienes.id', '=',  $bien)
            ->where('bienes.Departamento', 'like',  '%SI%')
            ->where('resguardos.activo', '=',  '1')
            ->where('resguardos.tipo', '=',  'TEMPORAL')
            ->select('*');
}


 //Resguardos  solo de una persona
 public function scopeBuscarResguardoTemporalDisponible1($query,$id)
 {
     return $query
         ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
         ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
         ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
         ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
         ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
         ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
         ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
         ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
         ->join('resguardos_temporal', 'resguardos_temporal.resguardo_id', '=', 'resguardos.id')
         ->where('resguardos.tipo', '=', 'TEMPORAL')
         ->where('bienes.Departamento', 'like',  '%SI%')
         ->where('resguardos_temporal.id', '=', $id)
         ->where('resguardo_detail.Activo', '=', '1')
         ->select('detalle_biens.*','historial__inventarios.*','mejoras_equipos.*','personas.*','personas.id as personaId', 'resguardo_detail.*', 'resguardo_detail.id as detail_id', 'bienes.*', 'bienes.id as bien_id', 'marcas.*', 'clasificacions.*'); }
 

  //lista de resguardo temporal
  public  function scopeBuscarResguardoTemporalPersona($query, $id)
  {
      if ($id) {
          return $query->where('resguardos_temporal.id', '=', $id)
              ->leftjoin('personas', 'personas.id', '=', 'resguardos.persona_id')
              ->join('resguardos_temporal', 'resguardos_temporal.resguardo_id', '=', 'resguardos.id')
              ->leftjoin('resguardo_detail', 'resguardo_detail.resguardo_id', '=', 'resguardos.id')
              ->leftjoin('bienes', 'bienes.id', '=', 'resguardo_detail.bien_id')
              ->leftjoin('marcas', 'marcas.id', '=', 'bienes.marca_id')
              ->leftjoin('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
              ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
              ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
              ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
              ->where('resguardo_detail.Activo', '=', '1')
              ->where('resguardos.activo', '=', '1')
              ->where('bienes.Departamento', 'like',  '%SI%')
              ->where('resguardos.tipo', '=', 'TEMPORAL')
              ->select('detalle_biens.*','historial__inventarios.*','mejoras_equipos.*','personas.*', 'resguardo_detail.*', 'bienes.*', 'marcas.*', 'clasificacions.*', 'resguardos.*');
      }
  }
////*************************pruebas */

 public function scopePrueba($query)
 {
         return $query
        ->select('resguardos.*' )->orderby('resguardos.persona_id','asc')->orderby('resguardos.id','asc')
         ;


 }
}

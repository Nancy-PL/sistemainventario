<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class baja_bienes extends Model
{
    protected $table = 'bajabienes';
    
    public $timestamps = false;
 
     use HasFactory;
     //relacion uno a uno inversa
    public function Bienes(){
        return $this->belongsTo('App/Models/Bienes');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MejorasEquipo extends Model
{
    protected $table = 'mejoras_equipos';

    use HasFactory;
}

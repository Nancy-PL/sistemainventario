<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Support\Facades\DB;
use App\Models\Resguardo;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


//se crean los macros para los estilos de las hojas 
Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
    $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
});
Sheet::macro('setOrientation', function (Sheet $sheet, $orientation) {
    $sheet->getDelegate()->getPageSetup()->setOrientation($orientation);
});


//Exportando las clases para el estilo de las tablas 
//class BienesExport implements ShouldAutoSize,WithStyles, WithHeadingRow, FromView, WithDrawings{
class BajaComputoExport implements
    WithTitle,
    ShouldAutoSize,
    WithStyles,
    WithHeadingRow,
    WithHeadings,
    WithMapping,
    WithEvents,
    FromCollection
{
    //consulta de las tablas para mostrar los datos al excel 
    public function collection()
    {
        //Realizando las consultas de los datos    que se van a exportar 
        return (Resguardo::BajaComputo()->get());
    }
     //escuchando los encabezados 
     public function headings(): array
     {
         return [
             'CLASIFICACIÓN',
             'MARCA',
             'MODELO',
             'No. SERIE',
             'No. INVENTARIO ANTERIOR',
             'No. INVENTARIO',
             'CARACTERISTÍCAS',
             'No. R.',
             'ÁREA',
             'PROFESIÓN',
             'NOMBRE',
             'APELLIDO PATERNO',
             'APELLIDO MATERNO',
             'ESTADO',
             'UBICACIÓN',
             'MEJORAS AL EQUIPO',
             'USUARIO ALTA',
             'FECHA ALTA DEL BIEN',
             'FECHA BAJA DEL BIEN',
             'OBSERVACIONES DE LA BAJA',
             'MOTIVOS BAJA',
         
 
         ];
     }
 
     //tomando los datos de la tabla
     //tomando los datos de la tabla
     public function map($resguardos): array
     {
 
 
 
         $NombreUsuario = '';
         if ($resguardos->name == NULL) {
             $NombreUsuario = $resguardos->usuario_alta;
         } else {
             $NombreUsuario = $resguardos->name;
         }
         $Mejoras = '';
         if ($resguardos->MejorasEquipo == NULL) {
             $Mejoras = 'NINGUNA';
         } else {
             $Mejoras = $resguardos->MejorasEquipo;
         }
         $ubicacion = '';
         if ($resguardos->ubicacion == NULL) {
             $ubicacion = 'SIN UBICACIÓN';
         } else {
             $ubicacion = $resguardos->ubicacion;
         }
         $Observaciones = '';
         if ($resguardos->obser ==NULL) {
             $Observaciones = 'SIN OBSERVACIONES';
         } else {
             $Observaciones = $resguardos->obser;
         }
         $factura = '';
         if ($resguardos->factura ==NULL) {
             $factura = 'SIN FACTURA';
         } else {
             $factura = $resguardos->factura;
         }
         $NoSerie = '';
         if ($resguardos->NoSerie ==NULL) {
             $NoSerie = 'S/N';
         } else {
             $NoSerie = $resguardos->NoSerie;
         }
         $Modelo = '';
         if ($resguardos->Modelo ==NULL) {
             $Modelo = 'S/M';
         } else {
             $Modelo = $resguardos->Modelo;
         }
 
         $motivos = '';
         if ($resguardos->motivos ==NULL) {
            $motivos = 'SIN MOTIVOS';
         } else {
             $motivos = $resguardos->motivos;
         }
         $NoInventarioAnterior = '';
         if ($resguardos->NoInventarioAnterior==NULL) {
             $NoInventarioAnterior = 'S/N';
         } else {
             $NoInventarioAnterior = $resguardos->NoInventarioAnterior;
         }
         
         //retornando los bienes 
         return [
             $resguardos->concepto,
             $resguardos->nombre,
             $Modelo,
             $NoSerie,
             $NoInventarioAnterior,
             $resguardos->ClaveBien,
             $resguardos->Caracteristicas,
             $resguardos->resguardo_id,
             $resguardos->NombreA,
             $resguardos->full_name,
             $resguardos->Estado,
             $ubicacion,
             $Mejoras,
             $NombreUsuario,
             $resguardos->fechita,
             $resguardos->fechabaja,
             $Observaciones,
             $motivos,
             
 
         ];
     }

   public function styles(Worksheet $sheet)
   {
       return [

           // Style the first row as bold text.
           1   => ['font' => ['bold' => true]],

       ];
   }
   public function title(): string
   {
       return 'BienesNoInventario ';
   }

   public function startCell(): string
   {
       return 'A1';
   }

   public function registerEvents(): array
   {
       return  [
           AfterSheet::class => function (AfterSheet $event) {
               $event->sheet->getStyle('A1:U1')->applyFromArray([
                   'font' => [
                       'bold' => true
                   ],


               ]);
               $event->sheet->styleCells(
                   'A1:U1',
                   [
                       'fill' => [
                           'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                           'rotation' => 90,
                           'startColor' => [
                               'argb' => 'B8EEB8',
                           ],
                           'endColor' => [
                               'argb' => 'FFFFFFFF',
                           ],
                       ],
                       'borders' => [
                           'outline' => [
                               'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                               'color' => ['argb' => 'B8EEB8'],
                           ],
                       ],
                       'font' => [
                           'bold' => true,
                       ]
                   ]
               );
           }
       ];
   }
}

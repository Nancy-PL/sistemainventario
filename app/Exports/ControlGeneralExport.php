<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\control_ip;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
    $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
});
Sheet::macro('setOrientation', function (Sheet $sheet, $orientation) {
    $sheet->getDelegate()->getPageSetup()->setOrientation($orientation);
});


//Exportando las clases para el estilo de las tablas 
//class BienesExport implements ShouldAutoSize,WithStyles, WithHeadingRow, FromView, WithDrawings{
class ControlGeneralExport implements
    WithTitle,
    ShouldAutoSize,
    WithStyles,
    WithHeadingRow,
    WithCustomStartCell,
    WithHeadings,
    WithMapping,
    WithEvents,
    FromCollection
{

    //consulta de las tablas para mostrar los datos al excel 
    public function collection()
    {
        //Realizando las consultas de los datos    que se van a exportar 
        return (control_ip::buscarIpGral()->get());
    }
    //escuchando los encabezados 
    //escuchando los encabezados 
    public function headings(): array
    {
        return [
            'IP',
            'NOMBRE',
          //  'ÁREA',
            'JEFATURA',
        //    'MEMO',
       //     'FECHA DE INICIO',
            'DEFINITIVO/TEMPORAL',
            'TERMINO',


        ];
    }

    //tomando los datos de la tabla
    public function map($control): array
    {
        $Area=strtoupper($control->NombreA);

        $estatus = '';
        if ($control->estatus == 1) {
            $estatus = 'libre';
        } else{
            $estatus = 'Ocupado';
        }
        
        $Definitivo = '';
        if ($control->Definitivo == 1) {
            $Definitivo = 'Definitivo';
        } else {
            $Definitivo = 'Temporal';
        }
        $jefatura = '';
        if ($control->NombreJ == null) {
            $jefatura = $Area;
        } else{
            $jefatura = $control->NombreJ ;

        }
        //retornando los bienes 
        return [

            $control->ip,
            $control->NombreP,
          //  $Area,
            $jefatura,
           // $control->No_memo,
          //  $control->inicia,
            $Definitivo,
            $control->vence,
           // $control->NivelJerarquia,



        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [

            // Style the first row as bold text.
            1   => ['font' => ['bold' => true]],

        ];
    }
    public function title(): string
    {
        return 'ControlGral ';
    }

    public function startCell(): string
    {
        return 'A1';
    }

    public function registerEvents(): array
    {
        return  [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:H1')->applyFromArray([
                    'font' => [
                        'bold' => true,
                        'color' => array('rgb' => '000000')

                    ],


                ]);
                $event->sheet->styleCells(
                    'A1:E1',
                    [
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                            'rotation' => 90,
                            'startColor' => [
                                'argb' => '004a2c',
                            ],
                            'endColor' => [
                                'argb' => '004a2c',
                            ],
                        ],
                        'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                                'color' => ['argb' => '004a2c'],
                            ],
                        ],
                        'font' => [
                            'bold' => true,
                            'color' => array('rgb' => 'FFFFFF')

                        ]
                    ]
                );
               
                

            }
        ];
    }
}

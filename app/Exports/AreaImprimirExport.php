<?php

namespace App\Exports;

use App\Bien;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Area;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

//inicio de la clase para la exportación de los datos a excel 
class AreaImprimirExport implements WithTitle, ShouldAutoSize,WithStyles, WithHeadingRow, FromView{
    //consulta de las tablas para mostrar los datos al excel 
    public function view(): View{
       
        return view('Bienes.Administrativos.Reportes.Area-Excel', [
            'areas' => Area::all()
        ]);


        
    }
    //funcion para modificar las celdas 
    /*public function celdas ( Worksheet $cells)
    {
              // Set all borders (top, right, bottom, left)
          $cells->setBorder('solid', 'none', 'none', 'solid');
  
          //bordes de la hoja de excel 
          $cells->setBorder(array(
              'top'   => array(
                  'style' => 'solid'
              ),
          ));
    }*/
//estilos de las hojas 
    public function styles(Worksheet $sheet)
    {
        return [
            
            // estilo de las fuentes de los encabezados de excel 
            1    => ['font' => ['bold' => true]],
           
        ];
      
        
    }
    //función para ponerle titulo a la hoja de excel 
    public function title(): string
    {
        return 'Áreas' ;
    }

}    
<?php

namespace App\Exports;


use App\Models\User;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
    $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
});
Sheet::macro('setOrientation', function (Sheet $sheet, $orientation) {
    $sheet->getDelegate()->getPageSetup()->setOrientation($orientation);
});


//Exportando las clases para el estilo de las tablas 
//class BienesExport implements ShouldAutoSize,WithStyles, WithHeadingRow, FromView, WithDrawings{
class UsuariosExport implements
    WithTitle,
    ShouldAutoSize,
    WithStyles,
    WithHeadingRow,


    WithHeadings,
    WithMapping,
    WithEvents,
    FromCollection
{
    //consulta de las tablas para mostrar los datos al excel 
    public function collection()
    {
        //Realizando las consultas de los datos    que se van a exportar 
        return (User::buscar()->get());
    }
    //escuchando los encabezados 
    public function headings(): array
    {
        return [
            'id',
            'Profesión',
            'Nombre',
            'Apellido Paterno',
            'Apellido Materno',
            'Nombre de Usuario',
            'Área',
            'Fecha de Creación',
        ];
    }

    //tomando los datos de la tabla
    public function map($users): array
    {
      
        //retornando los bienes 
        return [
            $users->UserId,
            $users->Profesion,
            $users->NombreP,
            $users->ApellidoP,
            $users->ApellidoM,
            $users->name,
            $users->NombreA,
            $users->created_at,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [

            // Style the first row as bold text.
            1  => ['font' => ['bold' => true]],

        ];
    }
    public function title(): string
    {
        return 'Lista de Usuarios ';
    }

    public function registerEvents(): array
    {
        return  [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:H1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],


                ]);
                $event->sheet->styleCells(
                    'A1:H1',
                    [
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                            'rotation' => 90,
                            'startColor' => [
                                'argb' => 'B8EEB8',
                            ],
                            'endColor' => [
                                'argb' => 'FFFFFFFF',
                            ],
                        ],
                        'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                                'color' => ['argb' => '090A09'],
                            ],
                        ],
                        'font' => [
                            'bold' => true,
                        ]
                    ]
                );
            }
        ];
    }
}

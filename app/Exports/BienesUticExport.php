<?php

namespace App\Exports;

use App\Bien;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Bienes;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Support\Facades\DB;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
    $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
});
Sheet::macro('setOrientation', function (Sheet $sheet, $orientation) {
    $sheet->getDelegate()->getPageSetup()->setOrientation($orientation);
});


//Exportando las clases para el estilo de las tablas 
//class BienesExport implements ShouldAutoSize,WithStyles, WithHeadingRow, FromView, WithDrawings{
class BienesUticExport implements
    WithTitle,
    ShouldAutoSize,
    WithStyles,
    WithHeadingRow,


    WithHeadings,
    WithMapping,
    WithEvents,
    FromCollection
{
    //consulta de las tablas para mostrar los datos al excel 
    public function collection()
    {
        //Realizando las consultas de los datos    que se van a exportar 
        return (DB::table('bienes')
            ->join('marcas', 'marcas.id', '=', 'bienes.marca_id')
            ->join('clasificacions', 'clasificacions.id', '=', 'bienes.Clasificacion_id')
            ->join('historial_biens', 'historial_biens.bien_id', '=', 'bienes.id', 'left outer')
            ->join('users', 'historial_biens.usuario_alta', '=', 'users.id', 'left outer')
            ->join('detalle_biens', 'detalle_biens.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('historial__inventarios', 'historial__inventarios.bienes_id', '=', 'bienes.id', 'left outer')
            ->join('mejoras_equipos', 'mejoras_equipos.bienes_id', '=', 'bienes.id', 'left outer')
            ->select('historial_biens.fecha_baja', 'marcas.nombre', 'clasificacions.clave', 'clasificacions.NoInventario', 'clasificacions.concepto','detalle_biens.*', 'historial__inventarios.*', 'mejoras_equipos.*', 'bienes.*', 'users.name', 'historial_biens.*')
            ->where('bienes.Departamento', '=', 'SI')
            ->get());
    }
    //escuchando los encabezados 
    public function headings(): array
    {
        return [
            'Clasificación',
            'No. Serie',
            'No. de Folio Fiscal',
            'No. Inventario Anterior',
            'No. Inventario Actual',
            'Modelo',
            'Marca',
            'Estado',
            'Ubicación',
            'Características',
            'Observaciones',
            'Fecha de Baja',
            'Disponibilidad',
            'Usuario alta',
            'Fecha de Creación',
        ];
    }

    //tomando los datos de la tabla
    public function map($bienes): array
    {
        //pasando el status a Dispoble u Ocupado
        $Status = '';
        if ($bienes->Status == 1) {
            $Status = 'OCUPADO';
        } else {
            $Status = 'DISPONIBLE';
        }
        $NombreUsuario = '';
        if ($bienes->name == NULL) {
            $NombreUsuario = $bienes->usuario_alta;
        } else {
            $NombreUsuario = $bienes->name;
        }
        $numero = '';
        if ($bienes->NoSerie == NULL) {
            $numero = 'S/N';
        } else {
            $numero = $bienes->NoSerie;
        }
        $ubicacion = '';
        if ($bienes->ubicacion == NULL) {
            $ubicacion = 'SIN UBICACIÓN';
        } else {
            $ubicacion = $bienes->ubicacion;
        }
        $Observaciones = '';
        if ($bienes->Observaciones == NULL) {
            $Observaciones = 'SIN OBSERVACIONES';
        } else {
            $Observaciones = $bienes->Observaciones;
        }
        $factura = '';
        if ($bienes->factura == NULL) {
            $factura = 'SIN FACTURA';
        } else {
            $factura = $bienes->Observaciones;
        }
        $Modelo = '';
        if ($bienes->Modelo == NULL) {
            $Modelo = 'S/N';
        } else {
            $Modelo = $bienes->Modelo;
        }

        //retornando los bienes 
        return [
            $bienes->concepto,
            $numero,
            $factura,
            $bienes->NoInventarioAnterior,
            $bienes->ClaveBien,
            $bienes->Modelo,
            $bienes->nombre,
            $bienes->Estado,
            $ubicacion,
            $bienes->Caracteristicas,
            $Observaciones,
            $bienes->fecha_baja,
            $Status,
            $NombreUsuario,
            $bienes->created_at,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [

            // Style the first row as bold text.
            1  => ['font' => ['bold' => true]],

        ];
    }
    public function title(): string
    {
        return 'Lista de Bienes Administrativos ';
    }

    public function registerEvents(): array
    {
        return  [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:O1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],


                ]);
                $event->sheet->styleCells(
                    'A1:O1',
                    [
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                            'rotation' => 90,
                            'startColor' => [
                                'argb' => 'B8EEB8',
                            ],
                            'endColor' => [
                                'argb' => 'FFFFFFFF',
                            ],
                        ],
                        'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                                'color' => ['argb' => '090A09'],
                            ],
                        ],
                        'font' => [
                            'bold' => true,
                        ]
                    ]
                );
            }
        ];
    }
}

<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use App\Models\logueo;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                $fecha = Carbon::now();
                $logueo = logueo::sesionIniciada(Auth::guard($guard)->user())->get();

                foreach ($logueo as $logueo) {
                    $logueo->fin_login = $fecha;
                    $logueo->update();
                    break;
                }
                if(auth()->user()->Activo==0){
                    $request->session()->invalidate();
                 //   Auth::logout();
                    $request->session()->regenerateToken();
                    return $next($request);

                }
                return redirect(RouteServiceProvider::HOME);
            }
        }

        return $next($request);
    }
}

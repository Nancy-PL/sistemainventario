<?php

use App\Models\Resguardo;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ListaUsuarioController;
use App\Http\Controllers\AreaController;
use App\Http\Controllers\AsignacionBienUTICController;
use App\Http\Controllers\AsignacionBienController;
use App\Http\Controllers\BienesUticController;
use App\Http\Controllers\BienesController;
use App\Http\Controllers\PersonasController;
use App\Http\Controllers\BajabienUTICController;
use App\Http\Controllers\BajabienController;
use App\Http\Controllers\ListaTodoRTemporalController;
use App\Http\Controllers\ReasignacionResguardoController;
use App\Http\Controllers\clasificacionController;
use App\Http\Controllers\ReasignacionResguardoUticController;
use App\Http\Controllers\SalidaEquipoController;
use App\Http\Controllers\ResguardoTemporalController;
use \App\Http\Controllers\DashboardController;
use App\Http\Controllers\HistorialBienController;
use App\Http\Controllers\DashboardCapAdminController    ;
use App\Http\Controllers\ActivarUserController;
use App\Http\Controllers\BajaController;
use Spatie\Activitylog\Contracts\Activity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Controllers\Control_IpController;
use App\Http\Controllers\jefaturaController;

if( Auth::check()){
    Route::get('/', function () {
        return redirect('public/Home');
    });
}else{
    
    Route::get('/', function () {
        return view('auth.login');
    });
}


//Auth route for both
Route::group(['middleware' => ['auth']], function () {
///////////////////////////////////////////HOME ADMINISTRADOR ////////////////////////////////////
Route::get('/Home', 'App\Http\Controllers\DashboardController@index')->name('Home');
    
//////////////////////////////////AREA/////////////////////////////////////////////////////////////
//Ruta de Areas
Route::resource('Areas', 'App\Http\Controllers\AreaController')->names('Areas');
//AJAX para lista de area
Route::get('get-Area', [AreaController::class, 'getArea'])->name('get-Area');
//////////////////////////////////BIENES DE COMPUTO Y ACCESORIOS////////////////////////////////////
/**************************Ruta de bienes UTIC***************************/
Route::resource('BienesUtic', 'App\Http\Controllers\BienesUticController')->names('BienesUtic');
//Consulta bienes
Route::get('get-BienesUtic', [BienesUticController::class, 'getBienesUtic'])->name('get-BienesUtic');
//Ruta debaja de resguardos  Admi
Route::resource('BajaBienUTIC', 'App\Http\Controllers\BajabienUTICController')->names('BajaBienUtic');
//Baja del bien consultas ajax
Route::get('getBajaBienesUtic', [BajabienUTICController::class, 'getBajaBienesUtic'])->name('get-getBajaBienesUtic');
//Ruta de bienes
Route::get('/bienbajaUtic', 'App\Http\Controllers\BajabienUTICController@listaUtic')->name('ListabajaUtic');
//lista historial de bienes SI
Route::get('/HistorialBienesUTIC', 'App\Http\Controllers\HistorialBienController@indexSI')->name('HistorialBienesUTIC');
// consulta de bienes administrativos 
Route::get('get-ListaBajaUtic', [BajabienUTICController::class, 'getListaBajaUtic'])->name('get-ListaBajaUtic');

/**************************** RESGUARDOS UTIC ******************** */
//Ruta de asignación, reasignación de resguardos  Y Lista de resguardo UTIC
Route::resource('AsignacionBienUTIC', 'App\Http\Controllers\AsignacionBienUTICController')->names('ResguardosUtic');
//AJAX para lista de resguardos
Route::get('get-AsignacionBienUtic', [AsignacionBienUTICController::class, 'getAsignacionBienUtic'])->name('get-AsignacionBienUtic');
//AJAX para lista de resguardos
Route::get('get-ResguardoUtic', [AsignacionBienUTICController::class, 'getResguardoUtic'])->name('get-ResguardoUtic');
//Ruta de lista de resguardos asignados 
Route::get('ListaAsignadosUtic/{id}', 'App\Http\Controllers\AsignacionBienUTICController@lista');

/*********************************LISTA DE RESGUARDOS UTIC******************* */
//Ruta de lista de resguardo en bienes de Computo
Route::get('ListaResguardanteUtic/{id}', 'App\Http\Controllers\BienesUticController@lista');
Route::get('ListaResguardanteAlmacenUtic/{nombreP}', 'App\Http\Controllers\BienesUticController@listaAlmacen');
/**************************REASIGNACION  UTIC************************** */
//Ruta de  reasignacion de resguardos UTIC
Route::resource('reasignacionUtic', 'App\Http\Controllers\ReasignacionResguardoUticController')->names('ReasignacionUtic');
//Ruta de  Activacion de resguardos equipo de Computo 
Route::get('ActivacionResguardoUtic/{id}', 'App\Http\Controllers\ActivacionResguardoUTICController@activar');
//Ruta de  Activacion de resguardos UTIC
Route::resource('ActivacionUTIC', 'App\Http\Controllers\ActivacionResguardoUTICController')->names('ActivacionUtic');
//ajax para listado de reasignacion de equipos UTIC
Route::get('get-reasignacionUtic', [ReasignacionResguardoUticController::class, 'getReasignacionUtic'])->name('get-reasignacionUtic');

//Reporte Resguardos de Equipo de computo y accesorios
//Vista para descargar reportes de resguardantes 
Route::get('/ResguardoUtic', 'App\Http\Controllers\AsignacionBienUTICController@resguardo')->name('ResguardoPDFUtic');
//Reporte Resguardo de equipos
Route::get('/ReporteResguardoUtic/{id}/{VoBo}', 'App\Http\Controllers\AsignacionBienUTICController@downloadResguardoPDF');
/////////////////////////////////////////////////Entrega  de Equipos////////////////////////////////////////////////
//Ruta de entregas de equipos salientes 
Route::resource('/EntregaEquipos', 'App\Http\Controllers\EntregaEquiposController')->names('EntregaEquipo');
/**********************************REPORTES PDF ********************************** */
//Reporte Resguardo de equipos
Route::get('/ReporteResguardo/{id}', 'App\Http\Controllers\AsignacionBienController@downloadResguardoPDF');
///////////////////////////////////////// RESGUARDOS TEMPORALES//////////////////////////////////////
//Ruta de resguardo temporal
Route::resource('ResguardoTemporal', 'App\Http\Controllers\ResguardoTemporalController')->names('ResguardoTemporal');
//Ruta de lista de resguardo temporales bienes 
Route::get('ListaResguardanteTempo/{id}', 'App\Http\Controllers\ResguardoTemporalController@lista');

//Ruta de lista de resguardo temporales bienes para reimprimir 
Route::get('ResguardanteTemporal/', 'App\Http\Controllers\ResguardoTemporalController@listas');

//Ruta de lista de resguardo temporales bienes 
Route::resource('/CancelarResguardoTemp', 'App\Http\Controllers\CancelacionRTemporalController')->names('CancelarResguardoTem');
//Lista de todos los resguardos temporales 
Route::resource('ListaResguardoTemporal', 'App\Http\Controllers\ListaTodoRTemporalController')->names('ResguardosTemporales');
//ajax para listado general de Resguardo temporal
Route::get('get-getResguardoTemp', [ListaTodoRTemporalController::class, 'getResguardoTemp'])->name('get-getResguardoTemp');
//Ajax para listados temporales disponible
Route::get('get-ResguardoTemp', [ResguardoTemporalController::class, 'getResguardoTemporalDis'])->name('get-ResguardoTemp');
//ajax para listado Resguardo temporal proximos a vencer
Route::get('get-getResguardoVence', [DashboardController::class, 'getResguardoVence'])->name('get-getResguardoVence');
/****************************************Reportes PDF */
//Prestamos de equipo (Resguardo temporal)
Route::get('/ReportePrestamo/{id}', 'App\Http\Controllers\ResguardoTemporalController@downloadPrestamoPDF');
/////////////////////////////////////////////// SALIDAS DE EQUIPO /////////////////////////////////

//Ruta de lista de SALIDA de bienes 
Route::resource('/SalidaEquipo', 'App\Http\Controllers\SalidaEquipoController')->names('SalidaEquipo');
//lista de las salidas de equipo
Route::get('/ListadoSalida/{id}', 'App\Http\Controllers\SalidaEquipoController@listadoSalidas')->name('listadoSalida');
//Salida de equipos
Route::get('get-getSalidaEquipo/', [SalidaEquipoController::class, 'getSalidaEquipo'])->name('get-getSalidaEquipo');
//lista de las salidas de equipo
Route::get('/ListadoSalidas', 'App\Http\Controllers\SalidaEquipoController@listadoSalidass')->name('listadoSalida');
//Salida de equipos
Route::get('get-getSalidaEquipoRed', [SalidaEquipoController::class, 'getSalidaEquipoRed'])->name('get-getSalidaEquipoRed');


//Ruta de lista de resguardos salientes
Route::get('finalizar/{id}/{area}', 'App\Http\Controllers\SalidaEquipoController@lista');
//Ruta de lista de resguardos salientes Reimpresion
Route::get('SalidaEquipos/', 'App\Http\Controllers\SalidaEquipoController@listas')->name('get-SalidasEquipo');;
//Ruta de lista de resguardo en bienes muebles
Route::get('ListaResguardante/{id}', 'App\Http\Controllers\BienesController@lista');
Route::get('ListaResguardanteAlmacen/{nombreP}', 'App\Http\Controllers\BienesController@listaAlmacen');
//ajax para listado Resguardo temporal proximos a vencer
Route::get('get-getSalidaEquipoVence', [DashboardController::class, 'getSalidaEquipo'])->name('get-getSalidaEquipoVence');

/*************************************************************Reportes PDF**************************************/
//reporte PDF salida
Route::get('/Reporte-salida/{id}/{area}', 'App\Http\Controllers\SalidaEquipoController@downloadSalidaPDF');
//reporte PDF  Prestamo de equipos 


/////////////////////////////////////// BIENES ADMINISTRADOR ////////////////////////////////////////////////////////////////
//Ruta de bienes
Route::resource('Bienes', 'App\Http\Controllers\BienesController')->names('BienesUA');
// consulta de bienes administrativos 
Route::get('get-BienesAdmi', [BienesController::class, 'getBienesAdmi'])->name('get-BienesAdmi');
//Ruta debaja de resguardos  Admi
Route::resource('BajaBien', 'App\Http\Controllers\BajabienController')->names('BajaBien');
//Baja del bien consultas
Route::get('getBajaBienes', [BajabienController::class, 'getBajaBienes'])->name('get-getBajaBienes');
//obtener historial de bienes UA
Route::get('get-getHistorialBienes', [HistorialBienController::class, 'getHistorialBienes'])->name('get-getHistorialBienes');
//obtener historial de bienes SI
Route::get('get-getHistorialBienesSI', [HistorialBienController::class, 'getHistorialBienesSI'])->name('get-getHistorialBienesSI');

//Ruta de bienes
Route::get('/bienbaja', 'App\Http\Controllers\BajabienController@lista')->name('listaBajaBien');
// consulta de bienes administrativos 
Route::get('get-ListaBaja', [BajabienController::class, 'getListaBaja'])->name('get-ListaBaja');
//Ruta de detalle  bienes
Route::resource('DetalleBien', 'App\Http\Controllers\DetalleBienController')->names('DetalleBien');
//Ruta de  Historial de bieness
Route::resource('HistorialBien', 'App\Http\Controllers\HistorialBienController')->names('HistorialBien');
//////////////////////////////////// RESGUARDOS  ADMON ////////////////////////////////////////////
//Ruta de asignación y reasignación de resguardos ADMI
Route::resource('AsignacionBien', 'App\Http\Controllers\AsignacionBienController')->names('AsignacionBien');
//Vista para descargar reportes de resguardantes 
Route::get('/Resguardo', 'App\Http\Controllers\AsignacionBienController@resguardo')->name('ResguardoAdmiPDF');
//Ruta de lista de resguardos asignados 
Route::get('ListaAsignados/{id}', 'App\Http\Controllers\AsignacionBienController@lista');
//AJAX para lista de resguardos
Route::get('get-AsignacionBienAdmi', [AsignacionBienController::class, 'getAsignacionBienAdmi'])->name('get-AsignacionBienAdmi');
//AJAX para lista de resguardos
Route::get('get-ResguardoAdmi', [AsignacionBienController::class, 'getResguardoAdmi'])->name('get-ResguardoAdmi');
//////////////////////////////////////////// REASIGNACIÓN ADMI /////////////////////////////////////////

//Ruta de  reasignacion de resguardos ADMI
Route::resource('reasignacion', 'App\Http\Controllers\ReasignacionResguardoController')->names('reasignacion');
//Ruta de  Activacion de resguardante ADMI
Route::resource('/Activacion', 'App\Http\Controllers\ActivacionResguardoController')->names('Activacion');
//Ruta de  Activacion de resguardos ADMI 
Route::get('ActivacionResguardo/{id}', 'App\Http\Controllers\ActivacionResguardoController@activar');
//ajax para listado de reasignacion de equipos Admi
Route::get('get-reasignacionAdmi', [ReasignacionResguardoController::class, 'getreasignacionAdmi'])->name('get-reasignacionAdmi');
////////////////////////////////////// REPORTE DE RESGUARDO GENERAL PARA ADMI///////////////
/**********************************REPORTES PDF ********************************** */
//vista para descargar reporte general
Route::get('ReporteGeneral', [AsignacionBienController::class,'ReporteGeneral'])->name('ReporteGral');
//Reporte Resguardo de equipos de computo y bienes muebles 
Route::get('/ReporteResguardoGral/{id}', 'App\Http\Controllers\AsignacionBienController@downloadResguardoGeneralPDF');
//////////////////////////Home Capturista Admi////////////////////////////////////
//obtener historial de bienes UA de los ultimos 3 dias
Route::get('get-HistorialBienesSI-UA', [DashboardCapAdminController::class, 'getHistorialBienes'])->name('get-HistorialBienesSI-UA');
//obtener historial de bienes SI de los ultimos 3 dias
Route::get('get-HistorialBienesUA', [DashboardCapAdminController::class, 'getHistorialBienesSI'])->name('get-HistorialBienesUA');

//////////////////////////////////////////////////////// Personas ///////////////////////////////////////////////////////
//Ruta de Alta de personal
Route::resource('personas', 'App\Http\Controllers\PersonasController')->names('personas');
//Ruta de  baja de Personal
Route::resource('BajaPersonas', 'App\Http\Controllers\BajaPersonalController')->names('BajaPersonas');
//Ruta de  Activacion de Personal
Route::resource('ActivarPersonas', 'App\Http\Controllers\ActivaPersonalController')->names('ActivarPersonas');
//Ruta de lista de usuario
Route::resource('ListaUsuario', 'App\Http\Controllers\ListaUsuarioController')->names('ListaUsuario');
//Ruta de lista de usuario para activar
Route::resource('ActivarUsuario', 'App\Http\Controllers\ActivarUserController')->names('ActivarUsuario');
//  Para la lista   clasificación de ajax  para baja
Route::get('get-getPersonas', [PersonasController::class, 'getPersonass'])->name('get-getPersonass');
//  Para la lista   clasificación de ajax para Activacion
Route::get('get-ActivarPersonas', [PersonasController::class, 'getPersonaActiva'])->name('get-ActivarPersonas');

////////////////////////////////////////////////////////USER////////////////////////////////////////////////////////
//Lista de usuarios
Route::resource('Users', ListaUsuarioController::class)->names('Users');
//  Para la lista usuario de ajax  activos
Route::get('get-getUsers', [ListaUsuarioController::class, 'getUsers'])->name('get-getUsers');
//  Para la lista usuario de ajax inactivos
Route::get('get-getUsersA', [ActivarUserController::class, 'getUsers'])->name('get-getUsersA');

//////////////////////////////////////////////////////// CLASIFICACIÓN ///////////////////////////////////////////////////////
//Ruta de Clasificación
Route::resource('Clasificacion', 'App\Http\Controllers\clasificacionController')->names('Clasificacion');
//  Para la lista   clasificación de ajax ajax
Route::get('get-getClasificacionAD', [clasificacionController::class, 'getClasificacionAD'])->name('get-getClasificacionAD');

//////////////////////////////////       MARCAS       /////////////////////////////////////////////////////////////
//Ruta de Marcas
Route::resource('Marcas', 'App\Http\Controllers\MarcaController')->names('Marcas');
////////////////////////////////// LOGS DE USUARIO /////////////////////////////////////////
Route::resource('LogsUser', 'App\Http\Controllers\LogsController')->names('LogsUser');
//Route::get('get-getLogs', [LogsController::class, 'getLogsUser'])->name('getLogs');
/////////////////////////////// logs /////////////////////////////////////////////////////
//Registro de inicio de sesion 
Route::get('/logueo', 'App\Http\Controllers\LogsController@login')->name('logueo');
//registro de fin de sesion
Route::get('/logoutR', 'App\Http\Controllers\LogsController@logout')->name('logoutR');
//ajax para vista de logs de inicio y fin de sesion de usuario
Route::get('get-getsession', [DashboardController::class, 'getsession'])->name('get-getsession');

///////////////////////////////////////////////////  REPORTES  ////////////////////////////////////////
//Ruta de lista de resguardos administrativos agrupados solo por No de Control
Route::get('NoControl', 'App\Http\Controllers\ReportesController@NoControl')->name('NoControl');
//Ruta de lista de bienes de cómputo agrupados solo por NoInventario
Route::get('TotalExport', 'App\Http\Controllers\ReportesController@TotalExport')->name('TotalExport');
//Ruta de lista de resguardos de admi
Route::get('AdmiResguardo', 'App\Http\Controllers\ReportesController@AdmiResguardo')->name('AdmiResguardo');
//Ruta de lista de resguardosagrupados por área administración
Route::get('AreaResguardo', 'App\Http\Controllers\ReportesController@AreaResguardo')->name('AreaResguardo');
//Ruta de lista de resguardosagrupados por área administración
Route::get('computo', 'App\Http\Controllers\ReportesController@computo')->name('computo');
//Ruta de lista de resguardo en bienes de Computo
Route::get('imprimir', 'App\Http\Controllers\ReportesController@export')->name('imprimir');
//Ruta de lista  de usuarios 
Route::get('UsuarioImprimir', 'App\Http\Controllers\ReportesController@UsuarioImprimir')->name('UsuarioImprimir');
//Ruta de lista  de bienes admin dados de baja  
Route::get('BajaMuebles', 'App\Http\Controllers\ReportesController@BajaMuebles')->name('BajaMuebles');
//Ruta de lista  de bienes admin dados de baja  
Route::get('BajaComputo', 'App\Http\Controllers\ReportesController@BajaComputo')->name('BajaComputo'); 
//Ruta de lista  de bienes admin dados de baja  
Route::get('MueblesGeneral', 'App\Http\Controllers\ReportesController@MueblesGeneral')->name('MueblesGeneral');
//Ruta de lista  de bienes admin dados de baja  
Route::get('ComputoGeneral', 'App\Http\Controllers\ReportesController@ComputoGeneral')->name('ComputoGeneral');
//Ruta de lista de reportes administrativos 
Route::get('Reportes', 'App\Http\Controllers\ReportesController@reportes')->name('ReporteAmon');
//Ruta de lista de reportes UTIC
Route::get('ReportesUTIC', 'App\Http\Controllers\ReportesController@reportesUTIC')->name('ReportesUTIC');
//Ruta de lista de reportes UTIC
Route::get('BienesComputo', 'App\Http\Controllers\ReportesController@BienesComputo')->name('BienesComputo');;
//Ruta de lista de resguardo en bienes con no de control
Route::get('inventarioControl', 'App\Http\Controllers\ReportesController@ControlComputo')->name('inventarioControl');
//Ruta de lista de clasificación
Route::get('ClasificaciónImprimir', 'App\Http\Controllers\ReportesController@ClasificaciónImprimir')->name('ClasificaciónImprimir');
//Ruta de lista de clasificación
Route::get('AreaImprimir', 'App\Http\Controllers\ReportesController@AreaImprimir')->name('AreaImprimir');
//Ruta de numero de inventario utic 
Route::get('NoInventarioComputo', 'App\Http\Controllers\ReportesController@NoInventarioComputo')->name('NoInventarioComputo');

Route::get('/Restemp/{id}', 'App\Http\Controllers\ResguardoTemporalController@editarresguardo');

Route::get('get-ResguardoTemp1/{id}', [ResguardoTemporalController::class, 'temporalesUSer'])->name('get-ResguardoTemp1');
Route::get('get-ResguardoTemp2/{id}', [ResguardoTemporalController::class, 'temporalesD'])->name('get-ResguardoTemp2');

Route::post('prueba', [ResguardoTemporalController::class, 'prueba'])->name('prueba');

//Route::get('get-ResguardoTemp', [ResguardoTemporalController::class, 'getResguardoTemporalDis'])->name('get-ResguardoTemp');

//////////////////////////////////Control ip/////////////////////////////////////////////////////////////
//Ruta de Control ip
Route::resource('Control_Ip', 'App\Http\Controllers\Control_IpController')->names('Control_Ip');
//AJAX para lista de  Control ip
Route::get('get-Ip', [Control_IpController::class, 'getControl'])->name('get-Ip');
//Reporte de Control ip
Route::resource('ReporteControl', 'App\Http\Controllers\ReportesControl_IpController')->names('ReporteControl');
//Baja de ip
Route::resource('BajaIP', 'App\Http\Controllers\BajaController')->names('BajaIP');
//AJAX para lista de bajas  Control ip
Route::get('getBaja-Ip', [BajaController::class, 'getBajaControl'])->name('getBaja-Ip');
//ajax para listado de proximos ip a vencer en el home
Route::get('get-getProximosIp', [DashboardController::class, 'getProximosIp'])->name('get-getProximosIp');
//AJAX para lista de bajas  Control ip
Route::get('getDisponibilidad-Ip', [BajaController::class, 'getDisponibilidad'])->name('getDisponibilidad-Ip');
//historial de control ip
Route::resource('historialControlIp', 'App\Http\Controllers\HistorialIpController')->names('historialControlIp');
//////////////////////////////////Jefatura/////////////////////////////////////////////////////////////
//Ruta de Jefatura
Route::resource('jefatura', 'App\Http\Controllers\jefaturaController')->names('jefatura');
//AJAX para lista de Jefatura
Route::get('get-jefatura', [jefaturaController::class, 'getjefatura'])->name('get-jefatura');

});
require __DIR__ . '/auth.php';
